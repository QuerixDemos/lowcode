########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses/Scenario:
# rate_id is SERIAL PK
########################################################################
# DB Table Schema
#CREATE TABLE test08 (
#	test08_primary_key SERIAL,
#	test08_fk_char  CHAR NOT NULL,
#	test08_varchar  VARCHAR(20),
#	test08_int  INTEGER,
#	test08_date  DATE,
#	FOREIGN KEY (test08_fk_char) 
#	REFERENCES test06 (test06_primary_key) ,
#	PRIMARY KEY (test08_primary_key) CONSTRAINT pk_test08_primary_key    
#################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT

	OPTIONS INPUT WRAP

#	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("test08")
	CALL fgl_settitle("test08")  

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("test08 Record","test08 Record","{CONTEXT}/public/querix/icon/svg/24/ic_tax_24px.svg",101,TRUE,"test08 Record - Display, Scroll and modify test08 table data","top") 
			CALL fgl_dialog_setkeylabel("test08 List","test08 List","{CONTEXT}/public/querix/icon/svg/24/ic_tax_list_24px.svg",102,TRUE,"test08 List - List all ttest08 table data","top")			

		ON ACTION "test08 Record"
			CALL db_table_test08_rec()

		ON ACTION "test08 List"
			CALL db_table_test08_list()	
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################


########################################################################
# FUNCTION db_table_test08_rec()
#
#
########################################################################
FUNCTION db_table_test08_rec()	
    CALL InteractFormFile("../test_data/db_table_test08_rec")                                # The main staff function
END FUNCTION
########################################################################
# END FUNCTION db_table_test08_rec()
########################################################################


########################################################################
# FUNCTION db_table_test08_list()
#
#
########################################################################
FUNCTION db_table_test08_list()	
	DEFINE settings InteractForm_Settings
    LET settings.paged_mode = TRUE
    LET settings.form_file = "../test_data/db_table_test08_list"
    --LET settings.display = TRUE
    CALL InteractForm(settings)
END FUNCTION
########################################################################
# END FUNCTION db_table_test08_list()
########################################################################