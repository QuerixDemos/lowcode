########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
#GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# anomaly
#    BLOB column in table
########################################################################
# https://querix.atlassian.net/browse/LYC-7914
########################################################################
# DB Table Schema

#
#          CREATE TABLE qxt_document_blob (
#            document_id          INTEGER NOT NULL UNIQUE,
#            document_blob        BYTE,
#            PRIMARY KEY (document_id)
#          )

# Is referenced/used by qxt_document
          
#          CREATE TABLE qxt_document (
#            document_id          SERIAL(1000),
#            document_filename    VARCHAR(100) NOT NULL UNIQUE,
#            document_app         CHAR(30),
#            document_category1   CHAR(40),
#            document_category2   CHAR(40),
#            document_category3   CHAR(40),
#            document_mod_date    DATE,
#            document_desc        TEXT,  --CHAR(1000),  NOTE: Some DB's like Oracle don't allow to have 2 large objs i.e. 2 byte fields or a byte and a TEXT field
#
#            PRIMARY KEY (document_id)
#          )
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Doc BLOB")
	CALL fgl_settitle("Document BLOB") 

	MENU
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("BLOB Record","BLOB Record","{CONTEXT}/public/querix/icon/svg/24/ic_blob_24px.svg",			101,TRUE,"BLOB Record - Display, Scroll and modify BLOB data","top") 
			CALL fgl_dialog_setkeylabel("BLOB List",	"BLOB List",	"{CONTEXT}/public/querix/icon/svg/24/ic_blob_list_24px.svg",102,TRUE,"BLOB List - List all BLOBs","top")			
		
		ON ACTION "BLOB Record"
			CALL db_qxt_document_blob_rec()

		ON ACTION "BLOB List"
			CALL db_qxt_document_blob_list()
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################