##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the tools library
#
#
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms_llc

GLOBALS

  DEFINE qxt_glob_int_switch SMALLINT

  DEFINE qxt_err_msg,qxt_tmp_str VARCHAR(250)


################################################################################
# Main Settings Record
################################################################################


  DEFINE t_qxt_settings TYPE AS 
    RECORD

  ##################################
  # Main configuration file
  ##################################

      #[Application] Section
      application_id               SMALLINT,         --application (will be matched in the application table) i.e. 1=cms 2=guidemo
      main_cfg_filename            VARCHAR(30),     --main configuration file name & Path    default: cfg/config.cfg
      displayKeyInstructions				SMALLINT,					--Switch - IF/WHERE KEY STROKE instructions should be displayed in the custom-statusbar
      displayUsageInstructions			SMALLINT,					--Switch - IF/WHERE Usage Instructions should be displayed in the custom-statusbar

      #[General] Section
      cfg_path                     VARCHAR(30),     --directory name for config files
      unl_path                     VARCHAR(30),     --directory name for unl files
      form_path                    VARCHAR(30),     --directory name for forms
      msg_path                     VARCHAR(30),     --directory name for forms
      document_path                VARCHAR(30),     --directory name for documents
      html_path                    VARCHAR(30),     --directory name for html documents
      image_path                   VARCHAR(30),     --directory name for normal images
      print_html_template_path     VARCHAR(30),     --server side location for print templates


      #[TempPath] Section
      server_app_temp_path         VARCHAR(30),     --directory name for the applications temporary files on the server
      server_blob_temp_path        VARCHAR(30),     --directory name for the temporary blob files on the server
 
      #[ConfigFiles]  --specific cfg files 
      language_cfg_filename        VARCHAR(40),     --Configuration fie for the language library
      string_cfg_filename          VARCHAR(40),     --Configuration file for the string library
      help_classic_cfg_filename    VARCHAR(40),     --Configuration file for classic 4gl help library
      help_html_cfg_filename       VARCHAR(40),     --Configuration fiel for the help html library
      toolbar_cfg_filename         VARCHAR(40),     --toolbar configuration/ini unl file name  default: toolbar.cfg
      icon_cfg_filename            VARCHAR(40),     --icon configuration/ini unl file name  default: toolbar.cfg
      print_html_cfg_filename      VARCHAR(40),     --Configuration fie for the print_html libraries
      dde_cfg_filename             VARCHAR(40),     --File with DDE configuration
      online_resource_cfg_filename VARCHAR(40),     --Online Resource configuration file
      webservice_cfg_filename      VARCHAR(40),     --Webservice configuration file


      #[Database] Section
      db_name_tool               VARCHAR(18),        -- Database used for the qxt_db tools libraries
      db_name_app                VARCHAR(18),        -- main Database used by the application
			dbWildcard							CHAR,								-- wildcard for queries informix % Other *

  ##################################
  # Language.cfg Configuration file
  ##################################

      #[Language] Section
      language_id                       SMALLINT,     --language id
      language_default_id               SMALLINT,     --default language to be used if a string does not exist in a particular language
      language_list_source              SMALLINT,     --source is 0=file 1=db
      language_list_filename            VARCHAR(40),  --Language import file - lists all languages (not strings)
      language_list_table_name          VARCHAR(18),  --Language table name - lists all languages (not strings)

  ##################################
  # String.cfg Configuration file
  ##################################


      #[String] Section
      string_tool_source           SMALLINT,     --0=file to mem 1= db direct 2=db to mem
      string_tool_filename         VARCHAR(40),  --multi lingual string file for tools lib  default: string_tool.unl
      string_tool_table_name       VARCHAR(18),  --multi lingual string db table for tools lib  default: string_tool.unl
      string_app_source            SMALLINT,     --0=file to mem 1= db direct 2=db to mem
      string_app_filename          VARCHAR(40),  --multi lingual string file for application    default: string.unl
      string_app_table_name        VARCHAR(18),  --multi lingual string db table for application    default: string.unl


  ##################################
  # HelpClassic.cfg Configuration file
  ##################################

      #[HelpClassic] Classic 4GL Help Section
      help_classic_multi_lang           SMALLINT,         --if classic help file supports multi languages
      help_classic_unl_filename         VARCHAR(40),     --file names for classic 4gl help file list (unl file)


  ##################################
  # help_html.cfg Configuration file
  ##################################

      #[HelpHtml] Section
      help_html_system_type             SMALLINT,      --Help System Type (from app server file or online webserver)
      help_html_url_map_fname           VARCHAR(40),   --File name of the html help url map config file
      help_html_url_map_tname           VARCHAR(18),   --Table name of the html help url map config information
      help_html_base_dir                VARCHAR(100),  --Base DIR for the help file url ie. help/help-html
      help_html_base_url                VARCHAR(200),  --Base URL for the help file url ie. www.querix.com/help
      help_html_win_x                   SMALLINT,      --x position of the help html window
      help_html_win_y                   SMALLINT,      --y position of the help html window
      help_html_win_width               SMALLINT,      --width of the help html window
      help_html_win_height              SMALLINT,      --height of the help html window


  ##################################
  # Toolbar.cfg Configuration file
  ##################################

      #[Toolbar] Section
      toolbar_unl_filename              VARCHAR(40),
      tooltip_unl_filename              VARCHAR(40),
      toolbar_icon_size                 SMALLINT,       --Size for the toolbar icon


  ##################################
  # Icon.cfg Configuration file
  ##################################

      #[Icon] Section
      icon10_path                       VARCHAR(30),   --directory name for 10x10 icons
      icon16_path                       VARCHAR(30),   --directory name for 16x16 icons
      icon24_path                       VARCHAR(30),   --directory name for 24x24 icons
      icon32_path                       VARCHAR(30),   --directory name for 32x32 icons


  ##################################
  # PrintHtml.cfg Configuration file
  ##################################

      #[PrintHtml] section
      print_html_output                 VARCHAR(150),   --Dynamically created print html (output) name inc. rel. path
      print_html_template_unl_filename  VARCHAR(40),   --Import file for html print template file name list
      print_html_image_unl_filename     VARCHAR(40),   --Import file for html print image file name list


  ##################################
  # dde.cfg Configuration file
  ##################################

      #[DDE] Section
      dde_font_excel_filename           VARCHAR(40),   --dde excel font list (unl)
      dde_timeout                       SMALLINT,        --timeout for dde tools


  ##################################
  # Online_resource.cfg Configuration file
  ##################################

      #[OnlineResource] Section
      online_demo_path                  VARCHAR(150),   --URL for online self running demo


  ##################################
  # Webservice.cfg Configuration file
  ##################################

      #[WebService] Section
      ws_name                           VARCHAR(50),   --Webservice Name
      ws_port                           VARCHAR(50)    --Webservice Port


    END RECORD
 

  DEFINE
   qxt_settings OF t_qxt_settings 



  ##################################
  # Main settings - FORM
  ##################################

  DEFINE t_qxt_settings_form TYPE AS
    RECORD

  ##################################
  # Main configuration file
  ##################################

      #[Application] Section
      application_name             VARCHAR(30),      --application (will be matched in the application table) i.e. 1=cms 2=guidemo
      main_cfg_filename            VARCHAR(40),     --main configuration file name & Path    default: cfg/config.cfg
      displayKeyInstructions				SMALLINT,					--Switch - IF/WHERE KEY STROKE instructions should be displayed in the custom-statusbar
      displayUsageInstructions			SMALLINT,					--Switch - IF/WHERE Usage Instructions should be displayed in the custom-statusbar

      #[General] Section
      cfg_path                     VARCHAR(30),     --directory name for config files
      unl_path                     VARCHAR(30),     --directory name for unl files
      form_path                    VARCHAR(30),     --directory name for forms
      msg_path                     VARCHAR(30),     --directory name for forms
      document_path                VARCHAR(30),     --directory name for documents
      html_path                    VARCHAR(30),     --directory name for html documents
      image_path                   VARCHAR(30),     --directory name for normal images
      print_html_template_path     VARCHAR(30),     --directory name server side location for print templates

      #[TempPath] Section
      server_app_temp_path         VARCHAR(30),     --directory name for the applications temporary files on the server
      server_blob_temp_path        VARCHAR(30),     --directory name for the temporary blob files on the server

      #[ConfigFiles]  --specific cfg files 
      language_cfg_filename        VARCHAR(40),     --Configuration fie for the language library
      string_cfg_filename          VARCHAR(40),     --Configuration file for the string library
      help_classic_cfg_filename    VARCHAR(40),     --Configuration file for classic 4gl help library
      help_html_cfg_filename       VARCHAR(40),     --Configuration fiel for the help html library
      toolbar_cfg_filename         VARCHAR(40),     --toolbar configuration/ini unl file name  default: toolbar.cfg
      icon_cfg_filename            VARCHAR(40),     --icon configuration/ini unl file name  default: toolbar.cfg
      print_html_cfg_filename      VARCHAR(40),     --Configuration fie for the print_html libraries
      dde_cfg_filename             VARCHAR(40),     --File with DDE configuration
      online_resource_cfg_filename VARCHAR(40),     --Online Resource configuration file
      webservice_cfg_filename      VARCHAR(40),     --Webservice configuration file


      #[Database] Section
      db_name_tool                      VARCHAR(18),  -- Database used for the qxt_db tools libraries
      db_name_app                       VARCHAR(18),  -- main Database used by the application


  ##################################
  # Language.cfg Configuration file
  ##################################

      #[Language] Section
      language_name                     VARCHAR(30),  --language name
      language_default_name             VARCHAR(30),  --default language to be used if a string does not exist in a particular language
      language_list_source_name         VARCHAR(30), --source is 0=file 1=db
      language_list_filename            VARCHAR(40),  --Language import file - lists all languages (not strings)
      language_list_table_name          VARCHAR(18),  --Language table name - lists all languages (not strings)


  ##################################
  # String.cfg Configuration file
  ##################################

      #[String] Section
      string_tool_source_name      VARCHAR(30),  --0=file to mem 1= db direct 2=db to mem
      string_tool_filename         VARCHAR(40),  --multi lingual string file for tools lib  default: string_tool.unl
      string_tool_table_name       VARCHAR(18),  --multi lingual string db table for tools lib  default: string_tool.unl
      string_app_source_name       VARCHAR(30),  --0=file to mem 1= db direct 2=db to mem
      string_app_filename          VARCHAR(40),  --multi lingual string file for application    default: string.unl
      string_app_table_name        VARCHAR(18),  --multi lingual string db table for application    default: string.unl

  ##################################
  # help_classic.cfg Configuration file
  ##################################

      #[HelpClassic] Classic 4GL Help Section
      help_classic_multi_lang_name      VARCHAR(30),         --if classic help file supports multi languages
      help_classic_unl_filename         VARCHAR(40),     --file names for classic 4gl help file list (unl file)


  ##################################
  # help_html.cfg Configuration file
  ##################################
      #[HelpHtml] Section
      help_html_system_type_name        VARCHAR(30),   --Help System Type (from app server file or online webserver)
      help_html_url_map_fname           VARCHAR(60),   --File name of the html help url map config file
      help_html_url_map_tname           VARCHAR(18),   --Table name of the html help url map config information
      help_html_base_dir                VARCHAR(100),  --Base DIR for the help file url ie. help/help-html
      help_html_base_url                VARCHAR(200),  --Base URL for the help file url ie. www.querix.com/help
      help_html_win_x                   SMALLINT,      --x position of the help html window
      help_html_win_y                   SMALLINT,      --y position of the help html window
      help_html_win_width               SMALLINT,      --width of the help html window
      help_html_win_height              SMALLINT,      --height of the help html window


  ##################################
  # toolbar.cfg Configuration file
  ##################################

      #[Toolbar] Section
      toolbar_unl_filename              VARCHAR(40),
      tooltip_unl_filename              VARCHAR(40),
      toolbar_icon_size                 SMALLINT,       --Size for the toolbar icon


  ##################################
  # icon.cfg Configuration file
  ##################################
      #[Icon] Section
      icon10_path                       VARCHAR(30),   --directory name for 10x10 icons
      icon16_path                       VARCHAR(30),   --directory name for 16x16 icons
      icon24_path                       VARCHAR(30),   --directory name for 24x24 icons
      icon32_path                       VARCHAR(30),   --directory name for 32x32 icons


  ##################################
  # PrintHtml.cfg Configuration file
  ##################################

      #[PrintHtml] section
      print_html_output                 VARCHAR(150),   --Dynamically created print html (output) name inc. rel. path
      print_html_template_unl_filename  VARCHAR(100),   --Import file for html print template file name list
      print_html_image_unl_filename     VARCHAR(100),   --Import file for html print image file name list

  ##################################
  # dde.cfg Configuration file
  ##################################

      #[DDE] Section
      dde_font_excel_filename           VARCHAR(100),   --dde excel font list (unl)
      dde_timeout                       SMALLINT,        --timeout for dde tools

  ##################################
  # online_resource.cfg Configuration file
  ##################################

      #[OnlineResource] Section
      online_demo_path                  VARCHAR(150),   --URL for online self running demo



  ##################################
  # webservice.cfg Configuration file
  ##################################

      #[WebService] Section
      ws_name                           VARCHAR(50),   --Webservice Name
      ws_port                           VARCHAR(50)    --Webservice Port

    END RECORD
 



  DEFINE
    qxt_previous_help_file_id SMALLINT,
    qxt_current_help_file_id SMALLINT

  DEFINE 
    qxt_server_client_user_temp_directory VARCHAR(255),  -- used for the temp drive on the client
    qxt_server_temp_directory VARCHAR(50),  -- used for the server temp drive
    qxt_client_temp_directory VARCHAR(100),  -- used for the application temp drive in the clients cache directory
    #client_cache_directory VARCHAR(255),         -- used for the clients cache directory

    qxt_user_name VARCHAR(30)



#####################################################
# Single toolbar icon object - required by the draw_tb_icon()
# The draw_toolbar_icon function is part of the standard qxt.lib
#####################################################
  DEFINE t_qxt_toolbar_item_rec TYPE AS
    RECORD
      event_type_id            SMALLINT,
      tbi_event_name           VARCHAR(25),
      tbi_obj_action_id        SMALLINT,
      tbi_scope_id             SMALLINT,
      tbi_position             SMALLINT,
      tbi_static_id            SMALLINT,
      icon_filename            VARCHAR(100),
      label_data               VARCHAR(100),
      string_data              VARCHAR(100)
    END RECORD



####################################################################################################
# Settings Record Types
####################################################################################################


#####################################################
# Settings - Main - Record
#####################################################
  DEFINE t_qxt_settings_main_rec TYPE AS
    RECORD
      #[Application] Section
      application_id               SMALLINT,         --application (will be matched in the application table) i.e. 1=cms 2=guidemo
      main_cfg_filename            VARCHAR(40),     --main configuration file name & Path    default: cfg/config.cfg

      #[General] Section
      cfg_path                     VARCHAR(30),     --directory name for config files
      unl_path                     VARCHAR(30),     --directory name for unl files
      form_path                    VARCHAR(30),     --directory name for forms
      msg_path                     VARCHAR(30),     --directory name for forms
      document_path                VARCHAR(30),     --directory name for documents
      html_path                    VARCHAR(30),     --directory name for html documents
      image_path                   VARCHAR(30),     --directory name for normal images
      print_html_template_path     VARCHAR(30),     --server side location for print templates

      #[TempPath] Section
      server_app_temp_path         VARCHAR(30),     --directory name for the applications temporary files on the server
      server_blob_temp_path        VARCHAR(30),     --directory name for the temporary blob files on the server

      #[ConfigFiles]  --specific cfg files 
      language_cfg_filename        VARCHAR(40),     --Configuration fie for the language library
      string_cfg_filename          VARCHAR(40),     --Configuration file for the string library
      help_classic_cfg_filename    VARCHAR(40),     --Configuration file for classic 4gl help library
      help_html_cfg_filename       VARCHAR(40),     --Configuration fiel for the help html library
      toolbar_cfg_filename         VARCHAR(40),     --toolbar configuration/ini unl file name  default: toolbar.cfg
      icon_cfg_filename            VARCHAR(40),     --icon configuration/ini unl file name  default: toolbar.cfg
      print_html_cfg_filename      VARCHAR(40),     --Configuration fie for the print_html libraries
      dde_cfg_filename             VARCHAR(40),     --File with DDE configuration
      online_resource_cfg_filename VARCHAR(40),     --Online Resource configuration file
      webservice_cfg_filename      VARCHAR(40),     --Webservice configuration file


      #[Database] Section
      db_name_tool               VARCHAR(18),        -- Database used for the qxt_db tools libraries
      db_name_app                VARCHAR(18)        -- main Database used by the application


    END RECORD

#####################################################
# Settings - Main - Form Record
#####################################################
  DEFINE t_qxt_settings_main_form_rec TYPE AS
    RECORD
      #[Application] Section
      application_name             VARCHAR(30),      --application (will be matched in the application table) i.e. 1=cms 2=guidemo
      main_cfg_filename            VARCHAR(40),     --main configuration file name & Path    default: cfg/config.cfg
      displayKeyInstructions				SMALLINT,				--Key instructions display
			displayUsageInstructions      SMALLINT,				--Usage insructions display

      #[General] Section
      cfg_path                     VARCHAR(30),     --directory name for config files
      unl_path                     VARCHAR(30),     --directory name for unl files
      form_path                    VARCHAR(30),     --directory name for forms
      msg_path                     VARCHAR(30),     --directory name for forms
      document_path                VARCHAR(30),     --directory name for documents
      html_path                    VARCHAR(30),     --directory name for html documents
      image_path                   VARCHAR(30),     --directory name for normal images
      print_html_template_path     VARCHAR(30),     --directory name server side location for print templates

      #[TempPath] Section
      server_app_temp_path         VARCHAR(30),     --directory name for the applications temporary files on the server
      server_blob_temp_path        VARCHAR(30),     --directory name for the temporary blob files on the server

      #[ConfigFiles]  --specific cfg files 
      language_cfg_filename        VARCHAR(40),     --Configuration fie for the language library
      string_cfg_filename          VARCHAR(40),     --Configuration file for the string library
      help_classic_cfg_filename    VARCHAR(40),     --Configuration file for classic 4gl help library
      help_html_cfg_filename       VARCHAR(40),     --Configuration fiel for the help html library
      toolbar_cfg_filename         VARCHAR(40),     --toolbar configuration/ini unl file name  default: toolbar.cfg
      icon_cfg_filename            VARCHAR(40),     --icon configuration/ini unl file name  default: toolbar.cfg
      print_html_cfg_filename      VARCHAR(40),     --Configuration fie for the print_html libraries
      dde_cfg_filename             VARCHAR(40),     --File with DDE configuration
      online_resource_cfg_filename VARCHAR(40),     --Online Resource configuration file
      webservice_cfg_filename      VARCHAR(40),     --Webservice configuration file

      #[Database] Section
      db_name_tool                      VARCHAR(18),  -- Database used for the qxt_db tools libraries
      db_name_app                       VARCHAR(18)  -- main Database used by the application

    END RECORD



  ##################################
  # Language.cfg Configuration file
  ##################################
  DEFINE t_qxt_settings_lang_rec TYPE AS
    RECORD
      #[Language] Section
      language_id                       SMALLINT,     --language id
      language_default_id               SMALLINT,     --default language to be used if a string does not exist in a particular language
      language_list_source              SMALLINT,     --source is 0=file 1=db
      language_list_filename            VARCHAR(40),  --Language import file - lists all languages (not strings)
      language_list_table_name          VARCHAR(18)   --Language table name - lists all languages (not strings)
    END RECORD

  DEFINE t_qxt_settings_lang_form_rec TYPE AS
    RECORD
      #[Language] Section
      language_name                     VARCHAR(30),  --language name
      language_default_name             VARCHAR(30),  --default language to be used if a string does not exist in a particular language
      language_list_source_name         VARCHAR(30),  --source is 0=file 1=db
      language_list_filename            VARCHAR(40),  --Language import file - lists all languages (not strings)
      language_list_table_name          VARCHAR(18)   --Language table name - lists all languages (not strings)
    END RECORD




 ##################################
  # string.cfg Configuration file
  ##################################
  DEFINE t_qxt_settings_string_rec TYPE AS
    RECORD
      #[String] Section
      string_tool_source           SMALLINT,     --0=file to mem 1= db direct 2=db to mem
      string_tool_filename         VARCHAR(40),  --multi lingual string file for tools lib  default: string_tool.unl
      string_tool_table_name       VARCHAR(18),  --multi lingual string db table for tools lib  default: string_tool.unl
      string_app_source            SMALLINT,     --0=file to mem 1= db direct 2=db to mem
      string_app_filename          VARCHAR(40),  --multi lingual string file for application    default: string.unl
      string_app_table_name        VARCHAR(18)   --multi lingual string db table for application    default: string.unl
    END RECORD


  DEFINE t_qxt_settings_string_form_rec TYPE AS
    RECORD
      #[String] Section
      string_tool_source_name      VARCHAR(30),  --0=file to mem 1= db direct 2=db to mem
      string_tool_filename         VARCHAR(40),  --multi lingual string file for tools lib  default: string_tool.unl
      string_tool_table_name       VARCHAR(18),  --multi lingual string db table for tools lib  default: string_tool.unl
      string_app_source_name       VARCHAR(30),  --0=file to mem 1= db direct 2=db to mem
      string_app_filename          VARCHAR(40),  --multi lingual string file for application    default: string.unl
      string_app_table_name        VARCHAR(18)   --multi lingual string db table for application    default: string.unl
    END RECORD

 ##################################
  # help_classic.cfg Configuration file
  ##################################
  DEFINE t_qxt_settings_help_classic_rec TYPE AS
    RECORD

      #[HelpClassic] Classic 4GL Help Section
      help_classic_multi_lang           SMALLINT,        --if classic help file supports multi languages
      help_classic_unl_filename         VARCHAR(40)      --file names for classic 4gl help file list (unl file)
    END RECORD


  DEFINE t_qxt_settings_help_classic_form_rec TYPE AS
    RECORD
      #[HelpClassic] Classic 4GL Help Section
      help_classic_multi_lang_name      VARCHAR(30),      --if classic help file supports multi languages
      help_classic_unl_filename         VARCHAR(40)       --file names for classic 4gl help file list (unl file)
    END RECORD


  ##################################
  # help_html.cfg Configuration file
  ##################################

  DEFINE t_qxt_settings_help_html_rec TYPE AS
    RECORD
      #[Help-Html] Section
      help_html_system_type_name        VARCHAR(30),      --Help System Type (from app server file or online webserver)
      help_html_url_map_fname           VARCHAR(60),   --File name of the html help url map config file
      help_html_url_map_tname           VARCHAR(18),   --Table name of the html help url map config information
      help_html_base_dir                VARCHAR(100),  --Base DIR for the help file url ie. help/help-html
      help_html_base_url                VARCHAR(200),  --Base URL for the help file url ie. www.querix.com/help
      help_html_win_x                   SMALLINT,      --x position of the help html window
      help_html_win_y                   SMALLINT,      --y position of the help html window
      help_html_win_width               SMALLINT,      --width of the help html window
      help_html_win_height              SMALLINT       --height of the help html window
    END RECORD

  DEFINE t_qxt_settings_help_html_form_rec TYPE AS
    RECORD
      #[HelpHtml] Section
      help_html_system_type_name        VARCHAR(30),   --Help System Type (from app server file or online webserver)
      help_html_url_map_fname           VARCHAR(60),   --File name of the html help url map config file
      help_html_url_map_tname           VARCHAR(18),   --Table name of the html help url map config information
      help_html_base_dir                VARCHAR(100),  --Base DIR for the help file url ie. help/help-html
      help_html_base_url                VARCHAR(200),  --Base URL for the help file url ie. www.querix.com/help
      help_html_win_x                   SMALLINT,      --x position of the help html window
      help_html_win_y                   SMALLINT,      --y position of the help html window
      help_html_win_width               SMALLINT,      --width of the help html window
      help_html_win_height              SMALLINT       --height of the help html window
    END RECORD


  ##################################
  # toolbar.cfg Configuration file
  ##################################

  DEFINE t_qxt_settings_toolbar_rec TYPE AS
    RECORD
      #[Toolbar] Section
      toolbar_unl_filename              VARCHAR(40),
      tooltip_unl_filename              VARCHAR(40),
      toolbar_icon_size                 SMALLINT        --Size for the toolbar icon
    END RECORD

  DEFINE t_qxt_settings_toolbar_form_rec TYPE AS
    RECORD
      #[Toolbar] Section
      toolbar_unl_filename              VARCHAR(40),
      tooltip_unl_filename              VARCHAR(40),
      toolbar_icon_size                 SMALLINT        --Size for the toolbar icon
    END RECORD


  ##################################
  # icon.cfg Configuration file
  ##################################
  DEFINE t_qxt_settings_icon_rec TYPE AS
    RECORD
      #[Icon] Section
      icon10_path                       VARCHAR(30),   --directory name for 10x10 icons
      icon16_path                       VARCHAR(30),   --directory name for 16x16 icons
      icon24_path                       VARCHAR(30),   --directory name for 24x24 icons
      icon32_path                       VARCHAR(30)    --directory name for 32x32 icons
    END RECORD


  DEFINE t_qxt_settings_icon_form_rec TYPE AS
    RECORD
      #[Icon] Section
      icon10_path                       VARCHAR(30),   --directory name for 10x10 icons
      icon16_path                       VARCHAR(30),   --directory name for 16x16 icons
      icon24_path                       VARCHAR(30),   --directory name for 24x24 icons
      icon32_path                       VARCHAR(30)    --directory name for 32x32 icons
    END RECORD



  ##################################
  # PrintHtml.cfg Configuration file
  ##################################
  DEFINE t_qxt_settings_print_html_rec TYPE AS
    RECORD
      #[PrintHtml] section
      print_html_output                 VARCHAR(150),   --Dynamically created print html (output) name inc. rel. path
      print_html_template_unl_filename  VARCHAR(40),   --Import file for html print template file name list
      print_html_image_unl_filename     VARCHAR(40)    --Import file for html print image file name list
    END RECORD

  DEFINE t_qxt_settings_print_html_form_rec TYPE AS
    RECORD
      #[PrintHtml] section
      print_html_output                 VARCHAR(150),   --Dynamically created print html (output) name inc. rel. path
      print_html_template_unl_filename  VARCHAR(100),   --Import file for html print template file name list
      print_html_image_unl_filename     VARCHAR(100)    --Import file for html print image file name list
    END RECORD



  ##################################
  # dde.cfg Configuration file
  ##################################
  DEFINE t_qxt_settings_dde_rec TYPE AS
    RECORD
      #[DDE] Section
      dde_font_excel_filename           VARCHAR(40),   --dde excel font list (unl)
      dde_timeout                       SMALLINT       --timeout for dde tools
    END RECORD

  DEFINE t_qxt_settings_dde_form_rec TYPE AS
    RECORD
      #[DDE] Section
      dde_font_excel_filename           VARCHAR(100),   --dde excel font list (unl)
      dde_timeout                       SMALLINT        --timeout for dde tools
    END RECORD


  ##################################
  # Online_resource.cfg Configuration file
  ##################################

  DEFINE t_qxt_settings_online_resource_rec TYPE AS
    RECORD
      #[OnlineResource] Section
      online_demo_path                  VARCHAR(150)    --URL for online self running demo
    END RECORD


  DEFINE t_qxt_settings_online_resource_form_rec TYPE AS
    RECORD
      #[OnlineResource] Section
      online_demo_path                  VARCHAR(150)    --URL for online self running demo
    END RECORD


  ##################################
  # webservice.cfg Configuration file
  ##################################
  DEFINE t_qxt_settings_webservice_rec TYPE AS
    RECORD
      #[WebService] Section
      ws_name                           VARCHAR(50),   --Webservice Name
      ws_port                           VARCHAR(50)    --Webservice Port
    END RECORD


  DEFINE t_qxt_settings_webservice_form_rec TYPE AS
    RECORD
      #[WebService] Section
      ws_name                           VARCHAR(50),   --Webservice Name
      ws_port                           VARCHAR(50)    --Webservice Port
    END RECORD

END GLOBALS
{

  DEFINE t_contact_ws_rec TYPE AS
    RECORD 
      cont_name      LIKE contact.cont_name,
      cont_title     LIKE contact.cont_title,
      cont_fname     LIKE contact.cont_fname,
      cont_lname     LIKE contact.cont_lname,
      cont_addr1     LIKE contact.cont_addr1,
      cont_addr2     LIKE contact.cont_addr2,
      cont_addr3     LIKE contact.cont_addr3,
      cont_city      LIKE contact.cont_city,
      cont_zone      LIKE contact.cont_zone,
      cont_zip       LIKE contact.cont_zip,
      cont_country   LIKE contact.cont_country,
      cont_phone     LIKE contact.cont_phone,
      cont_mobile    LIKE contact.cont_mobile,
      cont_fax       LIKE contact.cont_fax,
      cont_email     LIKE contact.cont_email,
      comp_name      LIKE company.comp_name,
      #cont_picture   LIKE contact.cont_picture,
      cont_notes     LIKE contact.cont_notes
    END RECORD
}
####################################################################################################
# EOF
####################################################################################################

