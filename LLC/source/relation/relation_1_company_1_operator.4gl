########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses/Scenario
# two tables joined 1:1
########################################################################


########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_rec_company_operator_settings InteractForm_Settings


  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Company-Operator")
	CALL fgl_settitle("Company-Operator")    

	MENU 
		BEFORE MENU			
			#CALL fgl_dialog_setkeylabel("Company 1 Operator 1 Rec","Contact & Company 1:1","{CONTEXT}/public/querix/icon/svg/24/ic_contact_24px.svg",101,TRUE,"Display and modify contact / company details","top") 
			
			CALL fgl_dialog_setkeylabel("Company 1 Operator 1 rec", "Company / Operator 1:1 Rec", 	"{CONTEXT}/public/querix/icon/svg/24/ic_company_24px.svg",     101,TRUE,"Display and modify company record view with corresponding account manager operator","top") 
			CALL fgl_dialog_setkeylabel("Company 1 Operator 1 List","Company / Operator 1:1 List",	"{CONTEXT}/public/querix/icon/svg/24/ic_company_list_24px.svg",101,TRUE,"Display and modify company list view with corresponding account manager operator","top") 


		ON ACTION "Company 1 Operator 1 rec"
      CALL company_1_operator_1_rec(l_rec_company_operator_settings)

		ON ACTION "Company 1 Operator 1 List"
      CALL company_1_operator_1_list(l_rec_company_operator_settings)
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
######################################################################## 

########################################################################
# FUNCTION company_1_operator_1_rec(p_rec_settings InteractForm_Settings)
########################################################################
FUNCTION company_1_operator_1_rec(p_rec_settings InteractForm_Settings)	
    LET p_rec_settings.form_file = "../relation/relation_1_company_rec_1_operator_rec"
		
    LET p_rec_settings.id = "company_1_operator_1"
    CALL InteractForm(p_rec_settings)                           
END FUNCTION
########################################################################
# END FUNCTION company_1_operator_1_rec(p_rec_settings InteractForm_Settings)	
########################################################################


########################################################################
# FUNCTION company_1_operator_1_list(p_rec_settings InteractForm_Settings)
########################################################################
FUNCTION company_1_operator_1_list(p_rec_settings InteractForm_Settings)	
    LET p_rec_settings.form_file = "../relation/relation_1_company_list_1_operator_rec"
		
    LET p_rec_settings.id = "company_1_operator_1"
    CALL InteractForm(p_rec_settings)                           
END FUNCTION
########################################################################
# END FUNCTION company_1_operator_1_list(p_rec_settings InteractForm_Settings)	
########################################################################
