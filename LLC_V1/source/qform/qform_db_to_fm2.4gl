########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
MAIN
	DEFINE msg STRING
	LET msg = "This program is only used as a container for all required files for the qform demo\nPlease change in your command line to your LowCode project source folder i.e. \n/home/informix/workdir/projects/LowCode/LLC/source"
	CALL fgl_winmessage("This is a command line demo",msg,"info")


	MENU
		COMMAND "output_file_1" "Test output_file_1.fm2 using LowCode"
			CALL low_code_output_file_1()
		COMMAND "output_file_2" "Test output_file_2.fm2 using LowCode"
			CALL low_code_output_file_2()
		COMMAND "output_file_3" "Test output_file_3.fm2 using LowCode"
			CALL low_code_output_file_3()
		COMMAND "Exit" "Exit"
			EXIT MENU

	END MENU
END MAIN


########################################################################
# FUNCTION low_code_output_file_1()	
#
#
########################################################################
FUNCTION low_code_output_file_1()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="form/output_file_1.fm2"
	LET l_rec_settings.navigation_status="nav_page_of"	
	CALL InteractForm(l_rec_settings)

END FUNCTION
########################################################################
# END FUNCTION low_code_output_file_1()
########################################################################


########################################################################
# FUNCTION low_code_output_file_2()	
#
#
########################################################################
FUNCTION low_code_output_file_2()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="form/output_file_2.fm2"
	LET l_rec_settings.navigation_status="nav_page_of"	
	CALL InteractForm(l_rec_settings)

END FUNCTION
########################################################################
# END FUNCTION low_code_output_file_2()
########################################################################


########################################################################
# FUNCTION low_code_output_file_3()	
#
#
########################################################################
FUNCTION low_code_output_file_3()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="form/output_file_3.fm2"
	LET l_rec_settings.navigation_status="nav_page_of"	
	CALL InteractForm(l_rec_settings)

END FUNCTION
########################################################################
# END FUNCTION low_code_output_file_3()
########################################################################