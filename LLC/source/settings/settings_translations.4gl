########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
PUBLIC DEFINE md_subtitle STRING #for demo subTitle text
PUBLIC DEFINE md_info STRING #for demo description text
PUBLIC DEFINE md_code_sample STRING #for demo code sniplet
PUBLIC DEFINE md_language STRING #Language 
PUBLIC DEFINE md_msg STRING #string for messages
########################################################################
# INFO
########################################################################
# settings.log_file
#
########################################################################
# DB Table Schema
#CREATE TABLE test05 (
#    test05_primary_key SERIAL,
#    test05_fk_char  CHAR,
#    test05_varchar  VARCHAR(20),
#    test05_int  INTEGER,
#    test05_date  DATE,
#                PRIMARY KEY (test05_primary_key) CONSTRAINT pk_test05_primary_key    
#            )
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_title STRING
	DEFINE settings InteractForm_Settings
	DEFINE ui_locale STRING

	DEFER INTERRUPT
	OPTIONS INPUT WRAP

	LET l_title = os.Path.basename(arg_val(0))
	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText(l_title)
	CALL fgl_settitle(l_title)  

	#Demo info to be displayed on screen
	LET md_subtitle = "InteractForm_Settings.translations[originalString]translatedString\n"
	LET md_info = "There are many ways to translate strings at runtime."
	LET md_code_sample = 
		"LET p_rec_settings.translations[\"fgl_winmessage() (not translated)\"] = \"My translated fgl_winmessage()\""

	
		MENU 
		BEFORE MENU
		CALL fgl_dialog_setkeylabel("Record Dev",	"Record/Detailed View",					"{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	101,TRUE,"(Original) Record/Detailed View - Display, Scroll and modify table data (record view)",	"top", "iconlabel")
		CALL fgl_dialog_setkeylabel("Record EN",	"Record/Detailed View (EN)",		"{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	102,TRUE,"(English) Record/Detailed View - Display, Scroll and modify table data (record view)",	"top", "iconlabel")
		CALL fgl_dialog_setkeylabel("Record DE",	"Record/Detailed View (DE)",		"{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	103,TRUE,"(German) Record/Detailed View - Display, Scroll and modify table data (record view)",		"top", "iconlabel")
		CALL fgl_dialog_setkeylabel("Record ES",	"Record/Detailed View (ES)",		"{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	104,TRUE,"(Spanish) Record/Detailed View - Display, Scroll and modify table data (record view)",	"top", "iconlabel")
		CALL fgl_dialog_setkeylabel("List Dev",		"Array/List View",							"{CONTEXT}/public/querix/icon/svg/24/ic_list_24px.svg",		105,TRUE,"(Original) Array/List View - List all table data (array view)",							"top", "iconlabel")
		CALL fgl_dialog_setkeylabel("List EN",		"Array/List View (EN)",					"{CONTEXT}/public/querix/icon/svg/24/ic_list_24px.svg",		106,TRUE,"(English) Array/List View - List all table data (array view)",							"top", "iconlabel")
		CALL fgl_dialog_setkeylabel("List DE",		"Array/List View (GE)",					"{CONTEXT}/public/querix/icon/svg/24/ic_list_24px.svg",		107,TRUE,"(German) Array/List View - List all table data (array view)",								"top", "iconlabel")
		CALL fgl_dialog_setkeylabel("List ES",		"Array/List View (ES)",					"{CONTEXT}/public/querix/icon/svg/24/ic_list_24px.svg",		108,TRUE,"(Spanish) Array/List View - List all table data (array view)",							"top", "iconlabel")

		ON ACTION "Record Dev"
			CALL settings_rec(settings, ui_locale)
		ON ACTION "Record EN"
			LET ui_locale = "en_US"
			CALL settings_rec(settings, ui_locale)
		ON ACTION "Record DE"
			LET ui_locale = "de_DE"
			CALL settings_rec(settings, ui_locale)
		ON ACTION "Record ES"
			LET ui_locale = "es_ES"
			CALL settings_rec(settings, ui_locale)

		ON ACTION "List Dev"
			CALL settings_list(settings, ui_locale)	
		ON ACTION "List EN"
			LET ui_locale = "en_US"
			CALL settings_list(settings, ui_locale)	
		ON ACTION "List DE"
			LET ui_locale = "de_DE"
			CALL settings_list(settings, ui_locale)		
		ON ACTION "List ES"
			LET ui_locale = "es_ES"
			CALL settings_list(settings, ui_locale)	
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################


########################################################################
# FUNCTION settings_rec(p_rec_settings InteractForm_Settings, p_ui_locale STRING)
#
#
########################################################################
FUNCTION settings_rec(p_rec_settings InteractForm_Settings, p_ui_locale STRING)	

	LET p_rec_settings.form_file="../settings/llc_settings_rec"
	LET p_rec_settings.id = "settings"
	LET p_rec_settings.log_file="myTranslationModuleLogFile.log"

	#Demo ERROR Message statement
	LET p_rec_settings.translations["This is a 4GL ERROR statement (not translated)"] = "This is a 4GL ERROR statement"
	#Demo MESSAGE Message statement
	LET p_rec_settings.translations["This is a 4GL MESSAGE statement (not translated)"] = "This is a 4GL MESSAGE statement"
	#Demo fgl_winmessage()
	LET p_rec_settings.translations["fgl_winmessage() (not translated)"] = "My translated fgl_winmessage()"
	LET p_rec_settings.translations["This is a fgl_winmessage() dialog box (not translated)"] = "This is a fgl_winmessage() dialog box"

	#Demo MENU
	LET p_rec_settings.translations["My MENU (original)"] = "My MENU"
	LET p_rec_settings.translations["ERROR (original)"] = "ERROR"
	LET p_rec_settings.translations["MESSAGE (original)"] = "MESSAGE"
	LET p_rec_settings.translations["ERROR Message (original)"] = "ERROR Message"
	LET p_rec_settings.translations["MESSAGE statement Message  (original)"] = "MESSAGE statement Message"
	LET p_rec_settings.translations["EXIT (original)"] = "Exit"
	LET p_rec_settings.translations["EXIT Program (original)"] = "Exit Program"

	LET p_rec_settings.translations["Contact ID"] = "Contact ID"
	LET p_rec_settings.translations["Type"] = "Type Code"
	LET p_rec_settings.translations["Contact Name"] = "Contact Name"
	LET p_rec_settings.translations["Experience Points"] = "Experience Points"
	LET p_rec_settings.translations["DOB"] = "DOB"

	LET p_rec_settings.translations["test05_primary_key"] = "Contact ID"
	LET p_rec_settings.translations["test05_fk_char"] = "Type Code"
	LET p_rec_settings.translations["test05_varchar"] = "Contact Name"
	LET p_rec_settings.translations["test05_int"] = "Experience Points"
	LET p_rec_settings.translations["test05_date"] = "DOB"		
	
	LET p_rec_settings.translations["Do you really want to delete this item?"] = "Do you really want to remove this record?"		
	LET p_rec_settings.translations["Delete"] = "Remove"	

	LET md_language = %"Display language set to ", p_ui_locale, " (Rec)"
	LET p_rec_settings.actions[""]["BEFORE DISPLAY" ] = FUNCTION display_header

	CALL fgl_set_ui_locale(p_ui_locale)

	#Display some information
	LET p_rec_settings.views["test05"].navigation_status="nav_page_of"	#Display current cursor location to with label by identifier
	LET p_rec_settings.views["test05"].actions[""]["BEFORE DIALOG" ] = FUNCTION display_info

	CALL InteractForm(p_rec_settings)
END FUNCTION	
########################################################################
# END FUNCTION settings_rec(p_rec_settings InteractForm_Settings)
########################################################################


########################################################################
# FUNCTION settings_list(p_rec_settings InteractForm_Settings, p_ui_locale STRING)
#
#
########################################################################
FUNCTION settings_list(p_rec_settings InteractForm_Settings, p_ui_locale STRING)	

  LET p_rec_settings.form_file="../settings/llc_settings_list"
	LET p_rec_settings.id = "settings"  #settings action on append is null is identical/ LowCode action processing are managed in the 4gl code
	LET p_rec_settings.log_file="myTranslationModuleLogFile.log"

	LET p_rec_settings.translations["Contact ID"] = "Contact ID"
	LET p_rec_settings.translations["Type"] = "Type Code"
	LET p_rec_settings.translations["Contact Name"] = "Contact Name"
	LET p_rec_settings.translations["Experience Points"] = "Experience Points"
	LET p_rec_settings.translations["DOB"] = "DOB"

	LET p_rec_settings.translations["test05_primary_key"] = "Contact ID"
	LET p_rec_settings.translations["test05_fk_char"] = "Type Code"
	LET p_rec_settings.translations["test05_varchar"] = "Contact Name"
	LET p_rec_settings.translations["test05_int"] = "Experience Points"
	LET p_rec_settings.translations["test05_date"] = "DOB"

	LET p_rec_settings.translations["Do you really want to delete this item?"] = "Do you really want to remove this record?"		
	LET p_rec_settings.translations["Delete"] = "Remove"

	LET md_language = "Display language set to ", p_ui_locale, " (List)"
	LET p_rec_settings.actions[""]["BEFORE DISPLAY" ] = FUNCTION display_header

	CALL fgl_set_ui_locale(p_ui_locale)

	#Display some information
	LET p_rec_settings.views["test05"].navigation_status="nav_page_of"	#Display current cursor location to with label by identifier
	LET p_rec_settings.views["test05"].actions[""]["BEFORE DIALOG" ] = FUNCTION display_info

	CALL InteractForm(p_rec_settings)
END FUNCTION	
########################################################################
# END FUNCTION settings_list(p_rec_settings InteractForm_Settings)
########################################################################


########################################################################
# FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL
#
# Display some useful information to explain the demo
########################################################################
FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL
	DEFINE msg_info STRING

	IF md_language IS NOT NULL THEN
		LET msg_info = md_language, "\n", md_info
	END IF
	DISPLAY md_subtitle TO lb_SubTitle
	DISPLAY msg_info TO lb_info
	DISPLAY md_code_sample TO code_sample

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL
########################################################################

FUNCTION display_header(iform InteractForm INOUT) RETURNS BOOL
	DEFINE msg_info STRING

	IF md_language IS NOT NULL THEN
	LET msg_info = md_language, "\n", md_info
	END IF
	DISPLAY msg_info TO lb_info
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION