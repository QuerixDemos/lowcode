GLOBALS "llc_query_view.4gl"


#########################################################################################################
#
#  Function QueryView
#  Creates InteractForm object and performs its interaction using provided settings
#
#  Arguments:
#  - settings (QueryView_Settings) - Settings for LLC QueryView object
#
#########################################################################################################
PUBLIC FUNCTION QueryView(settings QueryView_Settings) RETURNS BOOL
    DEFINE qv QueryView
    LET qv.settings.* = settings.*
    RETURN qv.Interact()
END FUNCTION


#########################################################################################################
#
#  Function InteractQuery
#  Creates InteractForm object and performs its interaction using currently opened form
#
#  Arguments:
#  - window_title (STRING) - title of the open window
#  - query        (STRING) - query that should be used for interacting
#
#########################################################################################################
PUBLIC FUNCTION InteractQuery(window_title STRING, query STRING) RETURNS BOOL
    DEFINE settings QueryView_Settings
    LET settings.query = query
    LET settings.window_title = window_title
    RETURN QueryView(settings)
END FUNCTION