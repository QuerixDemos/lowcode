########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
GLOBALS #only for demo purpose
	DEFINE md_info STRING #for demo description text
	DEFINE md_code_sample STRING #for demo description text
END GLOBALS

########################################################################
# MODULE Scope Variables
########################################################################
#PUBLIC DEFINE md_info STRING #for demo description text
#PUBLIC DEFINE md_code_sample STRING #for demo description text


{
########################################################################
# FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL

	DISPLAY md_info TO lb_info
	DISPLAY md_code_sample TO code_sample

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL
########################################################################
}