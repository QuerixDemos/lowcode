GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"


###############################################################################################
#
#  Creates template form file with Table widget if it is not exist
#
###############################################################################################
PRIVATE FUNCTION CreateFormFileWithTable(file_name STRING, table_id STRING)
    DEFINE xml_form TEXT

    LOCATE xml_form IN FILE file_name

    LET xml_form =
"<?xml version=\"1.0\" encoding=\"UTF-8\"?><form xmlns=\"http://namespaces.querix.com/2015/fglForms\">
    <form.rootContainer>
        <Table identifier=\"" || table_id || "\"/>
    </form.rootContainer>
</form>"
END FUNCTION


###############################################################################################
#
#  Function CreateTable
#  Fill table from template form with columns that defined in 'fields' array
#
###############################################################################################
PUBLIC FUNCTION OpenFormWithTable(fields DYNAMIC ARRAY OF RECORD name STRING, type STRING END RECORD, field_table STRING) RETURNS FORM
    DEFINE column_ident, field_ident, form_file, table_ident STRING                  # Identifier storages
    DEFINE c_arr    DYNAMIC ARRAY OF ui.TableColumn          # List of table columns for inserting to table
    DEFINE widget   ui.AbstractUiElement
    DEFINE sr_arr   DYNAMIC ARRAY OF STRING                  # List of field names for creating screen record
    DEFINE i        INT
    DEFINE t        ui.Table
    DEFINE c        ui.TableColumn
    DEFINE fm       FORM
    
    LET form_file = "llc_dynamic_form_table_" || field_table || ".fm2"
    LET table_ident = field_table || "_tbl"

    IF NOT os.Path.Exists(form_file) THEN
        CALL CreateFormFileWithTable(form_file, table_ident)
    END IF

    CALL fm.Open(form_file)
    CALL fm.Display()

    LET t = ui.Table.ForName(table_ident)
    IF t IS NULL THEN                                        # Check if table exists
        DISPLAY "ERROR: Table ", table_ident, " is not created"
        RETURN fm                                            # Table was not created
    END IF

    FOR i = 1 TO fields.GetSize()
        LET field_ident = fields[i].name CLIPPED             # Define text field identifier
        LET column_ident = "c_" || field_ident               # Define table column identifier
        LET c = ui.TableColumn.Create(column_ident, table_ident)  # Create TABLE COLUMN into the table
        CALL c.SetText(field_ident)                          # Set header of column
        CALL CreateWidget(column_ident, field_ident, fields[i].type) RETURNING widget
        IF field_table IS NOT NULL THEN
            CALL widget.SetFieldTable(field_table)
        END IF
        CALL c.Complete()                                    # Complete table column (internal)
        LET c_arr[i] = c                                     # Collect it in columns array
        LET sr_arr[i] = field_ident                          # Collect field names for creating screen record
    END FOR

    CALL t.SetTableColumns(c_arr)                            # Insert table columns into the table
    CALL t.Complete()                                        # Complete table (internal)
    CALL fm.SetScreenRecordFields(PREF_PRIMARY_TABLE || field_table, sr_arr) # Insert screen record

    RETURN fm
END FUNCTION


################################################################
#
#  Creates widget, that depends on data type
#
################################################################
PRIVATE FUNCTION CreateWidget(parent STRING, name STRING, type STRING) RETURNS ui.AbstractUiElement
    DEFINE spinner ui.Spinner

    CALL type.ToLowerCase()

    IF type.Substring(1,3) = "int" OR type = "bigint" THEN
        LET spinner = ui.Spinner.Create(name, parent)        # Create SPINNER in the column
        CALL spinner.SetStep(1)
        CALL spinner.SetMinValue(-2147483647)
        CALL spinner.SetMaxValue(2147483647)
        RETURN spinner
    END IF

    IF type.Substring(1,6) = "serial" OR type = "bigserail" THEN
        LET spinner = ui.Spinner.Create(name, parent)        # Create SPINNER in the column
        CALL spinner.SetStep(1)
        CALL spinner.SetMinValue(0)
        CALL spinner.SetMaxValue(2147483647)
        RETURN spinner
    END IF

    IF type.Substring(1,8) = "datetime" THEN
        RETURN ui.TimeEditField.Create(name, parent)         # Create TIMEEDITFIELD in the column
    END IF

    IF type.Substring(1,4) = "date" THEN
        RETURN ui.Calendar.Create(name, parent)              # Create CALENDAR in the column
    END IF

    IF type = "Text" THEN
        RETURN ui.TextArea.Create(name, parent)              # Create TextArea in the column
    END IF

    IF type = "Byte" THEN
        RETURN ui.BlobViewer.Create(name, parent)            # Create BlobViewer in the column
    END IF

    RETURN ui.TextField.Create(name, parent)                 # Create TEXT FIELD in the column
END FUNCTION