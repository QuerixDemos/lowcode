########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7909
# Anomaly/Difference
# No PK but the column is NOT NULL unique and only one column
########################################################################
# DB Table Schema
#	CREATE TABLE country (
#		country         VARCHAR(40) NOT NULL unique   --country lookup
#	)
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Country")
	CALL fgl_settitle("Country")       

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Country Record","Country","{CONTEXT}/public/querix/icon/svg/24/ic_country_24px.svg",101,TRUE,"Country Record - Display and modify contact data","top") 
			CALL fgl_dialog_setkeylabel("Country List","Country List","{CONTEXT}/public/querix/icon/svg/24/ic_country_list_24px.svg",102,TRUE,"Country List - List all contacts","top")			
		
		ON ACTION "Country Record"
			CALL db_cms_country_rec()
			
		ON ACTION "Country List"
			CALL db_cms_country_list()
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################