########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7911
# Addresses:
# Simple table without FK and PK of type SERIAL 
########################################################################
# DB Table Schema
#	CREATE TABLE pay_method (
#		pay_method_id       SERIAL,
#		pay_method_name     CHAR(10)  NOT NULL UNIQUE,
#		pay_method_desc     CHAR(20) NOT NULL,
#		PRIMARY KEY (pay_method_id)
#		)
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Payment Methods")
	CALL fgl_settitle("Payment Methods") 

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Payment Method List",	"Payment Method List","{CONTEXT}/public/querix/icon/svg/24/ic_payment_type_list_24px.svg",101,TRUE,"Payment Method List - List all Payment Methods","top")			
			CALL fgl_dialog_setkeylabel("Payment Method",				"Payment Method",			"{CONTEXT}/public/querix/icon/svg/24/ic_payment_type_24px.svg",			102,TRUE,"Payment Method - Display, Scroll and modify payment method data","top") 
			CALL fgl_dialog_setkeylabel("Pay Method",						"Payment Method",			"{CONTEXT}/public/querix/icon/svg/24/ic_cash_1_24px.svg",						103,TRUE,"(from original cms) Payment Method - Display, Scroll and modify payment method data","top") 

		ON ACTION "Payment Method"
			CALL db_cms_pay_method_rec()

		ON ACTION "Payment Method List"
			CALL db_cms_pay_method_list() 
				
		ON ACTION "Pay Method"
			CALL db_cms_pay_method() #original cms pay_method form
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################