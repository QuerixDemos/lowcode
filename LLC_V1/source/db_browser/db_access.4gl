########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_query_view/llc_query_view.4gl"

########################################################################
# MAIN
#
#
########################################################################
MAIN
    DEFINE l_rec_settings    QueryView_Settings

    CONNECT TO "cms_llc"

    LET l_rec_settings.window_title = "List of tables"    # Define window title
    LET l_rec_settings.query = "SELECT tabname FROM systables WHERE statlevel = 'A'"
    LET l_rec_settings.actions[""]["ON ACTION Accept"] = FUNCTION table_view  # Add custom action
    LET l_rec_settings.actions[""]["ON APPEND"] = NULL    # Hide action APPEND
    LET l_rec_settings.actions[""]["ON INSERT"] = NULL    # Hide action INSERT
    LET l_rec_settings.actions[""]["ON DELETE"] = NULL    # Hide action DELETE
    LET l_rec_settings.actions[""]["ON UPDATE"] = NULL    # Hide action UPDATE

    CALL QueryView(l_rec_settings) # Execute
END MAIN
########################################################################
# END MAIN
########################################################################


########################################################################
# FUNCTION table_view() RETURNS BOOL
#
# Custom function that will be executed on ACCEPT action
########################################################################
FUNCTION table_view() RETURNS BOOL
    DEFINE tabname     STRING
    DEFINE l_rec_settings    QueryView_Settings

    LET tabname = ui.Dialog.GetCurrent().GetFieldValue("tabname")

    OPEN WINDOW w1 AT 1,1 WITH 24 ROWS, 80 COLUMNS

    LET l_rec_settings.window_title = "View of table " || tabname   # Define window title
    LET l_rec_settings.attributes[""]["DOUBLECLICK"] = "Update"     # Attribute for executing UPDATE on DOUBLECLICK
    LET l_rec_settings.query = "SELECT * FROM " || tabname
    CALL QueryView(l_rec_settings)                                  # Execute

    CLOSE WINDOW w1
    RETURN TRUE
END FUNCTION
########################################################################
# END FUNCTION table_view() RETURNS BOOL
########################################################################