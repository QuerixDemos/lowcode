########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="../llc_settings/llc_settings_rec"

	
#	LET l_rec_settings.actions["INSERT"]["ON CHANGE test05_fk_char" ] = FUNCTION actions_insert_on_change_test05_fk_char
#	LET l_rec_settings.actions["UPDATE"]["ON CHANGE test05_fk_char" ] = FUNCTION actions_update_on_change_test05_fk_char
#
#	LET l_rec_settings.actions["INSERT"]["ON CHANGE test05_int, test05_varchar" ] = FUNCTION actions_insert_on_change_any_field_in_list
#	LET l_rec_settings.actions["UPDATE"]["ON CHANGE test05_int, test05_varchar" ] = FUNCTION actions_update_on_change_any_field_in_list

	LET l_rec_settings.actions["INSERT"]["ON CHANGE *" ] = FUNCTION actions_insert_on_change_any_field_in_list
	LET l_rec_settings.actions["UPDATE"]["ON CHANGE *" ] = FUNCTION actions_update_on_change_any_field_in_list


	CALL InteractForm(l_rec_settings) #Start the LowCode Engine

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="../llc_settings/llc_settings_list"

	#	LET l_rec_settings.actions["INSERT"]["ON CHANGE test05_fk_char" ] = FUNCTION actions_insert_on_change_test05_fk_char
	#	LET l_rec_settings.actions["UPDATE"]["ON CHANGE test05_fk_char" ] = FUNCTION actions_update_on_change_test05_fk_char
	#
	#	LET l_rec_settings.actions["INSERT"]["ON CHANGE test05_int, test05_varchar" ] = FUNCTION actions_insert_on_change_any_field_in_list
	#	LET l_rec_settings.actions["UPDATE"]["ON CHANGE test05_int, test05_varchar" ] = FUNCTION actions_update_on_change_any_field_in_list

	LET l_rec_settings.actions["INSERT"]["ON CHANGE *" ] = FUNCTION actions_insert_on_change_any_field_in_list
	LET l_rec_settings.actions["UPDATE"]["ON CHANGE *" ] = FUNCTION actions_update_on_change_any_field_in_list


	CALL InteractForm(l_rec_settings) #Start the LowCode Engine
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################



########################################################################
# FUNCTION actions_insert_on_change_any_field_in_list()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_insert_on_change_any_field_in_list(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING
	#Here, we could apply any data validation and correction
	#iForm is a REFERENCE to all fields ! 
	#JUST for demo purpose - we work in another function
	CALL fgl_winmessage("actions_insert_on_change_any_field_in_list","actions_insert_on_change_any_field_in_list","info")
	CALL display_dialog_record(iform)

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_insert_on_change_any_field_in_list()RETURNS BOOL
########################################################################

########################################################################
# FUNCTION actions_update_on_change_any_field_in_list()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_update_on_change_any_field_in_list(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING
	#Here, we could apply any data validation and correction
	#iForm is a REFERENCE to all fields ! 
	#JUST for demo purpose - we work in another function
	CALL fgl_winmessage("actions_update_on_change_any_field_in_list","actions_insert_on_change_any_field_in_list","info")
	CALL display_dialog_record(iform)

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_update_on_change_any_field_in_list()RETURNS BOOL
########################################################################

-----



{
########################################################################
# FUNCTION actions_insert_on_change_test05_fk_char()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_insert_on_change_test05_fk_char(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING
	#Here, we could apply any data validation and correction
	#iForm is a REFERENCE to all fields ! 
	#JUST for demo purpose - we work in another function
	CALL fgl_winmessage("actions_insert_on_change_test05_fk_char","actions_insert_on_change_test05_fk_char","info")
	CALL display_dialog_record(iform)

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_insert_on_change_test05_fk_char()RETURNS BOOL
########################################################################

########################################################################
# FUNCTION actions_update_on_change_test05_fk_char()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_update_on_change_test05_fk_char(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING
	#Here, we could apply any data validation and correction
	#iForm is a REFERENCE to all fields ! 
	#JUST for demo purpose - we work in another function
	CALL fgl_winmessage("actions_update_on_change_test05_fk_char","actions_insert_on_change_test05_fk_char","info")
	CALL display_dialog_record(iform)

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_update_on_change_test05_fk_char()RETURNS BOOL
########################################################################


-----

########################################################################
# FUNCTION actions_insert_on_change_test05_int()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_insert_on_change_test05_int(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING
	#Here, we could apply any data validation and correction
	#iForm is a REFERENCE to all fields ! 
	#JUST for demo purpose - we work in another function
	CALL fgl_winmessage("actions_insert_on_change_test05_int","actions_insert_on_change_test05_int","info")
	CALL display_dialog_record(iform)

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_insert_on_change_test05_int()RETURNS BOOL
########################################################################

########################################################################
# FUNCTION actions_update_on_change_test05_int()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_update_on_change_test05_int(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING
	#Here, we could apply any data validation and correction
	#iForm is a REFERENCE to all fields ! 
	#JUST for demo purpose - we work in another function
	CALL fgl_winmessage("actions_update_on_change_test05_int","actions_insert_on_change_test05_int","info")
	CALL display_dialog_record(iform)

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_update_on_change_test05_int()RETURNS BOOL
########################################################################

---------------

########################################################################
# FUNCTION actions_insert_on_change_test05_varchar()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_insert_on_change_test05_varchar(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING
	#Here, we could apply any data validation and correction
	#iForm is a REFERENCE to all fields ! 
	#JUST for demo purpose - we work in another function
	CALL fgl_winmessage("actions_insert_on_change_test05_varchar","actions_insert_on_change_test05_varchar","info")
	CALL display_dialog_record(iform)

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_insert_on_change_test05_varchar()RETURNS BOOL
########################################################################

########################################################################
# FUNCTION actions_update_on_change_test05_varchar()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_update_on_change_test05_varchar(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING
	#Here, we could apply any data validation and correction
	#iForm is a REFERENCE to all fields ! 
	#JUST for demo purpose - we work in another function
	CALL fgl_winmessage("actions_update_on_change_test05_varchar","actions_insert_on_change_test05_varchar","info")
	CALL display_dialog_record(iform)

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_update_on_change_test05_varchar()RETURNS BOOL
########################################################################

}