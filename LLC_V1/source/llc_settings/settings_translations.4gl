PUBLIC DEFINE md_language STRING

########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"

FUNCTION display_header(iform InteractForm INOUT) RETURNS BOOL
	DISPLAY md_language TO lbLanguage
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# FUNCTION settings_rec(p_settings InteractForm_Settings, p_ui_locale STRING)
#
#
########################################################################
FUNCTION settings_rec(p_settings InteractForm_Settings, p_ui_locale STRING)	

	LET p_settings.form_file="../llc_settings/llc_settings_rec"
	LET p_settings.log_file="myLogFile.log"

	#Demo ERROR Message statement
	LET p_settings.translations["This is a 4GL ERROR statement (not translated)"] = "This is a 4GL ERROR statement"
	#Demo MESSAGE Message statement
	LET p_settings.translations["This is a 4GL MESSAGE statement (not translated)"] = "This is a 4GL MESSAGE statement"
	#Demo fgl_winmessage()
	LET p_settings.translations["fgl_winmessage() (not translated)"] = "My translated fgl_winmessage()"
	LET p_settings.translations["This is a fgl_winmessage() dialog box (not translated)"] = "This is a fgl_winmessage() dialog box"

	#Demo MENU
	LET p_settings.translations["My MENU (original)"] = "My MENU"
	LET p_settings.translations["ERROR (original)"] = "ERROR"
	LET p_settings.translations["MESSAGE (original)"] = "MESSAGE"
	LET p_settings.translations["ERROR Message (original)"] = "ERROR Message"
	LET p_settings.translations["MESSAGE statement Message  (original)"] = "MESSAGE statement Message"
	LET p_settings.translations["EXIT (original)"] = "Exit"
	LET p_settings.translations["EXIT Program (original)"] = "Exit Program"

	LET p_settings.translations["Contact ID"] = "Contact ID"
	LET p_settings.translations["Type"] = "Type Code"
	LET p_settings.translations["Contact Name"] = "Contact Name"
	LET p_settings.translations["Experience Points"] = "Experience Points"
	LET p_settings.translations["DOB"] = "DOB"

	LET p_settings.translations["test05_primary_key"] = "Contact ID"
	LET p_settings.translations["test05_fk_char"] = "Type Code"
	LET p_settings.translations["test05_varchar"] = "Contact Name"
	LET p_settings.translations["test05_int"] = "Experience Points"
	LET p_settings.translations["test05_date"] = "DOB"		
	
	LET p_settings.translations["Do you really want to delete this item?"] = "Do you really want to remove this record?"		
	LET p_settings.translations["Delete"] = "Remove"	

	LET md_language = %"Display language set to ", p_ui_locale, " (Rec)"
	LET p_settings.actions[""]["BEFORE DISPLAY" ] = FUNCTION display_header

	CALL fgl_set_ui_locale(p_ui_locale)
	CALL InteractForm(p_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec(p_settings InteractForm_Settings)
########################################################################


########################################################################
# FUNCTION settings_list(p_settings InteractForm_Settings, p_ui_locale STRING)
#
#
########################################################################
FUNCTION settings_list(p_settings InteractForm_Settings, p_ui_locale STRING)	

    LET p_settings.form_file="../llc_settings/llc_settings_list"
    LET p_settings.log_file="myLogFile.log"

	LET p_settings.translations["Contact ID"] = "Contact ID"
	LET p_settings.translations["Type"] = "Type Code"
	LET p_settings.translations["Contact Name"] = "Contact Name"
	LET p_settings.translations["Experience Points"] = "Experience Points"
	LET p_settings.translations["DOB"] = "DOB"

	LET p_settings.translations["test05_primary_key"] = "Contact ID"
	LET p_settings.translations["test05_fk_char"] = "Type Code"
	LET p_settings.translations["test05_varchar"] = "Contact Name"
	LET p_settings.translations["test05_int"] = "Experience Points"
	LET p_settings.translations["test05_date"] = "DOB"

	LET p_settings.translations["Do you really want to delete this item?"] = "Do you really want to remove this record?"		
	LET p_settings.translations["Delete"] = "Remove"

	LET md_language = "Display language set to ", p_ui_locale, " (List)"
	LET p_settings.actions[""]["BEFORE DISPLAY" ] = FUNCTION display_header

	CALL fgl_set_ui_locale(p_ui_locale)
	CALL InteractForm(p_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list(p_settings InteractForm_Settings)
########################################################################
