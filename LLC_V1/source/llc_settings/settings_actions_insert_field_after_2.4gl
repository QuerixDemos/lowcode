########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="../llc_settings/llc_settings_rec"

	# Alternative to AFTER FIELD #########################################
	# LET l_rec_settings.actions["INSERT"]["ON CHANGE *" ] = FUNCTION actions_insert_after_field_all
	# LET l_rec_settings.actions["UPDATE"]["ON CHANGE *" ] = FUNCTION actions_insert_after_field_all
	######################################################################

	LET l_rec_settings.actions["INSERT"]["AFTER FIELD test05_primary_key" ] = 		FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["INSERT"]["AFTER FIELD test05_fk_char" ] = 				FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["INSERT"]["AFTER FIELD test05_varchar" ] = 				FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["INSERT"]["AFTER FIELD test05_int" ] = 						FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["INSERT"]["AFTER FIELD testest05_datet05_int" ] =	FUNCTION actions_insert_after_field_all

	LET l_rec_settings.actions["UPDATE"]["AFTER FIELD test05_primary_key" ] = 		FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["UPDATE"]["AFTER FIELD test05_fk_char" ] = 				FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["UPDATE"]["AFTER FIELD test05_varchar" ] = 				FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["UPDATE"]["AFTER FIELD test05_int" ] = 						FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["UPDATE"]["AFTER FIELD testest05_datet05_int" ] =	FUNCTION actions_insert_after_field_all

	CALL InteractForm(l_rec_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="../llc_settings/llc_settings_list"
	# Alternative to AFTER FIELD #########################################
	# LET l_rec_settings.actions["INSERT"]["ON CHANGE *" ] = FUNCTION actions_insert_after_field_all
	# LET l_rec_settings.actions["UPDATE"]["ON CHANGE *" ] = FUNCTION actions_insert_after_field_all
	######################################################################


	LET l_rec_settings.actions["INSERT"]["AFTER FIELD test05_primary_key" ] = 		FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["INSERT"]["AFTER FIELD test05_fk_char" ] = 				FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["INSERT"]["AFTER FIELD test05_varchar" ] = 				FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["INSERT"]["AFTER FIELD test05_int" ] = 						FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["INSERT"]["AFTER FIELD testest05_datet05_int" ] =	FUNCTION actions_insert_after_field_all

	LET l_rec_settings.actions["UPDATE"]["AFTER FIELD test05_primary_key" ] = 		FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["UPDATE"]["AFTER FIELD test05_fk_char" ] = 				FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["UPDATE"]["AFTER FIELD test05_varchar" ] = 				FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["UPDATE"]["AFTER FIELD test05_int" ] = 						FUNCTION actions_insert_after_field_all
	LET l_rec_settings.actions["UPDATE"]["AFTER FIELD testest05_datet05_int" ] =	FUNCTION actions_insert_after_field_all

	CALL InteractForm(l_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################

########################################################################
# FUNCTION actions_insert_after_field_all()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_insert_after_field_all(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING
	DEFINE msg STRING  #for messages

	LET dlg = ui.Dialog.getCurrent()
	LET msg = sfmt("Apply AFTER Field Logic for field %1", dlg.getCurrentItem() )

	CASE dlg.getCurrentItem()
		WHEN "test05.test05_primary_key" #test05.test05_primary_key
			#AFTER FIELD test05_primary_key
			MESSAGE msg		
		WHEN "test05.test05_fk_char" #test05.test05_fk_char
			#AFTER FIELD test05_fk_char
			MESSAGE msg		

		WHEN "test05.test05_varchar" #test05.test05_varchar
			#AFTER FIELD test05_varchar
			MESSAGE msg		

		WHEN "test05.test05_int" #test05.test05_int
			#AFTER FIELD test05_int
			MESSAGE msg		

		WHEN "test05.test05_date" #test05.test05_date
			#AFTER FIELD test05_date
			MESSAGE msg		

		OTHERWISE
			LET msg = sfmt("The form element/item %1 is not handled in this case statement", dlg.getCurrentItem())
			CALL fgl_winmessage("Invalid/Unknown Item",msg,"error")
	END CASE

	#JUST for demo purpose - we work in another function
	#You can do any field validation here and update/change the data
	#CALL display_dialog_record(iform)
	
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_insert_after_field_all()RETURNS BOOL
########################################################################

########################################################################
# Not used... just demonstrates the alternative approach of one function for each AFTER FIELD
########################################################################
# FUNCTION actions_insert_after_field_test05_int()RETURNS BOOL
#
#
########################################################################
#FUNCTION actions_insert_after_field_test05_int(iform InteractForm INOUT) RETURNS BOOL
#	DEFINE dlg ui.Dialog
#	DEFINE l_rec_test05 RECORD LIKE test05.*
#	DEFINE l_msg STRING
#
#	#JUST for demo purpose - we work in another function
#	#You can do any field validation here and update/change the data
#	CALL display_dialog_record(iform)
#	
#	RETURN FALSE # Means that built-in function should not be prevented
#END FUNCTION
########################################################################
# END FUNCTION actions_insert_after_field_test05_int()RETURNS BOOL
########################################################################
