########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7909
# Anomaly
# 
########################################################################
# DB Table Schema
#	CREATE TABLE country2 (
#	dept_id              serial                                  no
#	dept_name            char(25)                                no
#	user_def             smallint                                yes
#	)
########################################################################

########################################################################
# FUNCTION db_cms_contact_dept_rec(p_rec_settings InteractForm_Settings)	
#
# Edit Record
########################################################################
FUNCTION db_cms_contact_dept_rec(p_rec_settings InteractForm_Settings)	
		LET p_rec_settings.form_file = "../db_cms_contact_dept/db_cms_contact_dept_rec"
		CALL InteractForm(p_rec_settings)		                       
END FUNCTION
########################################################################
# END FUNCTION db_cms_contact_dept_rec(p_rec_settings InteractForm_Settings)	
########################################################################


########################################################################
# FUNCTION db_cms_contact_dept_list(p_rec_settings InteractForm_Settings)	
#
#
########################################################################
FUNCTION db_cms_contact_dept_list(p_rec_settings InteractForm_Settings)
		LET p_rec_settings.form_file = "../db_cms_contact_dept/db_cms_contact_dept_list"
		CALL InteractForm(p_rec_settings)    
END FUNCTION
########################################################################
# END FUNCTION db_cms_contact_dept_list(p_rec_settings InteractForm_Settings)	
########################################################################