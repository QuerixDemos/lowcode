########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses/Scenario
# two tables joined 1:1
########################################################################


########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_rec_contact_activity_settings InteractForm_Settings


  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Contact-Company")
	CALL fgl_settitle("Contact-Company")    

	MENU 
		BEFORE MENU			
			CALL fgl_dialog_setkeylabel("1 Contact n Activity Rec","1 Contact n Activity 1:n Rec",  "{CONTEXT}/public/querix/icon/svg/24/ic_contact_24px.svg",              101,TRUE,"Display and modify contact Rec  / activity details","top") 
			CALL fgl_dialog_setkeylabel("1 Contact n Activity List","1 Contact n Activity 1:n List","{CONTEXT}/public/querix/icon/svg/24/ic_contact_activity_list_24px.svg",102,TRUE,"Display and modify contact List / activity details","top") 
	
		ON ACTION "1 Contact n Activity Rec"
      CALL contact_1_activity_1_rec(l_rec_contact_activity_settings)

			ON ACTION "1 Contact n Activity List"
      CALL contact_1_activity_1_list(l_rec_contact_activity_settings)			
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
######################################################################## 

########################################################################
# FUNCTION contact_1_activity_1_rec(p_rec_settings InteractForm_Settings)
########################################################################
FUNCTION contact_1_activity_1_rec(p_rec_settings InteractForm_Settings)	
    LET p_rec_settings.form_file = "../relation/relation_1_contact_rec_n_activity_list"
		
    LET p_rec_settings.id = "1 contact n activity"
    CALL InteractForm(p_rec_settings)                           
END FUNCTION
########################################################################
# END FUNCTION contact_1_activity_1_rec(p_rec_settings InteractForm_Settings)	
########################################################################


########################################################################
# FUNCTION contact_1_activity_1_list(p_rec_settings InteractForm_Settings)
########################################################################
FUNCTION contact_1_activity_1_list(p_rec_settings InteractForm_Settings)	
    LET p_rec_settings.form_file = "../relation/relation_1_contact_list_n_activity_list"
		
    LET p_rec_settings.id = "1 contact n activity"
    CALL InteractForm(p_rec_settings)                           
END FUNCTION
########################################################################
# END FUNCTION contact_1_activity_1_list(p_rec_settings InteractForm_Settings)	
########################################################################