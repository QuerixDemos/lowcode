########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
################################################################
# MODULE scope variables
################################################################

########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7910
# Addresses/Scenario:
# PK is serial and FK contact.cont_id
########################################################################
# DB Schema table math_operation
#
#		WHEN 1  --create table
#          CREATE TABLE test03 (
#            math_operation_id   			SERIAL,
#						operator_val_1            DECIMAL(10,2) NOT NULL,
#						operator_val_2            DECIMAL(10,2) NOT NULL,
#						result_sum		            DECIMAL(10,2) NOT NULL,
#						result_difference	        DECIMAL(10,2) NOT NULL,
#						result_product		        DECIMAL(10,2) NOT NULL,
#						result_quotient           DECIMAL(10,2) NOT NULL,
#						PRIMARY KEY (math_operation_id) CONSTRAINT pk_math_operation_id
#					)
################################################################



########################################################################
# FUNCTION db_cms_virtual_field_rec()
#
#
########################################################################
#FUNCTION db_cms_virtual_field_rec(l_settings InteractForm_Settings INOUT) RETURNS BOOL
FUNCTION db_cms_virtual_field_rec()
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file = "../llc_misc/misc_virtual_field_rec"
	LET l_settings.log_file = "../log/db_cms_virtual_field.log"	#enable log file
	LET l_settings.actions[""]["ON ACTION Show Data"] = FUNCTION math_on_action_show_data	
	LET l_settings.actions["UPDATE"]["AFTER INPUT"] = FUNCTION math_after_input
	LET l_settings.actions["INSERT"]["AFTER INPUT"] = FUNCTION math_after_input
		
	CALL InteractForm(l_settings)	
END FUNCTION
########################################################################
# END FUNCTION db_cms_virtual_field_rec()
########################################################################




########################################################################
# FUNCTION db_cms_virtual_field_list()
#
#
########################################################################
FUNCTION db_cms_virtual_field_list()
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file = "../llc_misc/misc_virtual_field_list"
	LET l_settings.log_file = "../log/db_cms_virtual_field.log"	#enable log file
	LET l_settings.actions[""]["ON ACTION Show Data"] = FUNCTION math_on_action_show_data
	LET l_settings.actions["UPDATE"]["AFTER INPUT"          ] = FUNCTION math_after_input
	LET l_settings.actions["INSERT"]["AFTER INPUT"          ] = FUNCTION math_after_input

	CALL InteractForm(l_settings)	    
END FUNCTION
########################################################################
# END FUNCTION db_cms_virtual_field_list()
########################################################################


########################################################################
# FUNCTION db_cms_virtual_field_list_all()
#
#
########################################################################
FUNCTION db_cms_virtual_field_list_all()
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file = "../llc_misc/misc_virtual_field_all"
	LET l_settings.log_file = "../log/db_cms_virtual_field.log"	#enable log file
	LET l_settings.actions[""]["ON ACTION Show Data"] = FUNCTION math_on_action_show_data	
	LET l_settings.actions["UPDATE"]["AFTER INPUT"          ] = FUNCTION math_after_input
	LET l_settings.actions["INSERT"]["AFTER INPUT"          ] = FUNCTION math_after_input
	CALL InteractForm(l_settings)	    
END FUNCTION
########################################################################
# END FUNCTION db_cms_virtual_field_list_all()
########################################################################


########################################################################
# FUNCTION math_on_action_show_data(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION math_on_action_show_data(iform InteractForm INOUT) RETURNS BOOL
	DISPLAY "operator_val_1=", iform.GetFieldValue("operator_val_1")    
	DISPLAY "operator_val_2=", iform.GetFieldValue("operator_val_2")
	DISPLAY "result_sum=", iform.GetFieldValue("result_sum")    
	DISPLAY "result_difference=", iform.GetFieldValue("result_difference")
	DISPLAY "result_product=", iform.GetFieldValue("result_product")    
	DISPLAY "result_quotient=", iform.GetFieldValue("result_quotient")
	    
	RETURN FALSE
END FUNCTION
########################################################################
# END FUNCTION math_on_action_show_data(iform InteractForm INOUT) RETURNS BOOL
########################################################################


########################################################################
# FUNCTION math_after_input(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION math_after_input(iform InteractForm INOUT) RETURNS BOOL
  DEFINE l_operator_val_1 LIKE test03.operator_val_1
  DEFINE l_operator_val_2 LIKE test03.operator_val_2

	#CALL fgl_winmessage("math_after_input","math_after_input","info")
    
  LET l_operator_val_1 = iform.GetFieldValue("operator_val_1")	#get the data/value from the first operator field
  LET l_operator_val_2 = iform.GetFieldValue("operator_val_2")	#get the data/value from the second operator field
      
	CALL iform.SetFieldValue("result_sum", 				l_operator_val_1 + l_operator_val_2)
	CALL iform.SetFieldValue("result_difference",	l_operator_val_1 - l_operator_val_2)
	CALL iform.SetFieldValue("result_product",		l_operator_val_1 * l_operator_val_2)
	CALL iform.SetFieldValue("result_quotient",		l_operator_val_1 / l_operator_val_2)
    
	#DISPLAY "operator_val_1=", iform.GetFieldValue("operator_val_1")    
	#DISPLAY "operator_val_2=", iform.GetFieldValue("operator_val_2")
	#DISPLAY "result_sum=", iform.GetFieldValue("result_sum")    
	#DISPLAY "result_difference=", iform.GetFieldValue("result_difference")
	#DISPLAY "result_product=", iform.GetFieldValue("result_product")    
	#DISPLAY "result_quotient=", iform.GetFieldValue("result_quotient")
	    
	RETURN FALSE
END FUNCTION
########################################################################
# END FUNCTION math_after_input(iform InteractForm INOUT) RETURNS BOOL
########################################################################