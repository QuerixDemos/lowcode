{
Relationship: 1:1  (one to one)
contact.cont_id
contact.cont_city
contact.cont_dept
contact.cont_title
contact.cont_zone
contact.cont_org
contact.cont_name
contact.cont_zip
contact.cont_position
contact.cont_fname
contact.cont_country
contact.cont_lname
contact.cont_phone
contact.cont_password
contact.cont_addr1
contact.cont_fax
contact.cont_ipaddr
contact.cont_addr2
contact.cont_mobile
contact.cont_usemail
contact.cont_addr3
contact.cont_email
contact.cont_usephone
contact.cont_notes

company.comp_id
company.comp_name
company.comp_addr1
company.comp_addr2
company.comp_addr3
company.comp_city
company.comp_zone
company.comp_zip
company.comp_country
company.acct_mgr
company.comp_link
company.comp_industry
company.comp_priority
company.comp_type
company.comp_main_cont
company.comp_url
company.comp_notes

<ScreenRecord identifier="llcScrRecContact" fields="contact.cont_id,contact.cont_city,contact.cont_dept,contact.cont_title,contact.cont_zone,contact.cont_org,contact.cont_name,contact.cont_zip,contact.cont_position,contact.cont_fname,contact.cont_country,contact.cont_lname,contact.cont_phone,contact.cont_password,contact.cont_addr1,contact.cont_fax,contact.cont_ipaddr,contact.cont_addr2,contact.cont_mobile,contact.cont_usemail,contact.cont_addr3,contact.cont_email,contact.cont_usephone,contact.cont_notes"/>		
	<ScreenRecord identifier="llcScrRecCompany" fields="company.comp_id,company.comp_name,company.comp_addr1,company.comp_addr2,company.comp_addr3,company.comp_city,company.comp_zone,company.comp_zip,company.comp_country,company.acct_mgr,company.comp_link,company.comp_industry,company.comp_priority,company.comp_type,company.comp_main_cont,company.comp_url,company.comp_notes"/>
	
}

########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Anomaly
# Single table but byte and char(1000) field
########################################################################
# DB Table Schema
{
          CREATE TABLE contact (
            cont_id 	SERIAL(1000) NOT NULL,
            cont_title	VARCHAR(10),
            cont_name 	VARCHAR(20) NOT NULL UNIQUE,
            cont_fname 	VARCHAR(20),
            cont_lname 	VARCHAR(20),
            cont_addr1	VARCHAR(40),
            cont_addr2	VARCHAR(40),
            cont_addr3	VARCHAR(40),
            cont_city	VARCHAR(20),
            cont_zone	VARCHAR(15),
            cont_zip	VARCHAR(15),
            cont_country	VARCHAR(40),
            cont_phone	VARCHAR(15),
            cont_fax	VARCHAR(15),
            cont_mobile	VARCHAR(15),
            cont_email	VARCHAR(50) NOT NULL UNIQUE,
            cont_dept	VARCHAR(15),	# ref cont_dept_id
            cont_org	INTEGER,	# ref company_id
            cont_position	VARCHAR(15),	# ref position_id
            cont_picture	BYTE,
            cont_password	VARCHAR(15),
            cont_ipaddr	VARCHAR(15),	# IP V4
            cont_usemail	SMALLINT,
            cont_usephone	SMALLINT,
            cont_notes   CHAR(1000),
 
            PRIMARY KEY (cont_id) CONSTRAINT contact_pk,
            FOREIGN KEY (cont_org) REFERENCES company(comp_id) CONSTRAINT cont_org_fk_company          

          )
          CREATE TABLE company (
            comp_id     SERIAL(1000) NOT NULL,
            comp_name	CHAR(100) NOT NULL UNIQUE,
            comp_addr1	CHAR(40),
            comp_addr2	CHAR(40),
            comp_addr3	CHAR(40),
            comp_city	CHAR(20),
            comp_zone	CHAR(15),
            comp_zip	CHAR(15),
            comp_country	CHAR(40),
            acct_mgr	INTEGER,		# ref contact_id
            comp_link	INTEGER,		# future: cross ref link table /Inner Join
            comp_industry	INTEGER,		# ref industry_id
            comp_priority	INTEGER,
            comp_type	INTEGER,		# ref cont_dept_id
            comp_main_cont	INTEGER	NOT NULL,	# ref contact_id
            comp_url	VARCHAR(50),
            comp_notes	CHAR(1000),
 
            PRIMARY KEY (comp_id) CONSTRAINT comp_id_pk,
            FOREIGN KEY (acct_mgr) REFERENCES contact(cont_id) CONSTRAINT acct_mgr_fk_contact,
            FOREIGN KEY (comp_industry) REFERENCES industry_type(industry_type_id) CONSTRAINT industry_type_id_fk_industry_type,
            FOREIGN KEY (comp_type) REFERENCES company_type(company_type_id) CONSTRAINT comp_type_fk_company_type,
            FOREIGN KEY (comp_main_cont) REFERENCES contact(cont_id) CONSTRAINT comp_main_cont_fk_contact 
                        
          )

          CREATE TABLE industry_type (
            industry_type_id	SERIAL,
            itype_name	VARCHAR(25) NOT NULL UNIQUE,
            user_def	SMALLINT
            
            
					CREATE TABLE company_type (
            company_type_id		SERIAL,
            ctype_name	CHAR(25) NOT NULL UNIQUE,
            user_def	SMALLINT,
            base_priority	INTEGER
          )

}   
########################################################################

########################################################################
# FUNCTION db_cms_contact_company_lookup2()	
#
# Edit Record
########################################################################
FUNCTION db_cms_contact_company_lookup2()	
    CALL InteractFormFile("../db_cms_contact_company_lookup2/db_cms_contact_company_lookup2_rec")                            
END FUNCTION
########################################################################
# END FUNCTION db_cms_contact_company_lookup2()	
########################################################################

			
########################################################################
# FUNCTION db_cms_contact_company_list_lookup2()	
#
#
########################################################################
FUNCTION db_cms_contact_company_list_lookup2()
    CALL InteractFormFile("../db_cms_contact_company_lookup2/db_cms_contact_company_list_lookup2")                        
END FUNCTION
########################################################################
# END FUNCTION db_cms_contact_company_list_lookup2()	
########################################################################