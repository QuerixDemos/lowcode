########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# settings.log_file
#
########################################################################
# DB Table Schema
#CREATE TABLE test05 (
#    test05_primary_key SERIAL,
#    test05_fk_char  CHAR,
#    test05_varchar  VARCHAR(20),
#    test05_int  INTEGER,
#    test05_date  DATE,
#                PRIMARY KEY (test05_primary_key) CONSTRAINT pk_test05_primary_key    
#            )
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_title STRING
	DEFINE settings InteractForm_Settings
	DEFINE ui_locale STRING

	DEFER INTERRUPT
	OPTIONS INPUT WRAP
	LET ui_locale = fgl_getenv("QX_UI_LOCALE") # QX_UI_LOCALE variable is set by Lycia Web Client depending on the user browser display language settings
	CALL fgl_putenv("QX_LOCALIZE_ANY_STRING=1")
	-- LET ui_locale = "en_US"
	LET l_title = os.Path.basename(arg_val(0))
	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText(l_title)
	CALL fgl_settitle(l_title)  

	LET settings.actions[""]["ON ACTION message_1" ] = FUNCTION message_1
	LET settings.actions[""]["ON ACTION message_2" ] = FUNCTION message_2
	LET settings.actions[""]["ON ACTION message_3" ] = FUNCTION message_3
	
	LET settings.actions[""]["ON ACTION menu_1" ] = FUNCTION menu_1
	LET settings.actions[""]["ON ACTION menu_2" ] = FUNCTION menu_2
	LET settings.actions[""]["ON ACTION menu_3" ] = FUNCTION menu_3
	MENU 
		BEFORE MENU
		CALL fgl_dialog_setkeylabel("Record Dev",	"Record/Detailed View",						"{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	101,TRUE,"(Original) Record/Detailed View - Display, Scroll and modify table data (record view)",	"top", "iconlabel")
		CALL fgl_dialog_setkeylabel("Record EN",	"Record/Detailed View (EN)",				"{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	102,TRUE,"(English) Record/Detailed View - Display, Scroll and modify table data (record view)",	"top", "iconlabel")
		CALL fgl_dialog_setkeylabel("Record DE",	"Record/Detailed View (DE)",				"{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	103,TRUE,"(German) Record/Detailed View - Display, Scroll and modify table data (record view)",		"top", "iconlabel")
		CALL fgl_dialog_setkeylabel("Record ES",	"Record/Detailed View (ES)",				"{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	104,TRUE,"(Spanish) Record/Detailed View - Display, Scroll and modify table data (record view)",	"top", "iconlabel")
		CALL fgl_dialog_setkeylabel("List Dev",		"Array/List View",							"{CONTEXT}/public/querix/icon/svg/24/ic_list_24px.svg",		105,TRUE,"(Original) Array/List View - List all table data (array view)",							"top", "iconlabel")
		CALL fgl_dialog_setkeylabel("List EN",		"Array/List View (EN)",					"{CONTEXT}/public/querix/icon/svg/24/ic_list_24px.svg",		106,TRUE,"(English) Array/List View - List all table data (array view)",							"top", "iconlabel")
		CALL fgl_dialog_setkeylabel("List DE",		"Array/List View (GE)",					"{CONTEXT}/public/querix/icon/svg/24/ic_list_24px.svg",		107,TRUE,"(German) Array/List View - List all table data (array view)",								"top", "iconlabel")
		CALL fgl_dialog_setkeylabel("List ES",		"Array/List View (ES)",					"{CONTEXT}/public/querix/icon/svg/24/ic_list_24px.svg",		108,TRUE,"(Spanish) Array/List View - List all table data (array view)",							"top", "iconlabel")

		ON ACTION "Record Dev"
			CALL settings_rec(settings, ui_locale)
		ON ACTION "Record EN"
			LET ui_locale = "en_US"
			CALL settings_rec(settings, ui_locale)
		ON ACTION "Record DE"
			LET ui_locale = "de_DE"
			CALL settings_rec(settings, ui_locale)
		ON ACTION "Record ES"
			LET ui_locale = "es_ES"
			CALL settings_rec(settings, ui_locale)

		ON ACTION "List Dev"
			CALL settings_list(settings, ui_locale)	
		ON ACTION "List EN"
			LET ui_locale = "en_US"
			CALL settings_list(settings, ui_locale)	
		ON ACTION "List DE"
			LET ui_locale = "de_DE"
			CALL settings_list(settings, ui_locale)		
		ON ACTION "List ES"
			LET ui_locale = "es_ES"
			CALL settings_list(settings, ui_locale)	
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################


########################################################################
# Below are only some reusable demo functions used by multiple
# demo applicaitons to show the data 
#
#
########################################################################



########################################################################
# FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING

	LET dlg = ui.Dialog.GetCurrent()

	LET l_rec_test05.test05_primary_key = iform.GetFieldValue("test05.test05_primary_key")
	LET l_rec_test05.test05_fk_char = iform.GetFieldValue("test05.test05_fk_char")
	LET l_rec_test05.test05_varchar = iform.GetFieldValue("test05.test05_varchar")
	LET l_rec_test05.test05_int = iform.GetFieldValue("test05.test05_int")
	LET l_rec_test05.test05_date = iform.GetFieldValue("test05.test05_date")

	#For demo purpose - range 0-1000
	LET l_msg = %"The variable test05_int=", trim(l_rec_test05.test05_int), %" was outside of the valid range 0-1000 and was corrected to "
	IF l_rec_test05.test05_int < 0 THEN

		LET l_rec_test05.test05_int = 0	#min
		LET l_msg = l_msg , " ", trim(l_rec_test05.test05_int) 
		CALL fgl_winmessage("test05_int",l_msg,"error")
	ELSE
		IF l_rec_test05.test05_int > 1000 THEN
			LET l_rec_test05.test05_int = 1000 #max
			LET l_msg = l_msg , " ", trim(l_rec_test05.test05_int) 
			CALL fgl_winmessage("test05_int",l_msg,"error")
		END IF
	END IF
	
	CALL iform.SetFieldValue("test05.test05_int", l_rec_test05.test05_int)

	LET l_msg = %"Record Data:",
	"\n", trim(l_rec_test05.test05_primary_key) , 
	"\n", trim(l_rec_test05.test05_fk_char), 
	"\n", trim(l_rec_test05.test05_varchar), 
	"\n", trim(l_rec_test05.test05_int), 
	"\n", trim(l_rec_test05.test05_date)

	CALL fgl_winmessage(%"INSERT - AFTER FIELD test05_int",l_msg,"info")
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
########################################################################


FUNCTION message_1(iform InteractForm INOUT) RETURNS BOOL
	ERROR "This is a 4GL ERROR statement (not translated)"
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION

FUNCTION message_2(iform InteractForm INOUT) RETURNS BOOL
	MESSAGE "This is a 4GL MESSAGE statement (not translated)"
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION

FUNCTION message_3(iform InteractForm INOUT) RETURNS BOOL
	CALL fgl_winmessage("fgl_winmessage() (not translated)","This is a fgl_winmessage() dialog box (not translated)","info")
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION

FUNCTION menu_1(iform InteractForm INOUT) RETURNS BOOL

	MENU "My MENU (original)"
		COMMAND "ERROR (original)" "ERROR Message (original)"
			ERROR "ERROR statement Message (original)"
		COMMAND "MESSAGE (original)" "MESSAGE Message (original)"
			MESSAGE "MESSAGE statement Message (original)"
		COMMAND "EXIT (original)" "Exit Program (original)"
			EXIT MENU
	END MENU
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION

FUNCTION menu_2(iform InteractForm INOUT) RETURNS BOOL

	MENU "My MENU" ATTRIBUTE (style="dialog", comment="This is my menu")
		COMMAND "ERROR (original)" "ERROR Message (original)"
			ERROR "ERROR statement Message (original)"
		COMMAND "MESSAGE (original)" "MESSAGE Message (original)"
			MESSAGE "MESSAGE statement Message (original)"
		COMMAND "EXIT (original)" "Exit Program (original)"
			EXIT MENU
	END MENU

END FUNCTION

FUNCTION menu_3(iform InteractForm INOUT) RETURNS BOOL

	MENU "My MENU" ATTRIBUTE (style="popup", comment="This is my menu")
		COMMAND "ERROR (original)" "ERROR Message (original)"
			ERROR "ERROR statement Message (original)"
		COMMAND "MESSAGE (original)" "MESSAGE Message (original)"
			MESSAGE "MESSAGE statement Message (original)"
		COMMAND "EXIT (original)" "Exit Program (original)"
			EXIT MENU
	END MENU

END FUNCTION