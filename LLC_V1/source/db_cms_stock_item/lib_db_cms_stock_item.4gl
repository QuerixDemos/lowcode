########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
#GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7912
# Addresses/Scenario:
# PK CHAR and one FK rate_id integer
########################################################################
# DB Table Schema
#			CREATE TABLE stock_item (
#				stock_id            CHAR(10) NOT NULL,
#				item_desc           CHAR(80)  NOT NULL UNIQUE,
#				item_cost           MONEY(8,2),
#				rate_id             INTEGER NOT NULL,
#				quantity            INTEGER DEFAULT 0,
#				PRIMARY KEY (stock_id),
#				FOREIGN KEY (rate_id) REFERENCES tax_rate (rate_id)
#			)
########################################################################


########################################################################
# FUNCTION db_cms_stock_item()	
#
#
########################################################################
FUNCTION db_cms_stock_item()	
	CALL InteractFormFile("../db_cms_stock_item/db_cms_stock_item")
END FUNCTION
########################################################################
# END FUNCTION db_cms_stock_item()	
########################################################################

########################################################################
# FUNCTION db_cms_stock_item_rec()
#
#
########################################################################
FUNCTION db_cms_stock_item_rec()
    CALL InteractFormFile("../db_cms_stock_item/db_cms_stock_item_rec")
END FUNCTION
########################################################################
# END FUNCTION db_cms_stock_item_rec()
########################################################################

########################################################################
# FUNCTION db_cms_stock_item_list()
#
#
########################################################################
FUNCTION db_cms_stock_item_list()
    CALL InteractFormFile("../db_cms_stock_item/db_cms_stock_item_list")
END FUNCTION
########################################################################
# END FUNCTION db_cms_stock_item_list()
########################################################################