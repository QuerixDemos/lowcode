########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses/Scenario:
# rate_id is SERIAL PK
########################################################################
# DB Table Schema
#CREATE TABLE test07 (
#	test07_primary_key SERIAL,
#	test07_foreign_key  INT,  #FK pointing at table test07 PK
#	test07_fk_char  CHAR NOT NULL,						
#	test07_col1  VARCHAR(20),
#	test07_col2  VARCHAR(20),
#	test07_col3  DATE,
#	FOREIGN KEY (test07_foreign_key) 
#	REFERENCES test08 (test08_primary_key) ,
#	FOREIGN KEY (test07_fk_char) 
#	REFERENCES test06 (test06_primary_key) ,
#	PRIMARY KEY (test07_primary_key) CONSTRAINT pk_test07_primary_key    
#)
#################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT

	OPTIONS INPUT WRAP

#	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("test07")
	CALL fgl_settitle("test07")  

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("test07 Record","test07 Record","{CONTEXT}/public/querix/icon/svg/24/ic_tax_24px.svg",101,TRUE,"test07 Record - Display, Scroll and modify test07 table data","top") 
			CALL fgl_dialog_setkeylabel("test07 List","test07 List","{CONTEXT}/public/querix/icon/svg/24/ic_tax_list_24px.svg",102,TRUE,"test07 List - List all ttest07 table data","top")			

		ON ACTION "test07 Record"
			CALL db_table_test07_rec()

		ON ACTION "test07 List"
			CALL db_table_test07_list()	
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################


########################################################################
# FUNCTION db_table_test07_rec()
#
#
########################################################################
FUNCTION db_table_test07_rec()	
    CALL InteractFormFile("../test_data/db_table_test07_rec")                                # The main staff function
END FUNCTION
########################################################################
# END FUNCTION db_table_test07_rec()
########################################################################


########################################################################
# FUNCTION db_table_test07_list()
#
#
########################################################################
FUNCTION db_table_test07_list()	
	DEFINE settings InteractForm_Settings
    LET settings.paged_mode = TRUE
    LET settings.form_file = "../test_data/db_table_test07_list"
    --LET settings.display = TRUE
    CALL InteractForm(settings)
END FUNCTION
########################################################################
# END FUNCTION db_table_test07_list()
########################################################################