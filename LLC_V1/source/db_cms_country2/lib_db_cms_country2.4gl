########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7909
# Anomaly
# No PK but the column is NOT NULL unique and only one column
########################################################################
# DB Table Schema
#	CREATE TABLE country2 (
#		country_code         nchar(3)                                no
#		country_text         nvarchar(60,0)                          no
#		language_code        nchar(3)                                yes
#		post_code_text       nvarchar(20,0)                          yes
#		post_code_min_num    smallint                                yes
#		post_code_max_num    smallint                                yes
#		state_code_text      nvarchar(20,0)                          yes
#		state_code_min_num   smallint                                yes
#		state_code_max_num   smallint                                yes
#		bank_acc_format      smallint
#	)
########################################################################

########################################################################
# FUNCTION db_cms_country2_rec()	
#
# Edit Record
########################################################################
FUNCTION db_cms_country2_rec()	
    CALL InteractFormFile("../db_cms_country2/db_cms_country2_rec")                            
END FUNCTION
########################################################################
# END FUNCTION db_cms_country2_rec()	
########################################################################


########################################################################
# FUNCTION db_cms_country2_list()	
#
#
########################################################################
FUNCTION db_cms_country2_list()
    CALL InteractFormFile("../db_cms_country2/db_cms_country2_list")                        
END FUNCTION
########################################################################
# END FUNCTION db_cms_country2_list()	
########################################################################