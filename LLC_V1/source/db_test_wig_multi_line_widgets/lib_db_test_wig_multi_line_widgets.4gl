########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7959
# Addresses/Scenario:
# Test different multi line widgets
########################################################################
# DB Table Schema
{
          CREATE TABLE test02 (
          	fTextField VARCHAR(20)
            fListBox   VARCHAR(20),
						fListBox VARCHAR(20),
						fCombo VARCHAR(20),
						fRadioButtonListV VARCHAR(20),
						fTextArea VARCHAR(250),
						fBlobText TEXT
					)

}          
########################################################################

########################################################################
# FUNCTION db_test_wig_multi_line_widgets_rec()
#
#
########################################################################
FUNCTION db_test_wig_multi_line_widgets_rec()	
    CALL InteractFormFile("../db_test_wig_multi_line_widgets/db_test_wig_multi_line_widgets_rec")                                # The main staff function
END FUNCTION
########################################################################
# END FUNCTION db_test_wig_multi_line_widgets_rec()
########################################################################


########################################################################
# FUNCTION db_test_wig_multi_line_widgets_list()
#
#
########################################################################
FUNCTION db_test_wig_multi_line_widgets_list()	
    CALL InteractFormFile("../db_test_wig_multi_line_widgets/db_test_wig_multi_line_widgets_list")                                # The main staff function
END FUNCTION
########################################################################
# END FUNCTION db_test_wig_multi_line_widgets_list()
########################################################################