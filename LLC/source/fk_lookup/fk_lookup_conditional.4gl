########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
PUBLIC DEFINE md_subtitle STRING #for demo subTitle text
PUBLIC DEFINE md_info STRING #for demo description text
PUBLIC DEFINE md_code_sample STRING #for demo code sniplet
PUBLIC DEFINE md_language STRING #Language 
PUBLIC DEFINE md_msg STRING #string for messages
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7963
# Addresses/Scenario...
# Single table but byte and char(1000) field
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_title STRING

	DEFER INTERRUPT
	OPTIONS INPUT WRAP

	LET l_title = os.Path.basename(arg_val(0))
	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText(l_title)
	CALL fgl_settitle(l_title)  

	#Demo info to be displayed on screen
	LET md_subtitle = "iform.PopulateComboBoxWhere() Conditional comboBox controlled in 4gl"
	LET md_info = "Retrieve the field values controlling the condition using iform.GetFieldValue() and re-populate the comboBox using iform.PopulateComboBoxWhere()"
	LET md_code_sample = 
		"DEFINE l_comp_industry LIKE company.comp_industry #comp_industry", "\n",
		"LET l_comp_industry = iform.GetFieldValue(\"company.comp_industry\")	#retrieve the value of comp_industry", "\n",
		"CASE l_comp_industry", "\n",
		"	WHEN 1 #Software Tester", "\n",
		"		CALL iform.PopulateComboBoxWhere(\"company.comp_type\", \"company_type.company_type_id IN (1,8)\") #1Reseller 8=Supplier", "\n",
		"	WHEN 2 #Software Reseller", "\n",
		"		CALL iform.PopulateComboBoxWhere(\"company.comp_type\", \"company_type.company_type_id IN (1,3,8,9)\") #1Reseller 3=Distributor 8=Supplier 9=VAR", "\n",
		"	WHEN 3 #Consultancy", "\n"

	MENU 
		BEFORE MENU
		CALL fgl_dialog_setkeylabel("Record",	"Record/Detailed View","{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	101,TRUE,"Record/Detailed View - Display, Scroll and modify table data (record view)",	"top") 

		ON ACTION "Record"
			CALL fk_lookup_conditional_rec()
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################


########################################################################
# FUNCTION fk_lookup_conditional_rec()	
#
# Edit Record
########################################################################
FUNCTION fk_lookup_conditional_rec()	
	DEFINE l_rec_settings InteractForm_Settings
	LET l_rec_settings.form_file = "../fk_lookup/fk_lookup_conditional_rec"

	LET l_rec_settings.log_file = "../log/fk_lookup_conditional.log"	#enable log file
	LET l_rec_settings.pessimistic_locking = TRUE

	LET l_rec_settings.id = "combo-conditional"

	LET l_rec_settings.views["company"].navigation_status="nav_page_of"	#Display current cursor location to with label by identifier
	
	LET l_rec_settings.views["company"].actions["UPDATE"]["BEFORE FIELD comp_type"] = FUNCTION before_field_comp_type	
	LET l_rec_settings.views["company"].actions["INSERT"]["BEFORE FIELD comp_type"] = FUNCTION before_field_comp_type	

	#Control show/remove default events
	LET l_rec_settings.views["company"].view_attributes[""]["INSERT ROW"] = TRUE
	LET l_rec_settings.views["company"].view_attributes[""]["APPEND ROW"] = TRUE #Note, the LLC library uses DISPLAY/INPUT ARRAY for all data (single record and array record)
	LET l_rec_settings.views["company"].view_attributes[""]["DELETE ROW"] = FALSE

	# WE could also initialize a comboBox dropDownBox here with a function call
	#LET l_settings.views["company"].comboboxes["company.comp_industry"] = "industry_type.industry_type_id IN (2,8,13)"

	#Display some information
	LET l_rec_settings.views["company"].navigation_status="nav_page_of"	#Display current cursor location to with label by identifier
	LET l_rec_settings.views["company"].actions[""]["BEFORE DIALOG" ] = FUNCTION display_info
		
	CALL InteractForm(l_rec_settings)	

END FUNCTION
########################################################################
# END FUNCTION fk_lookup_conditional_rec()	
########################################################################



########################################################################
# FUNCTION before_field_comp_type(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION before_field_comp_type(iform InteractForm INOUT) RETURNS BOOL
	#	DEFINE priority INT		
		DEFINE l_comp_industry LIKE company.comp_industry #comp_industry

		LET l_comp_industry = iform.GetFieldValue("company.comp_industry")	#retrieve the value of comp_industry

		CASE l_comp_industry
			WHEN 1 #Software Tester
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,8)") #1Reseller 8=Supplier
			WHEN 2 #Software Reseller
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,3,8,9)") #1Reseller 3=Distributor 8=Supplier 9=VAR
			WHEN 3 #Consultancy
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,2,3,4,5,6,7,8,9)") 
			WHEN 4 #Marketing Agency
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (2,3)") 
			WHEN 5 #Oil Refinery
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1)") 
			WHEN 6 #Other
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,2,3,4,5,6,7,8,9)") 
			WHEN 7 #Recording Studio
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,2,3,4,5,6,7,8,9)") 
			WHEN 8 #Manufacturing
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (2,4)") #2=Enduser 4=Educational
			WHEN 9 #Hardware Manufact.
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,2,3,4,5,6,7,8,9)") 
			WHEN 10 #Education
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,2,3,4,5,6,7,8,9)") 
			WHEN 11 #Brewery
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,4,5,6,9)") 
			WHEN 12 #Entertainment
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,3,7,9)") 
			WHEN 13 #Software Distributor
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,3,8,9)") #1Reseller 3=Distributor 8=Supplier 9=VAR
			WHEN 14 #Fitness
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,9)") 
			WHEN 15 #ISV
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (4,5,6)") 
			WHEN 16 #Internal Maintenance
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (3,4,5,6,7,8,9)") 

				OTHERWISE
				CALL fgl_winmessage("ERROR","Invalid Company Industry\n(Internal 4gl error)","ERROR")
		END CASE

		RETURN TRUE

END FUNCTION
########################################################################
# END FUNCTION before_field_comp_type(iform InteractForm INOUT) RETURNS BOOL
########################################################################

#Table industry_type
#1|Software Tester|0|
#2|Software Reseller|0|
#3|Consultancy|0|
#4|Marketing Agency|0|
#5|Oil Refinery|0|
#6|Other|0|
#7|Recording Studio|0|
#8|Manufacturing|0|
#9|Hardware Manufact.|0|
#10|Education|0|
#11|Brewery|0|
#12|Entertainment|0|
#13|Software Distributor|0|
#14|Fitness|0|
#15|ISV|0|
#16|Internal Maintenance|0|





########################################################################
# Below are only some reusable demo functions used by multiple
# demo applications to show the data 
#
#
########################################################################

########################################################################
# FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL
#
# Display some useful information to explain the demo
########################################################################
FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL

	IF md_language IS NOT NULL THEN
		LET md_info = md_language, "\n", md_info
	END IF
	DISPLAY md_subtitle TO lb_SubTitle
	DISPLAY md_info TO lb_info
	DISPLAY md_code_sample TO code_sample

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL
########################################################################

########################################################################
# FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING


	LET dlg = ui.Dialog.GetCurrent()
    LET l_rec_test05.test05_primary_key = iform.GetFieldValue("test05.test05_primary_key")
    LET l_rec_test05.test05_fk_char = iform.GetFieldValue("test05.test05_fk_char")
    LET l_rec_test05.test05_varchar = iform.GetFieldValue("test05.test05_varchar")
    LET l_rec_test05.test05_int = iform.GetFieldValue("test05.test05_int")
    LET l_rec_test05.test05_date = iform.GetFieldValue("test05.test05_date")

	#For demo purpose - range 0-1000
	LET l_msg = "The variable test05_int=", trim(l_rec_test05.test05_int), " was outside of the valid range 0-1000 and was corrected to "
	IF l_rec_test05.test05_int < 0 THEN

		LET l_rec_test05.test05_int = 0	#min
		LET l_msg = l_msg , " ", trim(l_rec_test05.test05_int) 
		CALL fgl_winmessage("test05_int",l_msg,"error")
	ELSE
		IF l_rec_test05.test05_int > 1000 THEN
			LET l_rec_test05.test05_int = 1000 #max
			LET l_msg = l_msg , " ", trim(l_rec_test05.test05_int) 
			CALL fgl_winmessage("test05_int",l_msg,"error")
		END IF
	END IF
	
	CALL iform.SetFieldValue("test05.test05_int", l_rec_test05.test05_int)

	LET l_msg = md_msg,
	"\nRecord Data:",
	"\n", trim(l_rec_test05.test05_primary_key) , 
	"\n", trim(l_rec_test05.test05_fk_char), 
	"\n", trim(l_rec_test05.test05_varchar), 
	"\n", trim(l_rec_test05.test05_int), 
	"\n", trim(l_rec_test05.test05_date)

	CALL fgl_winmessage("Record Data",l_msg,"info")
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
########################################################################


########################################################################
# FUNCTION take_settings_from_4gl()
#
#
########################################################################
FUNCTION take_settings_from_4gl()

	MENU "unused" ATTRIBUTES ( STYLE="popup" )
		COMMAND "From Form"
			RETURN FALSE
		COMMAND "FROM Form and 4GL"
			RETURN TRUE
	END MENU	
	
	END FUNCTION

