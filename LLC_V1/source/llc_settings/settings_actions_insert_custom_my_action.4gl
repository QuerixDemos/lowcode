########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_rec"
	LET l_settings.actions[""]["ON ACTION my_action" ] = FUNCTION actions_root_custom_my_action

	CALL InteractForm(l_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_list"
	LET l_settings.actions[""]["ON ACTION my_action" ] = FUNCTION actions_root_custom_my_action

	CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################


########################################################################
# FUNCTION actions_root_custom_my_action()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_root_custom_my_action(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING

	CALL display_dialog_record(iform)

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_root_custom_my_action()RETURNS BOOL
########################################################################