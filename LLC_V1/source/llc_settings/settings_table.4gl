
########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# settings.table
#
########################################################################
# DB Table Schema
#CREATE TABLE test05 (
#    test05_primary_key SERIAL,
#    test05_fk_char  CHAR,
#    test05_varchar  VARCHAR(20),
#    test05_int  INTEGER,
#    test05_date  DATE,
#                PRIMARY KEY (test05_primary_key) CONSTRAINT pk_test05_primary_key    
#            )
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_title STRING

	DEFER INTERRUPT
	OPTIONS INPUT WRAP

	LET l_title = os.Path.basename(arg_val(0))
	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText(l_title)
	CALL fgl_settitle(l_title)  

	MENU 
		BEFORE MENU
		CALL fgl_dialog_setkeylabel("Record",	"Record/Detailed View","{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	101,TRUE,"Record/Detailed View - Display, Scroll and modify table data (record view)",	"top") 
		CALL fgl_dialog_setkeylabel("List",		"Array/List View",		"{CONTEXT}/public/querix/icon/svg/24/ic_list_24px.svg",		102,TRUE,"Array/List View - List all table data (array view)",							"top")			

		ON ACTION "Record"
			CALL settings_rec()

		ON ACTION "List"
			CALL settings_list()	
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################


########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
    DEFINE l_settings InteractForm_Settings

    LET l_settings.form_file="settings_table_rec"
	LET l_settings.table="test05"
	LET l_settings.screen_record = "myScreenRecord"
    LET l_settings.log_file="myLogFile.log"

    CALL InteractForm(l_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_settings InteractForm_Settings

    LET l_settings.form_file="settings_table_list"
    LET l_settings.log_file="myLogFile.log"

    CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################