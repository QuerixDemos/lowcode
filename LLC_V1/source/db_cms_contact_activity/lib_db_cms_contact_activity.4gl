########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses:
# 1:n relationship contact-activity
########################################################################
# DB Table Schema
{
          CREATE TABLE contact (
            cont_id 	SERIAL(1000) NOT NULL,
            cont_title	VARCHAR(10),
            cont_name 	VARCHAR(20) NOT NULL UNIQUE,
            cont_fname 	VARCHAR(20),
            cont_lname 	VARCHAR(20),
            cont_addr1	VARCHAR(40),
            cont_addr2	VARCHAR(40),
            cont_addr3	VARCHAR(40),
            cont_city	VARCHAR(20),
            cont_zone	VARCHAR(15),
            cont_zip	VARCHAR(15),
            cont_country	VARCHAR(40),
            cont_phone	VARCHAR(15),
            cont_fax	VARCHAR(15),
            cont_mobile	VARCHAR(15),
            cont_email	VARCHAR(50) NOT NULL UNIQUE,
            cont_dept	VARCHAR(15),	# ref cont_dept_id
            cont_org	INTEGER,	# ref company_id
            cont_position	VARCHAR(15),	# ref position_id
            cont_picture	BYTE,
            cont_password	VARCHAR(15),
            cont_ipaddr	VARCHAR(15),	# IP V4
            cont_usemail	SMALLINT,
            cont_usephone	SMALLINT,
            cont_notes   CHAR(1000),
 
            PRIMARY KEY (cont_id) CONSTRAINT contact_pk,
            FOREIGN KEY (cont_org) REFERENCES company(comp_id) CONSTRAINT cont_org_fk_company          

          )

          CREATE TABLE activity (
            activity_id	SERIAL UNIQUE,
            open_date	DATE NOT NULL,
            close_date	DATE,
            contact_id	INTEGER NOT NULL,	# ref contact_id
            comp_id		INTEGER,	# ref comp_id
            operator_id     INTEGER NOT NULL,	# ref operator_id
            act_type	INTEGER,		# ref activity_type
            long_desc	TEXT,
            short_desc	CHAR(80),
            #long_ref	INTEGER,		# ref possible blob
            a_owner		INTEGER,	# ref contact_id
            priority	INTEGER,
            
            PRIMARY KEY (activity_id) CONSTRAINT activity_pk,
            FOREIGN KEY (contact_id) REFERENCES contact(cont_id) CONSTRAINT contact_id_fk_contact,
            FOREIGN KEY (comp_id) REFERENCES company(cont_id) CONSTRAINT comp_id_fk_company,
            FOREIGN KEY (operator_id) REFERENCES operator(operator_id) CONSTRAINT operator_id_fk_operator,
            FOREIGN KEY (act_type) REFERENCES activity_type(atype_name) CONSTRAINT act_type_fk_activity_type
            
          )

}         
########################################################################



########################################################################
# FUNCTION db_cms_contact_activity_rec(p_rec_settings InteractForm_Settings)	
#
# Edit Record
########################################################################
FUNCTION db_cms_contact_activity_rec(p_rec_settings InteractForm_Settings)	
	LET p_rec_settings.form_file = "../db_cms_contact_activity/db_cms_contact_activity_rec"
	CALL InteractForm(p_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION db_cms_contact_activity_rec(p_rec_settings InteractForm_Settings)	
########################################################################


########################################################################
# FUNCTION db_cms_contact_activity_list(p_rec_settings InteractForm_Settings)	
#
# Edit Record
########################################################################
FUNCTION db_cms_contact_activity_list(p_rec_settings InteractForm_Settings)	
	LET p_rec_settings.form_file = "../db_cms_contact_activity/db_cms_contact_activity_list"
	CALL InteractForm(p_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION db_cms_contact_activity_list(p_rec_settings InteractForm_Settings)	
########################################################################