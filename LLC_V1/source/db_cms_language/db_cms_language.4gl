########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Adresses Scenario
# Single table but byte and char(1000) field
########################################################################
# DB Table Schema
{
	CREATE TABLE language (
		language_code        nchar(3)                                no
		language_text        nvarchar(80,0)                          no
		yes_flag             nchar(1)                                yes
		no_flag              nchar(1)                                yes
		national_text        nvarchar(80,0)                          yes    
          )
}          
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT
	OPTIONS INPUT WRAP
	
	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Language")
	CALL fgl_settitle("Language")

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Language Record","Language Record","{CONTEXT}/public/querix/icon/svg/24/ic_country_24px.svg"  ,101,TRUE,"Display and modify language data","top") 
			CALL fgl_dialog_setkeylabel("Language List","Language List",    "{CONTEXT}/public/querix/icon/svg/24/ic_country_list_24px.svg",102,TRUE,"List all languages (List)","top")			

		ON ACTION "Language Record"
			CALL db_cms_language_rec()

		ON ACTION "Language List"
			CALL db_cms_language_list()
								
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################