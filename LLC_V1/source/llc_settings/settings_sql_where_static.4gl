
########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_rec"
	LET l_settings.sql_where_static = "test05_fk_char='A'"	#SQL WHERE clause (STATIC, will not be overwritten by search/construct)
	LET l_settings.sql_where = "test05_primary_key=6"	#SQL WHERE clause (temp, not static, will be overwritten by search/construct)

	CALL InteractForm(l_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_list"
	LET l_settings.sql_where_static = "test05_fk_char='A'"	#SQL WHERE clause (STATIC, will not be overwritten by search/construct)
	LET l_settings.sql_where = "test05_primary_key=6"	#SQL WHERE clause (temp, not static, will be overwritten by search/construct)

	CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################