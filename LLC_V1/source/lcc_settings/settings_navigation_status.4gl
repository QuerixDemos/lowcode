
########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
    DEFINE l_rec_settings InteractForm_Settings

    LET l_rec_settings.form_file="../llc_settings/llc_settings_rec"
    LET l_rec_settings.navigation_status="nav_page_of"		

		CALL InteractForm(l_rec_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_rec_settings InteractForm_Settings

    LET l_rec_settings.form_file="../llc_settings/llc_settings_list"
    LET l_rec_settings.navigation_status="nav_page_of"		

    CALL InteractForm(l_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################