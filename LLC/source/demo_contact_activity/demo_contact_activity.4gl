########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses:
# 1:n relationship contact-activity
########################################################################
# DB Table Schema
{
          CREATE TABLE contact (
            cont_id 	SERIAL(1000) NOT NULL,
            cont_title	VARCHAR(10),
            cont_name 	VARCHAR(20) NOT NULL UNIQUE,
            cont_fname 	VARCHAR(20),
            cont_lname 	VARCHAR(20),
            cont_addr1	VARCHAR(40),
            cont_addr2	VARCHAR(40),
            cont_addr3	VARCHAR(40),
            cont_city	VARCHAR(20),
            cont_zone	VARCHAR(15),
            cont_zip	VARCHAR(15),
            cont_country	VARCHAR(40),
            cont_phone	VARCHAR(15),
            cont_fax	VARCHAR(15),
            cont_mobile	VARCHAR(15),
            cont_email	VARCHAR(50) NOT NULL UNIQUE,
            cont_dept	VARCHAR(15),	# ref cont_dept_id
            cont_org	INTEGER,	# ref company_id
            cont_position	VARCHAR(15),	# ref position_id
            cont_picture	BYTE,
            cont_password	VARCHAR(15),
            cont_ipaddr	VARCHAR(15),	# IP V4
            cont_usemail	SMALLINT,
            cont_usephone	SMALLINT,
            cont_notes   CHAR(1000),
 
            PRIMARY KEY (cont_id) CONSTRAINT contact_pk,
            FOREIGN KEY (cont_org) REFERENCES company(comp_id) CONSTRAINT cont_org_fk_company          

          )

          CREATE TABLE activity (
            activity_id	SERIAL UNIQUE,
            open_date	DATE NOT NULL,
            close_date	DATE,
            contact_id	INTEGER NOT NULL,	# ref contact_id
            comp_id		INTEGER,	# ref comp_id
            operator_id     INTEGER NOT NULL,	# ref operator_id
            act_type	INTEGER,		# ref activity_type
            long_desc	TEXT,
            short_desc	CHAR(80),
            #long_ref	INTEGER,		# ref possible blob
            a_owner		INTEGER,	# ref contact_id
            priority	INTEGER,
            
            PRIMARY KEY (activity_id) CONSTRAINT activity_pk,
            FOREIGN KEY (contact_id) REFERENCES contact(cont_id) CONSTRAINT contact_id_fk_contact,
            FOREIGN KEY (comp_id) REFERENCES company(cont_id) CONSTRAINT comp_id_fk_company,
            FOREIGN KEY (operator_id) REFERENCES operator(operator_id) CONSTRAINT operator_id_fk_operator,
            FOREIGN KEY (act_type) REFERENCES activity_type(atype_name) CONSTRAINT act_type_fk_activity_type
            
          )

}          
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
    DEFINE l_activity_settings_rec InteractForm_Settings
    DEFINE l_activity_settings_list InteractForm_Settings
    DEFINE l_contact_settings_rec InteractForm_Settings
    DEFINE l_contact_settings_list InteractForm_Settings
    DEFINE l_contact_activity_settings_rec InteractForm_Settings
    DEFINE l_contact_activity_settings_list InteractForm_Settings
	
	DEFER INTERRUPT

	OPTIONS INPUT WRAP
	
	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Contact Activity")
	CALL fgl_settitle("Contact Activity")    

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Contact Record", "Contact", "{CONTEXT}/public/querix/icon/svg/24/ic_contact_24px.svg", 101, TRUE, "Display and modify contact data","top") 
			CALL fgl_dialog_setkeylabel("Contact List", "Contact List", "{CONTEXT}/public/querix/icon/svg/24/ic_contacts_24px.svg", 102, TRUE, "List all contacts","top")			

			CALL fgl_dialog_setkeylabel("Activity Details", "Activity Details", "{CONTEXT}/public/querix/icon/svg/24/ic_communication_1_24px.svg", 111, TRUE, "Display and modify activity details (Record)","top") 
			CALL fgl_dialog_setkeylabel("Activity List", "Activity List", "{CONTEXT}/public/querix/icon/svg/24/ic_communication_list_1_24px.svg", 112, TRUE, "List all activities (List)","top")			

			CALL fgl_dialog_setkeylabel("Contact Activity Detail", "Contact Activity Detail", "{CONTEXT}/public/querix/icon/svg/24/ic_contact_activity_detail_24px.svg", 121, TRUE, "Contact activity Details/Records","top")
			CALL fgl_dialog_setkeylabel("Contact Activity List", "Contact Activity List", "{CONTEXT}/public/querix/icon/svg/24/ic_contact_activity_list_24px.svg", 122, TRUE, "List all contact activities","top")
			

----

		ON ACTION "Contact Record"
			CALL contact_rec(l_contact_settings_rec)
			
		ON ACTION "Contact List"
			CALL contact_list(l_contact_settings_list)
----

		ON ACTION "Activity Details"
			CALL activity_rec(l_activity_settings_rec)
			
		ON ACTION "Activity List"
			CALL activity_list(l_activity_settings_list)

----

		ON ACTION "Contact Activity Detail"
			CALL contact_activity_rec(l_contact_activity_settings_rec)

		ON ACTION "Contact Activity List"
			CALL contact_activity_list(l_contact_activity_settings_list)

			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################