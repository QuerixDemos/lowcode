########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
DEFINE m_pk LIKE test05.test05_primary_key

########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file = "../llc_settings/llc_settings_rec"
	LET l_settings.input_mode = TRUE
	LET l_settings.sql_where_static = "test05_primary_key=", trim(m_pk)	#SQL WHERE clause (STATIC, will not be overwritten by search/construct)
	CALL InteractForm(l_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_list"
	LET l_settings.actions[""]["ON ACTION DOUBLECLICK"] = FUNCTION settings_rec_input
	LET l_settings.actions[""]["BEFORE ROW"] = FUNCTION actions_set_m_pk
	CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################

########################################################################
# FUNCTION settings_rec_input()
#
#
########################################################################
FUNCTION settings_rec_input(iform InteractForm INOUT)	
    CALL settings_rec()
	CALL iform.Refresh(FALSE)
END FUNCTION
########################################################################
# END FUNCTION settings_rec_input()
########################################################################

########################################################################
# FUNCTION actions_set_m_pk(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION actions_set_m_pk(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog

	LET dlg = ui.Dialog.GetCurrent()
	LET m_pk = iform.GetFieldValue("test05.test05_primary_key")

	RETURN FALSE
END FUNCTION
########################################################################
# FUNCTION actions_set_m_pk(iform InteractForm INOUT) RETURNS BOOL
########################################################################