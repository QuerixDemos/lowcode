########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
PUBLIC DEFINE md_subtitle STRING #for demo subTitle text
PUBLIC DEFINE md_info STRING #for demo description text
PUBLIC DEFINE md_code_sample STRING #for demo code sniplet
PUBLIC DEFINE md_language STRING #Language 
PUBLIC DEFINE md_msg STRING #string for messages
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
    DEFINE l_settings InteractForm_Settings

    LET l_settings.form_file="../llc_settings/llc_settings_rec"
    LET l_settings.paged_mode= TRUE	#Turn paged mode on - Lycia will only handle one page of screen data (pulling from the DB and sending to the client)

    CALL InteractForm(l_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
    DEFINE l_settings InteractForm_Settings

    LET l_settings.form_file="../llc_settings/llc_settings_list"
    LET l_settings.paged_mode= TRUE	#Turn paged mode on - Lycia will only handle one page of screen data (pulling from the DB and sending to the client)

    CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################