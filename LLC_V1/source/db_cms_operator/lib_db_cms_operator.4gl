################################################################
# DATABASE
################################################################
DATABASE cms_llc
################################################################
# GLOBAL scope variables
################################################################
#GLOBALS "../common/glob_GLOBALS.4gl"
################################################################
# MODULE scope variables
################################################################

########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7910
# Addresses/Scenario:
# PK is serial and FK contact.cont_id
########################################################################
# DB Schema table operator
#
#		WHEN 1  --create table
#			CREATE TABLE operator (
#				operator_id         SERIAL,
#				name                CHAR(10)  NOT NULL UNIQUE,
#				password            CHAR(10),
#				type                CHAR,
#				cont_id             INTEGER, # ref contact_id
#				email_address       VARCHAR(100),
#				
#				PRIMARY KEY (operator_id),
#				FOREIGN KEY (cont_id) REFERENCES contact(cont_id)
#			)
################################################################

########################################################################
# FUNCTION db_cms_operator()	
#
#
########################################################################
FUNCTION db_cms_operator()	
	CALL InteractFormFile("../db_cms_operator/form/operator_det_l3")
END FUNCTION


########################################################################
# FUNCTION db_cms_operator_rec()
#
#
########################################################################
FUNCTION db_cms_operator_rec()
    CALL InteractFormFile("../db_cms_operator/db_cms_operator_rec")
END FUNCTION
########################################################################
# END FUNCTION db_cms_operator_rec()
########################################################################

########################################################################
# FUNCTION db_cms_operator_list()
#
#
########################################################################
FUNCTION db_cms_operator_list()
    CALL InteractFormFile("../db_cms_operator/db_cms_operator_list")
END FUNCTION
########################################################################
# END FUNCTION db_cms_operator_list()
########################################################################