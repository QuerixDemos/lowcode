########################################################################
# GLOBAL Scope Variables
########################################################################
#GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
# https://querix.atlassian.net/browse/LYC-7914
########################################################################
# Addresses
#    BLOB TEXT column in table
########################################################################
# DB Table Schema
#          CREATE TABLE qxt_document (
#            document_id          SERIAL(1000),
#            document_filename    VARCHAR(100) NOT NULL UNIQUE,
#            document_app         CHAR(30),
#            document_category1   CHAR(40),
#            document_category2   CHAR(40),
#            document_category3   CHAR(40),
#            document_mod_date    DATE,
#            document_desc        TEXT,  --CHAR(1000),  NOTE: Some DB's like Oracle don't allow to have 2 large objs i.e. 2 byte fields or a byte and a TEXT field
#
#            PRIMARY KEY (document_id)
#          )
########################################################################

########################################################################
# FUNCTION db_qxt_document_rec()
#
#
########################################################################
FUNCTION db_qxt_document_rec()	
    CALL InteractFormFile("../db_qxt_document/db_qxt_document_rec")                                # The main staff function
END FUNCTION
########################################################################
# END FUNCTION db_qxt_document_rec()
########################################################################


########################################################################
# FUNCTION db_qxt_document_rec2()
#
#
########################################################################
FUNCTION db_qxt_document_rec2()	
    CALL InteractFormFile("../db_qxt_document/db_qxt_document_original_cms")                                # The main staff function
END FUNCTION
########################################################################
# END FUNCTION db_qxt_document_rec2()
########################################################################


########################################################################
# FUNCTION db_qxt_document_list()
#
#
########################################################################
FUNCTION db_qxt_document_list()	
    CALL InteractFormFile("../db_qxt_document/db_qxt_document_list")                                # The main staff function
END FUNCTION
########################################################################
# END FUNCTION db_qxt_document_list()
########################################################################