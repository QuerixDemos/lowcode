########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7963
# Addresses/Scenario...
# Single table but byte and char(1000) field
########################################################################
# DB Table Schema
#          CREATE TABLE test04 (
#            test_primary_key SERIAL,
#            test04_i_1   INTEGER,
#            test04_i_2   INTEGER,
#            test04_i_3   INTEGER,
#            test04_s_1   VARCHAR(20),
#            test04_s_2   VARCHAR(20),
#            test04_s_3   VARCHAR(20)
#					)
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_settings_rec InteractForm_Settings
	DEFINE l_settings_list InteractForm_Settings
		
  DEFER INTERRUPT

	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("test04")
	CALL fgl_settitle("test04")   

	#You could initialise the LLC settings here
	#LET l_settings_rec.xxxx = yyyyy

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("test04 Details",			"test04 Details",			"{CONTEXT}/public/querix/icon/svg/24/ic_communication_1_24px.svg", 				101,TRUE,"Display and modify test04 details (Record)","top") 
			CALL fgl_dialog_setkeylabel("test04 List",   			"test04 List",				"{CONTEXT}/public/querix/icon/svg/24/ic_communication_list_1_24px.svg",		102,TRUE,"List all activities (List)","top")			

		ON ACTION "test04 Details"
			CALL db_table_test04_rec(l_settings_rec)
		
		ON ACTION "test04 List"
			CALL db_table_test04_list(l_settings_list)
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################

########################################################################
# FUNCTION db_table_test04_rec(p_settings InteractForm_Settings) 	
#
# Edit Record
########################################################################
FUNCTION db_table_test04_rec(p_settings InteractForm_Settings) 

	LET p_settings.form_file = "../test04/test04_rec"
	#Invoke LLC runtime engine
	CALL InteractForm(p_settings)	

END FUNCTION
########################################################################
# END FUNCTION db_table_test04_rec()	
########################################################################


########################################################################
# FUNCTION db_table_test04_list()	
#
#
########################################################################
FUNCTION db_table_test04_list(p_settings InteractForm_Settings) 

	LET p_settings.form_file = "../test04/test04_list"
		
	CALL InteractForm(p_settings)
END FUNCTION
########################################################################
# END FUNCTION db_table_test04_list()	
########################################################################
