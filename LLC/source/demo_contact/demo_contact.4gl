DATABASE cms_llc
MAIN
	MENU
	
	###############################################################################
	# 3 ways of invoking LowCode
	# PUBLIC FUNCTION InteractFormFile(formFile STRING)
	# PUBLIC FUNCTION InteractFormFileWithSettings(form_file STRING, setting_id STRING)
	# PUBLIC FUNCTION InteractForm(settings InteractForm_Settings)
	###############################################################################

		ON ACTION "Record"
			CALL InteractFormFile("contact_record") #form file name

			ON ACTION "List"
			CALL InteractFormFile("contact_list") #form file name

			ON ACTION "CANCEL"
			EXIT MENU
	END MENU
	
END MAIN