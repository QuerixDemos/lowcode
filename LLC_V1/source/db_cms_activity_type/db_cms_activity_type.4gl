########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7909
# Anomaly/Difference
# None
########################################################################
# DB Table Schema
#	CREATE TABLE activity_type (
#		#Column name          Type                                    Nulls
#		activity_type_id     serial                                  no
#		atype_name           char(15)                                no
#		user_def             smallint   
#	)
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT

	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Activity Type")
	CALL fgl_settitle("Activity Type")       

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Activity Type Record","Activity Type",		"{CONTEXT}/public/querix/icon/svg/24/ic_communication_1_24px.svg",			101,TRUE,"Activity Type Record - Display and modify Activity Type data","top") 
			CALL fgl_dialog_setkeylabel("Activity Type List","Activity Type List","{CONTEXT}/public/querix/icon/svg/24/ic_communication_list_1_24px.svg",	102,TRUE,"Activity Type List - List all Activity Type data","top")			
		
		ON ACTION "Activity Type Record"
			CALL db_cms_activity_type_rec()
			
		ON ACTION "Activity Type List"
			CALL db_cms_activity_type_list()
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################                             yes