########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7909
# Anomaly/Difference
# No PK but the column is NOT NULL unique and only one column
########################################################################
# DB Table Schema
#          CREATE TABLE test03 (
#            math_operation_id   			SERIAL,
#						operator_val_1            DECIMAL(10,2) NOT NULL,
#						operator_val_2            DECIMAL(10,2) NOT NULL,
#						result_sum		            DECIMAL(10,2) NOT NULL,
#						result_difference	        DECIMAL(10,2) NOT NULL,
#						result_product		        DECIMAL(10,2) NOT NULL,
#						result_quotient           DECIMAL(10,2) NOT NULL,
#						PRIMARY KEY (math_operation_id) CONSTRAINT pk_math_operation_id
#					)
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Virtual Fields")
	CALL fgl_settitle("Virtual Fields")       

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Virtual Fields Record",	"Virtual Fields Record",	"{CONTEXT}/public/querix/icon/svg/24/ic_calculation_2_24px.svg",		101,TRUE,"Virtual Fields - Record Display and modify Math Operation data (no virtual fields are shown)","top") 
			CALL fgl_dialog_setkeylabel("Virtual Fields List",		"Virtual Fields List",		"{CONTEXT}/public/querix/icon/svg/24/ic_calculation_1_24px.svg",		102,TRUE,"Virtual Fields - List all Math Operation Records (no virtual fields are shown)","top")			
			CALL fgl_dialog_setkeylabel("Show all Table Data",		"Show all Table Data",		"{CONTEXT}/public/querix/icon/svg/24/ic_view_list_24px.svg",				103,TRUE,"Show ALL Fields (including virtual fields) - List all Math Operation Records (to view stored data of virtual fields)","top")

 		
		ON ACTION "Virtual Fields Record"
			CALL db_cms_virtual_field_rec()
			
		ON ACTION "Virtual Fields List"
			CALL db_cms_virtual_field_list()

		ON ACTION "Show all Table Data"
			CALL db_cms_virtual_field_list_all()
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################