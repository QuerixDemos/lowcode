
########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_comboboxes_rec"
	#To define static WHERE clause for filtering ComboBox values, define the settings.comboboxes["<table_name>.<field_name>"] property.
	LET l_settings.comboBoxes["test05.test05_fk_char"]  = "test06.test06_primary_key BETWEEN 'A' AND 'E'"

	#Further Examples
	#LET l_settings.comboBoxes["test05.test05_fk_char"]  = "test06.test06_col1 =  'Lookup 1 A'"
	#LET settings.comboboxes["activity.state_code"] = "state.country_code BETWEEN 5 AND 9"
	#LET settings.comboboxes["activity.state_code"] = "state.country_code BETWEEN 'A' AND 'E'"	

	CALL InteractForm(l_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_comboboxes_list"
	#To define static WHERE clause for filtering ComboBox values, define the settings.comboboxes["<table_name>.<field_name>"] property.
	LET l_settings.comboBoxes["test05.test05_fk_char"]  = "test06.test06_primary_key BETWEEN 'A' AND 'E'"

	#Further Examples
	#LET l_settings.comboBoxes["test05.test05_fk_char"]  = "test06.test06_col1 =  'Lookup 1 A'"
	#LET settings.comboboxes["activity.state_code"] = "state.country_code BETWEEN 5 AND 9"
	#LET settings.comboboxes["activity.state_code"] = "state.country_code BETWEEN 'A' AND 'E'"	

	CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################