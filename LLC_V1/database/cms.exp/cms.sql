{ DATABASE cms  delimiter | }

grant dba to "huho";











{ TABLE "informix".qxt_application row size = 34 number of columns = 2 index size = 44 }

{ unload file name = qxt_a00100.unl number of rows = 3 }

create table "informix".qxt_application 
  (
    application_id serial not null ,
    application_name char(30) not null ,
    unique (application_name) ,
    primary key (application_id) 
  );

revoke all on "informix".qxt_application from "public" as "informix";

{ TABLE "informix".qxt_language row size = 38 number of columns = 4 index size = 9 }

{ unload file name = qxt_l00101.unl number of rows = 8 }

create table "informix".qxt_language 
  (
    language_id serial not null ,
    language_name varchar(20) not null ,
    language_dir char(2) not null ,
    language_url varchar(10),
    primary key (language_id) 
  );

revoke all on "informix".qxt_language from "public" as "informix";

{ TABLE "informix".qxt_str_category row size = 39 number of columns = 3 index size = 22 }

{ unload file name = qxt_s00102.unl number of rows = 213 }

create table "informix".qxt_str_category 
  (
    category_id integer not null ,
    language_id integer not null ,
    category_data varchar(30) not null ,
    primary key (category_id,language_id) 
  );

revoke all on "informix".qxt_str_category from "public" as "informix";

{ TABLE "informix".qxt_string_tool row size = 121 number of columns = 6 index size = 22 }

{ unload file name = qxt_s00103.unl number of rows = 982 }

create table "informix".qxt_string_tool 
  (
    string_id integer not null ,
    language_id integer not null ,
    string_data varchar(100),
    category1_id integer,
    category2_id integer,
    category3_id integer,
    primary key (string_id,language_id) 
  );

revoke all on "informix".qxt_string_tool from "public" as "informix";

{ TABLE "informix".qxt_string_app row size = 221 number of columns = 6 index size = 22 }

{ unload file name = qxt_s00104.unl number of rows = 5954 }

create table "informix".qxt_string_app 
  (
    string_id integer not null ,
    language_id integer not null ,
    string_data varchar(200),
    category1_id integer,
    category2_id integer,
    category3_id integer,
    primary key (string_id,language_id) 
  );

revoke all on "informix".qxt_string_app from "public" as "informix";

{ TABLE "informix".qxt_help_classic row size = 105 number of columns = 2 index size = 9 }

{ unload file name = qxt_h00105.unl number of rows = 2 }

create table "informix".qxt_help_classic 
  (
    id serial not null ,
    filename varchar(100) not null ,
    primary key (id) 
  );

revoke all on "informix".qxt_help_classic from "public" as "informix";

{ TABLE "informix".qxt_help_url_map row size = 210 number of columns = 4 index size = 13 }

{ unload file name = qxt_h00106.unl number of rows = 8 }

create table "informix".qxt_help_url_map 
  (
    map_id integer not null ,
    language_id integer not null ,
    map_fname varchar(100) not null ,
    map_ext varchar(100),
    primary key (map_id,language_id) 
  );

revoke all on "informix".qxt_help_url_map from "public" as "informix";

{ TABLE "informix".qxt_help_html_doc row size = 169 number of columns = 5 index size = 22 }

{ unload file name = qxt_h00107.unl number of rows = 7 }

create table "informix".qxt_help_html_doc 
  (
    help_html_doc_id integer not null ,
    language_id integer not null ,
    help_html_filename varchar(100),
    help_html_mod_date date,
    help_html_data text,
    primary key (help_html_doc_id,language_id) 
  );

revoke all on "informix".qxt_help_html_doc from "public" as "informix";

{ TABLE "informix".qxt_toolbar row size = 328 number of columns = 11 index size = 98 }

{ unload file name = qxt_t00108.unl number of rows = 0 }

create table "informix".qxt_toolbar 
  (
    tb_prog_id varchar(30),
    tb_menu_id varchar(30),
    tb_action varchar(30),
    tb_label varchar(20),
    tb_icon varchar(100),
    tb_position integer,
    tb_static smallint,
    tb_tooltip varchar(100),
    tb_type smallint,
    tb_scope smallint,
    tb_hide smallint,
    primary key (tb_prog_id,tb_menu_id,tb_action) 
  );

revoke all on "informix".qxt_toolbar from "public" as "informix";

{ TABLE "informix".qxt_icon_size row size = 5 number of columns = 1 index size = 10 }

{ unload file name = qxt_i00109.unl number of rows = 5 }

create table "informix".qxt_icon_size 
  (
    icon_size_name char(5) not null ,
    primary key (icon_size_name) 
  );

revoke all on "informix".qxt_icon_size from "public" as "informix";

{ TABLE "informix".qxt_icon_path row size = 72 number of columns = 2 index size = 82 }

{ unload file name = qxt_i00110.unl number of rows = 5 }

create table "informix".qxt_icon_path 
  (
    icon_path_name varchar(20) not null ,
    icon_path_dir varchar(50) not null ,
    unique (icon_path_dir) ,
    primary key (icon_path_name) 
  );

revoke all on "informix".qxt_icon_path from "public" as "informix";

{ TABLE "informix".qxt_icon_property row size = 47 number of columns = 3 index size = 62 }

{ unload file name = qxt_i00111.unl number of rows = 5 }

create table "informix".qxt_icon_property 
  (
    icon_property_name varchar(20) not null ,
    icon_size_name char(5) not null ,
    icon_path_name varchar(20) not null ,
    primary key (icon_property_name) 
  );

revoke all on "informix".qxt_icon_property from "public" as "informix";

{ TABLE "informix".qxt_icon_category row size = 49 number of columns = 3 index size = 68 }

{ unload file name = qxt_i00112.unl number of rows = 115 }

create table "informix".qxt_icon_category 
  (
    icon_category_id integer not null ,
    language_id integer not null ,
    icon_category_data varchar(40) not null ,
    unique (icon_category_data) ,
    primary key (icon_category_id,language_id) 
  );

revoke all on "informix".qxt_icon_category from "public" as "informix";

{ TABLE "informix".qxt_icon row size = 113 number of columns = 4 index size = 106 }

{ unload file name = qxt_i00113.unl number of rows = 146 }

create table "informix".qxt_icon 
  (
    icon_filename varchar(100) not null ,
    icon_category1_id integer not null ,
    icon_category2_id integer not null ,
    icon_category3_id integer not null ,
    primary key (icon_filename) 
  );

revoke all on "informix".qxt_icon from "public" as "informix";

{ TABLE "informix".qxt_tbi_tooltip row size = 152 number of columns = 7 index size = 22 }

{ unload file name = qxt_t00114.unl number of rows = 177 }

create table "informix".qxt_tbi_tooltip 
  (
    string_id integer not null ,
    language_id integer not null ,
    label_data varchar(30),
    string_data varchar(100),
    category1_id integer,
    category2_id integer,
    category3_id integer,
    primary key (string_id,language_id) 
  );

revoke all on "informix".qxt_tbi_tooltip from "public" as "informix";

{ TABLE "informix".qxt_tbi_event_type row size = 15 number of columns = 2 index size = 25 }

{ unload file name = qxt_t00115.unl number of rows = 2 }

create table "informix".qxt_tbi_event_type 
  (
    event_type_id serial not null ,
    event_type_name varchar(10) not null ,
    unique (event_type_name) ,
    primary key (event_type_id) 
  );

revoke all on "informix".qxt_tbi_event_type from "public" as "informix";

{ TABLE "informix".qxt_tbi_obj_event row size = 31 number of columns = 1 index size = 36 }

{ unload file name = qxt_t00116.unl number of rows = 58 }

create table "informix".qxt_tbi_obj_event 
  (
    tbi_obj_event_name varchar(30) not null ,
    primary key (tbi_obj_event_name) 
  );

revoke all on "informix".qxt_tbi_obj_event from "public" as "informix";

{ TABLE "informix".qxt_tbi_obj_action row size = 35 number of columns = 3 index size = 43 }

{ unload file name = qxt_t00117.unl number of rows = 4 }

create table "informix".qxt_tbi_obj_action 
  (
    tbi_action_id smallint not null ,
    tbi_action_name varchar(30) not null ,
    string_id smallint not null ,
    unique (tbi_action_name) ,
    primary key (tbi_action_id) 
  );

revoke all on "informix".qxt_tbi_obj_action from "public" as "informix";

{ TABLE "informix".qxt_tbi_obj row size = 190 number of columns = 9 index size = 163 }

{ unload file name = qxt_t00118.unl number of rows = 149 }

create table "informix".qxt_tbi_obj 
  (
    tbi_obj_name varchar(35) not null ,
    event_type_id integer not null ,
    tbi_obj_event_name varchar(30) not null ,
    tbi_obj_action_id smallint not null ,
    icon_filename varchar(100) not null ,
    string_id integer not null ,
    icon_category1_id integer not null ,
    icon_category2_id integer not null ,
    icon_category3_id integer not null ,
    primary key (tbi_obj_name) 
  );

revoke all on "informix".qxt_tbi_obj from "public" as "informix";

{ TABLE "informix".qxt_tbi_scope row size = 37 number of columns = 3 index size = 52 }

{ unload file name = qxt_t00119.unl number of rows = 2 }

create table "informix".qxt_tbi_scope 
  (
    tbi_scope_id smallint not null ,
    tbi_scope_name varchar(30) not null ,
    string_id integer not null ,
    unique (tbi_scope_name) ,
    unique (string_id) ,
    primary key (tbi_scope_id) 
  );

revoke all on "informix".qxt_tbi_scope from "public" as "informix";

{ TABLE "informix".qxt_tbi_static row size = 27 number of columns = 3 index size = 16 }

{ unload file name = qxt_t00120.unl number of rows = 2 }

create table "informix".qxt_tbi_static 
  (
    tbi_static_id smallint not null ,
    tbi_static_name varchar(20) not null ,
    string_id integer not null ,
    unique (string_id) ,
    primary key (tbi_static_id) 
  );

revoke all on "informix".qxt_tbi_static from "public" as "informix";

{ TABLE "informix".qxt_tbi row size = 124 number of columns = 11 index size = 113 }

{ unload file name = qxt_t00121.unl number of rows = 336 }

create table "informix".qxt_tbi 
  (
    application_id integer not null ,
    tbi_name varchar(30) not null ,
    tbi_obj_name varchar(35) not null ,
    event_type_id integer not null ,
    tbi_event_name varchar(30) not null ,
    tbi_scope_id smallint not null ,
    tbi_position smallint not null ,
    tbi_static_id smallint not null ,
    icon_category1_id integer not null ,
    icon_category2_id integer not null ,
    icon_category3_id integer not null ,
    primary key (application_id,tbi_name) 
  );

revoke all on "informix".qxt_tbi from "public" as "informix";

{ TABLE "informix".qxt_tb row size = 68 number of columns = 4 index size = 82 }

{ unload file name = qxt_t00122.unl number of rows = 784 }

create table "informix".qxt_tb 
  (
    application_id integer not null ,
    tb_name varchar(30) not null ,
    tb_instance smallint not null ,
    tbi_name varchar(30) not null ,
    primary key (application_id,tb_name,tb_instance,tbi_name) 
  );

revoke all on "informix".qxt_tb from "public" as "informix";

{ TABLE "informix".qxt_document row size = 315 number of columns = 8 index size = 115 }

{ unload file name = qxt_d00123.unl number of rows = 7 }

create table "informix".qxt_document 
  (
    document_id serial not null ,
    document_filename varchar(100) not null ,
    document_app char(30),
    document_category1 char(40),
    document_category2 char(40),
    document_category3 char(40),
    document_mod_date date,
    document_desc text,
    unique (document_filename) ,
    primary key (document_id) 
  );

revoke all on "informix".qxt_document from "public" as "informix";

{ TABLE "informix".qxt_document_blob row size = 60 number of columns = 2 index size = 9 }

{ unload file name = qxt_d00124.unl number of rows = 7 }

create table "informix".qxt_document_blob 
  (
    document_id integer,
    document_blob byte,
    primary key (document_id) 
  );

revoke all on "informix".qxt_document_blob from "public" as "informix";

{ TABLE "informix".qxt_dde_font row size = 324 number of columns = 11 index size = 9 }

{ unload file name = qxt_d00125.unl number of rows = 12 }

create table "informix".qxt_dde_font 
  (
    dde_font_id integer not null ,
    font_name varchar(40),
    font_bold varchar(30),
    font_size varchar(30),
    strike_through varchar(30),
    super_script varchar(30),
    lower_script varchar(30),
    option_1 varchar(30),
    option_2 varchar(30),
    option_3 varchar(30),
    font_color varchar(30),
    primary key (dde_font_id) 
  );

revoke all on "informix".qxt_dde_font from "public" as "informix";

{ TABLE "informix".qxt_print_template row size = 169 number of columns = 5 index size = 22 }

{ unload file name = qxt_p00126.unl number of rows = 16 }

create table "informix".qxt_print_template 
  (
    template_id integer not null ,
    language_id integer not null ,
    filename varchar(100),
    mod_date date,
    template_data text,
    primary key (template_id,language_id) 
  );

revoke all on "informix".qxt_print_template from "public" as "informix";

{ TABLE "informix".qxt_print_image row size = 165 number of columns = 4 index size = 9 }

{ unload file name = qxt_p00127.unl number of rows = 2 }

create table "informix".qxt_print_image 
  (
    image_id serial not null ,
    filename varchar(100),
    mod_date date,
    image_data byte,
    primary key (image_id) 
  );

revoke all on "informix".qxt_print_image from "public" as "informix";

{ TABLE "informix".qxt_reserve row size = 4 number of columns = 1 index size = 0 }

{ unload file name = qxt_r00128.unl number of rows = 1 }

create table "informix".qxt_reserve 
  (
    id integer not null 
  );

revoke all on "informix".qxt_reserve from "public" as "informix";

{ TABLE "informix".qxt_reserve2 row size = 4 number of columns = 1 index size = 0 }

{ unload file name = qxt_r00129.unl number of rows = 1 }

create table "informix".qxt_reserve2 
  (
    id integer not null 
  );

revoke all on "informix".qxt_reserve2 from "public" as "informix";

{ TABLE "informix".cms_info row size = 33 number of columns = 3 index size = 0 }

{ unload file name = cms_i00130.unl number of rows = 1 }

create table "informix".cms_info 
  (
    db_version varchar(10),
    db_build varchar(10),
    db_other varchar(10)
  );

revoke all on "informix".cms_info from "public" as "informix";

{ TABLE "informix".menus row size = 35 number of columns = 2 index size = 9 }

{ unload file name = menus00131.unl number of rows = 7 }

create table "informix".menus 
  (
    menu_id integer not null ,
    menu_name varchar(30),
    primary key (menu_id) 
  );

revoke all on "informix".menus from "public" as "informix";

{ TABLE "informix".menu_options row size = 27 number of columns = 5 index size = 13 }

{ unload file name = menu_00132.unl number of rows = 15 }

create table "informix".menu_options 
  (
    option_id serial not null ,
    menu_id integer not null ,
    option_name_id integer,
    option_comment_id integer,
    option_keypress varchar(10),
    primary key (option_id,menu_id) 
  );

revoke all on "informix".menu_options from "public" as "informix";

{ TABLE "informix".country row size = 41 number of columns = 1 index size = 46 }

{ unload file name = count00133.unl number of rows = 250 }

create table "informix".country 
  (
    country varchar(40) not null ,
    primary key (country) 
  );

revoke all on "informix".country from "public" as "informix";

{ TABLE "informix".user_fields row size = 110 number of columns = 4 index size = 56 }

{ unload file name = user_00135.unl number of rows = 1 }

create table "informix".user_fields 
  (
    field_id serial not null ,
    field_name varchar(50) not null ,
    field_desc varchar(50),
    field_type integer,
    unique (field_name) 
  );

revoke all on "informix".user_fields from "public" as "informix";

{ TABLE "informix".user_field_data row size = 59 number of columns = 3 index size = 0 }

{ unload file name = user_00136.unl number of rows = 1 }

create table "informix".user_field_data 
  (
    field_id integer,
    contact_id integer,
    field_data varchar(50)
  );

revoke all on "informix".user_field_data from "public" as "informix";

{ TABLE "informix".industry_type row size = 32 number of columns = 3 index size = 9 }

{ unload file name = indus00137.unl number of rows = 16 }

create table "informix".industry_type 
  (
    industry_type_id serial not null ,
    itype_name varchar(25) not null ,
    user_def smallint,
    primary key (industry_type_id) 
  );

revoke all on "informix".industry_type from "public" as "informix";

{ TABLE "informix".company_type row size = 35 number of columns = 4 index size = 39 }

{ unload file name = compa00138.unl number of rows = 9 }

create table "informix".company_type 
  (
    company_type_id serial not null ,
    ctype_name char(25) not null ,
    user_def smallint,
    base_priority integer,
    unique (ctype_name) ,
    primary key (company_type_id) 
  );

revoke all on "informix".company_type from "public" as "informix";

{ TABLE "informix".contact_dept row size = 31 number of columns = 3 index size = 39 }

{ unload file name = conta00139.unl number of rows = 12 }

create table "informix".contact_dept 
  (
    dept_id serial not null ,
    dept_name char(25) not null ,
    user_def smallint,
    unique (dept_name) ,
    primary key (dept_id) 
  );

revoke all on "informix".contact_dept from "public" as "informix";

{ TABLE "informix".position_type row size = 31 number of columns = 3 index size = 39 }

{ unload file name = posit00140.unl number of rows = 13 }

create table "informix".position_type 
  (
    position_type_id serial not null ,
    ptype_name char(25) not null ,
    user_def smallint,
    unique (ptype_name) ,
    primary key (position_type_id) 
  );

revoke all on "informix".position_type from "public" as "informix";

{ TABLE "informix".activity_type row size = 21 number of columns = 3 index size = 29 }

{ unload file name = activ00141.unl number of rows = 4 }

create table "informix".activity_type 
  (
    activity_type_id serial not null ,
    atype_name char(15) not null ,
    user_def smallint,
    unique (atype_name) ,
    primary key (activity_type_id) 
  );

revoke all on "informix".activity_type from "public" as "informix";

{ TABLE "informix".tax_rate row size = 88 number of columns = 3 index size = 94 }

{ unload file name = tax_r00142.unl number of rows = 5 }

create table "informix".tax_rate 
  (
    rate_id serial not null ,
    tax_rate decimal(5,2) not null ,
    tax_desc char(80) not null ,
    unique (tax_desc) ,
    primary key (rate_id) 
  );

revoke all on "informix".tax_rate from "public" as "informix";

{ TABLE "informix".pay_method row size = 34 number of columns = 3 index size = 24 }

{ unload file name = pay_m00143.unl number of rows = 4 }

create table "informix".pay_method 
  (
    pay_method_id serial not null ,
    pay_method_name char(10) not null ,
    pay_method_desc char(20) not null ,
    unique (pay_method_name) ,
    primary key (pay_method_id) 
  );

revoke all on "informix".pay_method from "public" as "informix";

{ TABLE "informix".titles row size = 20 number of columns = 2 index size = 0 }

{ unload file name = title00144.unl number of rows = 4 }

create table "informix".titles 
  (
    lang_id integer,
    title_name varchar(15)
  );

revoke all on "informix".titles from "public" as "informix";

{ TABLE "informix".delivery_type row size = 28 number of columns = 3 index size = 25 }

{ unload file name = deliv00145.unl number of rows = 3 }

create table "informix".delivery_type 
  (
    delivery_type_id serial not null ,
    delivery_type_name char(20) not null ,
    delivery_rate money(6,2) not null ,
    unique (delivery_type_name) 
  );

revoke all on "informix".delivery_type from "public" as "informix";

{ TABLE "informix".currency row size = 18 number of columns = 3 index size = 15 }

{ unload file name = curre00146.unl number of rows = 4 }

create table "informix".currency 
  (
    currency_id serial not null ,
    currency_name char(10) not null ,
    xchg_rate decimal(5,3) not null ,
    unique (currency_name) 
  );

revoke all on "informix".currency from "public" as "informix";

{ TABLE "informix".stock_item row size = 103 number of columns = 5 index size = 109 }

{ unload file name = stock00147.unl number of rows = 29 }

create table "informix".stock_item 
  (
    stock_id char(10) not null ,
    item_desc char(80) not null ,
    item_cost money(8,2),
    rate_id integer not null ,
    quantity integer 
        default 0,
    unique (item_desc) ,
    primary key (stock_id) 
  );

revoke all on "informix".stock_item from "public" as "informix";

{ TABLE "informix".contact row size = 1522 number of columns = 25 index size = 91 }

{ unload file name = conta00149.unl number of rows = 50 }

create table "informix".contact 
  (
    cont_id serial not null ,
    cont_title varchar(10),
    cont_name varchar(20) not null ,
    cont_fname varchar(20),
    cont_lname varchar(20),
    cont_addr1 varchar(40),
    cont_addr2 varchar(40),
    cont_addr3 varchar(40),
    cont_city varchar(20),
    cont_zone varchar(15),
    cont_zip varchar(15),
    cont_country varchar(40),
    cont_phone varchar(15),
    cont_fax varchar(15),
    cont_mobile varchar(15),
    cont_email varchar(50) not null ,
    cont_dept varchar(15),
    cont_org integer,
    cont_position varchar(15),
    cont_picture byte,
    cont_password varchar(15),
    cont_ipaddr varchar(15),
    cont_usemail smallint,
    cont_usephone smallint,
    cont_notes char(1000),
    unique (cont_name) ,
    unique (cont_email) ,
    primary key (cont_id)  constraint "informix".contact_pk
  );

revoke all on "informix".contact from "public" as "informix";

{ TABLE "informix".company_link row size = 8 number of columns = 2 index size = 13 }

{ unload file name = compa00150.unl number of rows = 1 }

create table "informix".company_link 
  (
    parent_comp_id integer,
    child_comp_id integer,
    primary key (parent_comp_id,child_comp_id) 
  );

revoke all on "informix".company_link from "public" as "informix";

{ TABLE "informix".operator row size = 130 number of columns = 6 index size = 33 }

{ unload file name = opera00151.unl number of rows = 8 }

create table "informix".operator 
  (
    operator_id serial not null ,
    name char(10) not null ,
    password char(10),
    type char(1),
    cont_id integer,
    email_address varchar(100),
    unique (name) ,
    primary key (operator_id) 
  );

revoke all on "informix".operator from "public" as "informix";

{ TABLE "informix".operator_session row size = 281 number of columns = 10 index size = 0 }

{ unload file name = opera00152.unl number of rows = 52 }

create table "informix".operator_session 
  (
    session_id varchar(50),
    operator_id integer,
    session_counter integer,
    expired smallint,
    session_created datetime year to second,
    session_modified datetime year to second,
    client_host varchar(50),
    client_ip varchar(50),
    reserve1 varchar(50),
    reserve2 varchar(50)
  );

revoke all on "informix".operator_session from "public" as "informix";

{ TABLE "informix".activity row size = 172 number of columns = 11 index size = 45 }

{ unload file name = activ00153.unl number of rows = 126 }

create table "informix".activity 
  (
    activity_id serial not null ,
    open_date date not null ,
    close_date date,
    contact_id integer not null ,
    comp_id integer,
    operator_id integer not null ,
    act_type integer,
    long_desc text,
    short_desc char(80),
    a_owner integer,
    priority integer,
    primary key (activity_id)  constraint "informix".activity_pk
  );

revoke all on "informix".activity from "public" as "informix";

{ TABLE "informix".mailbox row size = 232 number of columns = 11 index size = 9 }

{ unload file name = mailb00154.unl number of rows = 111 }

create table "informix".mailbox 
  (
    mail_id serial not null ,
    activity_id integer not null ,
    operator_id integer not null ,
    from_email_address varchar(100),
    from_cont_id integer,
    to_email_address varchar(100),
    to_cont_id integer,
    recv_date date,
    read_flag smallint,
    urgent_flag smallint,
    mail_status smallint,
    unique (activity_id) 
  );

revoke all on "informix".mailbox from "public" as "informix";

{ TABLE "informix".invoice row size = 210 number of columns = 20 index size = 18 }

{ unload file name = invoi00155.unl number of rows = 19 }

create table "informix".invoice 
  (
    invoice_id serial not null ,
    account_id integer not null ,
    invoice_date date not null ,
    pay_date date,
    status integer not null ,
    tax_total money(8,2) not null ,
    net_total money(8,2) not null ,
    inv_total money(8,2) not null ,
    currency_id integer,
    for_total money(8,2),
    operator_id integer not null ,
    pay_method_id integer not null ,
    pay_method_name char(20),
    pay_method_desc char(20),
    del_address_dif integer not null ,
    del_address1 char(30),
    del_address2 char(30),
    del_address3 char(30),
    del_method integer,
    invoice_po char(20),
    primary key (invoice_id) 
  );

revoke all on "informix".invoice from "public" as "informix";

{ TABLE "informix".invoice_line row size = 22 number of columns = 4 index size = 0 }

{ unload file name = invoi00156.unl number of rows = 30 }

create table "informix".invoice_line 
  (
    invoice_id integer not null ,
    quantity integer,
    stock_id char(10) not null ,
    item_tax integer not null 
  );

revoke all on "informix".invoice_line from "public" as "informix";

{ TABLE "informix".account row size = 16 number of columns = 4 index size = 9 }

{ unload file name = accou00157.unl number of rows = 8 }

create table "informix".account 
  (
    account_id serial not null ,
    comp_id integer not null ,
    credit_limit money(8,2),
    discount decimal(4,2) not null ,
    unique (comp_id) 
  );

revoke all on "informix".account from "public" as "informix";

{ TABLE "informix".title row size = 10 number of columns = 1 index size = 15 }

{ unload file name = title00158.unl number of rows = 10 }

create table "informix".title 
  (
    title char(10) not null ,
    unique (title) 
  );

revoke all on "informix".title from "public" as "informix";

{ TABLE "informix".zone row size = 49 number of columns = 3 index size = 55 }

{ unload file name = zone_00159.unl number of rows = 94 }

create table "informix".zone 
  (
    zone_id serial not null ,
    zone_name varchar(40) not null ,
    country_id integer not null ,
    unique (zone_id) ,
    unique (zone_name) 
  );

revoke all on "informix".zone from "public" as "informix";

{ TABLE "informix".status_invoice row size = 29 number of columns = 2 index size = 0 }

{ unload file name = statu00160.unl number of rows = 5 }

create table "informix".status_invoice 
  (
    status_id serial not null ,
    status_name char(25) not null 
  );

revoke all on "informix".status_invoice from "public" as "informix";

{ TABLE "informix".state_supply row size = 29 number of columns = 2 index size = 0 }

{ unload file name = state00161.unl number of rows = 3 }

create table "informix".state_supply 
  (
    state_id serial not null ,
    state_name char(25) not null 
  );

revoke all on "informix".state_supply from "public" as "informix";

{ TABLE "informix".supplies row size = 24 number of columns = 6 index size = 0 }

{ unload file name = suppl00162.unl number of rows = 6 }

create table "informix".supplies 
  (
    suppl_id serial not null ,
    operator_id integer not null ,
    suppl_date date,
    state integer not null ,
    account_id integer not null ,
    exp_date date
  );

revoke all on "informix".supplies from "public" as "informix";

{ TABLE "informix".supplies_line row size = 18 number of columns = 3 index size = 0 }

{ unload file name = suppl00163.unl number of rows = 21 }

create table "informix".supplies_line 
  (
    suppl_id integer not null ,
    stock_id char(10) not null ,
    quantity integer
  );

revoke all on "informix".supplies_line from "public" as "informix";

{ TABLE "informix".activity_birt row size = 176 number of columns = 12 index size = 0 }

{ unload file name = activ00164.unl number of rows = 0 }

create table "informix".activity_birt 
  (
    sesion_id integer,
    activity_id integer,
    open_date date not null ,
    close_date date,
    contact_id integer not null ,
    comp_id integer,
    operator_id integer not null ,
    act_type integer,
    long_desc text,
    short_desc char(80),
    a_owner integer,
    priority integer
  );

revoke all on "informix".activity_birt from "public" as "informix";

{ TABLE "informix".country2 row size = 119 number of columns = 10 index size = 8 }

{ unload file name = count00165.unl number of rows = 248 }

create table "informix".country2 
  (
    country_code nchar(3),
    country_text nvarchar(60) not null ,
    language_code nchar(3),
    post_code_text nvarchar(20),
    post_code_min_num smallint,
    post_code_max_num smallint,
    state_code_text nvarchar(20),
    state_code_min_num smallint,
    state_code_max_num smallint,
    bank_acc_format smallint,
    primary key (country_code) 
  );

revoke all on "informix".country2 from "public" as "informix";

{ TABLE "informix".language row size = 167 number of columns = 5 index size = 8 }

{ unload file name = langu00166.unl number of rows = 183 }

create table "informix".language 
  (
    language_code nchar(3),
    language_text nvarchar(80) not null ,
    yes_flag nchar(1),
    no_flag nchar(1),
    national_text nvarchar(80),
    primary key (language_code) 
  );

revoke all on "informix".language from "public" as "informix";

{ TABLE "informix".res_cms_68 row size = 4 number of columns = 1 index size = 0 }

{ unload file name = res_c00167.unl number of rows = 5 }

create table "informix".res_cms_68 
  (
    res_id integer
  );

revoke all on "informix".res_cms_68 from "public" as "informix";

{ TABLE "informix".res_cms_69 row size = 4 number of columns = 1 index size = 0 }

{ unload file name = res_c00168.unl number of rows = 5 }

create table "informix".res_cms_69 
  (
    res_id integer
  );

revoke all on "informix".res_cms_69 from "public" as "informix";

{ TABLE "informix".res_cms_70 row size = 4 number of columns = 1 index size = 0 }

{ unload file name = res_c00169.unl number of rows = 5 }

create table "informix".res_cms_70 
  (
    res_id integer
  );

revoke all on "informix".res_cms_70 from "public" as "informix";

{ TABLE "informix".test01 row size = 77 number of columns = 8 index size = 26 }

{ unload file name = test000170.unl number of rows = 5 }

create table "informix".test01 
  (
    f1_varchar varchar(20),
    f3_smallint smallint,
    f4_varchar varchar(20),
    f5_date date,
    f6_varchar varchar(20),
    f7_smallint smallint,
    f8_smallint smallint,
    f9_dt_h_t_s datetime hour to second,
    primary key (f1_varchar)  constraint "informix".pk_f1_varchar
  );

revoke all on "informix".test01 from "public" as "informix";

{ TABLE "informix".test02 row size = 391 number of columns = 6 index size = 26 }

{ unload file name = test000171.unl number of rows = 5 }

create table "informix".test02 
  (
    ftextfield varchar(20),
    flistbox varchar(20),
    fcombo varchar(20),
    fradiobuttonlistv varchar(20),
    ftextarea varchar(250),
    fblobtext text,
    primary key (ftextfield)  constraint "informix".pk_ftextfield
  );

revoke all on "informix".test02 from "public" as "informix";

{ TABLE "informix".test03 row size = 40 number of columns = 7 index size = 9 }

{ unload file name = test000172.unl number of rows = 1 }

create table "informix".test03 
  (
    math_operation_id serial not null ,
    operator_val_1 decimal(10,2) not null ,
    operator_val_2 decimal(10,2) not null ,
    result_sum decimal(10,2) not null ,
    result_difference decimal(10,2) not null ,
    result_product decimal(10,2) not null ,
    result_quotient decimal(10,2) not null ,
    primary key (math_operation_id)  constraint "informix".pk_math_operation_id
  );

revoke all on "informix".test03 from "public" as "informix";

{ TABLE "informix".test04 row size = 4 number of columns = 1 index size = 0 }

{ unload file name = test000173.unl number of rows = 5 }

create table "informix".test04 
  (
    res_id integer
  );

revoke all on "informix".test04 from "public" as "informix";

{ TABLE "informix".test05 row size = 4 number of columns = 1 index size = 0 }

{ unload file name = test000174.unl number of rows = 5 }

create table "informix".test05 
  (
    res_id integer
  );

revoke all on "informix".test05 from "public" as "informix";

{ TABLE "informix".state row size = 205 number of columns = 6 index size = 53 }

{ unload file name = state00175.unl number of rows = 454 }

create table "informix".state 
  (
    country varchar(40) not null ,
    country_code varchar(3) not null ,
    state_code varchar(6) not null ,
    state_code_iso3661 varchar(10),
    state_text varchar(60),
    state_text_enu varchar(80),
    primary key (country,state_code) 
  );

revoke all on "informix".state from "public" as "informix";

{ TABLE "informix".company row size = 1389 number of columns = 17 index size = 132 }

{ unload file name = compa00176.unl number of rows = 29 }

create table "informix".company 
  (
    comp_id serial not null ,
    comp_name char(100) not null ,
    comp_addr1 char(40),
    comp_addr2 char(40),
    comp_addr3 char(40),
    comp_city char(20),
    comp_zone char(15),
    comp_zip char(15),
    comp_country char(40),
    acct_mgr integer,
    comp_link integer,
    comp_industry integer,
    comp_priority integer,
    comp_type integer,
    comp_main_cont integer not null ,
    comp_url varchar(50),
    comp_notes char(1000),
    unique (comp_name) ,
    primary key (comp_id)  constraint "informix".comp_id_pk
  );

revoke all on "informix".company from "public" as "informix";




grant select on "informix".qxt_application to "public" as "informix";
grant update on "informix".qxt_application to "public" as "informix";
grant insert on "informix".qxt_application to "public" as "informix";
grant delete on "informix".qxt_application to "public" as "informix";
grant index on "informix".qxt_application to "public" as "informix";
grant select on "informix".qxt_language to "public" as "informix";
grant update on "informix".qxt_language to "public" as "informix";
grant insert on "informix".qxt_language to "public" as "informix";
grant delete on "informix".qxt_language to "public" as "informix";
grant index on "informix".qxt_language to "public" as "informix";
grant select on "informix".qxt_str_category to "public" as "informix";
grant update on "informix".qxt_str_category to "public" as "informix";
grant insert on "informix".qxt_str_category to "public" as "informix";
grant delete on "informix".qxt_str_category to "public" as "informix";
grant index on "informix".qxt_str_category to "public" as "informix";
grant select on "informix".qxt_string_tool to "public" as "informix";
grant update on "informix".qxt_string_tool to "public" as "informix";
grant insert on "informix".qxt_string_tool to "public" as "informix";
grant delete on "informix".qxt_string_tool to "public" as "informix";
grant index on "informix".qxt_string_tool to "public" as "informix";
grant select on "informix".qxt_string_app to "public" as "informix";
grant update on "informix".qxt_string_app to "public" as "informix";
grant insert on "informix".qxt_string_app to "public" as "informix";
grant delete on "informix".qxt_string_app to "public" as "informix";
grant index on "informix".qxt_string_app to "public" as "informix";
grant select on "informix".qxt_help_classic to "public" as "informix";
grant update on "informix".qxt_help_classic to "public" as "informix";
grant insert on "informix".qxt_help_classic to "public" as "informix";
grant delete on "informix".qxt_help_classic to "public" as "informix";
grant index on "informix".qxt_help_classic to "public" as "informix";
grant select on "informix".qxt_help_url_map to "public" as "informix";
grant update on "informix".qxt_help_url_map to "public" as "informix";
grant insert on "informix".qxt_help_url_map to "public" as "informix";
grant delete on "informix".qxt_help_url_map to "public" as "informix";
grant index on "informix".qxt_help_url_map to "public" as "informix";
grant select on "informix".qxt_help_html_doc to "public" as "informix";
grant update on "informix".qxt_help_html_doc to "public" as "informix";
grant insert on "informix".qxt_help_html_doc to "public" as "informix";
grant delete on "informix".qxt_help_html_doc to "public" as "informix";
grant index on "informix".qxt_help_html_doc to "public" as "informix";
grant select on "informix".qxt_toolbar to "public" as "informix";
grant update on "informix".qxt_toolbar to "public" as "informix";
grant insert on "informix".qxt_toolbar to "public" as "informix";
grant delete on "informix".qxt_toolbar to "public" as "informix";
grant index on "informix".qxt_toolbar to "public" as "informix";
grant select on "informix".qxt_icon_size to "public" as "informix";
grant update on "informix".qxt_icon_size to "public" as "informix";
grant insert on "informix".qxt_icon_size to "public" as "informix";
grant delete on "informix".qxt_icon_size to "public" as "informix";
grant index on "informix".qxt_icon_size to "public" as "informix";
grant select on "informix".qxt_icon_path to "public" as "informix";
grant update on "informix".qxt_icon_path to "public" as "informix";
grant insert on "informix".qxt_icon_path to "public" as "informix";
grant delete on "informix".qxt_icon_path to "public" as "informix";
grant index on "informix".qxt_icon_path to "public" as "informix";
grant select on "informix".qxt_icon_property to "public" as "informix";
grant update on "informix".qxt_icon_property to "public" as "informix";
grant insert on "informix".qxt_icon_property to "public" as "informix";
grant delete on "informix".qxt_icon_property to "public" as "informix";
grant index on "informix".qxt_icon_property to "public" as "informix";
grant select on "informix".qxt_icon_category to "public" as "informix";
grant update on "informix".qxt_icon_category to "public" as "informix";
grant insert on "informix".qxt_icon_category to "public" as "informix";
grant delete on "informix".qxt_icon_category to "public" as "informix";
grant index on "informix".qxt_icon_category to "public" as "informix";
grant select on "informix".qxt_icon to "public" as "informix";
grant update on "informix".qxt_icon to "public" as "informix";
grant insert on "informix".qxt_icon to "public" as "informix";
grant delete on "informix".qxt_icon to "public" as "informix";
grant index on "informix".qxt_icon to "public" as "informix";
grant select on "informix".qxt_tbi_tooltip to "public" as "informix";
grant update on "informix".qxt_tbi_tooltip to "public" as "informix";
grant insert on "informix".qxt_tbi_tooltip to "public" as "informix";
grant delete on "informix".qxt_tbi_tooltip to "public" as "informix";
grant index on "informix".qxt_tbi_tooltip to "public" as "informix";
grant select on "informix".qxt_tbi_event_type to "public" as "informix";
grant update on "informix".qxt_tbi_event_type to "public" as "informix";
grant insert on "informix".qxt_tbi_event_type to "public" as "informix";
grant delete on "informix".qxt_tbi_event_type to "public" as "informix";
grant index on "informix".qxt_tbi_event_type to "public" as "informix";
grant select on "informix".qxt_tbi_obj_event to "public" as "informix";
grant update on "informix".qxt_tbi_obj_event to "public" as "informix";
grant insert on "informix".qxt_tbi_obj_event to "public" as "informix";
grant delete on "informix".qxt_tbi_obj_event to "public" as "informix";
grant index on "informix".qxt_tbi_obj_event to "public" as "informix";
grant select on "informix".qxt_tbi_obj_action to "public" as "informix";
grant update on "informix".qxt_tbi_obj_action to "public" as "informix";
grant insert on "informix".qxt_tbi_obj_action to "public" as "informix";
grant delete on "informix".qxt_tbi_obj_action to "public" as "informix";
grant index on "informix".qxt_tbi_obj_action to "public" as "informix";
grant select on "informix".qxt_tbi_obj to "public" as "informix";
grant update on "informix".qxt_tbi_obj to "public" as "informix";
grant insert on "informix".qxt_tbi_obj to "public" as "informix";
grant delete on "informix".qxt_tbi_obj to "public" as "informix";
grant index on "informix".qxt_tbi_obj to "public" as "informix";
grant select on "informix".qxt_tbi_scope to "public" as "informix";
grant update on "informix".qxt_tbi_scope to "public" as "informix";
grant insert on "informix".qxt_tbi_scope to "public" as "informix";
grant delete on "informix".qxt_tbi_scope to "public" as "informix";
grant index on "informix".qxt_tbi_scope to "public" as "informix";
grant select on "informix".qxt_tbi_static to "public" as "informix";
grant update on "informix".qxt_tbi_static to "public" as "informix";
grant insert on "informix".qxt_tbi_static to "public" as "informix";
grant delete on "informix".qxt_tbi_static to "public" as "informix";
grant index on "informix".qxt_tbi_static to "public" as "informix";
grant select on "informix".qxt_tbi to "public" as "informix";
grant update on "informix".qxt_tbi to "public" as "informix";
grant insert on "informix".qxt_tbi to "public" as "informix";
grant delete on "informix".qxt_tbi to "public" as "informix";
grant index on "informix".qxt_tbi to "public" as "informix";
grant select on "informix".qxt_tb to "public" as "informix";
grant update on "informix".qxt_tb to "public" as "informix";
grant insert on "informix".qxt_tb to "public" as "informix";
grant delete on "informix".qxt_tb to "public" as "informix";
grant index on "informix".qxt_tb to "public" as "informix";
grant select on "informix".qxt_document to "public" as "informix";
grant update on "informix".qxt_document to "public" as "informix";
grant insert on "informix".qxt_document to "public" as "informix";
grant delete on "informix".qxt_document to "public" as "informix";
grant index on "informix".qxt_document to "public" as "informix";
grant select on "informix".qxt_document_blob to "public" as "informix";
grant update on "informix".qxt_document_blob to "public" as "informix";
grant insert on "informix".qxt_document_blob to "public" as "informix";
grant delete on "informix".qxt_document_blob to "public" as "informix";
grant index on "informix".qxt_document_blob to "public" as "informix";
grant select on "informix".qxt_dde_font to "public" as "informix";
grant update on "informix".qxt_dde_font to "public" as "informix";
grant insert on "informix".qxt_dde_font to "public" as "informix";
grant delete on "informix".qxt_dde_font to "public" as "informix";
grant index on "informix".qxt_dde_font to "public" as "informix";
grant select on "informix".qxt_print_template to "public" as "informix";
grant update on "informix".qxt_print_template to "public" as "informix";
grant insert on "informix".qxt_print_template to "public" as "informix";
grant delete on "informix".qxt_print_template to "public" as "informix";
grant index on "informix".qxt_print_template to "public" as "informix";
grant select on "informix".qxt_print_image to "public" as "informix";
grant update on "informix".qxt_print_image to "public" as "informix";
grant insert on "informix".qxt_print_image to "public" as "informix";
grant delete on "informix".qxt_print_image to "public" as "informix";
grant index on "informix".qxt_print_image to "public" as "informix";
grant select on "informix".qxt_reserve to "public" as "informix";
grant update on "informix".qxt_reserve to "public" as "informix";
grant insert on "informix".qxt_reserve to "public" as "informix";
grant delete on "informix".qxt_reserve to "public" as "informix";
grant index on "informix".qxt_reserve to "public" as "informix";
grant select on "informix".qxt_reserve2 to "public" as "informix";
grant update on "informix".qxt_reserve2 to "public" as "informix";
grant insert on "informix".qxt_reserve2 to "public" as "informix";
grant delete on "informix".qxt_reserve2 to "public" as "informix";
grant index on "informix".qxt_reserve2 to "public" as "informix";
grant select on "informix".cms_info to "public" as "informix";
grant update on "informix".cms_info to "public" as "informix";
grant insert on "informix".cms_info to "public" as "informix";
grant delete on "informix".cms_info to "public" as "informix";
grant index on "informix".cms_info to "public" as "informix";
grant select on "informix".menus to "public" as "informix";
grant update on "informix".menus to "public" as "informix";
grant insert on "informix".menus to "public" as "informix";
grant delete on "informix".menus to "public" as "informix";
grant index on "informix".menus to "public" as "informix";
grant select on "informix".menu_options to "public" as "informix";
grant update on "informix".menu_options to "public" as "informix";
grant insert on "informix".menu_options to "public" as "informix";
grant delete on "informix".menu_options to "public" as "informix";
grant index on "informix".menu_options to "public" as "informix";
grant select on "informix".country to "public" as "informix";
grant update on "informix".country to "public" as "informix";
grant insert on "informix".country to "public" as "informix";
grant delete on "informix".country to "public" as "informix";
grant index on "informix".country to "public" as "informix";
grant select on "informix".user_fields to "public" as "informix";
grant update on "informix".user_fields to "public" as "informix";
grant insert on "informix".user_fields to "public" as "informix";
grant delete on "informix".user_fields to "public" as "informix";
grant index on "informix".user_fields to "public" as "informix";
grant select on "informix".user_field_data to "public" as "informix";
grant update on "informix".user_field_data to "public" as "informix";
grant insert on "informix".user_field_data to "public" as "informix";
grant delete on "informix".user_field_data to "public" as "informix";
grant index on "informix".user_field_data to "public" as "informix";
grant select on "informix".industry_type to "public" as "informix";
grant update on "informix".industry_type to "public" as "informix";
grant insert on "informix".industry_type to "public" as "informix";
grant delete on "informix".industry_type to "public" as "informix";
grant index on "informix".industry_type to "public" as "informix";
grant select on "informix".company_type to "public" as "informix";
grant update on "informix".company_type to "public" as "informix";
grant insert on "informix".company_type to "public" as "informix";
grant delete on "informix".company_type to "public" as "informix";
grant index on "informix".company_type to "public" as "informix";
grant select on "informix".contact_dept to "public" as "informix";
grant update on "informix".contact_dept to "public" as "informix";
grant insert on "informix".contact_dept to "public" as "informix";
grant delete on "informix".contact_dept to "public" as "informix";
grant index on "informix".contact_dept to "public" as "informix";
grant select on "informix".position_type to "public" as "informix";
grant update on "informix".position_type to "public" as "informix";
grant insert on "informix".position_type to "public" as "informix";
grant delete on "informix".position_type to "public" as "informix";
grant index on "informix".position_type to "public" as "informix";
grant select on "informix".activity_type to "public" as "informix";
grant update on "informix".activity_type to "public" as "informix";
grant insert on "informix".activity_type to "public" as "informix";
grant delete on "informix".activity_type to "public" as "informix";
grant index on "informix".activity_type to "public" as "informix";
grant select on "informix".tax_rate to "public" as "informix";
grant update on "informix".tax_rate to "public" as "informix";
grant insert on "informix".tax_rate to "public" as "informix";
grant delete on "informix".tax_rate to "public" as "informix";
grant index on "informix".tax_rate to "public" as "informix";
grant select on "informix".pay_method to "public" as "informix";
grant update on "informix".pay_method to "public" as "informix";
grant insert on "informix".pay_method to "public" as "informix";
grant delete on "informix".pay_method to "public" as "informix";
grant index on "informix".pay_method to "public" as "informix";
grant select on "informix".titles to "public" as "informix";
grant update on "informix".titles to "public" as "informix";
grant insert on "informix".titles to "public" as "informix";
grant delete on "informix".titles to "public" as "informix";
grant index on "informix".titles to "public" as "informix";
grant select on "informix".delivery_type to "public" as "informix";
grant update on "informix".delivery_type to "public" as "informix";
grant insert on "informix".delivery_type to "public" as "informix";
grant delete on "informix".delivery_type to "public" as "informix";
grant index on "informix".delivery_type to "public" as "informix";
grant select on "informix".currency to "public" as "informix";
grant update on "informix".currency to "public" as "informix";
grant insert on "informix".currency to "public" as "informix";
grant delete on "informix".currency to "public" as "informix";
grant index on "informix".currency to "public" as "informix";
grant select on "informix".stock_item to "public" as "informix";
grant update on "informix".stock_item to "public" as "informix";
grant insert on "informix".stock_item to "public" as "informix";
grant delete on "informix".stock_item to "public" as "informix";
grant index on "informix".stock_item to "public" as "informix";
grant select on "informix".contact to "public" as "informix";
grant update on "informix".contact to "public" as "informix";
grant insert on "informix".contact to "public" as "informix";
grant delete on "informix".contact to "public" as "informix";
grant index on "informix".contact to "public" as "informix";
grant select on "informix".company_link to "public" as "informix";
grant update on "informix".company_link to "public" as "informix";
grant insert on "informix".company_link to "public" as "informix";
grant delete on "informix".company_link to "public" as "informix";
grant index on "informix".company_link to "public" as "informix";
grant select on "informix".operator to "public" as "informix";
grant update on "informix".operator to "public" as "informix";
grant insert on "informix".operator to "public" as "informix";
grant delete on "informix".operator to "public" as "informix";
grant index on "informix".operator to "public" as "informix";
grant select on "informix".operator_session to "public" as "informix";
grant update on "informix".operator_session to "public" as "informix";
grant insert on "informix".operator_session to "public" as "informix";
grant delete on "informix".operator_session to "public" as "informix";
grant index on "informix".operator_session to "public" as "informix";
grant select on "informix".activity to "public" as "informix";
grant update on "informix".activity to "public" as "informix";
grant insert on "informix".activity to "public" as "informix";
grant delete on "informix".activity to "public" as "informix";
grant index on "informix".activity to "public" as "informix";
grant select on "informix".mailbox to "public" as "informix";
grant update on "informix".mailbox to "public" as "informix";
grant insert on "informix".mailbox to "public" as "informix";
grant delete on "informix".mailbox to "public" as "informix";
grant index on "informix".mailbox to "public" as "informix";
grant select on "informix".invoice to "public" as "informix";
grant update on "informix".invoice to "public" as "informix";
grant insert on "informix".invoice to "public" as "informix";
grant delete on "informix".invoice to "public" as "informix";
grant index on "informix".invoice to "public" as "informix";
grant select on "informix".invoice_line to "public" as "informix";
grant update on "informix".invoice_line to "public" as "informix";
grant insert on "informix".invoice_line to "public" as "informix";
grant delete on "informix".invoice_line to "public" as "informix";
grant index on "informix".invoice_line to "public" as "informix";
grant select on "informix".account to "public" as "informix";
grant update on "informix".account to "public" as "informix";
grant insert on "informix".account to "public" as "informix";
grant delete on "informix".account to "public" as "informix";
grant index on "informix".account to "public" as "informix";
grant select on "informix".title to "public" as "informix";
grant update on "informix".title to "public" as "informix";
grant insert on "informix".title to "public" as "informix";
grant delete on "informix".title to "public" as "informix";
grant index on "informix".title to "public" as "informix";
grant select on "informix".zone to "public" as "informix";
grant update on "informix".zone to "public" as "informix";
grant insert on "informix".zone to "public" as "informix";
grant delete on "informix".zone to "public" as "informix";
grant index on "informix".zone to "public" as "informix";
grant select on "informix".status_invoice to "public" as "informix";
grant update on "informix".status_invoice to "public" as "informix";
grant insert on "informix".status_invoice to "public" as "informix";
grant delete on "informix".status_invoice to "public" as "informix";
grant index on "informix".status_invoice to "public" as "informix";
grant select on "informix".state_supply to "public" as "informix";
grant update on "informix".state_supply to "public" as "informix";
grant insert on "informix".state_supply to "public" as "informix";
grant delete on "informix".state_supply to "public" as "informix";
grant index on "informix".state_supply to "public" as "informix";
grant select on "informix".supplies to "public" as "informix";
grant update on "informix".supplies to "public" as "informix";
grant insert on "informix".supplies to "public" as "informix";
grant delete on "informix".supplies to "public" as "informix";
grant index on "informix".supplies to "public" as "informix";
grant select on "informix".supplies_line to "public" as "informix";
grant update on "informix".supplies_line to "public" as "informix";
grant insert on "informix".supplies_line to "public" as "informix";
grant delete on "informix".supplies_line to "public" as "informix";
grant index on "informix".supplies_line to "public" as "informix";
grant select on "informix".activity_birt to "public" as "informix";
grant update on "informix".activity_birt to "public" as "informix";
grant insert on "informix".activity_birt to "public" as "informix";
grant delete on "informix".activity_birt to "public" as "informix";
grant index on "informix".activity_birt to "public" as "informix";
grant select on "informix".country2 to "public" as "informix";
grant update on "informix".country2 to "public" as "informix";
grant insert on "informix".country2 to "public" as "informix";
grant delete on "informix".country2 to "public" as "informix";
grant index on "informix".country2 to "public" as "informix";
grant select on "informix".language to "public" as "informix";
grant update on "informix".language to "public" as "informix";
grant insert on "informix".language to "public" as "informix";
grant delete on "informix".language to "public" as "informix";
grant index on "informix".language to "public" as "informix";
grant select on "informix".res_cms_68 to "public" as "informix";
grant update on "informix".res_cms_68 to "public" as "informix";
grant insert on "informix".res_cms_68 to "public" as "informix";
grant delete on "informix".res_cms_68 to "public" as "informix";
grant index on "informix".res_cms_68 to "public" as "informix";
grant select on "informix".res_cms_69 to "public" as "informix";
grant update on "informix".res_cms_69 to "public" as "informix";
grant insert on "informix".res_cms_69 to "public" as "informix";
grant delete on "informix".res_cms_69 to "public" as "informix";
grant index on "informix".res_cms_69 to "public" as "informix";
grant select on "informix".res_cms_70 to "public" as "informix";
grant update on "informix".res_cms_70 to "public" as "informix";
grant insert on "informix".res_cms_70 to "public" as "informix";
grant delete on "informix".res_cms_70 to "public" as "informix";
grant index on "informix".res_cms_70 to "public" as "informix";
grant select on "informix".test01 to "public" as "informix";
grant update on "informix".test01 to "public" as "informix";
grant insert on "informix".test01 to "public" as "informix";
grant delete on "informix".test01 to "public" as "informix";
grant index on "informix".test01 to "public" as "informix";
grant select on "informix".test02 to "public" as "informix";
grant update on "informix".test02 to "public" as "informix";
grant insert on "informix".test02 to "public" as "informix";
grant delete on "informix".test02 to "public" as "informix";
grant index on "informix".test02 to "public" as "informix";
grant select on "informix".test03 to "public" as "informix";
grant update on "informix".test03 to "public" as "informix";
grant insert on "informix".test03 to "public" as "informix";
grant delete on "informix".test03 to "public" as "informix";
grant index on "informix".test03 to "public" as "informix";
grant select on "informix".test04 to "public" as "informix";
grant update on "informix".test04 to "public" as "informix";
grant insert on "informix".test04 to "public" as "informix";
grant delete on "informix".test04 to "public" as "informix";
grant index on "informix".test04 to "public" as "informix";
grant select on "informix".test05 to "public" as "informix";
grant update on "informix".test05 to "public" as "informix";
grant insert on "informix".test05 to "public" as "informix";
grant delete on "informix".test05 to "public" as "informix";
grant index on "informix".test05 to "public" as "informix";
grant select on "informix".state to "public" as "informix";
grant update on "informix".state to "public" as "informix";
grant insert on "informix".state to "public" as "informix";
grant delete on "informix".state to "public" as "informix";
grant index on "informix".state to "public" as "informix";
grant select on "informix".company to "public" as "informix";
grant update on "informix".company to "public" as "informix";
grant insert on "informix".company to "public" as "informix";
grant delete on "informix".company to "public" as "informix";
grant index on "informix".company to "public" as "informix";
















revoke usage on language SPL from public ;

grant usage on language SPL to public ;








alter table "informix".qxt_str_category add constraint (foreign 
    key (language_id) references "informix".qxt_language );
alter table "informix".qxt_string_tool add constraint (foreign 
    key (language_id) references "informix".qxt_language );
alter table "informix".qxt_string_app add constraint (foreign 
    key (language_id) references "informix".qxt_language );
alter table "informix".qxt_help_html_doc add constraint (foreign 
    key (language_id) references "informix".qxt_language );
alter table "informix".qxt_icon_property add constraint (foreign 
    key (icon_size_name) references "informix".qxt_icon_size );
    
alter table "informix".qxt_icon_property add constraint (foreign 
    key (icon_path_name) references "informix".qxt_icon_path );
    
alter table "informix".qxt_icon_category add constraint (foreign 
    key (language_id) references "informix".qxt_language );
alter table "informix".qxt_tbi_tooltip add constraint (foreign 
    key (language_id) references "informix".qxt_language );
alter table "informix".qxt_tbi_obj add constraint (foreign key 
    (event_type_id) references "informix".qxt_tbi_event_type );
    
alter table "informix".qxt_tbi_obj add constraint (foreign key 
    (icon_filename) references "informix".qxt_icon );
alter table "informix".qxt_tbi_obj add constraint (foreign key 
    (tbi_obj_action_id) references "informix".qxt_tbi_obj_action 
    );
alter table "informix".qxt_tbi add constraint (foreign key (event_type_id) 
    references "informix".qxt_tbi_event_type );
alter table "informix".qxt_tbi add constraint (foreign key (application_id) 
    references "informix".qxt_application );
alter table "informix".qxt_tbi add constraint (foreign key (tbi_obj_name) 
    references "informix".qxt_tbi_obj );
alter table "informix".qxt_tbi add constraint (foreign key (tbi_scope_id) 
    references "informix".qxt_tbi_scope );
alter table "informix".qxt_tbi add constraint (foreign key (tbi_static_id) 
    references "informix".qxt_tbi_static );
alter table "informix".qxt_tb add constraint (foreign key (application_id) 
    references "informix".qxt_application );
alter table "informix".qxt_print_template add constraint (foreign 
    key (language_id) references "informix".qxt_language  on delete 
    cascade);
alter table "informix".stock_item add constraint (foreign key 
    (rate_id) references "informix".tax_rate );
alter table "informix".company add constraint (foreign key (comp_industry) 
    references "informix".industry_type  constraint "informix".industry_type_id_fk_industry_type);
    
alter table "informix".company add constraint (foreign key (comp_type) 
    references "informix".company_type  constraint "informix".comp_type_fk_company_type);
    
alter table "informix".operator add constraint (foreign key (cont_id) 
    references "informix".contact );
alter table "informix".activity add constraint (foreign key (contact_id) 
    references "informix".contact  constraint "informix".contact_id_fk_contact);
    
alter table "informix".activity add constraint (foreign key (operator_id) 
    references "informix".operator  constraint "informix".operator_id_fk_operator);
    
alter table "informix".activity add constraint (foreign key (act_type) 
    references "informix".activity_type  constraint "informix".act_type_fk_activity_type);
    
alter table "informix".activity add constraint (foreign key (a_owner) 
    references "informix".contact  constraint "informix".contact_id_fk_a_owner);
    
alter table "informix".invoice add constraint (foreign key (pay_method_id) 
    references "informix".pay_method );







 



