{
* #%L
* QUERIX
* %%
* Copyright (C) 2015 QUERIX
* %%
* ALL RIGTHS RESERVED.
* 50 THE AVENUE
* SOUTHAMPTON SO17 1XQ
* UNITED KINGDOM
* Tel ; +(44)02380 385 180
* Fax : +(44)02380 635 118
* http://www.querix.com/
* #L%
}


###############################################################################################################################
# Public functions
###############################################################################################################################
#
# PUBLIC FUNCTION InteractFormFile(formFile STRING)
# PUBLIC FUNCTION InteractFormFileWithSettings(form_file STRING, setting_id STRING)
# PUBLIC FUNCTION InteractForm(settings InteractForm_Settings)
#
###############################################################################################################################


###############################################################################################################################
# Public methods of InteractForm object
###############################################################################################################################
#
# PUBLIC FUNCTION (this InteractForm) Interact() RETURNS BOOL
# PUBLIC FUNCTION (this InteractForm) InteractCurrentForm(settings InteractForm_Settings) RETURNS BOOL
# PUBLIC FUNCTION (this InteractForm) Refresh(withQuestion BOOL) RETURNS BOOL
# PUBLIC FUNCTION (this InteractForm) InitPopulatingComboBox(field STRING)
# PUBLIC FUNCTION (this InteractForm) PopulateComboBoxWhere(field STRING, where STRING)
# PUBLIC FUNCTION (this InteractForm) PopulateComboBox(field STRING, scr_line INT, filter BOOL, control_role INT)
# PUBLIC FUNCTION (this InteractForm) SetFieldValue(field STRING, val VARIANT)
# PUBLIC FUNCTION (this InteractForm) GetFieldValue(field STRING) RETURNS VARIANT
# PUBLIC FUNCTION (this InteractForm) LSTRS(str STRING) RETURNS STRING
# PUBLIC FUNCTION (this InteractForm) LSTRT(str STRING) RETURNS STRING
# PUBLIC FUNCTION (this InteractForm) LSTRC(str STRING) RETURNS STRING
# PUBLIC FUNCTION (this InteractForm) ClearNavigationStatus()
#
###############################################################################################################################

GLOBALS

CONSTANT TOP_DIALOG_ID = ""
CONSTANT SUB_DIALOG_UPDATE_ID = "UPDATE"
CONSTANT SUB_DIALOG_INSERT_ID = "INSERT"
CONSTANT SUB_DIALOG_DELETE_ID = "DELETE"
CONSTANT SUB_DIALOG_QUERY_ID  = "QUERY"


###############################################################################################################################
# TYPE ComboboxDef RECORD                        #
###############################################################################################################################
TYPE ComboboxDef RECORD                          #
      field_bare       STRING                    # Field name without table prefix
    , sql_where        STRING                    # Initial where clause for selecting values
    , foreign_key      STRING                    # Foreign key
    , combobox         ui.Combobox               # All Combobox widgets itself
    , combobox_edit    ui.Combobox               # Edit Combobox widget itself
    , combobox_control ui.Combobox               # Common Combobox widget itself
    , combobox_constr  ui.Combobox               # Construct Combobox widget for QUERY action
    , query            STRING                    # Query for selecting combobox values
    , depends_on       DYNAMIC ARRAY OF STRING   # List of fields on which values ​​depend
    , splintered       DYNAMIC ARRAY OF ui.DistributedObject # Splintered list of Combobox widgets in screen array on the form
END RECORD                                       #
###############################################################################################################################


###############################################################################################################################
# TYPE View_Internal RECORD                      #
###############################################################################################################################
TYPE View_Internal RECORD                        #
      view_table      STRING                     # The table name for query. It's mandatory.
    , table_select    STRING                     # The table name with an alias in format '<table_db> <table_alias>'
    , table_db        STRING                     # The Database table name (not the alias)
    , fields_scroll   DYNAMIC ARRAY OF STRING    # List of simple (not BYTE or TEXT) table's fields
    , fields_scroll_s STRING                     # List of simple (not BYTE or TEXT) table's fields separated by comma for selecting in scrollable cursor
    , fields_blobs    DYNAMIC ARRAY OF STRING    # List of BLOB (BYTE or TEXT) table's fields
    , fields_blobs_s  STRING                     # List of BLOB (BYTE or TEXT) table's fields separated by comma for selecting in NOT scrollable cursor
    , fields_form     DYNAMIC ARRAY OF RECORD fname, ftype STRING END RECORD # List of form fields controlled by the dialog
    , fields_find     DYNAMIC ARRAY OF RECORD fname, ftype STRING END RECORD # List of columns which should be used on FIND/QUERY/CONSTRUCT action
    , fields_where    DYNAMIC ARRAY OF STRING    # List of columns in WHERE clause for selecting current row
    , fields_insert   DYNAMIC ARRAY OF STRING    # List of columns which should be inserted (it's list of form fields without SERIAL columns)
    , fields_update   DYNAMIC ARRAY OF STRING    # List of columns which should be updated (it's list of form fields without PRIMARY KEY columns)
    , fields_noentry  DYNAMIC ARRAY OF STRING    # List of columns which can not be modified (it's list of PRIMARY KEY or SERIAL columns)
    , fields_virtual  DYNAMIC ARRAY OF STRING    # List of columns which don't have visual element on screen but should be managed by Select, Update, and Delete actions
    , fields_ins_db   DYNAMIC ARRAY OF STRING    # List of columns which should be retrieved from DB after inserting a new record (e.g. SERIAL column)
    , primary_key     DYNAMIC ARRAY OF STRING    # Primary key that should be used for fetching <column name>
    , buffer          DYNAMIC ARRAY OF HASHMAP   # Keeps all column values for making correct WHERE clause for current row
    , buffer_start    INT                        # Row index of buffer start
    , sqlHandle       BASE.SqlHandle             # The SQL handle for current query
    , refresh_buffer  BOOL                       # Indicates if we need to refresh buffer even if buffer start and length are not changed
    , single_row      BOOL                       # It's TRUE if there is just one row on screen can be shown and it is not a Table widget
    , curr_interact   INT                        # 0 - Display/Input Array; 1 - Update; 2 - Insert; 3 - Query
    , is_table        BOOL                       # It's TRUE if there is Table widget is used
    , comboboxes      HASHMAP OF ComboboxDef     # Map of field names and their comboboxe widgets, sql query and sql where clause
    , construct_vals  HASHMAP                    # Values entered on QUERY action, they should be reused on next QUERY action
    , arr_count       INT                        # Count of available rows that can be retrieved by current query
    , scr_line        INT                        # Current screen line which is being edited (it makes sence for DISPLAY mode only)
    , arr_curr        INT                        # Current array row index which is being edited (it makes sence for DISPLAY mode only)
    , fields_lookup   DYNAMIC ARRAY OF RECORD    #
                            field       STRING   #
                          , is_blob     BOOL     #
                          , depends_on  DYNAMIC ARRAY OF STRING
                          , query       STRING   #
                          , query_param STRING   #
                      END RECORD                 #
    , view_related    DYNAMIC ARRAY OF STRING    # List of view indexes that depend on this view state
    , selected_buf_i  INT                        # Index of selected data/row in buffer
    , sql_where       STRING                     # The WHERE clause of the main query that can NOT be overwritten by user, it is already parsed and have binded '?'
    , depends_on      DYNAMIC ARRAY OF RECORD    # The list of the views on which the view depends
                            view_parent  STRING  # The parent view index
                          , tab          STRING  # The field name in parent view
                          , field        STRING  # The field name in parent view
                          , val          VARIANT # The value of foreign key for dependent table which is used for query
                          , target_field STRING  # The field/column name in the current view
                          , target_exist BOOL    # It's TRUE if target field is exist in form
                          , target_ins   BOOL    # It's TRUE if it should be insert implicitly
                      END RECORD                 #
END RECORD                                       #
###############################################################################################################################


###############################################################################################################################
# TYPE Combobox RECORD                           #
#   Combobox population details                  #
###############################################################################################################################
TYPE Combobox RECORD                             #
    show_columns         DYNAMIC ARRAY OF STRING # The list of columns that will be displayed with comma-separated values
  , sql_where            STRING                  # The sql where clause for filtering population list, e.g. '<table>.<column> = <foreign_table>.<foreign_table_column> AND <foreign_table>.title = "Mr."'
END RECORD                                       #
###############################################################################################################################


###############################################################################################################################
# TYPE View RECORD                               #
###############################################################################################################################
TYPE View RECORD                                 #
      screen_record        STRING                # The screen record name in the form that should be used. It's mandatory property.
    , actions              HASHMAP OF HASHMAP    # Custom actions "SubIntercation - Action - Function", which will be added to the subdialog of defined table view and executed before LLC build-in event executing
    , actions_on_done      HASHMAP OF HASHMAP    # Custom actions "SubIntercation - Action - Function", which will be added to the subdialog of defined table view and executed after LLC build-in event executing
    , view_attributes      HASHMAP OF HASHMAP    # Custom attributes "SubIntercation - Attribute - Value", which will be applied to the subdialog of defined table view
    , not_update_fields    DYNAMIC ARRAY OF STRING # The list of field which should not be activated on the Edit action
    , paged_mode           INT                   # If positive then buffer should be used (in this case it will take less time for starting interaction)
    , input_mode           INT                   # If negative or 0 then  DISPLAY ARRAY should be used, otherwise (positive) INPUT ARRAY is used
    , pessimistic_locking  INT                   # Pessimistic row locking is disabled by default (optimistic does not lock table during input, but only for the time of the actual db update)
    , sql_where_search     STRING                # The WHERE clause of the main query that can be overwritten as soon the user applies a Search (Construct)
    , sql_where            STRING                # The WHERE clause of the main query that can NOT be overwritten by user
    , sql_order_by         STRING                # The ORDER BY clause for the main query
    , sql_top              INT                   # The option to limit the base cursor row using the SQL SELECT TOP clause
    , confirm_accept       INT                   # If positive then it shows a message box for yes/no/cancel when data has been changed AND the user presses ACCEPT. The default is negative (-1)
    , confirm_cancel       INT                   # If positive then it shows a message box for yes/no/cancel when data has been changed AND the user presses CANCEL. The default is positive (1)
    , comboboxes           HASHMAP OF Combobox   # Map of combobox field names and its sql where clause and dependencies
    , lookups              HASHMAP OF STRING     # Map of lookup field names and its 'match' string that will be used in SQL WHERE clause
    , navigation_status    STRING                # Target location for the DISPLAY of navigation status (which can be a label or a textField)
    , inside               View_Internal         # An internal set of settings which setup by LowCode, the 4gl developer doesn't need to set anything here
END RECORD                                       #
###############################################################################################################################


###############################################################################################################################
# TYPE InteractForm_Settings RECORD              #
###############################################################################################################################
TYPE InteractForm_Settings RECORD                #
      form_file            STRING                # The form file that should be opened
    , id                   STRING                # Optional, a form file can contain multiple InteractForm settings, and it can be selected by 'id'
    , log_file             STRING                # The path to log file. It's optional property
    , translations         HASHMAP OF STRING     # Map of message/table/column translations
    , views                HASHMAP OF View       # A map of the view table and its settings
    , actions              HASHMAP OF HASHMAP    # Custom actions "SubIntercation - Action - Function", which will be added to the subdialog of defined table view and executed before LLC build-in event executing
    , actions_on_done      HASHMAP OF HASHMAP    # Custom actions "SubIntercation - Action - Function", which will be added to the subdialog of defined table view and executed after LLC build-in event executing
    , view_attributes      HASHMAP OF HASHMAP    # Custom attributes "SubIntercation - Attribute - Value", which will be applied to Dialog
    , paged_mode           INT                   # If positive then buffer should be used (in this case it will take less time for starting interaction). This is the default value for all views that do not have a specified value
    , input_mode           INT                   # If negative or 0 then  DISPLAY ARRAY should be used, otherwise (positive) INPUT ARRAY is used. This is the default value for all views that do not have a specified value
    , pessimistic_locking  INT                   # Pessimistic row locking is disabled by default (optimistic does not lock table during input, but only for the time of the actual db update). This is the default value for all views that do not have a specified value
    , confirm_accept       INT                   # If positive then it shows a message box for yes/no/cancel when data has been changed AND the user presses ACCEPT. The default is negative (-1). This is the default value for all views that do not have a specified value
    , confirm_cancel       INT                   # If positive then it shows a message box for yes/no/cancel when data has been changed AND the user presses CANCEL. The default is positive (1). This is the default value for all views that do not have a specified value
END RECORD                                       #
###############################################################################################################################


###############################################################################################################################
# TYPE InteractForm RECORD                       #
#  InteractForm is a class for creating          #
#  interaction dynamically based on form         #
#                                                #
###############################################################################################################################
TYPE InteractForm RECORD                         #
      form_file           STRING                 # The form file that should be opened
    , log_file            STRING                 # The path to log file. It's optional property
    , log_to_file         BOOL                   # Indicates if error message should be logged to file
    , translations        HASHMAP OF STRING      # Map of message/table/column translations
    , views               HASHMAP OF View        # A set of views of tables that are depends each other
END RECORD                                       #
###############################################################################################################################

END GLOBALS

DEFINE windowCounter INT8                        # Counter of windows for making unique window name
DEFINE hasSubDialog  DYNAMIC ARRAY OF STRING     # A list of event names which contains subdialog names

CONSTANT INTERACT_MAIN     =  0
CONSTANT INTERACT_UPDATE   =  1
CONSTANT INTERACT_INSERT   =  2
CONSTANT INTERACT_QUERY    =  3
CONSTANT DEFAULT_ROW_COUNT = 38

###############################################################################################################################
#
#  Function InteractForm
#  Creates InteractForm object and performs its interaction using provided settings
#
#  Arguments:
#  - settings (InteractForm) - Settings for LLC IneractForm object
#
###############################################################################################################################
PUBLIC FUNCTION InteractForm(settings InteractForm_Settings INOUT)
    DEFINE iForm InteractForm
    CALL iForm.Interact(settings)
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION InteractForm(settings InteractForm_Settings INOUT)
################################################################################################################################


################################################################################################################################
#
#  Function InteractFormFile
#  Opens window with defined form
#  Creates InteractForm object and performs its interaction using opened form
#
#  Arguments:
#  - form_file     (STRING) - path to form file
#
################################################################################################################################
PUBLIC FUNCTION InteractFormFile(form_file STRING)
    DEFINE settings InteractForm_Settings
    LET settings.form_file = form_file
    CALL InteractForm(settings)
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION InteractFormFile(formFile STRING)
################################################################################################################################


################################################################################################################################
#
#  Function InteractFormFileWithSettings
#  Opens window with defined form
#  Selected defined InteractForm Settings id from form file and performs its interaction
#
#  Arguments:
#  - form_file     (STRING) - path to form file
#  - setting_id    (STRING) - id of the InteractForm settings that should be used to execute
#
################################################################################################################################
PUBLIC FUNCTION InteractFormFileWithSettings(form_file STRING, setting_id STRING)
    DEFINE settings InteractForm_Settings
    LET settings.form_file = form_file
    IF setting_id IS NULL THEN
        LET settings.id = " "
    ELSE
        LET settings.id = setting_id
    END IF
    CALL InteractForm(settings)
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION InteractFormFileWithSettings(form_file STRING, setting_id STRING)
################################################################################################################################




#===============================================================================================================================
#
#=========================================       InteractForm       ============================================================
#
#===============================================================================================================================




################################################################################################################################
# PUBLIC FUNCTION (this InteractForm) Interact(settings InteractForm_Settings INOUT) RETURNS BOOL
#
#  Public method Interact of InteractForm object
#  Creates interaction dynamically based on form
#
#  The main interaction Menu with actions:
#     "Query", "Edit", "Delete", "Add", "Next" and "Previous"
#
#  Returns TRUE if everything is done well,
#      and FALSE if executing is finished with error
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) Interact(settings InteractForm_Settings INOUT) RETURNS BOOL
    DEFINE ret, openForm BOOL
    DEFINE w WINDOW

    DEFER INTERRUPT

    LET openForm = settings.form_file IS NOT NULL

    IF openForm THEN
        LET windowCounter = windowCounter + 1
        CALL w.OpenWithForm("w_InteractForm_" || windowCounter, settings.form_file, 1, 1) # Open the new window with defined form
    END IF

    CALL this.InteractCurrentForm(settings) RETURNING ret

    IF openForm THEN
        CALL w.Close()                                       # Close the window
    ELSE
        CALL this.ClearNavigationStatus()
    END IF

    RETURN ret
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) Interact() RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PUBLIC FUNCTION (this InteractForm) InteractCurrentForm(settings InteractForm_Settings INOUT) RETURNS BOOL
#
#  Public method InteractCurrentForm of InteractForm object
#  Creates interaction dynamically based on form
#
#  The main interaction Menu with actions:
#     "Query", "Edit", "Delete", "Add", "Next" and "Previous"
#
#  Returns TRUE if everything is done well,
#      and FALSE if executing is finished with error
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) InteractCurrentForm(settings InteractForm_Settings INOUT) RETURNS BOOL
    DEFER INTERRUPT

    IF NOT this.MergeSettings(settings) THEN
        RETURN FALSE                                         # Mergin settings with settings from form file is failed, so return FALSE
    END IF

    RETURN this.ExecuteInteraction()
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) InteractCurrentForm() RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) ExecuteInteraction() RETURNS BOOL
#
# Private method ExecuteInteraction of InteractForm object
# Starts interaction on form
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ExecuteInteraction() RETURNS BOOL
    DEFINE event, view_event, view_event_table, view_table, cname, ikey STRING
    DEFINE ret, touched, isAccept           BOOL
    DEFINE appending, do_continue           BOOL
    DEFINE do_insert, bundleUpdate, empty   BOOL
    DEFINE noEntryFieldActive, proceed      BOOL
    DEFINE i, j, vi                         INT
    DEFINE deleted                          DYNAMIC ARRAY OF HASHMAP
    DEFINE selected_data                    HASHMAP
    DEFINE dlg                              UI.DIALOG

    IF NOT this.InitInteractForm() THEN
        RETURN FALSE                                    # Initiating object before executing is failed, so return FALSE
    END IF

    LET ret = FALSE
    LET dlg = ui.Dialog.createMultipleDialog()

    FOR i = 1 TO this.views.getSize()
        LET this.views[this.views.getKey(i)].inside.refresh_buffer = TRUE  # Reset refresh_buffer state
    END FOR

    LET view_table = this.AddSubDialog(dlg, "")

    WHENEVER ANY ERROR STOP

    WHILE (event := dlg.NextEvent()) IS NOT NULL         # Process dialog and wait for the next event
        IF view_table IS NULL THEN
            CALL this.ShowError(this.LSTRS("Event handler"), SFMT(this.LSTRS("Event '%1' can not be handled. Current view is undefined"), event))
            RETURN FALSE
        END IF

        CALL this.ParseEvent(event) RETURNING view_event, view_event_table
        IF view_event_table IS NULL THEN
            LET view_event_table = view_table           # Default view index for the event
        END IF

        CASE view_event
            WHEN "BEFORE DIALOG"
                # Initial display
                FOR i = 1 TO this.views.getSize()
                    LET ikey = this.views.getKey(i)
                    IF this.views[ikey].inside.depends_on.GetSize() = 0 THEN
                        CALL this.views[ikey].Show(this, -1, -1)
                    END IF
                END FOR
                CALL PopulateToolbar()

            WHEN "BEFORE INPUT"
                LET view_table = view_event_table
                LET touched = FALSE
                LET isAccept = TRUE
                LET do_insert = FALSE
                LET appending = FALSE
                LET bundleUpdate = NOT (this.views[view_table].paged_mode > 0 OR this.views[view_table].inside.single_row)
                CALL deleted.Clear()

            WHEN "BEFORE DISPLAY"
                LET view_table = view_event_table

            WHEN "BEFORE ROW"
                LET this.views[view_table].inside.scr_line = scr_line()
                LET this.views[view_table].inside.arr_curr = arr_curr()

                IF this.views[view_table].input_mode > 0 THEN
                    LET noEntryFieldActive = FALSE
                    IF NOT bundleUpdate THEN
                        LET noEntryFieldActive = this.views[view_table].inside.buffer[arr_curr()].KeyExists("LLC_new")
                    END IF

                    CALL this.views[view_table].DisableNoEntryFields(noEntryFieldActive)
                    LET appending = FALSE
                END IF

                LET i = arr_curr()
                IF this.views[view_table].paged_mode > 0 THEN
                    LET i = i - this.views[view_table].inside.buffer_start + 1
                    IF i < 1 OR (i > this.views[view_table].inside.buffer.getSize() AND this.views[view_table].inside.buffer.getSize() > 0) THEN
                        CALL this.views[view_table].Show(this, arr_curr(), 1)
                        LET i = 1
                    END IF
                END IF
                CALL this.views[view_table].SetSelectedData(this, i)

                IF this.views[view_table].input_mode > 0 THEN
                    CALL this.views[view_table].PopulateAllComboBox(TRUE, scr_line(), TRUE, 2)
                END IF

            WHEN "ON FILL BUFFER"
                IF NOT this.views[view_event_table].Show(this, FGL_DIALOG_GETBUFFERSTART(), FGL_DIALOG_GETBUFFERLENGTH()) THEN
                    CONTINUE WHILE                       # Skip executing custom action "ON FILL BUFFER"
                END IF
        END CASE

        IF this.views[view_table].ExecuteCustomAction(this, TOP_DIALOG_ID, view_event, TRUE) THEN
            CONTINUE WHILE
        END IF

        CASE view_event
            WHEN "ON UPDATE"
                LET this.views[view_table].inside.curr_interact = INTERACT_UPDATE
                CALL this.views[view_table].Edit(this, dlg, scr_line(), arr_curr())
                LET this.views[view_table].inside.curr_interact = INTERACT_MAIN

            WHEN "ON INSERT"
                LET this.views[view_table].inside.curr_interact = INTERACT_INSERT
                CALL this.views[view_table].InsertImpl(this, dlg, scr_line(), arr_curr())
                LET this.views[view_table].inside.curr_interact = INTERACT_MAIN

            WHEN "ON APPEND"
                LET this.views[view_table].inside.curr_interact = INTERACT_INSERT
                CALL this.views[view_table].InsertImpl(this, dlg, scr_line(), arr_curr())
                LET this.views[view_table].inside.curr_interact = INTERACT_MAIN

            WHEN "ON DELETE"
                CALL this.views[view_table].DeleteImpl(this, dlg, scr_line(), arr_curr())

            WHEN "ON ACTION QUERY"
                LET this.views[view_table].inside.curr_interact = INTERACT_QUERY
                IF this.views[view_table].Query(this) THEN
                    CALL this.views[view_table].Show(this, -1, -1)
                END IF
                LET this.views[view_table].inside.curr_interact = INTERACT_MAIN

            WHEN "ON ACTION Refresh"
                IF this.views[view_table].input_mode <= 0 THEN
                    # DISPLAY ARRAY
                    CALL this.views[view_table].Refresh(this, FALSE)
                ELSE
                    # INPUT ARRAY
                    LET proceed = TRUE
                    IF touched THEN
                        CASE fgl_winbutton(this.LSTRS("Refresh"),
                                        this.LSTRS("Do you want to save changes before refreshing data?"),
                                        this.LSTRS("Save Data and Refresh"),
                                        SFMT("%1|%2|%3", this.LSTRS("Save"),
                                                            this.LSTRS("Don't Save"),
                                                            this.LSTRS("Cancel") ),
                                        "Question")

                            WHEN this.LSTRS("Cancel")
                                LET proceed = FALSE

                            WHEN this.LSTRS("Save")
                                IF NOT this.views[view_table].ExecuteInteractionInput_Save(this, dlg, deleted, bundleUpdate, do_insert) THEN
                                    LET proceed = FALSE
                                END IF
                        END CASE

                        IF proceed THEN
                            LET do_insert = FALSE
                            LET touched = FALSE
                            CALL deleted.Clear()
                        END IF
                    END IF

                    IF proceed THEN
                        CALL this.views[view_table].Refresh(this, FALSE)
                    END IF
                END IF

            WHEN "AFTER ROW"                                          # Save update if row is touched
                IF this.views[view_table].IsCurrentRowTouched(dlg) THEN
                    LET int_flag = FALSE

                    IF bundleUpdate THEN
                        LET this.views[view_table].inside.buffer[arr_curr()]["LLC_touched"] = TRUE
                        LET touched = TRUE
                    ELSE
                        IF do_insert THEN
                            IF fgl_winbutton("Add", "Add new item?", "Yes", "Yes|No", "Question", 1) = "Yes" THEN
                                IF this.views[view_table].ExecuteInteractionInput_Insert(this, dlg) THEN
                                    LET do_insert = FALSE
                                END IF
                            ELSE
                                LET do_insert = FALSE
                            END IF
                        ELSE
                            CASE fgl_winbutton("Modify", "Save changes?", "Yes", "Yes|Cancel|Discard", "Question", 1)
                                WHEN "Yes"                                    # Updating is confirmed by user
                                    CALL this.views[view_table].ExecuteInteractionInput_Update(this, dlg)

                                WHEN "Cancel"
                                    CALL dlg.NextField("+CURR")                # Stay in current row

                                OTHERWISE                                      # Updating is rejected
                                    # Revert row data
                                    CALL this.views[view_table].GetSelectedData() RETURNING selected_data
                                    FOR i = 1 TO this.views[view_table].inside.fields_form.GetSize()    # For each edited form field
                                        LET cname = this.views[view_table].inside.fields_form[i].fname
                                        CALL dlg.SetFieldValue(cname, selected_data[cname])
                                    END FOR
                            END CASE
                        END IF
                    END IF
                END IF

                IF NOT this.views[view_table].inside.is_table THEN
                    CALL this.views[view_table].PopulateAllComboBox(TRUE, scr_line(), FALSE, 2)
                END IF

            WHEN "BEFORE INSERT"
                CALL this.views[view_table].ActivateFieldsForInsert()
                LET appending = arr_curr() > dlg.GetArrayLength()
                FOR i = 1 TO this.views[view_table].inside.depends_on.GetSize()
                    IF this.views[view_table].inside.depends_on[i].target_field IS NOT NULL THEN
                        LET j = this.views[view_table].inside.fields_form.Search("fname", this.views[view_table].inside.depends_on[i].target_field)
                        IF j > 0 THEN
                            CALL dlg.SetFieldValue(this.views[view_table].inside.depends_on[i].target_field,
                                                    this.views[view_table].inside.depends_on[i].val,
                                                    arr_curr())
                        END IF
                    END IF
                END FOR

            WHEN "AFTER INSERT"
                MESSAGE ""

                IF this.views[view_table].IsCurrentRowTouched(dlg) THEN
                    # Update parent array dialog with new values
                    CALL this.views[view_table].inside.buffer.insert(arr_curr())
                    LET this.views[view_table].inside.buffer[arr_curr()]["LLC_new"] = TRUE
                    LET touched = TRUE
                    IF NOT bundleUpdate THEN
                        LET do_insert = TRUE
                    END IF
                ELSE
                    CANCEL INSERT
                END IF

            WHEN "AFTER DELETE"
                IF bundleUpdate THEN
                    IF NOT this.views[view_table].inside.buffer[arr_curr()].KeyExists("LLC_new") THEN
                        CALL deleted.Append(this.views[view_table].inside.buffer[arr_curr()])
                        LET touched = TRUE
                    END IF
                    CALL this.views[view_table].inside.buffer.Delete(arr_curr())
                ELSE
                    CALL this.views[view_table].DeleteImpl(dlg, scr_line(), arr_curr())
                END IF

            WHEN "ON ACTION Save"
                IF this.views[view_table].Question(this.LSTRS("Modify"),
                                                   this.LSTRS("Save all changes?"),
                                                   this.LSTRS("Save"),
                                                   SFMT("%1|%2", this.LSTRS("Save"), this.LSTRS("Cancel")),
                                                   "Question",
                                                   isAccept) = "Save"
                THEN
                    IF this.views[view_table].ExecuteInteractionInput_Save(this, dlg, deleted, bundleUpdate, do_insert) THEN
                        LET do_insert = FALSE
                        LET touched = FALSE
                        CALL deleted.Clear()
                    END IF
                END IF

            WHEN "ON ACTION Reset"
                IF fgl_winbutton(this.LSTRS("Reset"),
                                 this.LSTRS("Reseting will remove all unsaved data. How do you want to proceed?"),
                                 this.LSTRS("Go Back to Editing"),
                                 SFMT("%1|%2", this.LSTRS("Reset"),
                                               this.LSTRS("Go Back to Editing") ),
                                 "Question") = "Reset"
                THEN
                    CALL this.views[view_table].Refresh(this, FALSE)
                    LET do_insert = FALSE
                    LET touched = FALSE
                    CALL deleted.Clear()
                END IF

            WHEN "ON ACTION Accept"
                LET ret = TRUE                           # Means proceeded by user
                LET isAccept = TRUE
                CALL dlg.Accept()

            WHEN "ON ACTION Cancel"
                LET ret = TRUE                           # Means proceeded by user
                LET isAccept = FALSE
                CALL dlg.Cancel()

            WHEN "AFTER INPUT"
                IF bundleUpdate THEN
                    LET do_continue = FALSE
                    IF touched THEN
                        CASE this.views[view_table].Question(this.LSTRS("Modify"),
                                                             this.LSTRS("Save all changes?"),
                                                             this.LSTRS("Yes"),
                                                             SFMT("%1|%2|%3", this.LSTRS("Yes"), this.LSTRS("Cancel"), this.LSTRS("Discard all")),
                                                             "Question",
                                                             isAccept)
                        WHEN this.LSTRS("Yes")
                            IF this.views[view_table].ExecuteInteractionInput_BundleUpdate(this, dlg, deleted) THEN
                                LET ret = TRUE               # Means proceeded by user
                            ELSE
                                LET do_continue = TRUE       # Continue dialog
                            END IF

                        WHEN this.LSTRS("Cancel")
                            LET do_continue = TRUE

                        OTHERWISE
                            # Updating is rejected
                            LET ret = TRUE                   # Means proceeded by user
                        END CASE
                    END IF

                    IF do_continue THEN
                        CALL dlg.NextField("+CURR")          # Stay in current row
                    END IF
                END IF

            WHEN "AFTER DIALOG"
                EXIT WHILE

            OTHERWISE
                IF appending THEN
                    CALL this.views[view_table].ManageAfterFieldOnInserting(dlg, view_event)
                END IF
                CALL this.views[view_table].ManageBuildInEvent(this, dlg, view_event)
        END CASE

        CALL this.views[view_table].ExecuteCustomAction(this, TOP_DIALOG_ID, view_event, FALSE)

        # Check if array is empty, so we need to hide UPDATE and DELETE actions
        LET empty = dlg.GetArrayLength(this.views[view_table].screen_record) = 0
        CALL dlg.SetActionHidden("UPDATE", empty)
        CALL dlg.SetActionHidden("DELETE", empty)
    END WHILE

    FOR i = 1 TO this.views.getSize()
        CALL this.views[this.views.getKey(i)].inside.sqlHandle.Close()      # Close all sql handlers
    END FOR
    CALL dlg.Close()                                     # Close and release dynamic dialog

    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) ExecuteInteraction() RETURNS BOOL
################################################################################################################################


################################################################################################################################
#
#  Public function ClearNavigationStatus clear the navigation status.
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) ClearNavigationStatus()
    DEFINE i INT
    FOR i = 1 TO this.views.GetSize()
        CALL this.views[this.views.getKey(i)].ClearNavigationStatus()
    END FOR
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) ClearNavigationStatus()
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) MergeSettings(settings InteractForm_Settings INOUT) RETURNS BOOL
#
#  Private method MergeSettings of InteractForm object
#  Returns TRUE if settings are merged well
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) MergeSettings(settings InteractForm_Settings INOUT) RETURNS BOOL
    DEFINE i, j, def_paged_mode                     INT
    DEFINE def_input_mode, def_pessimistic_locking  INT
    DEFINE def_confirm_accept, def_confirm_cancel   INT
    DEFINE formSettings                             InteractForm_Settings
    DEFINE settings_found                           BOOL
    DEFINE ikey, jkey                               STRING
    DEFINE def_attributes                           HASHMAP OF HASHMAP

    IF settings.id = " " THEN
        RETURN TRUE         # We have to skip all form's settings
    END IF

    LET settings_found = FindFormSettings(formSettings, settings.id)

    IF NOT settings_found THEN
        CALL this.ShowError(this.LSTRS("Interact settings"), SFMT(this.LSTRS("Interact setting '%1' was not found."), settings.id))
        RETURN FALSE
    END IF

    LET this.form_file = settings.form_file
    CALL settings.views.CopyTo(this.views)

    IF settings_found IS NULL THEN    # Nothing to merge, form file doesn't contain any LLC settings
        RETURN TRUE
    END IF

    # log_file
    LET this.log_file = settings.log_file
    IF this.log_file IS NULL THEN
        LET this.log_file = formSettings.log_file
    END IF
    LET this.log_file = this.log_file.Trim()

    # translations
    LET this.translations = settings.translations
    CALL this.translations.join(formSettings.translations)

    # paged_mode
    LET def_paged_mode = settings.paged_mode
    IF def_paged_mode = 0 OR def_paged_mode IS NULL THEN
        LET def_paged_mode = formSettings.paged_mode
    END IF

    # input_mode
    LET def_input_mode = settings.input_mode
    IF def_input_mode = 0 OR def_input_mode IS NULL THEN
        LET def_input_mode = formSettings.input_mode
    END IF

    # pessimistic_locking
    LET def_pessimistic_locking = settings.pessimistic_locking
    IF def_pessimistic_locking = 0 OR def_pessimistic_locking IS NULL THEN
        LET def_pessimistic_locking = formSettings.pessimistic_locking
    END IF

    # confirm_accept
    LET def_confirm_accept = settings.confirm_accept
    IF def_confirm_accept = 0 OR def_confirm_accept IS NULL THEN
        LET def_confirm_accept = formSettings.confirm_accept
    END IF

    # confirm_cancel
    LET def_confirm_cancel = settings.confirm_cancel
    IF def_confirm_cancel = 0 OR def_confirm_cancel IS NULL THEN
        LET def_confirm_cancel = formSettings.confirm_cancel
    END IF

    # attributes
    CALL settings.view_attributes.CopyTo(def_attributes)
    CALL MergeHashOfHash(def_attributes, formSettings.view_attributes)

    # validate
    FOR i = 1 TO this.views.GetSize()
        IF this.views.getKey(i) IS NULL THEN
            CALL this.ShowError(this.LSTRS("Interact settings"),
                                this.LSTRS("The 'view_table' property is required. Check if you have defined it for all views in 4gl code."))
            RETURN FALSE
        END IF
    END FOR

    FOR i = 1 TO formSettings.views.GetSize()
        LET ikey = formSettings.views.getKey(i)

        # validate
        IF ikey IS NULL THEN
            CALL this.ShowError(this.LSTRS("Interact settings"),
                                this.LSTRS("The 'view_table' property is required. Check if you have defined it for all views in the form file."))
            RETURN FALSE
        END IF

        # find view to merge
        LET j = iCaseSearchHashmap(this.views, ikey)
        IF j = 0 THEN
            # There is no view to merge, so just append view
            LET this.views[ikey] = formSettings.views.getValue(i)
            CONTINUE FOR
        END IF

        LET jkey = this.views.getKey(j)

        # attributes
        CALL MergeHashOfHash(this.views[jkey].view_attributes, formSettings.views[ikey].view_attributes)
        CALL MergeHashOfHash(this.views[jkey].view_attributes, def_attributes)

        # screen_record
        IF this.views[jkey].screen_record IS NULL THEN
            LET this.views[jkey].screen_record = formSettings.views[ikey].screen_record
        END If

        # sql_where
        IF this.views[jkey].sql_where IS NULL THEN
            LET this.views[jkey].sql_where = formSettings.views[ikey].sql_where
        END IF
        LET this.views[jkey].sql_where = this.views[jkey].sql_where.Trim()

        # sql_where_search
        IF this.views[jkey].sql_where_search IS NULL THEN
            LET this.views[jkey].sql_where_search = formSettings.views[ikey].sql_where_search
        END IF
        LET this.views[jkey].sql_where_search = this.views[jkey].sql_where_search.Trim()

        # sql_order_by
        IF this.views[jkey].sql_order_by IS NULL THEN
            LET this.views[jkey].sql_order_by = formSettings.views[ikey].sql_order_by
        END IF
        LET this.views[jkey].sql_order_by = this.views[jkey].sql_order_by.Trim()

        # navigation_status
        IF this.views[jkey].navigation_status IS NULL THEN
            LET this.views[jkey].navigation_status = formSettings.views[ikey].navigation_status
        END IF
        LET this.views[jkey].navigation_status = this.views[jkey].navigation_status.Trim()

        # paged_mode
        IF this.views[jkey].paged_mode = 0 OR this.views[jkey].paged_mode IS NULL THEN
            LET this.views[jkey].paged_mode = formSettings.views[ikey].paged_mode
        END IF

        # input_mode
        IF this.views[jkey].input_mode = 0 OR this.views[jkey].input_mode IS NULL THEN
            LET this.views[jkey].input_mode = formSettings.views[ikey].input_mode
        END IF

        # pessimistic_locking
        IF this.views[jkey].pessimistic_locking = 0 OR this.views[jkey].pessimistic_locking IS NULL THEN
            LET this.views[jkey].pessimistic_locking = formSettings.views[ikey].pessimistic_locking
        END IF

        # sql_top
        IF this.views[jkey].sql_top = 0 OR this.views[jkey].sql_top IS NULL THEN
            LET this.views[jkey].sql_top = formSettings.views[ikey].sql_top
        END IF

        # confirm_accept
        IF this.views[jkey].confirm_accept = 0 OR this.views[jkey].confirm_accept IS NULL THEN
            LET this.views[jkey].confirm_accept = formSettings.views[ikey].confirm_accept
        END IF

        # confirm_cancel
        IF this.views[jkey].confirm_cancel = 0 OR this.views[jkey].confirm_cancel IS NULL THEN
            LET this.views[jkey].confirm_cancel = formSettings.views[ikey].confirm_cancel
        END IF

        # comboboxes
        CALL this.views[jkey].comboboxes.Join(formSettings.views[ikey].comboboxes)

        # lookups
        CALL this.views[jkey].lookups.Join(formSettings.views[ikey].lookups)

        # actions are a string in a form file, so we need to convert them into function reference
        CALL MergeHashOfHash(this.views[jkey].actions, formSettings.views[ikey].actions)
        CALL MergeHashOfHash(this.views[jkey].actions_on_done, formSettings.views[ikey].actions_on_done)
    END FOR

    # events
    FOR i = 1 TO this.views.GetSize()
        CALL MergeHashOfHash(this.views[ikey].actions, settings.actions)
        CALL MergeHashOfHash(this.views[ikey].actions, formSettings.actions)
        CALL MergeHashOfHash(this.views[ikey].actions_on_done , settings.actions_on_done)
        CALL MergeHashOfHash(this.views[ikey].actions_on_done , formSettings.actions_on_done)
    END FOR

    # Set default values
    FOR j = 1 TO this.views.GetSize()
        LET jkey = this.views.getKey(j)

        IF this.views[jkey].screen_record IS NULL THEN
            CALL this.ShowError(this.LSTRS("Interact settings"),
                                this.LSTRS("The 'screen_record' property is required. Check if all final views have defined 'screen_record' in the form file or 4gl code."))
            RETURN FALSE
        END IF

        # paged_mode
        IF this.views[jkey].paged_mode = 0 OR this.views[jkey].paged_mode IS NULL THEN
            LET this.views[jkey].paged_mode = def_paged_mode
        END IF

        # input_mode
        IF this.views[jkey].input_mode = 0 OR this.views[jkey].input_mode IS NULL THEN
            LET this.views[jkey].input_mode = def_input_mode
        END IF

        # pessimistic_locking
        IF this.views[jkey].pessimistic_locking = 0 OR this.views[jkey].pessimistic_locking IS NULL THEN
            LET this.views[jkey].pessimistic_locking = def_pessimistic_locking
        END IF

        # confirm_accept
        IF this.views[jkey].confirm_accept = 0 OR this.views[jkey].confirm_accept IS NULL THEN
            LET this.views[jkey].confirm_accept = def_confirm_accept
        END IF

        # confirm_cancel
        IF this.views[jkey].confirm_cancel = 0  OR this.views[jkey].confirm_cancel IS NULL THEN
            LET this.views[jkey].confirm_cancel = def_confirm_cancel
        END IF
    END FOR

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) MergeSettings(settings InteractForm_Settings INOUT) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) CheckDependencies(depends_on_table STRING, done DYNAMIC ARRAY OF INT) RETURNS BOOL
#
# Private method CheckDependencies of InteractForm object
# Returns FALSE if there is circular dependencies
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) CheckDependencies(depends_on_table STRING, done DYNAMIC ARRAY OF INT) RETURNS BOOL
    DEFINE i, j  INT
    DEFINE proc  BOOL
    DEFINE ikey  STRING

    FOR i = 1 TO this.views.getSize()
        LET ikey = this.views.getKey(i)
        LET proc = FALSE
        IF depends_on_table IS NULL THEN
            IF this.views[ikey].inside.depends_on.GetSize() = 0 THEN
                LET proc = TRUE
            END IF
        ELSE
            FOR j = 1 TO this.views[ikey].inside.depends_on.GetSize()
                IF this.views[ikey].inside.depends_on[j].tab.equalsIgnoreCase(depends_on_table) THEN
                    LET proc = TRUE
                END IF
            END FOR
        END IF
        IF proc THEN
            IF done.search(NULL, i) > 0 THEN
                RETURN FALSE
            END IF
            CALL done.append(i)
            IF NOT this.CheckDependencies(this.views.getKey(i), done) THEN
                RETURN FALSE
            END IF
        END IF
    END FOR
    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) CheckDependencies(depends_on_table STRING, done DYNAMIC ARRAY OF INT) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) InitInteractForm() RETURNS BOOL
#
#  Private method InitInteractForm of InteractForm object
#  Returns TRUE if everything is initialized well
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) InitInteractForm() RETURNS BOOL
    DEFINE done DYNAMIC ARRAY OF INT
    DEFINE i, j INT
    DEFINE ikey STRING

# Reset all internal field of InteractForm object

    CALL ui.Dialog.Init()

    LET this.log_to_file = FALSE
    IF this.log_file IS NOT NULL THEN
        CALL startlog(this.log_file)
        IF NOT os.path.writable(this.log_file) THEN
            CALL fgl_winmessage(this.LSTRS("Log File Access Error"),
                                this.LSTRS("Invalid permission (Write) for log file ") || this.log_file,
                                "Error")
        ELSE
            LET this.log_to_file = TRUE
        END IF
    END IF

# Clear form and view
    MESSAGE ""

    IF this.views.getSize() = 0 THEN
        CALL this.ShowError(this.LSTRS("Initialize"), this.LSTRS("The view is undefined."))
        RETURN FALSE                                         # Return FALSE because of error
    END IF

    FOR i = 1 TO this.views.getSize()
        LET ikey = this.views.getKey(i) 
        IF NOT this.views[ikey].InitView(this, ikey) THEN
            RETURN FALSE
        END IF
    END FOR

    IF NOT this.CheckDependencies("", done) THEN
        CALL this.ShowError(this.LSTRS("Initialize"), this.LSTRS("There is circular view dependencies."))
    END IF

    RETURN TRUE                                              # Return TRUE because of NO error
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) InitInteractForm() RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PUBLIC FUNCTION (this InteractForm) PopulateComboBoxWhere(field STRING, where STRING)
#
#  Public method PopulateComboBoxWhere of InteractForm object
#  - Populates ComboBox with values from referenced DB table and defined filter
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) PopulateComboBoxWhere(field STRING, where STRING)
    DEFINE j, i, control_role, line INT
    DEFINE jkey STRING

    FOR j = 1 TO this.views.GetSize()
        LET jkey = this.views.getKey(j)
        LET i = iCaseSearchHashmap(this.views[jkey].inside.comboboxes, field)
        IF i > 0 THEN                                            # Check if the combobox exists
            CALL this.views[jkey].InitComboboxSqlWhere(this.views[jkey].inside.comboboxes[field], where)
            IF this.views[jkey].inside.curr_interact > INTERACT_MAIN OR this.views[jkey].input_mode > 0 THEN
                IF this.views[jkey].inside.curr_interact = INTERACT_QUERY THEN
                    LET control_role = 3
                    LET line = 1
                ELSE
                    LET control_role = 2
                    LET line = 0
                END IF
                CALL this.views[jkey].PopulateComboBox(this, field, line, TRUE, control_role)
            END IF
        END IF
    END FOR
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) PopulateComboBoxWhere(field STRING, where STRING)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) Error(title STRING, message STRING)
#    Shows and logs error message
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ShowError(title STRING, message STRING)
    IF this.log_to_file THEN
        CALL errorlog(title || ": " || message)
    END IF
    CALL fgl_winmessage(title, message, "error")
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) Error(title STRING, message STRING)
################################################################################################################################


################################################################################################################################
#
#  Public function LSTRS translates the string to localized string or to human text/name
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) LSTRS(str STRING) RETURNS STRING
    RETURN this.LSTR(str, 0)
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) LSTRS(str STRING) RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Public function LSTRT translates the table name to localized string or to human text/name
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) LSTRT(str STRING) RETURNS STRING
    RETURN this.LSTR(str, 1)
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) LSTRT(str STRING) RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Public function LSTRC translates the column name to localized string or to human text/name
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) LSTRC(str STRING) RETURNS STRING
    RETURN this.LSTR(str, 2)
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) LSTRC(str STRING) RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Private function LSTR translates the string to localized string or to human text/name
#  type: 0 - any
#        1 - table name
#        2 - column name in format <table_name>.<column_name>
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) LSTR(str STRING, type INT) RETURNS STRING
    DEFINE i                 INT
    DEFINE ret, t, a         STRING
    DEFINE field             ui.AbstractUiElement
    DEFINE strField          ui.AbstractStringField
    DEFINE tableColumn       ui.TableColumn

    # Check if map has such string translation
    LET i = iCaseSearchHashmap(this.translations, str)
    IF i > 0 THEN
        RETURN this.translations.GetValue(i)
    END IF

    # Lazy searching for translating
    CASE type
        WHEN 1 # table name
            LET t = "$human$", str
            LET ret = ui.Window.GetCurrent().GetForm().ResolveTableAlias(t)
            IF NOT ret.equals(t) THEN
                LET this.translations[str] = ret
                RETURN ret
            END IF

        WHEN 2 # column name in format <table_name>.<column_name>
            LET strField = ui.AbstractStringField.ForName("lb_" || str)
            IF strField IS NOT NULL THEN
                LET ret = strField.getText()
                LET this.translations[str] = ret
                RETURN ret
            END IF

            LET field = ui.AbstractUiElement.ForName(str)
            IF field IS NOT NULL THEN
                LET tableColumn = field.GetContainer()
                IF tableColumn IS NOT NULL THEN
                    LET ret = tableColumn.getText()
                    LET this.translations[str] = ret
                    RETURN ret
                END IF
            END IF
    END CASE

    # Get localized string
    LET ret = lstr(str)
    LET this.translations[str] = ret
    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) LSTR(str STRING, type INT) RETURNS STRING
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) AddSubDialog(dlg UI.DIALOG, depends_on_table STRING) RETURNS INT
#
# Private method AddSubDialog of InteractForm object
# Adds SubDialog according to view that depends on specified table.field
# Returns the view table of the first added view
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) AddSubDialog(dlg UI.DIALOG, depends_on_table STRING) RETURNS STRING
    DEFINE i, j             INT
    DEFINE first_view, ikey STRING
    DEFINE proc             BOOL

    FOR i = 1 TO this.views.getSize()
        LET ikey = this.views.getKey(i)
        LET proc = FALSE
        IF depends_on_table IS NULL THEN
            IF this.views[ikey].inside.depends_on.GetSize() = 0 THEN
                LET proc = TRUE
            END IF
        ELSE
            FOR j = 1 TO this.views[ikey].inside.depends_on.GetSize()
                IF this.views[ikey].inside.depends_on[j].tab.equalsIgnoreCase(depends_on_table) THEN
                    LET proc = TRUE
                END IF
            END FOR
        END IF
        IF proc THEN
            IF first_view IS NULL THEN
                LET first_view = ikey
            END IF
            CALL this.views[ikey].ApplySubDialog(dlg)
            CALL this.AddSubDialog(dlg, ikey)
        END IF
    END FOR
    RETURN first_view
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) AddSubDialog(dlg UI.DIALOG, depends_on_table STRING) RETURNS INT
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) GetScreenRecordName(event STRING) RETURNS STRING
#
# Private method GetScreenRecordName of InteractForm object
# Extract subdialog name (it is a screen record name as well) from event identifier.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) GetScreenRecordName(event STRING) RETURNS (STRING, STRING)
    DEFINE split DYNAMIC ARRAY OF STRING
    CALL event.split(".") RETURNING split
    IF split.GetSize() > 1 THEN
    RETURN split[1], split[2]
    END IF
    RETURN "", event
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) GetScreenRecordName(event STRING) RETURNS STRING
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) ParseEvent(scr_rec STRING) RETURNS (STRING, INT)
#
# Private method ParseEvent of InteractForm object
# Returns the bare event name and the table of view for the specified event identifier
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ParseEvent(event STRING) RETURNS (STRING, STRING)
    DEFINE i             INT
    DEFINE e, sub, ikey  STRING
    DEFINE view_table    STRING
    DEFINE split         DYNAMIC ARRAY OF STRING

    IF hasSubDialog.getSize() = 0 THEN
        CALL hasSubDialog.append("ON ACTION")
        CALL hasSubDialog.append("ON DRAG START")
        CALL hasSubDialog.append("ON DRAG FINISHED")
        CALL hasSubDialog.append("ON DRAG ENTER")
        CALL hasSubDialog.append("ON DRAG OVER")
        CALL hasSubDialog.append("ON DROP")
        CALL hasSubDialog.append("ON CHANGE ROW")
        CALL hasSubDialog.append("ON FILL BUFFER")
        CALL hasSubDialog.append("BEFORE INPUT")
        CALL hasSubDialog.append("BEFORE CONSTRUCT")
        CALL hasSubDialog.append("BEFORE INPUT ROW")
        CALL hasSubDialog.append("BEFORE DISPLAY")
        CALL hasSubDialog.append("BEFORE ROW")
        CALL hasSubDialog.append("BEFORE INSERT")
        CALL hasSubDialog.append("BEFORE DELETE")
        CALL hasSubDialog.append("AFTER INPUT")
        CALL hasSubDialog.append("AFTER CONSTRUCT")
        CALL hasSubDialog.append("AFTER INPUT ROW")
        CALL hasSubDialog.append("AFTER DISPLAY")
        CALL hasSubDialog.append("AFTER ROW")
        CALL hasSubDialog.append("AFTER INSERT")
        CALL hasSubDialog.append("AFTER DELETE")
    END IF

    FOR i = 1 TO hasSubDialog.getSize()
        LET e = hasSubDialog[i]
        IF StartWith(event, hasSubDialog[i]) THEN
            LET sub = event.SubString(e.GetLength() + 1, event.GetLength())
            LET sub = sub.Trim()
            CALL sub.split(".") RETURNING split
            IF split.GetSize() > 1 THEN
                LET e = e, " ", split[2]
            END IF
            FOR i = 1 TO this.views.getSize()
                LET ikey = this.views.getKey(i)
                IF this.views[ikey].screen_record.equalsIgnoreCase(split[1]) THEN
                    LET view_table = ikey
                    EXIT FOR
                END IF
            END FOR
            RETURN e, view_table
        END IF
    END FOR

    RETURN event, view_table
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) ParseEvent(scr_rec STRING) RETURNS (STRING, INT)
################################################################################################################################


################################################################################################################################
#
#  Public function SetFieldValue sets the value to the defined field (form field or virtual)
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) SetFieldValue(field STRING, val VARIANT)
    DEFINE dlg    ui.Dialog
    DEFINE i, j   INT
    DEFINE ikey   STRING

    LET dlg = ui.Dialog.GetCurrent()
    CALL dlg.SetFieldValue(field, val)

    FOR i = 1 TO this.views.getSize()
        LET ikey = this.views.getKey(i)
        FOR j = 1 TO this.views[ikey].inside.fields_form.getSize()
            IF this.views[ikey].inside.fields_form[j].fname.equalsIgnoreCase(field) THEN
                CALL this.views[ikey].ManageOnChangeEvent(this, dlg, "ON CHANGE " || field)
                RETURN
            END IF
        END FOR
    END FOR
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) SetFieldValue(field STRING, val VARIANT)
################################################################################################################################


################################################################################################################################
#
#  Public function GetFieldValue returns a value of the defined field (form field or virtual)
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) GetFieldValue(field STRING) RETURNS VARIANT
    DEFINE dlg ui.Dialog
    DEFINE val VARIANT
    LET dlg = ui.Dialog.GetCurrent()
    CALL dlg.GetFieldValue(field) RETURNING val
    RETURN val
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) GetFieldValue(field STRING) RETURNS VARIANT
################################################################################################################################




#===============================================================================================================================
#
#=========================================           VIEW           ============================================================
#
#===============================================================================================================================




################################################################################################################################
# PRIVATE FUNCTION (this View) InitView(iForm InteractForm INOUT, view_table STRING) RETURNS BOOL
#
#  Private method InitView of View object
#  Returns TRUE if everything is initialized well
#
################################################################################################################################
PRIVATE FUNCTION (this View) InitView(iForm InteractForm INOUT, view_table STRING) RETURNS BOOL
    DEFINE record_fields, select_list          DYNAMIC ARRAY OF STRING
    DEFINE lookup_list, scr_records            DYNAMIC ARRAY OF STRING
    DEFINE query, col, ftype, fname, db_table  STRING
    DEFINE sql_where, tlist, lf, t, jkey       STRING
    DEFINE i, j, lookup_index, search_index    INT
    DEFINE isIfxDb                             BOOL
    DEFINE field                               ui.AbstractUiElement
    DEFINE sh                                  BASE.SqlHandle                   # SQL handler for query
    DEFINE matchRes                            util.MATCH_RESULTS
    DEFINE regex                               util.REGEX

    LET isIfxDb = db_get_database_type() = "IFX"
    INITIALIZE this.inside TO NULL

    LET this.inside.view_table = view_table
    LET this.inside.curr_interact = INTERACT_MAIN
    LET this.inside.buffer_start = 1
    LET this.inside.is_table = FALSE

    IF this.paged_mode = 0 OR this.paged_mode IS NULL THEN
        LET this.paged_mode = -1
    END IF

    IF this.input_mode = 0 OR this.input_mode IS NULL THEN
        LET this.input_mode = -1
    END IF

    IF this.pessimistic_locking = 0 OR this.pessimistic_locking IS NULL THEN
        LET this.pessimistic_locking = -1
    END IF

    IF this.confirm_accept = 0 OR this.confirm_accept IS NULL THEN
        LET this.confirm_accept = -1
    END IF

    IF this.confirm_cancel = 0 OR this.confirm_cancel IS NULL THEN
        LET this.confirm_cancel = 1
    END IF

    IF this.inside.view_table IS NULL THEN
        CALL iForm.ShowError(iForm.LSTRS("Initialize"),
                             iForm.LSTRS("The mandatory setting view_table is not defined."))
        RETURN FALSE                                         # Return FALSE because of error
    END IF

    IF this.screen_record IS NULL THEN
        CALL iForm.ShowError(iForm.LSTRS("Initialize"),
                             iForm.LSTRS("The mandatory setting screen_record is not defined."))
        RETURN FALSE                                         # Return FALSE because of error
    END IF

    LET t = ui.Window.GetCurrent().GetForm().ResolveTableAlias(this.inside.view_table)
    IF t.equalsIgnoreCase(this.inside.view_table) THEN
        LET this.inside.table_select = this.inside.view_table
        LET this.inside.table_db = this.inside.view_table
    ELSE
        LET this.inside.table_select = t || " " || this.inside.view_table
        LET this.inside.table_db = t
    END IF

# ======== Get screen record's fields ========
# Gets list of fields of the defined screen record in the form
    CALL ui.Window.GetCurrent().GetForm().getScreenRecordFields(this.screen_record) RETURNING record_fields
    IF record_fields.GetSize() = 0 THEN                      # If screen record is missing or is empty, then we can not move on
        CALL iForm.ShowError(iForm.LSTRS("Initialize"),
                    SFMT(iForm.LSTRS("Screen record '%1' is not defined in the form or it is empty in the form '%2'."), this.screen_record, iForm.form_file))
        RETURN FALSE                                         # Return FALSE because of error
    END IF

# ======== Add lookups to record_fields =========
    FOR i = 1 TO this.lookups.getSize()
      LET lf = this.lookups.getKey(i)
      LET j = iCaseSearchArray(record_fields, lf)
      IF j = 0 THEN
          CALL record_fields.append(lf)
      END IF
    END FOR
    CALL ui.Window.GetCurrent().GetForm().setScreenRecordFields(this.screen_record, record_fields)   # TODO: Maybe we need to revert this changes on exit

# ======== Identify Primary KEY =========
    CALL this.InitPrimaryKey()

# ======== Update form for managing virtual fields
    CALL this.InitVirtualFields(record_fields)

# ======== Get DataType of each form field from screen record ========
    FOR i = 1 TO record_fields.GetSize()
        LET field = ui.AbstractUiElement.ForName(record_fields[i])
        IF field IS NULL THEN                                # Field can not be found in the form
            CALL iForm.ShowError(iForm.LSTRS("Initialize"),
                        SFMT(iForm.LSTRS("Field '%1' from screen record '%2' doesn't exist in the form '%3'."), iForm.LSTRC(record_fields[i]), this.screen_record, iForm.form_file))
            RETURN FALSE                                     # Return FALSE because of error
        ELSE
            IF this.inside.single_row IS NULL THEN
                CALL GetViewType(field) RETURNING this.inside.single_row, this.inside.is_table
            END IF
        END IF

        LET db_table = field.GetFieldTable()
        IF db_table.equalsIgnoreCase(this.inside.view_table) THEN
            CALL this.InitPopulatingComboBox(iForm, record_fields[i])
            CALL select_list.append(record_fields[i])
        ELSE
            LET lookup_index = this.AppendLookup(db_table, field.GetIdentifier())
            IF lookup_index > 0 THEN                         # Not valid foreign key/lookup is ignored
                CALL lookup_list.append(record_fields[i])
                CALL select_list.append(this.inside.fields_lookup[lookup_index].query)
            ELSE
                CALL iForm.ShowError(iForm.LSTRS("Initialize"), SFMT(iForm.LSTRS("There is no rule for getting value of field '%1' in screen record '%2'."), iForm.LSTRC(record_fields[i]), this.screen_record))
                RETURN FALSE                                 # Return FALSE because of error
            END IF
        END IF
    END FOR

    FOR i = 1 TO select_list.getSize()
        IF query IS NOT NULL THEN
            LET query = query, ", "
        END IF
        LET query = query, select_list[i]
    END FOR
    LET query = "SELECT ", query, " FROM ", this.inside.table_select

    LET  sh = BASE.SqlHandle.Create()                        # Instantiate SqlHandle object
    CALL sh.Prepare(query)                                   # Prepare query

    IF sh.GetResultCount() = 0 THEN                          # Nothing can be fetch
        CALL iForm.ShowError(iForm.LSTRS("Initialize"), SFMT(iForm.LSTRS("Table '%1' doesn't exist in database or some fields are missing in the table. (%2)"), iForm.LSTRT(this.inside.view_table), query))
        RETURN FALSE                                         # Return FALSE because of error
    END IF

# Fill array with result column's names and types
    FOR i = 1 TO sh.GetResultCount()
        LET fname = record_fields[i] CLIPPED
        LET ftype = sh.GetResultType(i)

        LET this.inside.fields_form[i].fname = fname        # Define name of column
        LET this.inside.fields_form[i].ftype = ftype        # Define type of column

        IF isIfxDb AND (ftype = "Byte" OR ftype = "Text") THEN
            # Make string with list of blob fields in the form (comma separated) for SELECT statement
            IF this.inside.fields_blobs_s IS NOT NULL THEN
                LET this.inside.fields_blobs_s = this.inside.fields_blobs_s, ", "
            END IF
            LET this.inside.fields_blobs_s = this.inside.fields_blobs_s, select_list[i]
            CALL this.inside.fields_blobs.Append(fname)
            LET lookup_index = this.SearchLookup(fname)
            IF lookup_index > 0 THEN
                LET this.inside.fields_lookup[lookup_index].is_blob = TRUE
            END IF
        ELSE
            # Make string with list of NOT blob fields in the form (comma separated) for scrollable cursor
            IF this.inside.fields_scroll_s IS NOT NULL THEN
                LET this.inside.fields_scroll_s = this.inside.fields_scroll_s, ", "
            END IF
            LET this.inside.fields_scroll_s = this.inside.fields_scroll_s, select_list[i]
            CALL this.inside.fields_scroll.Append(fname)
        END IF

        IF lookup_list.Search("", fname) > 0 THEN
            CALL this.inside.fields_noentry.Append(fname)
        ELSE
            LET this.inside.fields_find[this.inside.fields_find.getSize() + 1].fname = fname
            LET this.inside.fields_find[this.inside.fields_find.getSize()].ftype = ftype

            IF iCaseSearchArray(this.inside.primary_key, fname) > 0
               OR ftype.substring(1,6) = "serial"
               OR iCaseSearchArray(this.not_update_fields, fname) > 0
            THEN
                CALL this.inside.fields_noentry.Append(fname)  # Keep a list of fields which can not be updated, because they are UNIQUE (PRIMARY KEY or SERIAL)
            ELSE
                CALL this.inside.fields_update.Append(fname)   # Keep a list of fields which can be updated
            END IF

            IF ftype.substring(1,6) ="serial" THEN
                CALL this.inside.fields_ins_db.Append(fname)   # Keep a list of fields which should be retrieved from the database as soon as you insert a new record
            ELSE
                CALL this.inside.fields_insert.Append(fname)   # Keep a list of fields which can be used for INSERT statement
            END IF
        END IF
    END FOR

    IF this.inside.primary_key.GetSize() > 0 THEN
        # Select columns from PRIMARY KEY in scrollable cursor if them are not included already
        FOR i = 1 TO this.inside.primary_key.GetSize()
            CALL this.inside.fields_where.Append(this.inside.primary_key[i])
            IF this.SearchFormField(this.inside.primary_key[i]) = 0 THEN
                IF this.inside.fields_scroll_s IS NOT NULL THEN
                    LET this.inside.fields_scroll_s = this.inside.fields_scroll_s, ", "
                END IF
                LET this.inside.fields_scroll_s = this.inside.fields_scroll_s, this.inside.primary_key[i]
                CALL this.inside.fields_scroll.Append(this.inside.primary_key[i])
            END IF
        END FOR
    ELSE # There is no PRIMARY KEY so we need to select all columns for making WHERE clause for specific (current) row
        # ======== Get DataType of each table field ========
        CALL sh.Prepare("SELECT * FROM " || this.inside.table_select) # Prepare query

        # Fill array with result column's names and types
        FOR i = 1 TO sh.GetResultCount()
            LET fname = this.inside.view_table, ".", sh.GetResultName(i) CLIPPED
            LET ftype = sh.GetResultType(i)

            # Make list of simple fields in the table
            IF NOT isIfxDb OR (ftype <> "Byte" AND ftype <> "Text") THEN
                CALL this.inside.fields_where.Append(fname)
                IF this.SearchFormField(this.inside.primary_key[i]) = 0 THEN
                    LET this.inside.fields_scroll_s = this.inside.fields_scroll_s, ", ", fname
                    CALL this.inside.fields_scroll.Append(fname)
                END IF
            END IF
        END FOR
    END IF

# Remove custom ON FILL BUFFER event/action in case it's not paged_mode
    IF this.paged_mode <= 0 THEN
        LET i = iCaseSearchHashmap(this.actions[TOP_DIALOG_ID], "ON FILL BUFFER")
        IF i > 0 THEN
            CALL this.actions[TOP_DIALOG_ID].delete(i)
        END IF
        LET i = iCaseSearchHashmap(this.actions_on_done[TOP_DIALOG_ID], "ON FILL BUFFER")
        IF i > 0 THEN
            CALL this.actions_on_done[TOP_DIALOG_ID].delete(i)
        END IF
    END IF

# Parse SQL_WHERE if there is link to value from another VIEW
    IF this.sql_where.GetLength() > 0 THEN
        CALL this.inside.depends_on.Clear()
        LET sql_where = this.sql_where
        FOR i = 1 TO iForm.views.GetSize()
            IF NOT iForm.views.getKey(i).equalsIgnoreCase(view_table) THEN
                IF tlist.GetLength() > 0 THEN
                    LET tlist = tlist, "|"
                END IF
                LET tlist = tlist, iForm.views.getKey(i)
            END IF
        END FOR

        IF tlist.GetLength() > 0 THEN
            LET i = 0

            # Find
            CALL regex.appendFlag("i")
            CALL regex.setPattern("(?:\s+|^)(?:" || this.inside.view_table || "\\.([a-zA-Z_][a-zA-Z0-9_]*)\\s*=\\s*)?(" || tlist || ")\\.([a-zA-Z_][a-zA-Z0-9_]*)(?:\\s*=\\s*" || this.inside.view_table || "\\.([a-zA-Z_][a-zA-Z0-9_]*))?(?:\s+|$)")
            WHILE TRUE
                CALL util.REGEX.search(sql_where, regex) RETURNING matchRes
                IF NOT matchRes.matched(0) THEN
                    EXIT WHILE
                END IF
                IF matchRes.str(1) IS NULL AND matchRes.str(4) IS NULL THEN
                    CONTINUE WHILE
                END IF

                LET i = i + 1
                FOR j = 1 TO iForm.views.getSize()
                    IF iForm.views.getKey(j).equalsIgnoreCase(matchRes.str(2)) THEN
                        LET jkey = iForm.views.getKey(j)
                        LET this.inside.depends_on[i].view_parent = jkey
                        IF iForm.views[jkey].inside.view_related.Search(NULL, view_table) = 0 THEN
                            CALL iForm.views[jkey].inside.view_related.append(view_table)
                        END IF
                        EXIT FOR
                    END IF
                END FOR
                LET this.inside.depends_on[i].tab = matchRes.str(2)
                LET this.inside.depends_on[i].field = matchRes.str(2), ".", matchRes.str(3)
                IF matchRes.str(1) IS NOT NULL THEN
                    LET this.inside.depends_on[i].target_field = this.inside.view_table, ".", matchRes.str(1)
                ELSE IF matchRes.str(4) IS NOT NULL THEN
                    LET this.inside.depends_on[i].target_field = this.inside.view_table, ".", matchRes.str(4)
                END IF END IF

                LET sql_where = sql_where.subString(1, matchRes.position(2))
                              , "?"
                              , sql_where.subString(matchRes.position(3) + matchRes.length(3) + 1, sql_where.getLength())
            END WHILE
        END IF

        LET this.inside.sql_where = sql_where
    END IF

    FOR i = 1 TO this.inside.depends_on.GetSize()
        IF this.inside.depends_on[i].target_field IS NULL THEN
            LET this.inside.depends_on[i].target_exist = FALSE
            LET this.inside.depends_on[i].target_ins = FALSE
        ELSE
            LET this.inside.depends_on[i].target_ins = iCaseSearchArray(this.inside.fields_insert, this.inside.depends_on[i].target_field) = 0
                                                   AND iCaseSearchArray(this.inside.primary_key, this.inside.depends_on[i].target_field) = 0
            FOR j = 1 TO this.inside.fields_form.getSize()
                IF this.inside.fields_form[j].fname.equalsIgnoreCase(this.inside.depends_on[i].target_field) THEN
                    LET this.inside.depends_on[i].target_exist = TRUE
                    EXIT FOR
                END IF
            END FOR

            IF this.inside.depends_on[i].target_exist IS NULL THEN
                LET this.inside.depends_on[i].target_exist = FALSE
            END IF
        END IF
    END FOR

    RETURN TRUE                                              # Return TRUE because of NO error
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this View) InitView() RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this View) InitPrimaryKey()
#
#  Private method InitPrimaryKey of View object
#  - Collect fields that are Primary Key depends on database type
#
################################################################################################################################
PRIVATE FUNCTION (this View) InitPrimaryKey()
    CALL this.inside.primary_key.Clear()

    CASE db_get_database_type()
        WHEN "IFX"                                           # INFORMIX database
            CALL this.InitPrimaryKeyInformix()
        WHEN "MSV"                                           # SQL Server database
            CALL this.InitPrimaryKeySQLServer()
    END CASE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this View) InitPrimaryKey()
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this View) InitPrimaryKeyInformix()
#
#  Private method InitPrimaryKeyInformix of View object
#  - Collect fields that are Primary Key for Informix database
#
################################################################################################################################
PRIVATE FUNCTION (this View) InitPrimaryKeyInformix()
    DEFINE query, col STRING
    DEFINE i          INT
    DEFINE sh         BASE.SqlHandle                         # SQL handler for query

    WHENEVER ERROR CONTINUE                                  # Don't stop on error because we will handle error

    LET query =
        "SELECT decode(si.part1,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part1  AND sc.tabid = si.tabid))),",
        "       decode(si.part2,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part2  AND sc.tabid = si.tabid))),",
        "       decode(si.part3,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part3  AND sc.tabid = si.tabid))),",
        "       decode(si.part4,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part4  AND sc.tabid = si.tabid))),",
        "       decode(si.part5,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part5  AND sc.tabid = si.tabid))),",
        "       decode(si.part6,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part6  AND sc.tabid = si.tabid))),",
        "       decode(si.part7,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part7  AND sc.tabid = si.tabid))),",
        "       decode(si.part8,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part8  AND sc.tabid = si.tabid))),",
        "       decode(si.part9,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part9  AND sc.tabid = si.tabid))),",
        "       decode(si.part10, 0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part10 AND sc.tabid = si.tabid))),",
        "       decode(si.part11, 0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part11 AND sc.tabid = si.tabid))),",
        "       decode(si.part12, 0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part12 AND sc.tabid = si.tabid))),",
        "       decode(si.part13, 0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part13 AND sc.tabid = si.tabid))),",
        "       decode(si.part14, 0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part14 AND sc.tabid = si.tabid))),",
        "       decode(si.part15, 0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part15 AND sc.tabid = si.tabid))),",
        "       decode(si.part16, 0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part16 AND sc.tabid = si.tabid))) ",
        "  FROM sysindexes si,           ",
        "       sysconstraints sc,       ",
        "       systables st             ",
        " WHERE si.idxname = sc.idxname  ",
        "   AND sc.constrtype = \"P\"    ",
        "   AND si.tabid = st.tabid      ",
        "   AND st.tabname LIKE '", this.inside.table_db, "'"

    LET  sh = BASE.SqlHandle.Create()                        # Instantiate SqlHandle object
    CALL sh.Prepare(query)
    CALL sh.Open()

    CALL sh.Fetch()
    IF sqlca.sqlcode <> NOTFOUND THEN
        FOR i = 1 TO sh.GetResultCount()                     # Process columns of primary key one by one
            LET col = sh.GetResultValue(i) CLIPPED           # Get next column name of primary key
            IF col.GetLength() > 0 THEN                      # If it's empty then there is no more columns in primary key
                CALL this.AddPrimaryKeyField(col)
            ELSE
                EXIT FOR                                     # There is no more columns in primary key
            END IF
        END FOR
    END IF

    WHENEVER ERROR STOP
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this View) InitPrimaryKeyInformix()
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this View) InitPrimaryKeySQLServer()
#
#  Private method InitPrimaryKeySQLServer of View object
#  - Collect fields that are Primary Key for SQL Server database
#
################################################################################################################################
PRIVATE FUNCTION (this View) InitPrimaryKeySQLServer()
    DEFINE query  STRING
    DEFINE sh     BASE.SqlHandle                             # SQL handler for query

    WHENEVER ERROR CONTINUE                                  # Don't stop on error because we will handle error

    LET query =
        "SELECT scol.name              ",
        "  FROM syscolumns scol,       ",
        "       sysindexkeys sik,      ",
        "       sysindexes si,         ",
        "       sysobjects so,         ",
        "       sysconstraints sc,     ",
        "       sysobjects soc         ",
        " WHERE so.xtype = 'U'         ",
        "   AND sc.id = so.id          ",
        "   AND soc.id = sc.constid    ",
        "   AND soc.name = si.name     ",
        "   AND si.indid = sik.indid   ",
        "   AND si.id = sik.id         ",
        "   AND scol.id = si.id        ",
        "   AND scol.colid = sik.colid ",
        "   AND so.name = '", this.inside.table_db, "'"

    LET  sh = BASE.SqlHandle.Create()                        # Instantiate SqlHandle object
    CALL sh.Prepare(query)
    CALL sh.Open()

    WHILE TRUE                                               # Process columns of primary key one by one
        CALL sh.Fetch()
        IF sqlca.sqlcode = NOTFOUND THEN
            EXIT WHILE                                       # There is no more columns in primary key
        END IF
        CALL this.AddPrimaryKeyField(sh.GetResultValue(1) CLIPPED)
    END WHILE

    WHENEVER ERROR STOP
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this View) InitPrimaryKeySQLServer()
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this View) AddPrimaryKeyField(col STRING)
#
# Private method AddPrimaryKeyField of View object
#  - Store column as a Primary Key in View.primary_key
#
################################################################################################################################
PRIVATE FUNCTION (this View) AddPrimaryKeyField(col STRING)
    IF col.GetLength() > 0 THEN                      # If it's empty then there is no more columns in primary key
        CALL this.inside.primary_key.Append(this.inside.view_table || "." || col)
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this View) AddPrimaryKeyField(col STRING)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this View) AppendLookup(table STRING, field STRING) RETURNS INT
#
#  Private method AppendLookup of View object
#  - Collect, resolve and make query for getting lookup values
#
################################################################################################################################
PRIVATE FUNCTION (this View) AppendLookup(table STRING, field STRING) RETURNS INT
    DEFINE formField, sql_where, where_param, lookupTable, fieldF, fieldK, s STRING
    DEFINE k                INT
    DEFINE matchRes         util.MATCH_RESULTS
    DEFINE regex            util.REGEX

    LET formField = table, ".", field

    CALL regex.appendFlag("i")

    FOR k = 1 TO this.lookups.GetSize()
        LET s = this.lookups.GetKey(k)
        # Lookup field could be defined in format "<real_form.field> (<db_table>.<db.column>)". It's useful in case 'formonly' field
        CALL regex.setPattern("^\\s*" || formField || "\\s*\\(\\s*([a-zA-Z_][a-zA-Z0-9_]*)\\.([a-zA-Z_][a-zA-Z0-9_]*)\\s*\\)\\s*$")
        LET matchRes = util.REGEX.search(s, regex)
        IF matchRes.matched(0) THEN
            LET table = matchRes.str(1)
            LET field = matchRes.str(2)
            EXIT FOR
        END IF
    END FOR

    IF k > this.lookups.GetSize() THEN
        LET k = iCaseSearchHashmap(this.lookups, formField)
    END IF

    IF k = 0 THEN
        RETURN 0
    END IF

    LET sql_where = this.lookups.GetValue(k)
    LET k = this.inside.fields_lookup.getSize() + 1

    LET lookupTable = ui.Window.GetCurrent().GetForm().ResolveTableAlias(table)
    IF NOT lookupTable.equalsIgnoreCase(table) THEN
        LET lookupTable = lookupTable || " " || table
    END IF

    LET this.inside.fields_lookup[k].field = formField
    LET this.inside.fields_lookup[k].is_blob = FALSE

    LET where_param = sql_where
    WHILE TRUE
        CALL regex.setPattern("(?:[^a-zA-Z_]|^)(" || this.inside.view_table || ")\\.([a-zA-Z_][a-zA-Z0-9_]*)(?:[^a-zA-Z_]|$)")
        LET matchRes = util.REGEX.search(where_param, regex)
        IF NOT matchRes.matched(0) THEN
            EXIT WHILE
        END IF
        CALL this.inside.fields_lookup[k].depends_on.append(matchRes.str(2))
        LET where_param = where_param.subString(1, matchRes.position(1))
                        , "?"
                        , where_param.subString(matchRes.position(2) + matchRes.length(2) + 1, where_param.getLength())
    END WHILE

    LET this.inside.fields_lookup[k].query =
           "(SELECT ", field,
            "  FROM ", lookupTable,
            " WHERE ", sql_where,
           ") ", field

    LET this.inside.fields_lookup[k].query_param = "SELECT ", field, " FROM ", lookupTable, " WHERE ", where_param

    RETURN k
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this View) AppendLookup(table STRING, field STRING) RETURNS INT
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this View) InitPopulatingComboBox(field STRING)
#
#  Public method InitPopulatingComboBox of View object
#  - Make dependencies that affect on the list of available values
#  - Populates ComboBox with values from referenced DB table
#
################################################################################################################################
PUBLIC FUNCTION (this View) InitPopulatingComboBox(iForm InteractForm INOUT, field STRING)
    DEFINE combo                     ui.ComboBox
    DEFINE tabCol                    ui.TableColumn
    DEFINE absObj                    ui.AbstractUiElement
    DEFINE distObj                   ui.DistributedObject
    DEFINE combobox_def              ComboboxDef
    DEFINE comboDet                  Combobox
    DEFINE i, c, k                   INT
    DEFINE query, sql_where          STRING
    DEFINE splitDot                  DYNAMIC ARRAY OF STRING
    DEFINE matchRes                  util.MATCH_RESULTS

    LET combo = ui.ComboBox.ForName(field)
    IF combo IS NULL THEN
        RETURN                                               # There is no ComboBox with such name
    END IF

# Make main query for getting value-title for combobox
    FOR i = 1 TO this.comboboxes.GetSize()
        LET matchRes = util.REGEX.search(this.comboboxes.GetKey(i),
                       "^\\s*(([a-zA-Z_][a-zA-Z0-9_]*)\\.([a-zA-Z_][a-zA-Z0-9_]*))\\s*=\\s*(([a-zA-Z_][a-zA-Z0-9_]*)\\.([a-zA-Z_][a-zA-Z0-9_]*))\\s*$")
        IF matchRes.matched(0) THEN
            IF field.equalsIgnoreCase(matchRes.str(1)) THEN
                LET k = 4
                EXIT FOR
            ELSE IF field.equalsIgnoreCase(matchRes.str(4)) THEN
                LET k = 1
                EXIT FOR
            END IF END IF
        END IF
    END FOR

    IF k = 0 THEN
        RETURN                                               # There is no matched combobox setting
    END IF

    LET comboDet = this.comboboxes.GetValue(i)

    LET query = "SELECT ", matchRes.str(k)
    FOR i = 1 TO comboDet.show_columns.getSize()
        LET query = query, ", ", comboDet.show_columns[i]
    END FOR
    LET query = query, " FROM ", ui.Window.GetCurrent().GetForm().ResolveTableAlias(matchRes.str(k + 1))

    CALL this.InitComboboxSqlWhere(combobox_def, comboDet.sql_where)

    LET combobox_def.foreign_key      = matchRes.str(k)
    LET combobox_def.query            = query
    LET combobox_def.combobox         = combo
    LET combobox_def.field_bare       = GetColumnName(field)

    IF this.inside.is_table THEN
        LET absObj = combo
        LET tabCol = absObj.GetContainer()
        LET combobox_def.combobox_edit    = tabCol.GetEditControl()
        LET combobox_def.combobox_control = tabCol.GetControl()
        LET combobox_def.combobox_constr  = tabCol.GetConstructControl()
    ELSE
        LET distObj = combo
        LET combobox_def.splintered = distObj.split()
    END IF
    LET this.inside.comboboxes[field].* = combobox_def.*

    CALL this.PopulateComboBox(iForm, field, 0, FALSE, 0)
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this View) InitPopulatingComboBox(field STRING)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) PopulateAllComboBox(iForm InteractForm INOUT, dependent_only BOOL, scr_line INT, filter BOOL, control_role INT)
#
#  Private method PopulateAllComboBox of VIEW object
#  - Populates All ComboBoxs with values from referenced DB table
#
#  Parameters:
#  * dependent_only (BOOL) - If TRUE then populate comboboxes that are depend on other values only
#  * scr_line       (INT ) - The index of screen line that should be populated. It should be 0 if populates all rows/lines
#  * filter         (BOOL) - Use filter from dependent field's values
#  * control_role   (INT ) - The role of combobox (0 - General/All; 1 - Common control; 2 - Edit control; 3 - Construct control)
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) PopulateAllComboBox(iForm InteractForm INOUT, dependent_only BOOL, scr_line INT, filter BOOL, control_role INT)
    DEFINE i             INT
    DEFINE combobox_def  ComboboxDef

    FOR i = 1 TO this.inside.comboboxes.GetSize()
        LET combobox_def = this.inside.comboboxes.GetValue(i)
        IF dependent_only AND combobox_def.depends_on.getSize() = 0 THEN
            CONTINUE FOR
        END IF
        CALL this.PopulateComboBox(iForm, this.inside.comboboxes.GetKey(i), scr_line, filter, control_role)
    END FOR
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) PopulateAllComboBox(iForm InteractForm INOUT, dependent_only BOOL, scr_line INT, filter BOOL, control_role INT)
################################################################################################################################


################################################################################################################################
# PUBLIC FUNCTION (this View) PopulateComboBox(iForm InteractForm INOUT, field STRING, scr_line INT, filter BOOL, control_role INT)
#
#  Public method PopulateComboBox of View object
#  - Populates ComboBox with values from referenced DB table
#
#  Parameters:
#  * field        (STRING) - The filed/combobox name
#  * scr_line     (INT   ) - The index of screen line that should be populated. It should be 0 if populates all rows/lines
#  * filter       (BOOL  ) - Use filter from dependent field's values
#  * control_role (INT   ) - The role of combobox (0 - General/All; 1 - Common control; 2 - Edit control; 3 - Construct control)
#
################################################################################################################################
PUBLIC FUNCTION (this View) PopulateComboBox(iForm InteractForm INOUT, field STRING, scr_line INT, filter BOOL, control_role INT)
    DEFINE combo         ui.ComboBox
    DEFINE combobox_def  ComboboxDef
    DEFINE i, j          INT
    DEFINE sh            BASE.SqlHandle
    DEFINE val           REFERENCE
    DEFINE query, item_label, ftype, sql_where  STRING

    LET i = iCaseSearchHashmap(this.inside.comboboxes, field)
    IF i = 0 THEN                                            # Check if the combobox exists
        RETURN
    END IF

    LET combobox_def = this.inside.comboboxes.GetValue(i)

    LET query = combobox_def.query
    IF query IS NULL THEN
        RETURN                                               # There is no query defined
    END IF

    IF this.inside.is_table THEN
        CASE control_role
            WHEN 1      # Control
                LET combo = combobox_def.combobox_control
            WHEN 2      # Edit
                LET combo = combobox_def.combobox_edit
            WHEN 3      # Construct
                LET combo = combobox_def.combobox_constr
            OTHERWISE   # All
                LET combo = combobox_def.combobox
        END CASE
    ELSE
        IF scr_line > combobox_def.splintered.getSize() THEN # Check if scr_line is valid index
            LET scr_line = 1
        END IF

        IF scr_line > 0 THEN
            LET combo = combobox_def.splintered[scr_line]
        ELSE
            LET combo = combobox_def.combobox
        END IF
    END IF

    IF combo IS NULL THEN
        RETURN                                               # There is no ComboBox with such name
    END IF

    LET sql_where = combobox_def.sql_where

    IF filter THEN
        IF sql_where IS NOT NULL THEN
            LET query = query, " WHERE ", sql_where
        END IF
    END IF

    CALL sh.Prepare(query)

    IF filter THEN
        IF sql_where IS NOT NULL THEN
            LET j = 0
            FOR i = 1 TO combobox_def.depends_on.getSize()
                LET j = sql_where.getIndexOf("?", j + 1)
                IF j > 0 THEN
                    WHENEVER ANY ERROR CONTINUE
                    CALL ui.Dialog.GetCurrent().GetFieldValue(combobox_def.depends_on[i]) RETURNING val
                    IF status <> 0 THEN
                        RETURN                                   # Combobox can not be populated, because referenced value is not constructed
                    END IF
                    WHENEVER ANY ERROR STOP
                    CALL sh.SetParameter(i, val)
                ELSE
                    EXIT FOR
                END IF
            END FOR
        END IF
    END IF

    CALL sh.Open()

    IF sqlca.sqlcode < 0 THEN                                # Check for valid query
        CALL iForm.ShowError(iForm.LSTRT("Combobox populating"),
                        SFMT(iForm.LSTRS("Populating of combobox '%1' is failed. Error code %2. %3"), iForm.LSTRC(field), sqlca.sqlcode, err_get(sqlca.sqlcode)))
        RETURN
    END IF

    CALL combo.clear()

    IF combo.GetNotNull() <> TRUE THEN
        CALL combo.AddItem(NULL, "")
    END IF

    WHILE TRUE
        CALL sh.Fetch()
        IF sqlca.sqlcode = NOTFOUND THEN
            EXIT WHILE                                       # There is no more data
        END IF

        LET item_label = ""
        FOR i = 2 TO sh.GetResultCount()
            LET ftype = sh.GetResultType(i)
            IF ftype = "Byte" OR ftype = "Text" THEN
                CONTINUE FOR                                 # Text or Byte can not be used for making title
            END IF
            IF item_label.GetLength() > 0 THEN
                LET item_label = item_label, ", "            # All labels in title are  separated by comma
            END IF
            LET item_label = item_label, sh.GetResultValue(i)
        END FOR

        CALL combo.AddItem(sh.GetResultValue(1), item_label) # Add the new ComboBox item with value and title
    END WHILE
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this View) PopulateComboBox(iForm InteractForm INOUT, field STRING, scr_line INT, filter BOOL, control_role INT)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this View) InitComboboxSqlWhere(combobox_def ComboboxDef INOUT, sql_where STRING)
#
#  Private method PopulateComboBox of View object
#  - Parse SQL_WHERE if there is link to value from another VIEW
#
################################################################################################################################
PRIVATE FUNCTION (this View) InitComboboxSqlWhere(combobox_def ComboboxDef INOUT, sql_where STRING)
    DEFINE matchRes    util.MATCH_RESULTS
    DEFINE regex       util.REGEX

    CALL combobox_def.depends_on.Clear()
    LET combobox_def.sql_where = sql_where

    IF sql_where.GetLength() = 0 THEN
        RETURN
    END IF

    CALL regex.appendFlag("i")

    WHILE TRUE
        CALL regex.setPattern("(?:[^a-zA-Z_]|^)(" || this.inside.view_table || ")\\.([a-zA-Z_][a-zA-Z0-9_]*)(?:[^a-zA-Z_]|$)")
        LET matchRes = util.REGEX.search(sql_where, regex)
        IF NOT matchRes.matched(0) THEN
            EXIT WHILE
        END IF
        CALL combobox_def.depends_on.append(matchRes.str(2))
        LET sql_where = sql_where.subString(1, matchRes.position(1))
                      , "?"
                      , sql_where.subString(matchRes.position(2) + matchRes.length(2) + 1, sql_where.getLength())
    END WHILE

    LET combobox_def.sql_where = sql_where
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this View) InitComboboxSqlWhere(combobox_def ComboboxDef INOUT, sql_where STRING)
################################################################################################################################


################################################################################################################################
#
#  Private function InitVirtualFields creates and append fields to the form for managing virtual fields from screen record
#
################################################################################################################################
PRIVATE FUNCTION (this View) InitVirtualFields(record_fields DYNAMIC ARRAY OF STRING)
    DEFINE i, j, controls_count         INT
    DEFINE distributedObj               ui.DistributedObject
    DEFINE field, prototype, parent     ui.AbstractUiElement
    DEFINE tabCol                       ui.TableColumn
    DEFINE controls                     DYNAMIC ARRAY OF ui.DistributedObject
    DEFINE parentId, tableId, columnId  STRING
    DEFINE isTable                      BOOL

    FOR i = 1 TO record_fields.GetSize()
        LET field = ui.AbstractUiElement.ForName(record_fields[i])
        IF field IS NULL THEN
            CALL this.inside.fields_virtual.Append(record_fields[i])
        ELSE
            LET prototype = field
        END IF
    END FOR

    IF prototype IS NULL THEN
        RETURN
    END IF

    LET parent = prototype.GetContainer()

    IF parent IS NULL THEN
        RETURN
    END IF

    LET tabCol = parent
    LET isTable = tabCol IS NOT NULL

    IF isTable THEN
        LET parent = tabCol.GetContainer()
        LET tableId = parent.GetIdentifier()
    ELSE
        LET distributedObj = prototype
        CALL distributedObj.Split() RETURNING controls
        LET controls_count = controls.GetSize()
        LET parentId = parent.GetIdentifier()
    END IF

    FOR i = 1 TO this.inside.fields_virtual.GetSize()
        IF isTable THEN
            LET columnId = this.inside.fields_virtual[i], "_clm"
            LET tabCol = ui.TableColumn.Create(columnId, tableId)
            LET field = ui.TextField.Create(this.inside.fields_virtual[i], columnId)
            CALL tabCol.SetVisible(FALSE)
            CALL tabCol.SetCollapsed(TRUE)
            CALL field.SetVisible(FALSE)
            CALL field.SetCollapsed(TRUE)
            CALL tabCol.Complete()
        ELSE
            FOR j = 1 TO controls_count
                LET field = ui.TextField.Create(this.inside.fields_virtual[i], parentId)
                CALL field.SetVisible(FALSE)
                CALL field.SetCollapsed(TRUE)
            END FOR
        END IF
    END FOR
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this View) InitVirtualFields(record_fields DYNAMIC ARRAY OF STRING)
################################################################################################################################


################################################################################################################################
#
#  Private function SearchFormField makes case insensitive search of record in fields_form
#
################################################################################################################################
PRIVATE FUNCTION (this View) SearchFormField(field STRING) RETURNS INT
    DEFINE i INT
    FOR i = 1 TO this.inside.fields_form.GetSize()
        IF this.inside.fields_form[i].fname.equalsIgnoreCase(field) THEN
            RETURN i
        END IF
    END FOR
    RETURN 0
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this View) SearchFormField(field STRING) RETURNS INT
################################################################################################################################


################################################################################################################################
#
#  Private function SearchLookup makes case insensitive search of record in fields_lookup
#
################################################################################################################################
PRIVATE FUNCTION (this View) SearchLookup(field STRING) RETURNS INT
    DEFINE i INT
    FOR i = 1 TO this.inside.fields_lookup.GetSize()
        IF this.inside.fields_lookup[i].field.equalsIgnoreCase(field) THEN
            RETURN i
        END IF
    END FOR
    RETURN 0
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this View) SearchLookup(field STRING) RETURNS INT
################################################################################################################################


################################################################################################################################
#
#  Public function ClearNavigationStatus clear the navigation status.
#
################################################################################################################################
PUBLIC FUNCTION (this View) ClearNavigationStatus()
    DEFINE f ui.AbstractStringField

    IF this.navigation_status IS NULL THEN
        RETURN
    END IF

    LET f = ui.AbstractStringField.ForName(this.navigation_status)
    IF f IS NOT NULL THEN
        CALL f.SetText("")
    END IF
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this View) ClearNavigationStatus()
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) GetActions(actionsOrig HASHMAP OF HASHMAP, subDialog STRING) RETURNS HASHMAP
#
# Private method GetActions of VIEW object
# Returns actions for defined subdialog
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) GetActions(actionsOrig HASHMAP OF HASHMAP, subDialog STRING) RETURNS HASHMAP
    DEFINE actions, expanded HASHMAP
    DEFINE act, s STRING
    DEFINE i, j, k, inf INT
    DEFINE fields DYNAMIC ARRAY OF STRING

    CONSTANT onAction = "ON ACTION "
    CONSTANT onChange = "ON CHANGE "

    LET actions = GetHashmapVal(actionsOrig, subDialog)
    CALL actions.CopyTo(expanded)

    FOR i = 1 TO actions.GetSize()
        LET act = actions.GetKey(i)

        # Expand INFIELD field1, field2, ...
        LET inf = act.GetIndexOf(" INFIELD ", onAction.GetLength() + 1)
        IF inf > 0 THEN
            LET expanded[act.SubString(1, inf).Trim()] = actions.GetValue(i)
        END IF

        # Expand ON CHANGE field1, field2, ...
        IF StartWith(act, onChange) THEN
            CALL act.SubString(onChange.GetLength() + 1, act.GetLength()).Split(",") RETURNING fields
            FOR j = 1 TO fields.GetSize()
                IF fields[j].Trim() = "*" THEN # ON CHANGE all fields
                    FOR k = 1 TO this.inside.fields_form.GetSize()
                        LET expanded[onChange || GetColumnName(this.inside.fields_form[k].fname)] = actions.GetValue(i)
                    END FOR
                    EXIT FOR
                END IF
                IF fields.GetSize() > 1 THEN
                    LET expanded[onChange || GetColumnName(fields[j])] = actions.GetValue(i)
                END IF
            END FOR
        END IF
    END FOR

    RETURN expanded
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) GetActions(actionsOrig HASHMAP OF HASHMAP, subDialog STRING) RETURNS HASHMAP
################################################################################################################################


################################################################################################################################
#
#  Private function ValidateData validates data before putting it into DB.
#  Returns FALSE if validation is failed.
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) ValidateData(iform InteractForm INOUT, dlg ui.Dialog) RETURNS BOOL
    DEFINE i, j, k, err      INT
    DEFINE combobox_def      ComboboxDef
    DEFINE v                 VARIANT
    DEFINE field, deps       STRING
    DEFINE sh                BASE.SqlHandle
    DEFINE val               REFERENCE
    DEFINE query, sql_where  STRING

    FOR i = 1 TO this.inside.fields_form.getSize()
        LET field = this.inside.fields_form[i].fname
        LET err = dlg.Validate(field)
        IF err <> 0 THEN           
            CALL err_print(err)
            CALL dlg.NextField(field)
            RETURN FALSE
        END IF
    END FOR

    # Validate Comboboxes
    FOR i = 1 TO this.inside.comboboxes.GetSize()
        LET combobox_def = this.inside.comboboxes.GetValue(i)
        IF combobox_def.depends_on.getSize() = 0 THEN
            CONTINUE FOR                                     # There is nothing to validate
        END IF

        LET field = this.inside.comboboxes.GetKey(i)
        LET v = dlg.GetFieldValue(field)

        IF v IS NULL THEN
            CONTINUE FOR                                     # Value is not set, ignore it
        END IF

        LET query = combobox_def.query
        IF query IS NULL THEN
            CONTINUE FOR                                     # There is no query defined
        END IF

        LET sql_where = combobox_def.sql_where

        IF sql_where IS NULL THEN
            CONTINUE FOR
        END IF

        LET query = query, " WHERE ", sql_where, " AND ", combobox_def.foreign_key, " = ?"

        CALL sh.Prepare(query)

        LET j = 0
        FOR k = 1 TO combobox_def.depends_on.getSize()
            LET j = sql_where.getIndexOf("?", j + 1)
            IF j > 0 THEN
                WHENEVER ANY ERROR CONTINUE
                CALL dlg.GetFieldValue(combobox_def.depends_on[k]) RETURNING val
                IF status <> 0 THEN
                    CALL iform.ShowError(iform.LSTRT("Validating"), SFMT(iform.LSTRS("Validating of conditional lookup combobox '%1' is failed. Value of field '%2' can not be retrived"), iform.LSTRC(field), combobox_def.depends_on[k]))
                    RETURN FALSE
                END IF
                WHENEVER ANY ERROR STOP
                CALL sh.SetParameter(k, val)
            ELSE
                EXIT FOR
            END IF
        END FOR
        CALL sh.SetParameter(k, v)

        CALL sh.Open()

        IF sqlca.sqlcode < 0 THEN                            # Check for valid query
            CALL iform.ShowError(iform.LSTRT("Validating"), SFMT(iform.LSTRS("Validating of conditional lookup combobox '%1' is failed. Error code %2. %3"), iform.LSTRC(field), sqlca.sqlcode, err_get(sqlca.sqlcode)))
            RETURN FALSE
        END IF

        CALL sh.Fetch()
        IF sqlca.sqlcode = NOTFOUND THEN                     # Value is not valid, show the error message
            FOR j = 1 TO combobox_def.depends_on.getSize()
                IF deps IS NULL THEN
                    LET deps = combobox_def.depends_on[j]
                ELSE
                    IF j = combobox_def.depends_on.getSize() THEN
                        LET deps = deps, ", "
                    ELSE
                        LET deps = deps, " and "
                    END IF
                    LET deps = deps, combobox_def.depends_on[j]
                END IF
            END FOR
            CALL iform.ShowError(iform.LSTRS("Validating"),
                            SFMT(iform.LSTRS("%1 value '%2' is not valid for the %3"),
                            field, v, deps))
            CALL dlg.NextField(field)
            RETURN FALSE
        END IF
    END FOR

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) ValidateData(iform InteractForm INOUT, dlg ui.Dialog) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) ApplySubDialog(dlg UI.DIALOG)
#
# Private method ApplySubDialog of VIEW object
# Adds SubDialog according to view
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) ApplySubDialog(dlg UI.DIALOG)
    DEFINE exec_before, exec_after, view_attributes HASHMAP
    DEFINE absField    ui.AbstractUiElement
    DEFINE tblColumn   ui.TableColumn
    DEFINE i           INT

    CALL GetHashmapVal(this.view_attributes, TOP_DIALOG_ID) RETURNING view_attributes
    CALL this.GetActions(this.actions, TOP_DIALOG_ID) RETURNING exec_before
    CALL this.GetActions(this.actions_on_done, TOP_DIALOG_ID) RETURNING exec_after

    IF this.input_mode > 0 THEN
        # INPUT ARRAY
        CALL dlg.addInputArrayFrom(this.inside.fields_form, this.screen_record)

        CALL AddTrigger(dlg, exec_before, "ON ACTION Accept")    # Add custom ON ACTION ACCEPT action

        IF this.paged_mode <= 0 AND NOT this.inside.single_row THEN  # Bundle update should be performed
            CALL AddTrigger(dlg, exec_before, "ON ACTION Save")  # Add custom ON ACTION SAVE action
            CALL AddTrigger(dlg, exec_before, "ON ACTION Reset") # Add custom ON ACTION RESET action
        END IF
    ELSE
        # DISPLAY ARRAY
        CALL dlg.addDisplayArrayTo(this.inside.fields_form, this.screen_record)

        IF GetAttributeBool(view_attributes, "INSERT ROW", TRUE) THEN
            CALL AddTrigger(dlg, exec_before, "ON INSERT")       # Add custom ON INSERT action
        END IF

        IF GetAttributeBool(view_attributes, "APPEND ROW", TRUE) THEN
            CALL AddTrigger(dlg, exec_before, "ON APPEND")       # Add custom ON INSERT action
        END IF

        IF GetAttributeBool(view_attributes, "DELETE ROW", TRUE) THEN
            CALL AddTrigger(dlg, exec_before, "ON DELETE")       # Add custom ON DELETE action
        END IF

        IF this.inside.fields_update.GetSize() > 0 THEN
            CALL AddTrigger(dlg, exec_before, "ON UPDATE")       # Add custom ON UPDATE action
        END IF

        IF this.inside.single_row THEN
            CALL dlg.SetDialogAttribute("CURRENT ROW DISPLAY", "DIM")
        END IF
    END IF

    CALL AddTrigger(dlg, exec_before, "ON ACTION QUERY")         # Add custom ON ACTION QUERY action
    CALL AddTrigger(dlg, exec_before, "ON ACTION Cancel")        # Add custom CANCEL action
    CALL AddTrigger(dlg, exec_before, "ON ACTION Refresh")       # Add custom REFRESH action

    FOR i = 1 TO exec_before.getSize()
        CALL AddTrigger(dlg, exec_before, exec_before.GetKey(i))         # Add custom action "before event"
    END FOR

    FOR i = 1 TO exec_after.getSize()
        CALL AddTrigger(dlg, exec_after, exec_after.GetKey(i))   # Add custom action "after event"
    END FOR

    IF this.paged_mode > 0 THEN
        CALL AddTrigger(dlg, exec_before, "ON FILL BUFFER")      # Add custom ON FILL BUFFER action

        IF this.inside.is_table THEN
            # Set all table columns unsortable, because it's virtualizing data
            FOR i = 1 TO this.inside.fields_form.GetSize()
                LET absField = ui.AbstractUiElement.ForName(this.inside.fields_form[i].fname)
                IF absField IS NOT NULL THEN
                    LET tblColumn = absField.GetContainer()
                    IF tblColumn IS NOT NULL THEN
                        CALL tblColumn.SetUnsortable(TRUE)
                    END IF
                END IF
            END FOR
        END IF
    END IF

    CALL dlg.SetDialogAttribute("COUNT", -1)
    CALL SetAttributes(dlg, view_attributes)
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) ApplySubDialog(dlg UI.DIALOG)
################################################################################################################################


################################################################################################################################
#
#  Private function GetWhereClause creates WHERE clause for the main query
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) GetWhereClause() RETURNS STRING
    DEFINE where_clause STRING

    LET where_clause = this.sql_where_search

    IF this.inside.sql_where IS NOT NULL THEN
        IF where_clause IS NOT NULL THEN
            LET where_clause = where_clause, " AND "
        END IF
        LET where_clause = where_clause, this.inside.sql_where
    END IF

    IF where_clause IS NULL THEN
        LET where_clause = " "                                   # Avoid NULL for concatenation
    ELSE
        LET where_clause = " WHERE ", where_clause
    END IF

    RETURN where_clause
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) GetWhereClause() RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Private function GetOrderBy creates ORDER BY clause for the main query
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) GetOrderBy() RETURNS STRING
    DEFINE order_by_clause STRING
    LET order_by_clause = " "                                    # Avoid NULL for concatenation

    IF this.sql_order_by IS NOT NULL THEN
        LET order_by_clause = " ORDER BY ", this.sql_order_by
    END IF

    RETURN order_by_clause
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) GetOrderBy() RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Private function GetTopClause creates FIRST or TOP clause for the main query
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) GetTopClause() RETURNS STRING
    DEFINE top_clause STRING
    LET top_clause = " "                                         # Avoid NULL for concatenation

    IF this.sql_top > 0 THEN
        CASE db_get_database_type()
            WHEN "IFX"                                           # INFORMIX database
                LET top_clause = " FIRST ", this.sql_top
            WHEN "MSV"                                           # SQL Server database
                LET top_clause = " TOP ", this.sql_top
        END CASE
    END IF

    RETURN top_clause
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) GetTopClause() RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Private function GetLimitClause creates LIMITS or FETCH FIRST clause for the main query
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) GetLimitClause() RETURNS STRING
    DEFINE limit_clause STRING
    LET limit_clause = " "                                       # Avoid NULL for concatenation

    IF this.sql_top > 0 THEN
        CASE db_get_database_type()
            WHEN "MYS"                                           # MySQL database
                LET limit_clause = " LIMIT ", this.sql_top
            WHEN "ORA"                                           # ORACLE Server database
                LET limit_clause = " FETCH FIRST ", this.sql_top, " ROWS ONLY"
        END CASE
    END IF

    RETURN limit_clause
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) GetLimitClause() RETURNS STRING
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) GetWhereClauseForRow(index INT) RETURNS STRING
#
# Private method GetWhereClauseForRow of VIEW object
# Makes and returns full WHERE clause for the particular current row.
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) GetWhereClauseForRow(index INT) RETURNS STRING
    RETURN this.GetWhereClauseForRowBuf(this.inside.buffer[index])
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) GetWhereClauseForRow(index INT) RETURNS STRING
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) GetWhereClauseForRowBuf(buf HASHMAP) RETURNS STRING
#
# Private method GetWhereClauseForRowBuf of VIEW object
# Makes and returns full WHERE clause for the particular current row.
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) GetWhereClauseForRowBuf(buf HASHMAP) RETURNS STRING
    DEFINE ret, tmp STRING
    DEFINE i        INT

    FOR i = 1 TO this.inside.fields_where.GetSize()        # For each column
        IF buf[this.inside.fields_where[i]] IS NULL THEN
            LET tmp = "IS NULL"                              # Check for NULL value
        ELSE
            LET tmp = " = ?"                                 # Create constrain using field's variable
        END IF
        IF tmp.GetLength() > 0 THEN
            IF ret.GetLength() > 0 THEN
                LET ret = ret, " AND "
            END IF
            LET ret = ret, this.inside.fields_where[i] CLIPPED, " ", tmp
        END IF
    END FOR

    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) GetWhereClauseForRowBuf(buf HASHMAP) RETURNS STRING
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) SetWhereClauseParametersForRow(sh BASE.SqlHandle, param INT, index INT)
#
# Private method SetWhereClauseParametersForRow of VIEW object
# Set parameters for SqlHandler for WHERE clause for the specific row.
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) SetWhereClauseParametersForRow(sh BASE.SqlHandle, param INT, index INT)
    CALL this.SetWhereClauseParametersForRowBuf(sh, param, this.inside.buffer[index])
END FUNCTION
################################################################################################################################
# PRIVATE FUNCTION (this VIEW) SetWhereClauseParametersForRow(sh BASE.SqlHandle, param INT, index INT)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) SetWhereClauseParametersForRowBuf(sh BASE.SqlHandle, param INT, buf HASHMAP)
#
# Private method SetWhereClauseParametersForRowBuf of VIEW object
# Set parameters for SqlHandler for WHERE clause for the specific row.
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) SetWhereClauseParametersForRowBuf(sh BASE.SqlHandle, param INT, buf HASHMAP)
    DEFINE i INT

    FOR i = 1 TO this.inside.fields_where.GetSize()        # For each column
        IF buf[this.inside.fields_where[i]] IS NOT NULL THEN
            CALL sh.SetParameter(param, buf[this.inside.fields_where[i]])
            LET param = param + 1
        END IF
    END FOR
END FUNCTION
################################################################################################################################
# PRIVATE FUNCTION (this VIEW) SetWhereClauseParametersForRowBuf(sh BASE.SqlHandle, param INT, buf HASHMAP)
################################################################################################################################


################################################################################################################################
#
#  Private function UpdateCount updates the variable internal.arr_count with count of rows.
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) UpdateCount()
    DEFINE sh                BASE.SqlHandle
    DEFINE sql_count         STRING
    DEFINE i                 INT

    LET sql_count = "SELECT COUNT(*) FROM ", this.inside.table_select, this.GetWhereClause()

    IF db_get_database_type() = "MSV" THEN
        LET sql_count = sql_count, " WITH (NOLOCK)"
    END IF

    SET ISOLATION TO DIRTY READ                              # Set such isolation for geting count even if there is locked row

    LET  sh = BASE.SqlHandle.Create()
    CALL sh.Prepare(sql_count)
    FOR i = 1 TO this.inside.depends_on.getSize()
        CALL sh.SetParameter(1, this.inside.depends_on[i].val)
    END FOR
    CALL sh.Open()
    CALL sh.Fetch()
    LET this.inside.arr_count = sh.GetResultValue(1)

    SET ISOLATION TO COMMITTED READ                          # Back to default isolation

    IF this.sql_top > 0 AND this.inside.arr_count > this.sql_top THEN
        LET this.inside.arr_count = this.sql_top
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) UpdateCount()
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) SetSelectedData(iForm InteractForm, row INT)
#
# Private method Show of SetSelectedData object
# Store selected data/row and does all update dependent views.
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) SetSelectedData(iForm InteractForm, row INT)
    DEFINE i INT

    IF row < 1 OR row > this.inside.buffer.getSize() THEN
        INITIALIZE this.inside.selected_buf_i TO NULL
    ELSE
        LET this.inside.selected_buf_i = row
    END IF

    CALL this.ShowNavigationStatus(iForm)

    # Refresh dependent views
    FOR i = 1 TO this.inside.view_related.getSize()
        IF NOT iForm.views[this.inside.view_related[i]].Show(iForm, -1, -1) THEN
            RETURN FALSE
        END IF
    END FOR
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) SetSelectedData(iForm InteractForm, row INT)
################################################################################################################################


################################################################################################################################
# PUBLIC FUNCTION (this VIEW) GetSelectedData() RETURNS HASHMAP
#
# Public method GetselectedData of VIEW object
# Returns the selected data from internal buffer
#
################################################################################################################################
PUBLIC FUNCTION (this VIEW) GetSelectedData() RETURNS HASHMAP
    DEFINE ret HASHMAP

    IF this.inside.selected_buf_i IS NULL THEN
        RETURN ret
    END IF

    IF this.inside.buffer.getSize() < this.inside.selected_buf_i THEN
        RETURN ret
    END IF

    RETURN this.inside.buffer[this.inside.selected_buf_i]
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this VIEW) GetSelectedData() RETURNS HASHMAP
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) Show(iForm InteractForm INOUT, start INT, length INT) RETURNS BOOL
#
# Private method Show of InteractForm object
# Displays the defined record.
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) Show(iForm InteractForm INOUT, start INT, length INT) RETURNS BOOL
    DEFINE i, row_idx, idx        INT
    DEFINE cname, query           STRING
    DEFINE shBlobs                BASE.SqlHandle              # SqlHandle for selecting blob's data
    DEFINE dlg                    UI.DIALOG
    DEFINE total_refresh          BOOL
    DEFINE key_ref                REFERENCE
    DEFINE selected_data          HASHMAP

    IF start IS NULL OR length IS NULL THEN
        RETURN FALSE  # Skip such calling
    END IF

    LET dlg = ui.DIalog.GetCurrent()

    FOR i = 1 TO this.inside.depends_on.GetSize()
        LET selected_data = iForm.views[this.inside.depends_on[i].view_parent].GetSelectedData()
        LET idx = iCaseSearchHashmap(selected_data, this.inside.depends_on[i].field)
        IF idx > 0 THEN
            LET key_ref = selected_data.getValue(idx)
        END IF
        IF key_ref IS NULL THEN
            CALL dlg.SetArrayLength(this.screen_record, 0)
            CALL this.SetSelectedData(iForm, 0)
            RETURN FALSE
        END IF

        # Check if foreign key value in dependent view is changed
        IF this.inside.depends_on[i].val IS NULL THEN
            LET total_refresh = TRUE
        ELSE
            IF this.inside.depends_on[i].val <> key_ref THEN
                LET total_refresh = TRUE
            END IF
        END IF

        IF total_refresh THEN
            LET this.inside.depends_on[i].val = key_ref
            LET this.inside.refresh_buffer = TRUE
            LET start = -1
            LET length = -1
        END IF
    END FOR

# Check if we really need to update buffer (performance optimization)
    IF NOT this.inside.refresh_buffer THEN
        IF start = this.inside.buffer_start AND length <= this.inside.buffer.getSize() THEN
            RETURN FALSE
        END IF
    END IF

# Update count of query rows
    CALL this.UpdateCount()
    CALL dlg.SetArrayLength(this.screen_record, this.inside.arr_count)

    SET ISOLATION TO DIRTY READ

# Fetch data from DB
    LET  this.inside.sqlHandle = BASE.SqlHandle.Create()         # Instantiate the new SqlHandle object
    LET  query = "SELECT", this.GetTopClause(), " ", this.inside.fields_scroll_s
               , " FROM ", this.inside.table_select
               , this.GetWhereClause()
               , this.GetOrderBy()
               , this.GetLimitClause()
    CALL this.inside.sqlHandle.Prepare(query)                    # Prepare the query

    FOR i = 1 TO this.inside.depends_on.GetSize()
        CALL this.inside.sqlHandle.SetParameter(1, this.inside.depends_on[i].val)
    END FOR

    CALL this.inside.sqlHandle.OpenScrollCursor()                # Open scroll cursor
    LET  this.inside.refresh_buffer = FALSE

    IF this.paged_mode > 0 THEN
        IF start < 0 THEN
            LET start = 1
        END IF
        IF length < 0 THEN
            LET length = fgl_scr_size(this.screen_record)
        END IF
        IF length <= 0 THEN
            LET length = DEFAULT_ROW_COUNT
        END IF
    ELSE
        LET start  = 1                                           # Start from the first row
        LET length = this.inside.arr_count                       # Keep all rows
    END IF

    LET this.inside.buffer_start  = start                        # Keep buffer start index

    CALL this.inside.buffer.Clear()                              # Reset buffer store
    FOR row_idx = 1 TO length
        LET idx = start + row_idx - 1                            # Calculate index of fetching row
        CALL this.inside.sqlHandle.FetchAbsolute(idx)            # Fetch new row from DB
        IF sqlca.sqlcode == NOTFOUND OR status <> 0 THEN         # There is no more rows
            CALL dlg.SetArrayLength(this.screen_record, idx - 1) # Set length of DISPLAY ARRAY.
            EXIT FOR                                             # Stop fetching
        END IF
        FOR i = 1 TO this.inside.sqlHandle.GetResultCount()      # Set value in each field in the row
            LET cname = this.inside.fields_scroll[i] CLIPPED
            LET this.inside.buffer[row_idx][cname] = this.inside.sqlHandle.GetResultValue(i)
            IF this.SearchFormField(cname) > 0 THEN              # If form field exists
                CALL dlg.SetFieldValue(cname, this.inside.sqlHandle.GetResultValue(i), idx) # Fill field with value
            END IF
        END FOR

# Fetch BLOBs of particular row from DB
        IF this.inside.fields_blobs_s IS NOT NULL THEN
            LET shBlobs = BASE.SqlHandle.Create()
            CALL shBlobs.Prepare("SELECT " || this.inside.fields_blobs_s ||
                                 "  FROM " || this.inside.table_select ||
                                 " WHERE " || this.GetWhereClauseForRow(row_idx))
            CALL this.SetWhereClauseParametersForRow(shBlobs, 1, row_idx)
            CALL shBlobs.Open()
            CALL shBlobs.Fetch()
            FOR i = 1 TO shBlobs.GetResultCount()
                LET cname = this.inside.fields_blobs[i] CLIPPED
                LET this.inside.buffer[row_idx][cname] = shBlobs.GetResultValue(i)
                CALL dlg.SetFieldValue(cname, this.inside.buffer[row_idx][cname], idx)
            END FOR
        END IF
    END FOR

    SET ISOLATION TO COMMITTED READ

    IF total_refresh OR this.inside.selected_buf_i IS NULL THEN
        IF this.inside.arr_count > 0 THEN
            CALL dlg.setCurrentRow(this.screen_record, 1)
        END IF
        CALL this.SetSelectedData(iForm, 1)
    ELSE
        CALL this.SetSelectedData(iForm, this.inside.selected_buf_i) # Refresh all dependent views
    END IF

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) Show(iForm InteractForm INOUT, start INT, length INT) RETURNS BOOL
################################################################################################################################


################################################################################################################################
#
#  Private function ExecuteCustomAction check if custom action is defined and execute it
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) ExecuteCustomAction(
    iForm InteractForm INOUT,
    dialog_id STRING,
    event STRING,
    is_before_action BOOL
    ) RETURNS BOOL

    DEFINE ret          BOOL
    DEFINE i            INT
    DEFINE execFunc     FUNCTION(iform InteractForm INOUT) RETURNS BOOL
    DEFINE actions      HASHMAP

    CALL event.Replace("ON CHANGE FIELD ", "ON CHNAGE ")

    IF is_before_action THEN
        CALL this.GetActions(this.actions, dialog_id) RETURNING actions
    ELSE
        CALL this.GetActions(this.actions_on_done , dialog_id) RETURNING actions
    END IF

    LET i = iCaseSearchHashmap(actions, event)
    IF i = 0 THEN                                     # Check for custom function for current event
        RETURN FALSE
    END IF

    LET execFunc = actions.GetValue(i)                # Check if custom function is not NULL
    IF execFunc IS NULL THEN
        RETURN FALSE
    END IF

# Execute custom function for current event
    CALL execFunc(iForm) RETURNING ret

    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) ExecuteCustomAction(iForm InteractForm INOUT, dialog_id STRING, event STRING, is_before_action BOOL) RETURNS BOOL
################################################################################################################################


################################################################################################################################
#
#  Public function ShowNavigationStatus updates the navigation status in format <arr_curr>/<arr_count>.
#
################################################################################################################################
PUBLIC FUNCTION (this VIEW) ShowNavigationStatus(iForm InteractForm INOUT)
    DEFINE f ui.AbstractStringField

    IF this.navigation_status IS NULL THEN
        RETURN
    END IF

    LET f = ui.AbstractStringField.ForName(this.navigation_status)
    IF f IS NULL THEN
        CALL iForm.ShowError(iForm.LSTRS("Show Navigation Status"),
                             iForm.LSTRS("Field " || this.navigation_status || " was not found."))
        RETURN
    END IF
    IF this.inside.arr_count > 0 OR this.input_mode > 0 THEN   # arr_curr() is 1 even when arr_count() is 0, but "1/0" looks not good
        CALL f.SetText(this.inside.arr_curr || "/" || this.inside.arr_count)
    ELSE
        CALL f.SetText("0/0")
    END IF
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this VIEW) ShowNavigationStatus(iForm InteractForm INOUT)
################################################################################################################################


################################################################################################################################
#
#  Private function ActivateFieldsForInsert activates fields for inserting
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) ActivateFieldsForInsert()
    DEFINE dlg    ui.Dialog
    DEFINE field  STRING
    DEFINE i      INT

    LET dlg = ui.Dialog.GetCurrent()

    # Disable all form fields that are not in insert list
    FOR i = 1 TO this.inside.fields_form.GetSize()
        CALL dlg.SetFieldActive(this.inside.fields_form[i].fname, this.inside.fields_insert.Search("", this.inside.fields_form[i].fname) > 0)
    END FOR

    # Disable all form fields that are lookup
    FOR i = 1 TO this.inside.fields_lookup.GetSize()
        CALL dlg.SetFieldActive(this.inside.fields_lookup[i].field, FALSE)
    END FOR

    # Disable fields which are dependent on parent view
    FOR i = 1 TO this.inside.depends_on.GetSize()
        IF this.inside.depends_on[i].target_exist THEN
            CALL ui.Dialog.GetCurrent().SetFieldActive(this.inside.depends_on[i].target_field, FALSE)
        END IF
    END FOR
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) ActivateFieldsForInsert()
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) IsCurrentRowTouched(dlg ui.DIALOG) RETURNS BOOL
# Private method IsCurrentRowTouched of VIEW object
# Checks if the current row is touched
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) IsCurrentRowTouched(dlg ui.DIALOG) RETURNS BOOL
    DEFINE i INT

    FOR i = 1 TO this.inside.fields_form.GetSize()     # For each edited form field
        IF dlg.getFieldTouched(this.inside.fields_form[i].fname) THEN
            RETURN TRUE
        END IF
    END FOR

    RETURN FALSE
END FUNCTION
################################################################################################################################
# PRIVATE FUNCTION (this VIEW) IsCurrentRowTouched(dlg ui.DIALOG) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) LockRow(iForm InteractForm INOUT, index INT) RETURNS BOOL
# Private method LockRow of VIEW object
#
# Arguments:
# - index : index of row in data row buffer
#
# Returns FALSE if row is locked or being modified otherwise it returns TRUE.
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) LockRow(iForm InteractForm INOUT, index INT) RETURNS BOOL
    RETURN this.LockRowBuf(iform, this.inside.buffer[index])
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) LockRow(iForm InteractForm INOUT, index INT) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) LockRowBuf(iForm InteractForm INOUT, buf HASHMAP) RETURNS BOOL
#
# Private method LockRowBuf of VIEW object
#
# Arguments:
# - buf : row buffer
#
# Returns FALSE if row is locked or being modified otherwise it returns TRUE.
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) LockRowBuf(iForm InteractForm INOUT, buf HASHMAP) RETURNS BOOL
    DEFINE sh            BASE.SqlHandle              # SQL handler for query
    DEFINE i             INT
    DEFINE cname, ftype  STRING

    WHENEVER ERROR CONTINUE                          # Continue on error, we will handle it itself

    # Prepare sql handle FOR UPDATE
    LET  sh = BASE.SqlHandle.Create()
    CALL sh.Prepare("SELECT * FROM " || this.inside.table_select || " WHERE " || this.GetWhereClauseForRowBuf(buf) || " FOR UPDATE")
    CALL this.SetWhereClauseParametersForRowBuf(sh, 1, buf)
    CALL sh.Open()
    CALL sh.Fetch()

    IF sqlca.sqlcode = -244 THEN                     # Check if record is being modified already
        CALL iform.ShowError(iform.LSTRT(this.inside.view_table), iform.LSTRS("Another user/session is already modifying record"))
        RETURN FALSE                                 # Return FALSE that means it is being modified
    END IF

    IF sqlca.sqlcode == NOTFOUND THEN                # Check if record is being modified already
        CALL iform.ShowError(iform.LSTRT(this.inside.view_table), SFMT(iform.LSTRS("Row in the table '%1' doesn't exist any more or primary key has been modified"), iform.LSTRT(this.inside.view_table)))
        RETURN FALSE                                 # Return FALSE that means it is being modified
    END IF

    IF sqlca.sqlerrd[2] = 104 OR sqlca.sqlcode < 0 THEN
        CALL iform.ShowError(iform.LSTRT(this.inside.view_table), SFMT(iform.LSTRS("Row in the table '%1' is locked. Error code %2. %3"), iform.LSTRT(this.inside.view_table), sqlca.sqlcode, err_get(sqlca.sqlcode)))
        RETURN FALSE                                 # Return TRUE that means it is locked
    END IF

    FOR i = 1 TO sh.GetResultCount()                 # Process all columns
        LET cname = this.inside.view_table, ".", sh.GetResultName(i) CLIPPED
        IF buf.KeyExists(cname) THEN
            LET ftype = sh.GetResultType(i)
            IF NOT(ftype = "Byte" OR ftype = "Text") THEN
                IF NOT Equal(buf[cname], sh.GetResultValue(i)) THEN  # Check if data has been modified
                    CALL iform.ShowError(iform.LSTRT(this.inside.view_table), iform.LSTRS("Another user/session has already modified record"))
                    IF this.inside.curr_interact < INTERACT_INSERT THEN
                        CALL this.Refresh(iForm, FALSE)
                    END IF
                    RETURN FALSE
                END IF
            END IF
        END IF
    END FOR

    WHENEVER ERROR STOP

    RETURN TRUE                                      # Return TRUE, It means it is locked successfully
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) LockRowBuf(iForm InteractForm INOUT, buf HASHMAP) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) ExecuteInteractionInput_Save(
#    iForm InteractForm INOUT, dlg ui.Dialog, deleted DYNAMIC ARRAY OF HASHMAP, bundleUpdate BOOL, inserting BOOL)
#    RETURNS BOOL
#
# Private method ExecuteInteractionInput_Save of VIEW object
# Do all updates like Update/Insert/Delete
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) ExecuteInteractionInput_Save(
      iForm InteractForm INOUT
    , dlg ui.Dialog
    , deleted DYNAMIC ARRAY OF HASHMAP
    , bundleUpdate BOOL
    , inserting BOOL
) RETURNS BOOL
    IF bundleUpdate THEN
        RETURN this.ExecuteInteractionInput_BundleUpdate(iform, dlg, deleted)
    ELSE
        IF inserting THEN
            RETURN this.ExecuteInteractionInput_Insert(iForm, dlg)
        ELSE
            RETURN this.ExecuteInteractionInput_Update(iForm, dlg)
        END IF
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) ExecuteInteractionInput_Save(...) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) ExecuteInteractionInput_BundleUpdate(iForm InteractForm INOUT, dlg ui.Dialog, deleted DYNAMIC ARRAY OF HASHMAP)
#         RETURNS BOOL
#
# Private method ExecuteInteractionInput_BundleUpdate of VIEW object
# Do all updates like Update/Insert/Delete in bundle
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) ExecuteInteractionInput_BundleUpdate(iForm InteractForm INOUT, dlg ui.Dialog, deleted DYNAMIC ARRAY OF HASHMAP) RETURNS BOOL
    DEFINE doRollback             BOOL
    DEFINE i, arr_curr, scr_line  INT

    LET arr_curr = arr_curr()
    LET scr_line = scr_line()

    CALL this.BeginWork(iForm, TOP_DIALOG_ID)                # Start transaction

    LET int_flag = FALSE
    LET doRollback = FALSE

    # Delete rows from DB
    FOR i = 1 TO deleted.GetSize()
        IF this.LockRowBuf(iForm, deleted[i]) THEN
            LET doRollback = TRUE
            EXIT FOR
        END IF
        IF NOT this.DeleteDB(iForm, deleted[i]) THEN
            LET doRollback = TRUE
            EXIT FOR
        END IF
    END FOR

    IF NOT doRollback THEN
        FOR i = 1 TO this.inside.buffer.GetSize()
            CALL dlg.setCurrentRow(this.screen_record, i)
            IF this.inside.buffer[i].KeyExists("LLC_new") THEN
                # Validate row
                IF NOT this.ValidateData(iform, dlg) THEN
                    LET doRollback = TRUE
                    EXIT FOR
                END IF

                # Insert row
                IF NOT this.InsertDB(iform, dlg) THEN
                    LET doRollback = TRUE
                    EXIT FOR
                END IF
            ELSE IF this.inside.buffer[i].KeyExists("LLC_touched") THEN
                # Validate row
                IF NOT this.ValidateData(iform, dlg) THEN
                    LET doRollback = TRUE
                    EXIT FOR
                END IF

                # Update row
                IF this.LockRow(iForm, i) THEN
                    IF NOT this.UpdateDB(iform, dlg, i) THEN       # Update values in DB
                        LET doRollback = TRUE
                        EXIT FOR
                    END IF
                ELSE
                    LET doRollback = TRUE
                    EXIT FOR
                END IF
            END IF END IF
        END FOR
    END IF

    IF doRollback THEN                                       # There is error, so:
        CALL dlg.setCurrentRow(this.screen_record, i)
        CALL this.RollbackWork(iForm, TOP_DIALOG_ID)         # Rollback transaction
        RETURN FALSE
    END IF

    CALL fgl_dialog_setcurrline(scr_line, arr_curr)
    CALL dlg.setCurrentRow(this.screen_record, arr_curr)
    RETURN this.CommitWork(iForm, TOP_DIALOG_ID)                 # Commit transaction
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) ExecuteInteractionInput_BundleUpdate(iForm InteractForm INOUT, dlg ui.Dialog, deleted DYNAMIC ARRAY OF HASHMAP)
#             RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) ExecuteInteractionInput_Insert(iForm InteractForm INOUT, dlg ui.Dialog) RETURNS BOOL
#
# Private method ExecuteInteractionInput_Insert of VIEW object
# Inserts the new record
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) ExecuteInteractionInput_Insert(iForm InteractForm INOUT, dlg ui.Dialog) RETURNS BOOL
    DEFINE cname    STRING
    DEFINE idx, i   INT

    IF NOT this.ValidateData(iform, dlg) THEN
        RETURN FALSE
    END IF

    CALL this.BeginWork(iForm, SUB_DIALOG_INSERT_ID)         # Start transaction

    LET this.inside.refresh_buffer = TRUE                    # We need to refresh buffer next time of fetching data

    IF this.InsertDB(iform, dlg) THEN
        MESSAGE SFMT(iform.LSTRS("Insert %1  Successful operation"), iform.LSTRT(this.inside.view_table))

        IF NOT this.CommitWork(iForm, SUB_DIALOG_INSERT_ID) THEN   # Commit transaction
            RETURN FALSE
        END IF

        # Update parent array dialog with new values
        LET idx = arr_curr() - this.inside.buffer_start + 1
        CALL this.inside.buffer.insert(scr_line())
        FOR i = 1 TO this.inside.fields_form.GetSize()       # For each edited form field
            LET cname = this.inside.fields_form[i].fname
            LET this.inside.buffer[idx][cname] = dlg.GetFieldValue(cname)
            CALL dlg.setFieldTouched(cname, FALSE)
        END FOR
        RETURN TRUE
    END IF

    CALL this.RollbackWork(iForm, SUB_DIALOG_INSERT_ID)      # Rollback transaction
    CALL dlg.NextField("+CURR")                              # Remain in current row
    RETURN FALSE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) ExecuteInteractionInput_Insert(iForm InteractForm INOUT, dlg ui.Dialog) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) ExecuteInteractionInput_Update(iForm InteractForm INOUT, dlg ui.Dialog) RETURNS BOOL
#
# Private method ExecuteInteractionInput_Update of VIEW object
# Saves updates in the current record
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) ExecuteInteractionInput_Update(iForm InteractForm INOUT, dlg ui.Dialog) RETURNS BOOL
    DEFINE cname    STRING
    DEFINE idx, i   INT

    IF NOT this.ValidateData(iform, dlg) THEN
        RETURN FALSE
    END IF

    CALL this.BeginWork(iForm, SUB_DIALOG_UPDATE_ID)         # Start transaction
    LET this.inside.refresh_buffer = TRUE                    # We need to refresh buffer next time of fetching data

    LET idx = arr_curr() - this.inside.buffer_start + 1
    IF this.LockRow(iForm, idx) THEN
        IF this.UpdateDB(iform, dlg, idx) THEN               # Update values in DB
            # Updating is done well
            MESSAGE SFMT(iform.LSTRS("Edit %1  Successful operation"), iform.LSTRT(this.inside.view_table))

            IF NOT this.CommitWork(iForm, SUB_DIALOG_UPDATE_ID) THEN # Commit transaction
                RETURN FALSE
            END IF

            # Update buffer with new values
            FOR i = 1 TO this.inside.fields_update.GetSize() # For each edited form field
                LET cname = this.inside.fields_update[i]
                LET this.inside.buffer[idx][cname] = dlg.GetFieldValue(cname)
            END FOR
            RETURN TRUE
        ELSE
            CALL this.RollbackWork(iForm, SUB_DIALOG_UPDATE_ID) # Rollback transaction
            CALL dlg.NextField("+CURR")                       # Stay in current row
        END IF
    ELSE
        CALL this.RollbackWork(iForm, SUB_DIALOG_UPDATE_ID)   # Rollback transaction
        CALL dlg.NextField("+CURR")                           # Continue input in current row
    END IF
    RETURN FALSE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) ExecuteInteractionInput_Update(iForm InteractForm INOUT, dlg ui.Dialog) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) UpdateDB(iForm InteractForm INOUT, dlg ui.Dialog, index INT) RETURNS BOOL
#
# Private method UpdateDB of VIEW object
# Updates row in Database
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) UpdateDB(iForm InteractForm INOUT, dlg ui.Dialog, index INT) RETURNS BOOL
    DEFINE str_set, query  STRING
    DEFINE sh              BASE.SqlHandle
    DEFINE i               INT

    IF this.ExecuteCustomAction(iForm, SUB_DIALOG_INSERT_ID, "SQL UPDATE", TRUE) THEN
        RETURN FALSE # It was prevented be custom action
    END IF

    # Make query for updating record   UPDATE table_name SET(field1,field2,...) = (?,?,...) WHERE where_clause
    FOR i = 1 TO this.inside.fields_update.GetSize()
        IF query.GetLength() > 0 THEN
            LET str_set = str_set, ","
            LET query = query, ","
        END IF
        LET query = query, GetColumnName(this.inside.fields_update[i])
        LET str_set = str_set, "?"
    END FOR
    LET query = "UPDATE ", this.inside.table_db, " SET (", query, ") = (", str_set, ") WHERE ", this.GetWhereClauseForRow(index)

    WHENEVER ERROR CONTINUE

    # Perform the prepared UPDATE statement
    LET sh = BASE.SqlHandle.Create()
    CALL sh.Prepare(query)                                   # Prepare query

    # Set all parameters
    FOR i = 1 TO this.inside.fields_update.GetSize()         # For each edited form field
        CALL sh.SetParameter(i, dlg.GetFieldValue(this.inside.fields_update[i]))  # Set parameter
    END FOR
    CALL this.SetWhereClauseParametersForRow(sh, i, index)   # Set parameter of sql handle for current row
    CALL sh.Execute()                                        # Execute updating in the database

    WHENEVER ERROR STOP

    IF sqlca.sqlcode < 0 THEN
        # Updating is failed
        CALL iForm.ShowError(iForm.LSTRT(this.inside.view_table),
                        SFMT(iForm.LSTRS("Editing item in the table '%1' failed with the error code %2. %3"), iForm.LSTRT(this.inside.view_table), sqlca.sqlcode, err_get(sqlca.sqlcode)))
        RETURN FALSE
    END IF

    CALL this.ExecuteCustomAction(iForm, SUB_DIALOG_INSERT_ID, "SQL UPDATE", FALSE)

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) UpdateDB(iForm InteractForm INOUT, dlg ui.Dialog, index INT) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) InsertDB(iForm InteractForm INOUT, dlg ui.Dialog) RETURNS BOOL
#
#  Private method InsertDB of VIEW object
#  Inserts row to Database
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) InsertDB(iForm InteractForm INOUT, dlg ui.Dialog) RETURNS BOOL
    DEFINE str_set, query, query_ins, query_where  STRING
    DEFINE sh              BASE.SqlHandle
    DEFINE i, j            INT
    DEFINE vlist, wlist    DYNAMIC ARRAY OF REFERENCE

    IF this.ExecuteCustomAction(iForm, SUB_DIALOG_INSERT_ID, "SQL INSERT", TRUE) THEN
        RETURN FALSE                                 # It was prevented be custom action
    END IF

    # Insert new row
    WHENEVER ERROR CONTINUE

    # Make query for inserting record   INSERT INTO table_name (field1,field2,...) VALUES (?,?,...)
    FOR i = 1 TO this.inside.fields_insert.GetSize()
        IF i > 1 THEN
            LET str_set = str_set, ","
            LET query_ins = query_ins, ","
        END IF
        LET query_ins = query_ins, GetColumnName(this.inside.fields_insert[i])
        LET str_set = str_set, "?"
        CALL vlist.Append(dlg.GetFieldValue(this.inside.fields_insert[i]))
    END FOR

    # Add value that are depends on parent view
    FOR i = 1 TO this.inside.depends_on.GetSize()
        IF this.inside.depends_on[i].target_ins THEN
            LET str_set = str_set, ","
            LET query_ins = query_ins, ",", GetColumnName(this.inside.depends_on[i].target_field)
            LET str_set = str_set, "?"
            CALL vlist.Append(this.inside.depends_on[i].val)
        END IF
    END FOR

    LET query_ins = "INSERT INTO ", this.inside.table_db, " (", query_ins, ") VALUES (", str_set, ")"

    LET  sh = BASE.SqlHandle.Create()
    CALL sh.Prepare(query_ins)                       # Prepare sql handle
    FOR i = 1 TO vlist.GetSize()                     # Set parameters with values from the form/input
        CALL sh.SetParameter(i, vlist[i])
    END FOR
    CALL sh.Execute()                                # Execute INSERT into DB

    WHENEVER ERROR STOP

    IF sqlca.sqlcode < 0 THEN
        CALL iForm.ShowError(iForm.LSTRT(this.inside.view_table),
                        SFMT(iForm.LSTRS("Adding item to the table '%1' failed with the error code %2. %3"), iForm.LSTRT(this.inside.view_table), sqlca.sqlcode, err_get(sqlca.sqlcode)))
        RETURN FALSE
    END IF

    IF this.inside.fields_ins_db.getSize() > 0 THEN  # We need to get SERIAL id from DB after inserting 
        FOR i = 1 TO this.inside.fields_ins_db.GetSize()
            IF i > 1 THEN
                LET query = query, ","
            END IF
            LET query = query, GetColumnName(this.inside.fields_ins_db[i])
        END FOR

        FOR i = 1 TO this.inside.fields_insert.GetSize()
            IF iCaseSearchArray(this.inside.fields_blobs, this.inside.fields_insert[i]) = 0 THEN
                IF dlg.GetFieldValue(this.inside.fields_insert[i]) IS NOT NULL THEN
                    IF query_where.getLength() > 0 THEN
                        LET query_where = query_where, " AND "
                    END IF
                    LET query_where = query_where, dlg.getQueryFromField(this.inside.fields_insert[i])
                END IF
            END IF
        END FOR

        FOR i = 1 TO this.inside.depends_on.GetSize()
            IF this.inside.depends_on[i].target_ins THEN
                IF query_where.getLength() > 0 THEN
                    LET query_where = query_where, " AND "
                END IF
                LET query_where = query_where, GetColumnName(this.inside.depends_on[i].target_field), " = ?"
                CALL wlist.Append(this.inside.depends_on[i].val)
            END IF
        END FOR

        LET query = SFMT("SELECT %1 FROM %2 WHERE %3", query, this.inside.table_db, query_where)

        LET  sh = BASE.SqlHandle.Create()
        CALL sh.Prepare(query)                       # Prepare sql handle
        FOR i = 1 TO wlist.GetSize()                 # Set parameters with values from the form/input
            CALL sh.SetParameter(i, wlist[i])
        END FOR
        CALL sh.OpenScrollCursor()
        CALL sh.FetchLast()
        FOR i = 1 TO this.inside.fields_ins_db.GetSize()
            CALL dlg.SetFieldValue(this.inside.fields_ins_db[i], sh.GetResultValue(i))
        END FOR
    END IF

    CALL this.ExecuteCustomAction(iForm, SUB_DIALOG_INSERT_ID, "SQL INSERT", FALSE)

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) InsertDB(iForm InteractForm INOUT, dlg ui.Dialog, index INT) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) DeleteDB(iForm InteractForm INOUT, buf HASHMAP) RETURNS BOOL
#
#  Private method DeleteDB of VIEW object
#  Deletes row from buf parameter in Database
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) DeleteDB(iForm InteractForm INOUT, buf HASHMAP) RETURNS BOOL
    DEFINE sh              BASE.SqlHandle

    IF this.ExecuteCustomAction(iForm, SUB_DIALOG_INSERT_ID, "SQL DELETE", TRUE) THEN
        RETURN FALSE # It was prevented be custom action
    END IF

    # === Deleting ===
    WHENEVER ERROR CONTINUE

    LET  sh = BASE.SqlHandle.Create()
    CALL sh.Prepare("DELETE FROM " || this.inside.table_db || " WHERE " || this.GetWhereClauseForRowBuf(buf))
    CALL this.SetWhereClauseParametersForRowBuf(sh, 1, buf)  # Set parameter of sql handle for current row
    CALL sh.Execute()                                        # Execute deleting

    WHENEVER ERROR STOP

    IF sqlca.sqlcode < 0 THEN                                # Check if deleting is successful
        CALL iForm.ShowError(iForm.LSTRT(this.inside.view_table),
                        SFMT(iForm.LSTRS("Deleting from the table '%1' failed with the error code %2. %3"), iForm.LSTRT(this.inside.view_table), sqlca.sqlcode, err_get(sqlca.sqlcode)))
        RETURN FALSE
    END IF

    CALL this.ExecuteCustomAction(iForm, SUB_DIALOG_INSERT_ID, "SQL DELETE", FALSE)

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) DeleteDB(iForm InteractForm INOUT, buf HASHMAP) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) Edit(iForm InteractForm INOUT, parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
#
#  Private method Edit of InteractForm object
#  Shows INPUT interaction for editing record and saves updated values to the DB
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) Edit(iForm InteractForm INOUT, parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
    DEFINE event, cname                             STRING
    DEFINE i, idx                                   INT
    DEFINE dlg                                      UI.DIALOG
    DEFINE sh                                       BASE.SqlHandle
    DEFINE exec_before, exec_after, view_attributes HASHMAP
    DEFINE doRollback, isAccept, isTouched          BOOL
    DEFINE dlg_fields DYNAMIC ARRAY OF RECORD fname STRING, ftype STRING END RECORD # List of form fields controlled by the SubDialog for updating or inserting

    MESSAGE ""
    LET isAccept = TRUE
    LET isTouched = FALSE

    LET idx = arr_curr - this.inside.buffer_start + 1

    IF idx < 1 OR (this.paged_mode > 0 AND idx > this.inside.buffer.getSize()) THEN
        CALL fgl_winmessage(iForm.LSTRS("Selected row is out of buffer"), iForm.LSTRS("Make sure that you select the correct row for the update"), "info")
        CALL fgl_dialog_setcurrline(scr_line, arr_curr)
        RETURN
    END IF

    CALL GetHashmapVal(this.view_attributes, SUB_DIALOG_UPDATE_ID) RETURNING view_attributes
    CALL this.GetActions(this.actions, SUB_DIALOG_UPDATE_ID) RETURNING exec_before
    CALL this.GetActions(this.actions_on_done, SUB_DIALOG_UPDATE_ID) RETURNING exec_after

    LET doRollback = FALSE

    IF this.pessimistic_locking > 0 THEN
        CALL this.BeginWork(iForm, SUB_DIALOG_UPDATE_ID)     # Start transaction
        LET doRollback = TRUE                                # We need to do ROLLBACK (if no COMMIT)
        IF NOT this.LockRow(iForm, idx) THEN
            CALL this.RollbackWork(iForm, SUB_DIALOG_UPDATE_ID) # Rollback transaction
            RETURN                                           # Stop editing
        END IF
    END IF

# Make list of field names for interaction
    CALL this.inside.fields_form.copyTo(dlg_fields)
    FOR i = 1 TO dlg_fields.GetSize()
        LET dlg_fields[i].fname = dlg_fields[i].fname, "[", scr_line, "]"
    END FOR

# Create interaction dialog
    LET  dlg = ui.Dialog.CreateInputByName(dlg_fields)
    CALL AddTrigger(dlg, exec_before, "ON ACTION Accept")    # Add custom ACCEPT action
    CALL AddTrigger(dlg, exec_before, "ON ACTION Cancel")    # Add custom CANCEL action
    CALL AddTrigger(dlg, exec_before, "ON ACTION Refresh")   # Add custom REFRESH action

    FOR i = 1 TO exec_before.getSize()
        CALL AddTrigger(dlg, exec_before, exec_before.GetKey(i)) # Add custom action "before event"
    END FOR

    FOR i = 1 TO exec_after.getSize()
        CALL AddTrigger(dlg, exec_after, exec_after.GetKey(i))  # Add custom action "after event"
    END FOR

    CALL SetAttributes(dlg, view_attributes)                 # Set custom attributes

    WHILE (event := dlg.NextEvent()) IS NOT NULL             # Process dialog and wait for the next event
        CASE event
            WHEN "BEFORE DIALOG"
                # Display value in each form field in the input
                FOR i = 1 TO this.inside.fields_form.GetSize()
                    LET cname = this.inside.fields_form[i].fname
                    CALL dlg.SetFieldValue(cname, this.inside.buffer[idx][cname])
                END FOR

                CALL this.DisableNoEntryFields(FALSE)
                CALL this.PopulateAllComboBox(TRUE, scr_line, TRUE, 2)
        END CASE

        IF this.ExecuteCustomAction(iForm, SUB_DIALOG_UPDATE_ID, event, TRUE) THEN
            CONTINUE WHILE
        END IF

        CASE event
            WHEN "AFTER DIALOG"
                IF isTouched OR this.IsCurrentRowTouched(dlg) THEN
                    LET isTouched = TRUE
                    CASE this.Question(iForm.LSTRS("Modify"),
                                       iForm.LSTRS("Do you want to save changes?"),
                                       iForm.LSTRS("Yes"),
                                       SFMT("%1|%2|%3", iForm.LSTRS("Yes"), iForm.LSTRS("No"), iForm.LSTRS("Cancel")),
                                       "Question",
                                       isAccept)
                        WHEN iForm.LSTRS("Yes")                          # Updating is confirmed by user
                            IF NOT this.ValidateData(iform, dlg) THEN
                                CONTINUE WHILE
                            END IF

                            IF this.pessimistic_locking <= 0 THEN
                                CALL this.BeginWork(iForm, SUB_DIALOG_UPDATE_ID)        # Start transaction
                                IF NOT this.LockRow(iForm, idx) THEN
                                    CALL this.RollbackWork(iForm, SUB_DIALOG_UPDATE_ID) # Rollback transaction
                                    CONTINUE WHILE
                                END IF
                            END IF

                            IF this.UpdateDB(iForm, dlg, idx) THEN       # Update values in DB
                                # Updating is done well
                                MESSAGE SFMT(iForm.LSTRS("Edit %1 Successful operation"), iForm.LSTRT(this.inside.view_table))

                                LET doRollback = FALSE                   # We don't need to do ROLLBACK
                                LET int_flag = FALSE
                                LET this.inside.refresh_buffer = TRUE    # We need to refresh buffer next time of fetching data

                                IF NOT this.CommitWork(iForm, SUB_DIALOG_UPDATE_ID) THEN # Commit transaction
                                    CONTINUE WHILE
                                END IF

                                # Update parent array dialog with new values
                                FOR i = 1 TO this.inside.fields_form.GetSize()    # For each edited form field
                                    LET cname = this.inside.fields_form[i].fname
                                    LET this.inside.buffer[idx][cname] = dlg.GetFieldValue(cname)
                                    CALL parent_dlg.SetFieldValue(cname, dlg.GetFieldValue(cname), arr_curr)
                                END FOR
                            ELSE
                                IF this.pessimistic_locking <= 0 THEN
                                    CALL this.RollbackWork(iForm, SUB_DIALOG_UPDATE_ID) # Rollback transaction
                                END IF
                                CONTINUE WHILE
                            END IF

                        WHEN iForm.LSTRS("Cancel")                       # Back focus to the current field and continue updating
                            CONTINUE WHILE
                    END CASE
                END IF

                EXIT WHILE

            WHEN "ON ACTION Refresh"
                IF this.Refresh(iForm, isTouched OR this.IsCurrentRowTouched(dlg)) THEN
                    LET isTouched = FALSE                                # Reset touch attribute
                END IF

            WHEN "ON ACTION Accept"
                LET isAccept = TRUE
                CALL dlg.Accept()

            WHEN "ON ACTION Cancel"
                LET isAccept = FALSE
                CALL dlg.Cancel()

            OTHERWISE
                CALL this.ManageBuildInEvent(iForm, dlg, event)
        END CASE

        CALL this.ExecuteCustomAction(iForm, SUB_DIALOG_UPDATE_ID, event, FALSE)
    END WHILE

    IF doRollback THEN
        CALL this.RollbackWork(iForm, SUB_DIALOG_UPDATE_ID)  # Rollback transaction
    END IF

    IF NOT this.inside.is_table THEN
        CALL this.PopulateAllComboBox(TRUE, scr_line, FALSE, 2)
    END IF

    CALL dlg.Close()
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) Edit(iForm InteractForm INOUT, parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) InsertImpl(iForm InteractForm INOUT, parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
#
#  Private method Add of VIEW object
#  Prompts to input values and adds the new record.
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) InsertImpl(iForm InteractForm INOUT, parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
    DEFINE event, query, str_set, cname             STRING
    DEFINE i, j, idx                                INT
    DEFINE doCancel, isAccept, isTouched            BOOL
    DEFINE dlg                                      UI.DIALOG         # Dynamic DIALOG
    DEFINE exec_before, exec_after, view_attributes HASHMAP
    DEFINE dlg_fields DYNAMIC ARRAY OF RECORD fname STRING, ftype STRING END RECORD # List of form fields controlled by the SubDialog for updating or inserting

    MESSAGE ""

    LET doCancel = TRUE
    LET isAccept = TRUE
    LET isTouched = FALSE

    CALL GetHashmapVal(this.view_attributes, SUB_DIALOG_INSERT_ID) RETURNING view_attributes
    CALL this.GetActions(this.actions, SUB_DIALOG_INSERT_ID) RETURNING exec_before
    CALL this.GetActions(this.actions_on_done, SUB_DIALOG_INSERT_ID) RETURNING exec_after

# Make list of field names for interaction
    CALL this.inside.fields_form.copyTo(dlg_fields)
    FOR i = 1 TO dlg_fields.GetSize()
        LET dlg_fields[i].fname = dlg_fields[i].fname, "[", scr_line, "]"
    END FOR

# Create interaction dialog
    LET  dlg = ui.Dialog.CreateInputByName(dlg_fields)       # Create dynamic DIALOG with INPUT
    CALL AddTrigger(dlg, exec_before, "ON ACTION Accept")    # Add custom ACCEPT action
    CALL AddTrigger(dlg, exec_before, "ON ACTION Cancel")    # Add custom CANCEL action

    FOR i = 1 TO exec_before.getSize()
        CALL AddTrigger(dlg, exec_before, exec_before.GetKey(i)) # Add custom action "before event"
    END FOR

    FOR i = 1 TO exec_after.getSize()
        CALL AddTrigger(dlg, exec_after, exec_after.GetKey(i)) # Add custom action "after event"
    END FOR

    CALL SetAttributes(dlg, view_attributes)                 # Set custom attributes

    WHILE (event := dlg.NextEvent()) IS NOT NULL             # Process dialog and wait for the next event

        CASE event
            WHEN "BEFORE DIALOG"
                CALL this.ActivateFieldsForInsert()
                CALL this.PopulateAllComboBox(TRUE, scr_line, TRUE, 2)
                FOR i = 1 TO this.inside.fields_form.GetSize()  # For each edited form field
                    CALL dlg.SetFieldValue(this.inside.fields_form[i].fname, NULL)
                END FOR
                FOR i = 1 TO this.inside.depends_on.GetSize()
                    IF this.inside.depends_on[i].target_exist THEN
                        CALL dlg.SetFieldValue(this.inside.depends_on[i].target_field, this.inside.depends_on[i].val)
                    END IF
                END FOR
        END CASE

        IF this.ExecuteCustomAction(iForm, SUB_DIALOG_INSERT_ID, event, TRUE) THEN
            CONTINUE WHILE
        END IF

        CASE event
            WHEN "AFTER DIALOG"
                IF isTouched OR this.IsCurrentRowTouched(dlg) THEN
                    LET isTouched = TRUE
                    CASE this.Question(iForm.LSTRS("Add"),
                                       iForm.LSTRS("Add new item?"),
                                       iForm.LSTRS("Yes"),
                                       SFMT("%1|%2|%3", iForm.LSTRS("Yes"), iForm.LSTRS("No"), iForm.LSTRS("Cancel")),
                                       "Question",
                                       isAccept)
                        WHEN iForm.LSTRS("Yes")               # Inserting is confirmed
                            IF NOT this.ValidateData(iform, dlg) THEN
                                CONTINUE WHILE
                            END IF

                            CALL this.BeginWork(iForm, SUB_DIALOG_INSERT_ID) # Start transaction

                            LET this.inside.refresh_buffer = TRUE  # We need to refresh buffer next time of fetching data

                            IF this.InsertDB(iForm, dlg) THEN
                                LET doCancel = FALSE
                                MESSAGE SFMT(iForm.LSTRS("Insert %1  Successful operation"), iForm.LSTRT(this.inside.view_table))
                                IF NOT this.CommitWork(iForm, SUB_DIALOG_INSERT_ID) THEN # Commit transaction
                                    CONTINUE WHILE
                                END IF

                                LET int_flag = FALSE

                                LET idx = arr_curr - this.inside.buffer_start + 1
                                IF idx > 0 AND (this.paged_mode <= 0 OR idx <= this.inside.buffer.getSize()) THEN
                                    # Update parent array dialog with new values
                                    CALL this.inside.buffer.insert(idx)
                                    FOR i = 1 TO this.inside.fields_form.GetSize()  # For each edited form field
                                        LET cname = this.inside.fields_form[i].fname
                                        LET this.inside.buffer[idx][cname] = dlg.GetFieldValue(cname)
                                        CALL parent_dlg.SetFieldValue(cname, dlg.GetFieldValue(cname), arr_curr)
                                    END FOR
                                END IF
                            ELSE
                                CALL this.RollbackWork(iForm, SUB_DIALOG_INSERT_ID) # Rollback transaction
                                CONTINUE WHILE
                            END IF

                        WHEN iForm.LSTRS("Cancel")            # Continue adding without saving
                            CONTINUE WHILE
                    END CASE
                END IF

                EXIT WHILE

            WHEN "ON ACTION Accept"
                LET isAccept = TRUE
                CALL dlg.Accept()

            WHEN "ON ACTION Cancel"
                LET isAccept = FALSE
                CALL dlg.Cancel()

            OTHERWISE
                CALL this.ManageBuildInEvent(iForm, dlg, event)
        END CASE

        CALL this.ExecuteCustomAction(iForm, SUB_DIALOG_UPDATE_ID, event, FALSE)
    END WHILE

    CALL dlg.Close()                                         # Close and release dynamic dialog

    IF doCancel THEN
        MESSAGE ""                                           # Clear previous message
        CANCEL INSERT
    END IF

    IF NOT this.inside.is_table THEN
        CALL this.PopulateAllComboBox(TRUE, scr_line, FALSE, 2)
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) InsertImpl(iForm InteractForm INOUT, parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) DeleteImpl(iForm InteractForm INOUT, parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
#
#  Private method Delete of VIEW object
#  Deletes current record.
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) DeleteImpl(iForm InteractForm INOUT, parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
    DEFINE idx                INT

    MESSAGE ""

    LET idx = arr_curr - this.inside.buffer_start + 1

    IF idx < 1 OR (this.paged_mode > 0 AND idx > this.inside.buffer.getSize()) THEN
        CALL fgl_winmessage(iForm.LSTRS("Selected row is out of buffer"), iForm.LSTRS("Make sure that you select the correct row to delete"), "info")
        CANCEL DELETE
        RETURN
    END IF

    # Confirm deleting
    IF fgl_winbutton(iForm.LSTRS("Delete"),
                     iForm.LSTRS("Do you really want to delete this item?"),
                     iForm.LSTRS("Yes"),
                     SFMT("%1|%2", iForm.LSTRS("Yes"), iForm.LSTRS("No")),
                     "Question",
                     1) = iForm.LSTRS("No")
    THEN
        CANCEL DELETE
        RETURN                                               # Deleting is rejected
    END IF

    LET int_flag = FALSE

    CALL this.BeginWork(iForm, SUB_DIALOG_DELETE_ID)         # Start transaction
    LET this.inside.refresh_buffer = TRUE                    # We need to refresh buffer next time of fetching data

    IF NOT this.LockRow(iForm, idx) THEN
        CALL this.RollbackWork(iForm, SUB_DIALOG_DELETE_ID)  # Rollback transaction
        CANCEL DELETE
        RETURN                                               # Stop deleting
    END IF

    IF this.DeleteDB(iForm, this.inside.buffer[idx]) THEN    # Check if deleting is successful
        MESSAGE SFMT(iForm.LSTRS("Delete %1  Successful operation"), iForm.LSTRT(this.inside.view_table))
        CALL this.inside.buffer.delete(idx)
        CALL this.CommitWork(iForm, SUB_DIALOG_DELETE_ID)    # Commit transaction
    ELSE
        CANCEL DELETE
        CALL this.RollbackWork(iForm, SUB_DIALOG_DELETE_ID)  # Rollback transaction
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) DeleteImpl(iForm InteractForm INOUT, parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) Query(iForm InteractForm INOUT) RETURNS BOOL
#
# Private method Query of VIEW object
# Prompts for query restrictions and creates new WHERE clause string
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) Query(iForm InteractForm INOUT) RETURNS BOOL
    DEFINE dlg                                      UI.DIALOG # Interaction dynamic DIALOG
    DEFINE event, fname                             STRING
    DEFINE i                                        INT
    DEFINE ret                                      BOOL
    DEFINE exec_before, exec_after, view_attributes HASHMAP

    MESSAGE iForm.LSTRS("Please input query criteria")       # Show prompt message

    LET ret = FALSE

    CALL GetHashmapVal(this.view_attributes, SUB_DIALOG_QUERY_ID) RETURNING view_attributes
    CALL this.GetActions(this.actions, SUB_DIALOG_QUERY_ID) RETURNING exec_before
    CALL this.GetActions(this.actions_on_done, SUB_DIALOG_QUERY_ID) RETURNING exec_after

    LET  dlg = ui.Dialog.CreateConstructByName(this.inside.fields_form) # Create dynamic DIALOG with CONSTRUCT
    CALL AddTrigger(dlg, exec_before, "ON ACTION Accept")               # Add custom ACCEPT action
    CALL AddTrigger(dlg, exec_before, "ON ACTION Cancel")               # Add custom CANCEL action
    CALL AddTrigger(dlg, exec_before, "ON ACTION Clear" )               # Add custom CLEAR  action

    FOR i = 1 TO exec_before.getSize()
        CALL AddTrigger(dlg, exec_before, exec_before.GetKey(i))        # Add custom action "before event"
    END FOR

    FOR i = 1 TO exec_after.getSize()
        CALL AddTrigger(dlg, exec_after, exec_after.GetKey(i))          # Add custom action "after event"
    END FOR

    CALL SetAttributes(dlg, view_attributes)                            # Set custom attributes

    WHILE (event := dlg.NextEvent()) IS NOT NULL                        # Process dialog and wait for the next event
        CASE event
            WHEN "BEFORE DIALOG"
                # Disable all form fields that are lookup
                FOR i = 1 TO this.inside.fields_lookup.GetSize()
                    CALL dlg.SetFieldActive(this.inside.fields_lookup[i].field, FALSE)
                END FOR
                CALL this.PopulateAllComboBox(TRUE, 1, TRUE, 3)
                FOR i = 1 TO this.inside.construct_vals.getSize()
                    CALL dlg.SetFieldValue(this.inside.construct_vals.getKey(i), this.inside.construct_vals.GetValue(i))
                END FOR
        END CASE

        IF this.ExecuteCustomAction(iForm, SUB_DIALOG_QUERY_ID, event, TRUE) THEN
            CONTINUE WHILE
        END IF

        CASE event
            WHEN "AFTER DIALOG"
                IF ret THEN
                    LET this.sql_where_search = ""
                    CALL this.inside.construct_vals.Clear()
                    FOR i = 1 TO this.inside.fields_find.GetSize()      # Check each field
                        LET fname = this.inside.fields_find[i].fname
                        IF dlg.GetFieldValue(fname) IS NOT NULL THEN    # Check if field has been touched
                            # Add WHERE criteria
                            IF this.sql_where_search IS NOT NULL THEN
                                LET this.sql_where_search = this.sql_where_search, " AND "
                            END IF
                            LET this.sql_where_search = this.sql_where_search, dlg.GetQueryFromField(fname)

                            # Store values for reusing them on the next QUERY action
                            LET this.inside.construct_vals[fname] = dlg.GetFieldValue(fname)
                        END IF
                    END FOR
                END IF
                EXIT WHILE

            WHEN "ON ACTION Clear"                                      # Clear all fields
                FOR i = 1 TO this.inside.fields_find.GetSize()
                    CALL dlg.SetFieldValue(this.inside.fields_find[i].fname, NULL)
                END FOR

            WHEN "ON ACTION Accept"
                LET ret = TRUE
                CALL dlg.Accept()

            WHEN "ON ACTION Cancel"
                LET ret = FALSE
                CALL dlg.Cancel()

            OTHERWISE
                CALL this.ManageBuildInEvent(iForm, dlg, event)
        END CASE

        CALL this.ExecuteCustomAction(iForm, SUB_DIALOG_QUERY_ID, event, FALSE)
    END WHILE
    CALL dlg.Close()                                                    # Close and release dynamic dialog

    IF NOT this.inside.is_table THEN
        CALL this.PopulateAllComboBox(TRUE, 1, FALSE, 3)
    END IF

    MESSAGE ""                                                          # Clear any messages
    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) Query(iForm InteractForm INOUT) RETURNS BOOL
################################################################################################################################


################################################################################################################################
#
#  Public function Refresh renews all buffered data.
#
################################################################################################################################
PUBLIC FUNCTION (this VIEW) Refresh(iForm InteractForm INOUT, withQuestion BOOL) RETURNS BOOL
    DEFINE idx, i   INT
    DEFINE sh       BASE.SqlHandle
    DEFINE cname    STRING
    DEFINE dlg      ui.Dialog

    CASE this.inside.curr_interact
        WHEN INTERACT_UPDATE
            LET idx = this.inside.arr_curr - this.inside.buffer_start + 1
            IF this.IsEqualInDB(idx, sh) THEN
                RETURN FALSE                                 # There is nothing to update
            END IF

        WHEN INTERACT_INSERT
            RETURN FALSE                                     # It's inserting action, there is nothing to update

        WHEN INTERACT_QUERY
            RETURN FALSE                                     # It's query action, there is nothing to update
    END CASE

    IF withQuestion AND (this.input_mode > 0 OR this.inside.curr_interact = INTERACT_UPDATE) THEN
        IF fgl_winbutton(iForm.LSTRS("Refresh"),
                         iForm.LSTRS("Refreshing will remove all unsaved data. Do you want to proceed and refresh?"),
                         iForm.LSTRS("Refresh"),
                         SFMT("%1|%2", iForm.LSTRS("Refresh"), iForm.LSTRS("Cancel")),
                         "Question") = iForm.LSTRS("Cancel")
        THEN
            RETURN FALSE
        END IF
    END IF

    CASE this.inside.curr_interact
        WHEN INTERACT_MAIN
            LET this.inside.refresh_buffer = TRUE

            IF this.paged_mode > 0 THEN
                CALL this.Show(iForm, this.inside.buffer_start, this.inside.buffer.getSize())
            ELSE
                CALL this.Show(iForm, -1, -1)
            END IF
        WHEN INTERACT_UPDATE
            LET dlg = ui.Dialog.GetCurrent()
            # Update current data
            FOR i = 1 TO sh.GetResultCount()
                LET cname = this.inside.view_table, ".", sh.GetResultName(i) CLIPPED
                IF this.inside.buffer[idx].KeyExists(cname) THEN
                    LET this.inside.buffer[idx][cname] = sh.GetResultValue(i)
                    IF this.SearchFormField(cname) > 0 THEN
                        CALL dlg.SetFieldValue(cname, this.inside.buffer[idx][cname])
                        CALL dlg.SetFieldTouched(cname, FALSE)
                    END IF
                END IF
            END FOR
            CALL this.PopulateAllComboBox(TRUE, this.inside.scr_line, TRUE, 2)
    END CASE

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this VIEW) Refresh(iForm InteractForm INOUT, withQuestion BOOL) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) ManageBuildInEvent(iForm InteractForm INOUT, dlg ui.DIALOG, event STRING)
#
#  Private method ManageBuildInEvent of VIEW object
#  - Manage ON CHANGE event for filling lookup values
#  - Manage BEFORE FIELD event for populating combobox values
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) ManageBuildInEvent(iForm InteractForm INOUT, dlg ui.DIALOG, event STRING)
    CALL this.ManageOnChangeEvent(iForm, dlg, event)
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) ManageBuildInEvent(iForm InteractForm INOUT, dlg ui.DIALOG, event STRING)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) ManageOnChangeEvent(iForm InteractForm INOUT, dlg ui.DIALOG, event STRING)
#
#  Private method ManageOnChangeEvent of VIEW object
#  - Manage ON CHANGE event for filling lookup values
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) ManageOnChangeEvent(iForm InteractForm INOUT, dlg ui.DIALOG, event STRING)
    DEFINE i, j, k, control_role, line INT
    DEFINE sh            BASE.SqlHandle
    DEFINE combo_def     ComboboxDef
    DEFINE fname         STRING

    IF event.subString(1, 16).equals("ON CHANGE FIELD ") THEN
        LET fname = event.subString(17, event.getLength())
    ELSE IF event.subString(1, 10).equals("ON CHANGE ") THEN
        LET fname = event.subString(11, event.getLength())
    END IF END IF

    IF fname IS NULL THEN
        RETURN
    END IF

    FOR i = 1 TO this.inside.fields_lookup.getSize()
        FOR j = 1 TO this.inside.fields_lookup[i].depends_on.getSize()
            IF fname.equalsIgnoreCase(this.inside.fields_lookup[i].depends_on[j]) THEN
            # Update dependent lookup fields
                LET sh = BASE.SqlHandle.Create()
                CALL sh.Prepare(this.inside.fields_lookup[i].query_param)
                FOR k = 1 TO this.inside.fields_lookup[i].depends_on.getSize()
                    CALL sh.SetParameter(k, dlg.GetFieldValue(this.inside.fields_lookup[i].depends_on[k]))
                END FOR
                CALL sh.Open()
                CALL sh.Fetch()

                IF sqlca.sqlcode = NOTFOUND THEN
                    CALL dlg.SetFieldValue(this.inside.fields_lookup[i].field, "")
                ELSE
                    CALL dlg.SetFieldValue(this.inside.fields_lookup[i].field, sh.GetResultValue(1))
                END IF

                EXIT FOR
            END IF
        END FOR
    END FOR

    IF this.inside.curr_interact = INTERACT_QUERY THEN
        LET control_role = 3
        LET line = 1
    ELSE
        LET control_role = 2
        LET line = scr_line()
    END IF

    FOR i = 1 TO this.inside.comboboxes.getSize()
        LET combo_def = this.inside.comboboxes.GetValue(i)
        FOR j = 1 TO combo_def.depends_on.getSize()
            IF fname.equalsIgnoreCase(combo_def.depends_on[j]) THEN
                CALL this.PopulateComboBox(iForm, this.inside.comboboxes.GetKey(i), line, TRUE, control_role)
            END IF
        END FOR
    END FOR
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) ManageOnChangeEvent(iForm InteractForm INOUT, dlg ui.DIALOG, event STRING)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this VIEW) ManageAfterFieldOnInserting(dlg ui.DIALOG, event STRING)
#  Private method ManageAfterFieldOnInserting of InteractForm object
#  - Manage ON AFTER FIELD event for checking appending new row if previous append row is not touched
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) ManageAfterFieldOnInserting(dlg ui.DIALOG, event STRING)
    IF event.subString(1, 12).equals("AFTER FIELD ") THEN
        IF fgl_lastaction() = "nextrow" THEN
            IF NOT this.IsCurrentRowTouched(dlg) THEN
                CALL dlg.nextField("+CURR")
            END IF
        END IF
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) ManageAfterFieldOnInserting(dlg ui.DIALOG, event STRING)
################################################################################################################################


################################################################################################################################
#
#  Private function IsEqualInDB checks record has been changed in DB.
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) IsEqualInDB(index INT, sh BASE.SqlHandle) RETURNS BOOL
    DEFINE buf           HASHMAP
    DEFINE i             INT
    DEFINE cname, ftype  STRING

    LET buf = this.inside.buffer[index]
    LET  sh = BASE.SqlHandle.Create()
    CALL sh.Prepare("SELECT * FROM " || this.inside.table_select || " WHERE " || this.GetWhereClauseForRowBuf(buf))
    CALL this.SetWhereClauseParametersForRowBuf(sh, 1, buf)
    CALL sh.Open()
    CALL sh.Fetch()

    FOR i = 1 TO sh.GetResultCount()                 # Process all columns
        LET cname = this.inside.view_table, ".", sh.GetResultName(i) CLIPPED
        IF buf.KeyExists(cname) THEN
            LET ftype = sh.GetResultType(i)
            IF NOT(ftype = "Byte" OR ftype = "Text") THEN
                IF NOT Equal(buf[cname], sh.GetResultValue(i)) THEN  # Check if data has been modified
                    RETURN FALSE
                END IF
            END IF
        END IF
    END FOR

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) IsEqualInDB(index INT, sh BASE.SqlHandle) RETURNS BOOL
################################################################################################################################


################################################################################################################################
#
#  Private function Question shows message box with buttons
#  If it is called on ACCEPT and this.confirm_accept is FALSE then it returns default button immediately
#  If it is called on CANCEL and this.confirm_cancel is FALSE then it returns default button immediately
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) Question
(
      title     STRING
    , message   STRING
    , default   STRING
    , buttons   STRING
    , icon      STRING
    , isAccept  BOOL
) RETURNS STRING

    IF isAccept THEN
        IF this.confirm_accept < 0 THEN
            RETURN default
        END IF
    ELSE
        IF this.confirm_cancel < 0 THEN
            RETURN default
        END IF
    END IF

    RETURN fgl_winbutton(title, message, default, buttons, icon)
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) Question(....) RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Private function DisableNoEntryFields
#  It disables all fields which are related to parent view
#  It disables all no entry fields like primary keys or not_update_fields if it is not an inserting action
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) DisableNoEntryFields(isInserting BOOL)
    DEFINE i INT
    FOR i = 1 TO this.inside.fields_noentry.GetSize()
        CALL ui.Dialog.GetCurrent().SetFieldActive(this.inside.fields_noentry[i], isInserting)
    END FOR
    FOR i = 1 TO this.inside.depends_on.GetSize()
        IF this.inside.depends_on[i].target_exist THEN
            CALL ui.Dialog.GetCurrent().SetFieldActive(this.inside.depends_on[i].target_field, FALSE)
        END IF
    END FOR
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) DisableNoEntryFields(isInserting BOOL)
################################################################################################################################




#===============================================================================================================================
#
#=========================================       Help Functions     ============================================================
#
#===============================================================================================================================





################################################################################################################################
#
#  Private function iCaseSearchArray makes case insensitive search of string in DYNAMIC ARRAY OF STRING
#
################################################################################################################################
PRIVATE FUNCTION iCaseSearchArray(arr DYNAMIC ARRAY OF STRING, val STRING) RETURNS INT
    DEFINE i INT
    FOR i = 1 TO arr.GetSize()
        IF arr[i].equalsIgnoreCase(val) THEN
            RETURN i
        END IF
    END FOR
    RETURN 0
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION iCaseSearchArray(arr DYNAMIC ARRAY OF STRING, val STRING) RETURNS INT
################################################################################################################################


################################################################################################################################
#
#  PUBLIC function iCaseSearchHashmap makes case insensitive search of key in HASHMAP
#
################################################################################################################################
PUBLIC FUNCTION iCaseSearchHashmap(hash HASHMAP, key STRING) RETURNS INT
    DEFINE i INT
    DEFINE s STRING
    FOR i = 1 TO hash.GetSize()
        LET s = hash.GetKey(i)
        IF s.equalsIgnoreCase(key) THEN
            RETURN i
        END IF
    END FOR
    RETURN 0
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION iCaseSearchHashmap(arr DYNAMIC ARRAY OF STRING, val STRING) RETURNS INT
################################################################################################################################


################################################################################################################################
#
#  Private function GetViewType returns
#      1. TRUE if there is just one row is shown on the screen (means screen record with on row)
#      2. TRUE if there is Table widget is used.
#
################################################################################################################################
PRIVATE FUNCTION GetViewType(field) RETURNS (BOOL, BOOL)
    DEFINE field, parent  ui.AbstractUiElement
    DEFINE distObj        ui.DistributedObject
    DEFINE tabCol         ui.TableColumn
    DEFINE arr            DYNAMIC ARRAY OF ui.DistributedObject

    IF field IS NULL THEN
        RETURN TRUE, FALSE
    END IF

    WHENEVER ERROR CONTINUE

    LET tabCol = field.GetContainer()
    IF status = 0 AND tabCol IS NOT NULL THEN
        RETURN FALSE, TRUE
    END IF

    LET distObj = field
    LET arr = distObj.split()

    WHENEVER ERROR STOP

    RETURN arr.GetSize() < 2, FALSE
END FUNCTION
################################################################################################################################
# PRIVATE FUNCTION GetViewType(field) RETURNS BOOL, BOOL
################################################################################################################################


################################################################################################################################
#
#  Private function GetColumnName splites full field name on table name and column name and returns the column name
#
################################################################################################################################
PRIVATE FUNCTION GetColumnName(fullName STRING) RETURNS STRING
    DEFINE split DYNAMIC ARRAY OF STRING
    CALL fullName.Split(".") RETURNING split
    IF split.GetSize() > 1 THEN
        RETURN split[2].Trim()
    END IF
    RETURN fullName
END FUNCTION
################################################################################################################################
# PRIVATE FUNCTION GetColumnName(fullName STRING) RETURNS STRING
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION FindFormSettings(formSettings InteractForm_Settings INOUT, id STRING) RETURNS BOOL
#
#  Private function FindFormSettings
#  Returns TRUE if settings was found or FALSE if settings was not found
#  Returns NULL if there is no any settings
#
################################################################################################################################
PRIVATE FUNCTION FindFormSettings(formSettings InteractForm_Settings INOUT, id STRING) RETURNS BOOL
    DEFINE form_settings_str STRING
    DEFINE settingsArr       DYNAMIC ARRAY OF InteractForm_Settings
    DEFINE i                 INT

    LET form_settings_str = ui.Window.getCurrent().getInteractSettings()
    IF form_settings_str IS NOT NULL THEN
        CALL util.JSON.parse(form_settings_str, settingsArr)
        IF id IS NOT NULL THEN
            FOR i = 1 TO settingsArr.getSize()
                IF settingsArr[i].id.equalsIgnoreCase(id) THEN
                    LET formSettings.* = settingsArr[i].*
                    RETURN TRUE
                END IF
            END FOR
            RETURN FALSE
        ELSE
            IF settingsArr.getSize() > 0 THEN
                LET formSettings.* = settingsArr[1].*
                RETURN TRUE
            END IF
        END IF
    END IF

    RETURN NULL
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION FindFormSettings(formSettings InteractForm INOUT, id STRING) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION MergeHashOfHash(l HASHMAP OF HASHMAP INOUT, r HASHMAP OF HASHMAP)
#
#  Private function MergeHashOfHash
#  Merges all attribute, copies all items from 'r' to 'l' if such item is absent in 'l'
#
################################################################################################################################
PRIVATE FUNCTION MergeHashOfHash(l HASHMAP OF HASHMAP, r HASHMAP OF HASHMAP)
    DEFINE i INT
    DEFINE ikey STRING
    FOR i = 1 TO r.GetSize()
        IF l.KeyExists(ikey) THEN
            CALL l[ikey].Join(r[ikey])
        END IF
    END FOR
    CALL l.Join(r)
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION MergeHashOfHash(l HASHMAP OF HASHMAP INOUT, r HASHMAP OF HASHMAP)
################################################################################################################################


################################################################################################################################
#
#  Private function GetHashmapVal returns value related to the specific subdialog identifier
#
################################################################################################################################
PRIVATE FUNCTION GetHashmapVal(hash HASHMAP, subDialog STRING) RETURNS HASHMAP
    DEFINE h HASHMAP
    DEFINE i INT
    LET i = iCaseSearchHashmap(hash, subDialog)
    IF i > 0 THEN
        RETURN hash.GetValue(i)
    END IF
    RETURN h
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION GetHashmapVal(hash HASHMAP, subDialog STRING) RETURNS HASHMAP
################################################################################################################################


################################################################################################################################
#
#  Private function GetAttributeBool returns boolean attribute by key or defined default value if key doesn't exist
#
################################################################################################################################
PRIVATE FUNCTION GetAttributeBool(attributes HASHMAP, key STRING, def BOOL) RETURNS BOOL
    DEFINE ret BOOL
    LET ret = def
    IF attributes.KeyExists(key) THEN
        LET ret = attributes[key]
    END IF
    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION GetAttributeBool(attributes HASHMAP, key STRING, def BOOL) RETURNS BOOL
################################################################################################################################


################################################################################################################################
#
#  PUBLIC function SetAttributes adds custom attributes
#
################################################################################################################################
PUBLIC FUNCTION SetAttributes(dlg ui.Dialog, attributes HASHMAP)
    DEFINE i INT
    FOR i = 1 TO attributes.getSize()
        CALL dlg.setDialogAttribute(attributes.GetKey(i), attributes.GetValue(i)) # Add custom attribute
    END FOR
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION SetAttributes(dlg ui.Dialog, attributes HASHMAP)
################################################################################################################################


################################################################################################################################
#
#  Begin transaction
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) BeginWork(iForm InteractForm INOUT, dialog_id STRING)
    IF NOT this.ExecuteCustomAction(iForm, dialog_id, "SQL BEGIN WORK", TRUE) THEN
        BEGIN WORK
        CALL ExecRetainUpdateLocks()
    END IF
    CALL this.ExecuteCustomAction(iForm, dialog_id, "SQL BEGIN WORK", FALSE)
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) BeginWork(iForm InteractForm INOUT, dialog_id STRING)
################################################################################################################################


################################################################################################################################
#
#  Commit transaction
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) CommitWork(iForm InteractForm INOUT, dialog_id STRING) RETURNS BOOL
    DEFINE ret BOOL
    LET ret = NOT this.ExecuteCustomAction(iForm, dialog_id, "SQL COMMIT WORK", TRUE)
    IF ret THEN
        CALL DoNotRetainUpdateLocks()
        COMMIT WORK
    END IF
    CALL this.ExecuteCustomAction(iForm, dialog_id, "SQL COMMIT WORK", FALSE)
    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) CommitWork(iForm InteractForm INOUT, dialog_id STRING)
################################################################################################################################


################################################################################################################################
#
#  Rollback transaction
#
################################################################################################################################
PRIVATE FUNCTION (this VIEW) RollbackWork(iForm InteractForm INOUT, dialog_id STRING)
    IF NOT this.ExecuteCustomAction(iForm, dialog_id, "SQL ROLLBACK WORK", TRUE) THEN
        CALL DoNotRetainUpdateLocks()
        ROLLBACK WORK
    END IF
    CALL this.ExecuteCustomAction(iForm, dialog_id, "SQL ROLLBACK WORK", FALSE)
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this VIEW) RollbackWork(iForm InteractForm INOUT, dialog_id STRING)
################################################################################################################################


################################################################################################################################
#
#  Set isolation to committed read retain update locks
#
################################################################################################################################
PRIVATE FUNCTION ExecRetainUpdateLocks()
    IF db_get_database_type() = "IFX" THEN           # INFORMIX database only
        EXECUTE IMMEDIATE "SET ISOLATION TO COMMITTED READ RETAIN UPDATE LOCKS"
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION ExecRetainUpdateLocks()
################################################################################################################################


################################################################################################################################
#
#  Set isolation to committed read
#
################################################################################################################################
PRIVATE FUNCTION DoNotRetainUpdateLocks()
    IF db_get_database_type() = "IFX" THEN           # INFORMIX database only
        EXECUTE IMMEDIATE "SET ISOLATION TO COMMITTED READ"
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION DoNotRetainUpdateLocks()
################################################################################################################################


################################################################################################################################
#
#  Private function Equal check if variables are equal
#
################################################################################################################################
PRIVATE FUNCTION Equal(l REFERENCE, r REFERENCE) RETURNS BOOL
    IF l IS NULL THEN
        RETURN r IS NULL
    END IF
    IF r IS NULL THEN
        RETURN FALSE
    END IF
    RETURN l == r
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION Equal(l REFERENCE, r REFERENCE) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION StartWith(source STRING, substr STRING) RETURNS BOOL
#
# Returns TRUE if string 'source' starts with 'substr' string
#
################################################################################################################################
PRIVATE FUNCTION StartWith(source STRING, substr STRING) RETURNS BOOL
    RETURN source.SubString(1, substr.GetLength()).equalsIgnoreCase(substr)
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION StartWith(source STRING, substr STRING) RETURNS BOOL
################################################################################################################################


################################################################################################################################
#
#  Populates toolbar with buttons Update, Insert, Delete, Help, FirstRow, PrevPage, NextPage and LastRow.
#
################################################################################################################################
PRIVATE FUNCTION PopulateToolbar()
    CALL ui.ActionView.ForAction("FirstRow", "dialog").SetDefaultView("yes")
    CALL ui.ActionView.ForAction("PrevPage", "dialog").SetDefaultView("yes")
    CALL ui.ActionView.ForAction("NextPage", "dialog").SetDefaultView("yes")
    CALL ui.ActionView.ForAction("LastRow" , "dialog").SetDefaultView("yes")

    CALL ui.ActionView.ForAction("FirstRow", "dialog").SetShowInContextMenu("yes")
    CALL ui.ActionView.ForAction("PrevPage", "dialog").SetShowInContextMenu("yes")
    CALL ui.ActionView.ForAction("NextPage", "dialog").SetShowInContextMenu("yes")
    CALL ui.ActionView.ForAction("LastRow" , "dialog").SetShowInContextMenu("yes")

    CALL ui.ActionView.ForAction("FirstRow", "dialog").SetActionImage("qx://embedded/firstrow.png")
    CALL ui.ActionView.ForAction("PrevPage", "dialog").SetActionImage("qx://embedded/prevrow.png" )
    CALL ui.ActionView.ForAction("NextPage", "dialog").SetActionImage("qx://embedded/nextrow.png" )
    CALL ui.ActionView.ForAction("LastRow" , "dialog").SetActionImage("qx://embedded/lastrow.png" )
    CALL ui.ActionView.ForAction("Query"   , "dialog").SetActionImage("qx://embedded/find.png"    )

    CALL ui.ActionView.ForAction("FirstRow", "dialog").SetComment("First record")
    CALL ui.ActionView.ForAction("LastRow" , "dialog").SetComment("Last record" )

    #IF single_row THEN
        CALL ui.ActionView.ForAction("PrevPage", "dialog").SetComment("Previous record")
        CALL ui.ActionView.ForAction("NextPage", "dialog").SetComment("Next record"    )
    #END IF

    CALL ui.ActionView.ForAction("FirstRow", "dialog").SetPlace("top")
    CALL ui.ActionView.ForAction("PrevPage", "dialog").SetPlace("top")
    CALL ui.ActionView.ForAction("NextPage", "dialog").SetPlace("top")
    CALL ui.ActionView.ForAction("LastRow" , "dialog").SetPlace("top")
    CALL ui.ActionView.ForAction("Update"  , "dialog").SetPlace("top")
    CALL ui.ActionView.ForAction("Insert"  , "dialog").SetPlace("top")
    CALL ui.ActionView.ForAction("Delete"  , "dialog").SetPlace("top")
    CALL ui.ActionView.ForAction("Query"   , "dialog").SetPlace("top")

    CALL ui.ActionView.ForAction("FirstRow", "dialog").SetOrder(1)
    CALL ui.ActionView.ForAction("PrevPage", "dialog").SetOrder(2)
    CALL ui.ActionView.ForAction("NextPage", "dialog").SetOrder(3)
    CALL ui.ActionView.ForAction("LastRow" , "dialog").SetOrder(4)
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION PopulateToolbar()
################################################################################################################################


################################################################################################################################
# PUBLIC FUNCTION AddTrigger(dlg UI.DIALOG, actions HASHMAP, event STRING)
#
# Checks if custom action is not NULL then add this trigger to dialog
#
################################################################################################################################
PUBLIC FUNCTION AddTrigger(dlg UI.DIALOG, actions HASHMAP, event STRING)
    DEFINE i INT
    LET i = iCaseSearchHashmap(actions, event)
    IF i > 0 THEN
        IF actions.GetValue(i) IS NULL THEN            # Check for custom function for current event
            RETURN
        END IF
    END IF
    CALL dlg.addTrigger(event)
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION AddTrigger(dlg UI.DIALOG, actions HASHMAP, event STRING)
################################################################################################################################