########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses/Scenario
# two tables joined 1:1
########################################################################


########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE rec_contact_company_contact_settings InteractForm_Settings


  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Contact-Company-Contact")
	CALL fgl_settitle("Contact-Company-Contact")    

	MENU 
		BEFORE MENU			
			CALL fgl_dialog_setkeylabel("Contact 1 Company 1 Contact 1 rec", "Contact Company Contact 1:1:1 Rec", "{CONTEXT}/public/querix/icon/svg/24/ic_contact_24px.svg",     101,TRUE,"Display and modify contact / company details","top") 
			CALL fgl_dialog_setkeylabel("Contact 1 Company 1 Contact 1 list","Contact Company Contact 1:1:1 List","{CONTEXT}/public/querix/icon/svg/24/ic_contacts_24px.svg",102,TRUE,"Display and modify contact / company details","top") 
	
		ON ACTION "Contact 1 Company 1 Contact 1 rec"
      CALL contact_1_company_1_main_contact_1_rec(rec_contact_company_contact_settings)

		ON ACTION "Contact 1 Company 1 Contact 1 list"
      CALL contact_1_company_1_main_contact_1_list(rec_contact_company_contact_settings)
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
######################################################################## 

########################################################################
# FUNCTION contact_1_company_1_main_contact_1_rec(p_rec_settings InteractForm_Settings)
########################################################################
FUNCTION contact_1_company_1_main_contact_1_rec(p_rec_settings InteractForm_Settings)	
    LET p_rec_settings.form_file = "../relation/relation_1_contact_rec_1_company__rec_1_main_contact_rec"
		
    LET p_rec_settings.id = "1 contact 1 company 1 main contact"
    CALL InteractForm(p_rec_settings)                           
END FUNCTION
########################################################################
# END FUNCTION contact_1_company_1_main_contact_1_rec(p_rec_settings InteractForm_Settings)	
########################################################################



########################################################################
# FUNCTION contact_1_company_1_main_contact_1_list(p_rec_settings InteractForm_Settings)
########################################################################
FUNCTION contact_1_company_1_main_contact_1_list(p_rec_settings InteractForm_Settings)	
    LET p_rec_settings.form_file = "../relation/relation_1_contact_list_1_company__rec_1_main_contact_rec"
		
    LET p_rec_settings.id = "1 contact 1 company 1 main contact"
    CALL InteractForm(p_rec_settings)                           
END FUNCTION
########################################################################
# END FUNCTION contact_1_company_1_main_contact_1_list(p_rec_settings InteractForm_Settings)	
########################################################################
