########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7909
# Anomaly/Difference
# 
########################################################################
# DB Table Schema
#	CREATE TABLE contact_dept (
#	dept_id              serial                                  no
#	dept_name            char(25)                                no
#	user_def             smallint                                yes
#	)
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_rec_settings_rec InteractForm_Settings
	DEFINE l_rec_settings_list InteractForm_Settings

  DEFER INTERRUPT

	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Contact Department")
	CALL fgl_settitle("Contact Department")       

	#You could initialise the LLC settings here
	LET l_rec_settings_rec.navigation_status="nav_page_of"
	LET l_rec_settings_list.navigation_status="nav_page_of"

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("contact_dept Record","contact_dept","{CONTEXT}/public/querix/icon/svg/24/ic_country_24px.svg",101,TRUE,"contact_dept Record - Display and modify contact data","top") 
			CALL fgl_dialog_setkeylabel("contact_dept List","contact_dept List","{CONTEXT}/public/querix/icon/svg/24/ic_country_list_24px.svg",102,TRUE,"contact_dept List - List all contacts","top")			
		
		ON ACTION "contact_dept Record"
			CALL db_cms_contact_dept_rec(l_rec_settings_rec)
			
		ON ACTION "contact_dept List"
			CALL db_cms_contact_dept_list(l_rec_settings_list)
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################