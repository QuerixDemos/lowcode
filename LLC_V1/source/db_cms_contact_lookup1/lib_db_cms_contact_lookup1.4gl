########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Anomaly
# Single table but byte and char(1000) field
########################################################################
# DB Table Schema
{
          CREATE TABLE contact (
            cont_id 	SERIAL(1000) NOT NULL,
            cont_title	VARCHAR(10),
            cont_name 	VARCHAR(20) NOT NULL UNIQUE,
            cont_fname 	VARCHAR(20),
            cont_lname 	VARCHAR(20),
            cont_addr1	VARCHAR(40),
            cont_addr2	VARCHAR(40),
            cont_addr3	VARCHAR(40),
            cont_city	VARCHAR(20),
            cont_zone	VARCHAR(15),
            cont_zip	VARCHAR(15),
            cont_country	VARCHAR(40),
            cont_phone	VARCHAR(15),
            cont_fax	VARCHAR(15),
            cont_mobile	VARCHAR(15),
            cont_email	VARCHAR(50) NOT NULL UNIQUE,
            cont_dept	VARCHAR(15),	# ref cont_dept_id
            cont_org	INTEGER,	# ref company_id
            cont_position	VARCHAR(15),	# ref position_id
            cont_picture	BYTE,
            cont_password	VARCHAR(15),
            cont_ipaddr	VARCHAR(15),	# IP V4
            cont_usemail	SMALLINT,
            cont_usephone	SMALLINT,
            cont_notes   CHAR(1000),
 
            PRIMARY KEY (cont_id) CONSTRAINT contact_pk,
            FOREIGN KEY (cont_org) REFERENCES company(comp_id) CONSTRAINT cont_org_fk_company          

          )
}   
########################################################################

########################################################################
# FUNCTION db_cms_contact_rec_lookup1(p_rec_settings InteractForm_Settings)
#
# Edit Record
########################################################################
FUNCTION db_cms_contact_rec_lookup1(p_rec_settings InteractForm_Settings)
	LET p_rec_settings.form_file = "../db_cms_contact_lookup1/db_cms_contact_rec_lookup1"
	CALL InteractForm(p_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION db_cms_contact_rec_lookup1(p_rec_settings InteractForm_Settings)
########################################################################


########################################################################
# FUNCTION db_cms_contact_list_lookup1(p_rec_settings InteractForm_Settings)	
#
#
########################################################################
FUNCTION db_cms_contact_list_lookup1(p_rec_settings InteractForm_Settings)
	LET p_rec_settings.form_file = "../db_cms_contact_lookup1/db_cms_contact_list_lookup1"
	CALL InteractForm(p_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION db_cms_contact_list_lookup1(p_rec_settings InteractForm_Settings)	
########################################################################