################################################################
# DATABASE
################################################################
DATABASE cms_llc
################################################################
# GLOBAL scope variables
################################################################
#GLOBALS "../common/glob_GLOBALS.4gl"
################################################################
# MODULE scope variables
################################################################

########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7911
# Addresses:
# Simple table without FK and PK of type SERIAL 
########################################################################
# DB Table Schema
#	CREATE TABLE pay_method (
#		pay_method_id       SERIAL,
#		pay_method_name     CHAR(10)  NOT NULL UNIQUE,
#		pay_method_desc     CHAR(20) NOT NULL,
#		PRIMARY KEY (pay_method_id)
#		)
########################################################################

########################################################################
# FUNCTION db_cms_pay_method()	
#
#
########################################################################
FUNCTION db_cms_pay_method()	
	CALL InteractFormFile("../db_cms_pay_method/pay_method_l3")
END FUNCTION
########################################################################
# END FUNCTION db_cms_pay_method()	
########################################################################

########################################################################
# FUNCTION db_cms_pay_method_rec()
#
#
########################################################################
FUNCTION db_cms_pay_method_rec()
    CALL InteractFormFile("../db_cms_pay_method/db_cms_pay_method_rec")
END FUNCTION
########################################################################
# END FUNCTION db_cms_pay_method_rec()
########################################################################

########################################################################
# FUNCTION db_cms_pay_method_list()
#
#
########################################################################
FUNCTION db_cms_pay_method_list()
    CALL InteractFormFile("../db_cms_pay_method/db_cms_pay_method_list")
END FUNCTION
########################################################################
# END FUNCTION db_cms_pay_method_list()
########################################################################