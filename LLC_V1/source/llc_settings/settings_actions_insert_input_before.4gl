########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="../llc_settings/llc_settings_rec"
	LET l_rec_settings.actions["INSERT"]["BEFORE INPUT"] = FUNCTION actions_insert_before_input
	CALL InteractForm(l_rec_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="../llc_settings/llc_settings_list"
	LET l_rec_settings.actions["INSERT"]["BEFORE INPUT"] = FUNCTION actions_insert_before_input

	CALL InteractForm(l_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################


########################################################################
# FUNCTION actions_insert_before_field_test05_int()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_insert_before_input(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING

	CALL fgl_winmessage("actions_insert_before_input","actions_insert_before_input","info")

	LET dlg = ui.Dialog.GetCurrent()

	#retrieve record/field values
	LET l_rec_test05.test05_primary_key = iform.GetFieldValue("test05.test05_primary_key")
	LET l_rec_test05.test05_fk_char = iform.GetFieldValue("test05.test05_fk_char")
	LET l_rec_test05.test05_varchar = iform.GetFieldValue("test05.test05_varchar")
	LET l_rec_test05.test05_int = iform.GetFieldValue("test05.test05_int")
	LET l_rec_test05.test05_date = iform.GetFieldValue("test05.test05_date")

	#Process data validation in case the business rules have changed

	#test05_primary_key
	LET l_rec_test05.test05_primary_key = 987987

	#test05_fk_char
	LET l_rec_test05.test05_fk_char = 'A'

	#test05_varchar
	LET l_rec_test05.test05_varchar = 'Hubert'

	#test05_int
	LET l_rec_test05.test05_int = 123456789

	#test05_date
	LET l_rec_test05.test05_date = TODAY()

	#Assign values back to INPUT program data
	#CALL iform.SetFieldValue("test05.test05_primary_key", l_rec_test05.test05_primary_key)
	CALL iform.SetFieldValue("test05.test05_fk_char",l_rec_test05.test05_fk_char)
	CALL iform.SetFieldValue("test05.test05_varchar",l_rec_test05.test05_varchar)
	CALL iform.SetFieldValue("test05.test05_int",l_rec_test05.test05_int)
	CALL iform.SetFieldValue("test05.test05_date",l_rec_test05.test05_date)	

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_insert_before_field_test05_int()RETURNS BOOL
########################################################################