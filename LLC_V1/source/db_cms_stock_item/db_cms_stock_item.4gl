########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
#GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7912
# Addresses/Scenario:
# PK CHAR and one FK rate_id integer
########################################################################
# DB Table Schema
#			CREATE TABLE stock_item (
#				stock_id            CHAR(10) NOT NULL,
#				item_desc           CHAR(80)  NOT NULL UNIQUE,
#				item_cost           MONEY(8,2),
#				rate_id             INTEGER NOT NULL,
#				quantity            INTEGER DEFAULT 0,
#				PRIMARY KEY (stock_id),
#				FOREIGN KEY (rate_id) REFERENCES tax_rate (rate_id)
#			)
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Stock Item")
	CALL fgl_settitle("Stock Item") 

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Stock Item Record","Stock Item Record",									"{CONTEXT}/public/querix/icon/svg/24/ic_stock_24px.svg", 					101,TRUE,"Stock item Record - Display, Scroll and modify stock items data","top") 
			CALL fgl_dialog_setkeylabel("Stock Item List","Stock Item List",											"{CONTEXT}/public/querix/icon/svg/24/ic_stock_list_24px.svg",			102,TRUE,"Stock item List - List all stock items","top")			
			CALL fgl_dialog_setkeylabel("Stock Item Record Original","Stock Item Record Original","{CONTEXT}/public/querix/icon/svg/24/ic_product_transit_24px.svg",103,TRUE,"Original Stock item Record - Display, Scroll and modify stock items data","top") 

		ON ACTION "Stock Item Record"
			CALL db_cms_stock_item_rec() #original cms stock_item form
	
		ON ACTION "Stock Item List"
			CALL db_cms_stock_item_list() 

		ON ACTION "Stock Item Record Original"
			CALL db_cms_stock_item() #original cms stock_item form

			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################