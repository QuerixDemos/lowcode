########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7939
# Adresses Scenario
# Single table but byte and char(1000) field
########################################################################
# DB Table Schema
{
          CREATE TABLE contact (
            cont_id 	SERIAL(1000) NOT NULL,
            cont_title	VARCHAR(10),
            cont_name 	VARCHAR(20) NOT NULL UNIQUE,
            cont_fname 	VARCHAR(20),
            cont_lname 	VARCHAR(20),
            cont_addr1	VARCHAR(40),
            cont_addr2	VARCHAR(40),
            cont_addr3	VARCHAR(40),
            cont_city	VARCHAR(20),
            cont_zone	VARCHAR(15),
            cont_zip	VARCHAR(15),
            cont_country	VARCHAR(40),
            cont_phone	VARCHAR(15),
            cont_fax	VARCHAR(15),
            cont_mobile	VARCHAR(15),
            cont_email	VARCHAR(50) NOT NULL UNIQUE,
            cont_dept	VARCHAR(15),	# ref cont_dept_id
            cont_org	INTEGER,	# ref company_id
            cont_position	VARCHAR(15),	# ref position_id
            cont_picture	BYTE,
            cont_password	VARCHAR(15),
            cont_ipaddr	VARCHAR(15),	# IP V4
            cont_usemail	SMALLINT,
            cont_usephone	SMALLINT,
            cont_notes   CHAR(1000),
 
            PRIMARY KEY (cont_id) CONSTRAINT contact_pk,
            FOREIGN KEY (cont_org) REFERENCES company(comp_id) CONSTRAINT cont_org_fk_company          

          )
}          
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_rec_settings_rec InteractForm_Settings
	DEFINE l_rec_settings_list InteractForm_Settings

  DEFER INTERRUPT

	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Contact")
	CALL fgl_settitle("Contact")       

	#You could initialise the LLC settings here (or anywhere else BEFORE you call the LowCodeEngine function)
	LET l_rec_settings_rec.navigation_status="nav_page_of"
	LET l_rec_settings_list.navigation_status="nav_page_of"

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Contact Record",	"Contact",			"{CONTEXT}/public/querix/icon/svg/24/ic_contact_24px.svg",	101,TRUE,"Display and modify contact data","top") 
			CALL fgl_dialog_setkeylabel("Contact List",		"Contact List",	"{CONTEXT}/public/querix/icon/svg/24/ic_contacts_24px.svg",	102,TRUE,"List all contacts","top")			
#			CALL fgl_dialog_setkeylabel("Company Record","Company","{CONTEXT}/public/querix/icon/svg/24/ic_company_24px.svg",103,TRUE,"Display and modify Company data","top") 
#			CALL fgl_dialog_setkeylabel("Company List","Company List","{CONTEXT}/public/querix/icon/svg/24/ic_company_list_24px.svg",104,TRUE,"List all Companys","top")			

		
		ON ACTION "Contact Record"
			CALL db_cms_contact_rec_lookup1(l_rec_settings_rec)
			
		ON ACTION "Contact List"
			CALL db_cms_contact_list_lookup1(l_rec_settings_list)

# You can simply integrate company by removing these comments and adding the 4gl file lib_db_cms_company.4gl				
#		ON ACTION "Company List"
#			CALL db_cms_company_list() 
#		
#		ON ACTION "Company Record"
#			CALL db_cms_company_rec()
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################