########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
#GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7912
# Addresses/Scenario:
# 2 PK columns
# for WHERE test (state by country)
########################################################################
# DB Table Schema
#					CREATE TABLE state (
#						country         VARCHAR(40) NOT NULL,  #only for legacy compatibility
#						country_code         VARCHAR(3) NOT NULL, #only for future compatibility
#						state_code           VARCHAR(6) NOT NULL,
#						state_code_iso3661   VARCHAR(10),
#						state_text           VARCHAR(30),
#						state_text_enu       VARCHAR(30),
#            PRIMARY KEY (country_code,state_code)
#					)
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_where_static STRING		#String to pass static WHERE clause to the LowCode Engine
	DEFINE l_where STRING				#String to pass temp WHERE clause to the LowCode Engine
	#Note: both WHERE clauses get joined in the LowCode engine but the l_where (temp) WHERE clause will be overwritten by i.e. CONSTRUCT (Search/Filter)

  DEFER INTERRUPT

	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("State")
	CALL fgl_settitle("State") 

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("State Record","State Record",							"{CONTEXT}/public/querix/icon/svg/24/ic_state_24px.svg", 				101,TRUE,"State Record - Display, Scroll and modify state data","top") 
			CALL fgl_dialog_setkeylabel("State List","State List",								"{CONTEXT}/public/querix/icon/svg/24/ic_state_list_24px.svg",			102,TRUE,"State List - List all states","top")			
 			CALL fgl_dialog_setkeylabel("German State Record","German State Record",			"{CONTEXT}/public/querix/icon/svg/24/ic_filter_1_24px.svg",				103,TRUE,"German State Record","top")
 			CALL fgl_dialog_setkeylabel("German State List","German State List",				"{CONTEXT}/public/querix/icon/svg/24/ic_filter_2_24px.svg",				104,TRUE,"German State List - List states in Germany","top")
 			CALL fgl_dialog_setkeylabel("Switzerland State Record","Switzerland State Record",	"{CONTEXT}/public/querix/icon/svg/24/ic_filter_3_24px.svg",				105,TRUE,"Switzerland State Record","top")
 			CALL fgl_dialog_setkeylabel("Switzerland State List","Switzerland State List",		"{CONTEXT}/public/querix/icon/svg/24/ic_filter_4_24px.svg",				106,TRUE,"Switzerland State List - List states in Switzerland","top")

		ON ACTION "State Record"
			LET l_where = NULL
			LET l_where_static = NULL
			CALL db_cms_state_rec(l_where_static,l_where) #original cms state form
	
		ON ACTION "State List"
			LET l_where = NULL
			LET l_where_static = NULL			
			CALL db_cms_state_list(l_where_static,l_where) 

		ON ACTION "German State Record"
			LET l_where = NULL
			LET l_where_static = "country=\"Germany\""
			CALL db_cms_state_rec(l_where_static,l_where) 

		ON ACTION "German State List"
			LET l_where = NULL
			LET l_where_static = "country=\"Germany\""
			CALL db_cms_state_list(l_where_static,l_where) 

		ON ACTION "Switzerland State Record"
			LET l_where = NULL
			LET l_where_static = "country=\"Switzerland\""
			CALL db_cms_state_rec(l_where_static,l_where) 

		ON ACTION "Switzerland State List"
			LET l_where = NULL
			LET l_where_static = "country=\"Switzerland\""
			CALL db_cms_state_list(l_where_static,l_where) 

		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################