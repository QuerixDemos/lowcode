########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7959
# Addresses/Scenario:
# Test different single line widgets
########################################################################
# DB Table Schema
{
          CREATE TABLE test02 (
          	fTextField VARCHAR(20)
            fListBox   VARCHAR(20),
						fListBox VARCHAR(20),
						fCombo VARCHAR(20),
						fRadioButtonListV VARCHAR(20),
						fTextArea VARCHAR(250),
						fBlobText TEXT
					)

}          
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")

	MENU
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Widget (ML) Record",			"Details",		"{CONTEXT}/public/querix/icon/svg/24/ic_line_single_24px.svg", 	101,TRUE,"Single line widget (Record)","top") 
			CALL fgl_dialog_setkeylabel("Widget (ML) List",   			"List",				"{CONTEXT}/public/querix/icon/svg/24/ic_line_multi_24px.svg",		102,TRUE,"List Single line widgets (List)","top")			
		
		ON ACTION "Widget (ML) Record"
			CALL db_test_wig_multi_line_widgets_rec()
			
		ON ACTION "Widget (ML) List"
			CALL db_test_wig_multi_line_widgets_list()
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################