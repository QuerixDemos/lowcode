{
* #%L
* QUERIX
* %%
* Copyright (C) 2015 QUERIX
* %%
* ALL RIGTHS RESERVED.
* 50 THE AVENUE
* SOUTHAMPTON SO17 1XQ
* UNITED KINGDOM
* Tel ; +(44)02380 385 180
* Fax : +(44)02380 635 118
* http://www.querix.com/
* #L%
}


###############################################################################################################################
# Public functions
###############################################################################################################################
#
# PUBLIC FUNCTION InteractForm(settings InteractForm_Settings)
# PUBLIC FUNCTION InteractFormFile(formFile STRING)
#
###############################################################################################################################


###############################################################################################################################
# Public methods of InteractForm object
###############################################################################################################################
#
# PUBLIC FUNCTION (this InteractForm) Interact() RETURNS BOOL
# PUBLIC FUNCTION (this InteractForm) InteractCurrentForm() RETURNS BOOL
# PUBLIC FUNCTION (this InteractForm) Refresh(withQuestion BOOL) RETURNS BOOL
# PUBLIC FUNCTION (this InteractForm) InitPopulatingComboBox(field STRING)
# PUBLIC FUNCTION (this InteractForm) PopulateComboBoxWhere(field STRING, where STRING)
# PUBLIC FUNCTION (this InteractForm) PopulateComboBox(field STRING, scr_line INT, filter BOOL, control_role INT)
# PUBLIC FUNCTION (this InteractForm) SetFieldValue(field STRING, val VARIANT)
# PUBLIC FUNCTION (this InteractForm) GetFieldValue(field STRING) RETURNS VARIANT
# PUBLIC FUNCTION (this InteractForm) LSTRS(str STRING) RETURNS STRING
# PUBLIC FUNCTION (this InteractForm) LSTRT(str STRING) RETURNS STRING
# PUBLIC FUNCTION (this InteractForm) LSTRC(str STRING) RETURNS STRING
# PUBLIC FUNCTION (this InteractForm) ShowNavigationStatus()
# PUBLIC FUNCTION (this InteractForm) ClearNavigationStatus()
#
###############################################################################################################################

GLOBALS

CONSTANT TOP_DIALOG_ID = ""
CONSTANT SUB_DIALOG_UPDATE_ID = "UPDATE"
CONSTANT SUB_DIALOG_INSERT_ID = "INSERT"
CONSTANT SUB_DIALOG_QUERY_ID  = "QUERY"
CONSTANT PREF_PRIMARY_TABLE   = "$primary$"
CONSTANT PREF_FILTER_COMBOBOX = "$filter$"


###############################################################################################################################
# TYPE InteractForm_Settings RECORD            #
###############################################################################################################################
TYPE InteractForm_Settings RECORD              #
      form_file           STRING               # The form file that should be opened
    , table               STRING               # The table name for query. It's optional. '$primary.<table_name>' screen record is default
    , screen_record       STRING               # The screen record name in the form that should be used. It's optional. '$primary.<table_name>' screen record is default
    , actions             HASHMAP OF HASHMAP   # Custom actions "SubIntercation - Action - Function"
    , attributes          HASHMAP OF HASHMAP   # Custom actions "SubIntercation - Attribute - Value"
    , paged_mode          BOOL                 # Set to TRUE if buffer should be used (in this case it will take less time for starting interaction)
    , input_mode          BOOL                 # BOOL - Set to FALSE if DISPLAY ARRAY should be used, otherwise (TRUE) INPUT ARRAY is used
    , log_file            STRING               # The path to log file. It's optional property.
    , pessimistic_locking BOOL                 # Pessimistic row locking is disabled by default (optimistic does not lock table during input, but only for the time of the actual db update)
    , sql_where           STRING               # The WHERE clause of the main query that can be overwritten as soon the user applies a Search (Construct)
    , sql_where_static    STRING               # The WHERE clause of the main query that can NOT be overwritten by user, it's concatenated to sql_where
    , sql_order_by        STRING               # The ORDER BY clause for the main query
    , sql_top             INT                  # The option to limit the base cursor row using the SQL SELECT TOP clause
    , confirm_accept      INT                  # If positive then it shows a message box for yes/no/cancel when data has been changed AND the user presses ACCEPT. The default is negative (-1)
    , confirm_cancel      INT                  # If positive then it shows a message box for yes/no/cancel when data has been changed AND the user presses CANCEL. The default is positive (1)
    , translations        HASHMAP OF STRING    # Map of message/table/column translations
    , comboboxes          HASHMAP OF STRING    # Map of combobox field names and its sql where clause
    , navigation_status   STRING               # Target location for the DISPLAY of navigation status (which can be a label or a textField).
END RECORD                                     #
###############################################################################################################################


###############################################################################################################################
# TYPE InteractForm_Output RECORD              #
###############################################################################################################################
TYPE InteractForm_Output RECORD                #
      selected_data   HASHMAP                  # Will keep selected row data
END RECORD                                     #
###############################################################################################################################


###############################################################################################################################
# TYPE ComboboxDef RECORD                      #
###############################################################################################################################
TYPE ComboboxDef RECORD                        #
      field_bare       STRING                  # Field name without table prefix
    , sql_where        STRING                  # Initial where clause for selecting values
    , sql_where_static STRING                  # Static where clause for selecting values
    , foreign_key      STRING                  # Foreign key
    , combobox         ui.Combobox             # All Combobox widgets itself
    , combobox_edit    ui.Combobox             # Edit Combobox widget itself
    , combobox_control ui.Combobox             # Common Combobox widget itself
    , combobox_constr  ui.Combobox             # Construct Combobox widget for QUERY action
    , query            STRING                  # Query for selecting combobox values
    , depends_on       DYNAMIC ARRAY OF STRING # List of fields on which values ​​depend
    , splintered       DYNAMIC ARRAY OF ui.DistributedObject # Splintered list of Combobox widgets in screen array on the form
END RECORD                                     #
###############################################################################################################################


###############################################################################################################################
# TYPE InteractForm_Internal RECORD            #
###############################################################################################################################
TYPE InteractForm_Internal RECORD              #
      fields_scroll   DYNAMIC ARRAY OF STRING  # List of simple (not BYTE or TEXT) table's fields
    , fields_scroll_s STRING                   # List of simple (not BYTE or TEXT) table's fields separated by comma for selecting in scrollable cursor
    , fields_blobs    DYNAMIC ARRAY OF STRING  # List of BLOB (BYTE or TEXT) table's fields
    , fields_blobs_s  STRING                   # List of BLOB (BYTE or TEXT) table's fields separated by comma for selecting in NOT scrollable cursor
    , fields_form     DYNAMIC ARRAY OF RECORD name, type STRING END RECORD # List of form fields controlled by the dialog
    , fields_find     DYNAMIC ARRAY OF RECORD name, type STRING END RECORD # List of columns which should be used on FIND/QUERY/CONSTRUCT action
    , fields_where    DYNAMIC ARRAY OF STRING  # List of columns in WHERE clause for selecting current row
    , fields_insert   DYNAMIC ARRAY OF STRING  # List of columns which should be inserted (it's list of form fields without SERIAL columns)
    , fields_update   DYNAMIC ARRAY OF STRING  # List of columns which should be updated (it's list of form fields without PRIMARY KEY columns)
    , fields_noentry  DYNAMIC ARRAY OF STRING  # List of columns which can not be modified (it's list of PRIMARY KEY or SERIAL columns)
    , fields_virtual  DYNAMIC ARRAY OF STRING  # List of columns which don't have visual element on screen but should be managed by Select, Update, and Delete actions
    , primary_key     DYNAMIC ARRAY OF STRING  # Primary key that should be used for fetching <column name>
    , buffer          DYNAMIC ARRAY OF HASHMAP # Keeps all column values for making correct WHERE clause for current row
    , buffer_start    INT                      # Row index of buffer start
    , sqlHandle       BASE.SqlHandle           # The SQL handle for current query
    , refresh_buffer  BOOL                     # Indicates if we need to refresh buffer even if buffer start and length are not changed
    , log_to_file     BOOL                     # Indicates if error message should be logged to file
    , single_row      BOOL                     # It's TRUE if there is just one row on screen can be shown and it is not a Table widget
    , curr_interact   INT                      # 0 - Display/Input Array; 1 - Update; 2 - Insert; 3 - Query
    , is_table        BOOL                     # It's TRUE if there is Table widget is used
    , comboboxes      HASHMAP OF ComboboxDef   # Map of field names and their comboboxe widgets, sql query and sql where clause
    , construct_vals  HASHMAP                  # Values entered on QUERY action, they should be reused on next QUERY action
    , count           INT                      # Count of available rows that can be retrieved by current query
    , scr_line        INT                      # Current screen line which is being edited (it makes sence for DISPLAY mode only)
    , arr_curr        INT                      # Current array row index which is being edited (it makes sence for DISPLAY mode only)
    , fields_lookup   DYNAMIC ARRAY OF RECORD  #
                            field       STRING #
                          , is_blob     BOOL   #
                          , depends_on  DYNAMIC ARRAY OF STRING
                          , query       STRING #
                          , query_param STRING #
                      END RECORD               #
END RECORD                                     #
###############################################################################################################################


###############################################################################################################################
# TYPE InteractForm RECORD                     #
#  InteractForm is a class for creating        #
#  interaction dynamically based on form       #
#                                              #
###############################################################################################################################
TYPE InteractForm RECORD                       #
      settings   InteractForm_Settings         #
    , output     InteractForm_Output           #
    , internal   InteractForm_Internal         #
END RECORD                                     #
###############################################################################################################################

END GLOBALS

DEFINE windowCounter INT8                      # Counter of windows for making unique window name
CONSTANT INTERACT_MAIN   = 0
CONSTANT INTERACT_UPDATE = 1
CONSTANT INTERACT_INSERT = 2
CONSTANT INTERACT_QUERY  = 3

###############################################################################################################################
#
#  Function InteractForm
#  Creates InteractForm object and performs its interaction using provided settings
#
#  Arguments:
#  - settings (InteractForm_Settings) - Settings for LLC IneractForm object
#
###############################################################################################################################
PUBLIC FUNCTION InteractForm(settings InteractForm_Settings)
    DEFINE iForm InteractForm
    LET iForm.settings.* = settings.*
    CALL iForm.Interact()
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION InteractForm(settings InteractForm_Settings)
################################################################################################################################


################################################################################################################################
#
#  Function InteractFormFile
#  Opens window with defined form
#  Creates InteractForm object and performs its interaction using opened form
#
#  Arguments:
#  - formFile     (STRING) - path to form file
#  - screenRecord (STRING) - name of screen record that should be interacted,
#                            it's optional, if parameter is NULL,
#                            then will be used the first screen record in form file
#
################################################################################################################################
PUBLIC FUNCTION InteractFormFile(formFile STRING)
    DEFINE settings InteractForm_Settings
    LET settings.form_file = formFile
    CALL InteractForm(settings)
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION InteractFormFile(formFile STRING)
################################################################################################################################


################################################################################################################################
# PUBLIC FUNCTION (this InteractForm) Interact() RETURNS BOOL
#
#  Public method InteractForm of InteractForm object
#  Creates interaction dynamically based on form
#
#  The main interaction Menu with actions:
#     "Query", "Edit", "Delete", "Add", "Next" and "Previous"
#
#  Returns TRUE if everything is done well,
#      and FALSE if executing is finished with error
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) Interact() RETURNS BOOL
    DEFINE ret, openForm BOOL
    DEFINE w WINDOW

    DEFER INTERRUPT

    LET openForm = this.settings.form_file IS NOT NULL

    IF openForm THEN
        LET windowCounter = windowCounter + 1
        CALL w.OpenWithForm("w_InteractForm_" || windowCounter, this.settings.form_file, 1, 1) # Open the new window with defined form
    END IF

    CALL this.InteractCurrentForm() RETURNING ret

    IF openForm THEN
        CALL w.Close()                                       # Close the window
    ELSE
        CALL this.ClearNavigationStatus()
    END IF

    RETURN ret
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) Interact() RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PUBLIC FUNCTION (this InteractForm) InteractCurrentForm() RETURNS BOOL
#
#  Public method InteractCurrentForm of InteractForm object
#  Creates interaction dynamically based on form
#
#  The main interaction Menu with actions:
#     "Query", "Edit", "Delete", "Add", "Next" and "Previous"
#
#  Returns TRUE if everything is done well,
#      and FALSE if executing is finished with error
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) InteractCurrentForm() RETURNS BOOL
    DEFER INTERRUPT

    IF NOT this.Init() THEN
        RETURN FALSE                                         # Initiating object before executing is failed, so return FALSE
    END IF

    IF this.settings.input_mode THEN
        RETURN this.ExecuteInteractionInput()
    ELSE
        RETURN this.ExecuteInteractionDisplay()
    END IF
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) InteractCurrentForm() RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) Init() RETURNS BOOL
#
#  Private method INIT of InteractForm object
#  Returns TRUE if everything is initialized well
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) Init() RETURNS BOOL
    DEFINE record_fields, select_list, lookup_list, scr_records DYNAMIC ARRAY OF STRING
    DEFINE query, col, ftype, fname, db_table  STRING
    DEFINE i, lookup_index  INT
    DEFINE isIfxDb          BOOL
    DEFINE field            ui.AbstractUiElement
    DEFINE sh               BASE.SqlHandle                   # SQL handler for query

    LET isIfxDb = db_get_database_type() = "IFX"

# Reset all internal field of InteractForm object
    INITIALIZE this.internal.fields_scroll_s TO NULL
    INITIALIZE this.internal.fields_blobs_s  TO NULL
    INITIALIZE this.internal.single_row      TO NULL

    CALL this.internal.fields_form.Clear()
    CALL this.internal.fields_scroll.Clear()
    CALL this.internal.fields_blobs.Clear()
    CALL this.internal.fields_where.Clear()
    CALL this.internal.fields_insert.Clear()
    CALL this.internal.fields_update.Clear()
    CALL this.internal.fields_noentry.Clear()
    CALL this.internal.fields_find.Clear()
    CALL this.internal.fields_virtual.Clear()
    CALL this.internal.buffer.Clear()
    CALL this.internal.comboboxes.Clear()
    CALL this.internal.fields_lookup.Clear()
    CALL this.internal.construct_vals.Clear()

    LET this.internal.curr_interact = INTERACT_MAIN
    LET this.internal.buffer_start = 1
    LET this.internal.is_table = FALSE

    IF this.settings.paged_mode IS NULL THEN
        LET this.settings.paged_mode = FALSE
    END IF

    IF this.settings.input_mode IS NULL THEN
        LET this.settings.input_mode = FALSE
    END IF

    IF this.settings.pessimistic_locking IS NULL THEN
        LET this.settings.pessimistic_locking = FALSE
    END IF

    IF this.settings.confirm_accept = 0 OR this.settings.confirm_accept IS NULL THEN
        LET this.settings.confirm_accept = -1
    END IF

    IF this.settings.confirm_cancel = 0 OR this.settings.confirm_cancel IS NULL THEN
        LET this.settings.confirm_cancel = 1
    END IF

    CALL ui.Dialog.Init()

    IF this.settings.log_file IS NOT NULL THEN
        CALL startlog(this.settings.log_file)
        IF NOT os.path.writable(this.settings.log_file) THEN
            CALL fgl_winmessage(this.LSTRS("Log File Access Error"),
                                this.LSTRS("Invalid permission (Write) for log file ") || this.settings.log_file,
                                "Error")
            LET this.internal.log_to_file = FALSE
        ELSE
            LET this.internal.log_to_file = TRUE
        END IF
    ELSE
        LET this.internal.log_to_file = FALSE
    END IF

# Clear form and view
    MESSAGE ""

    IF this.settings.table IS NULL AND this.settings.screen_record IS NOT NULL THEN
        CALL this.Error(this.LSTRS("Initialize"),
                        this.LSTRS("If screen record name is defined implicitly then table has to be defined implicitly as well."))
        RETURN FALSE                                         # Return FALSE because of error
    END IF

    IF this.settings.table IS NOT NULL AND this.settings.screen_record IS NULL THEN
        CALL this.Error(this.LSTRS("Initialize"),
                        this.LSTRS("If table name is defined implicitly then screen record has to be defined implicitly as well.\n\n(You may have defined the table name twice, in the form and in your 4gl sources."))
        RETURN FALSE                                         # Return FALSE because of error
    END IF

    IF this.settings.table IS NULL AND this.settings.screen_record IS NULL THEN
# ======== Get primary table name and appropriate screen record from the form
        LET this.settings.screen_record = PREF_PRIMARY_TABLE || "<table_name>"
        CALL ui.Window.GetCurrent().GetForm().getScreenRecords() RETURNING scr_records
        FOR i = 1 TO scr_records.GetSize()
            IF scr_records[i].substring(1,PREF_PRIMARY_TABLE.GetLength()).equalsIgnoreCase(PREF_PRIMARY_TABLE) THEN
                LET this.settings.screen_record = scr_records[i]
                LET this.settings.table = scr_records[i].substring(PREF_PRIMARY_TABLE.GetLength() + 1, scr_records[i].GetLength())
                EXIT FOR
            END IF
        END FOR
    END IF

# ======== Get screen record's fields ========
# Gets list of fields of the defined screen record in the form
    CALL ui.Window.GetCurrent().GetForm().getScreenRecordFields(this.settings.screen_record) RETURNING record_fields
    IF record_fields.GetSize() = 0 THEN                      # If screen record is missing or is empty, then we can not move on
        CALL this.Error(this.LSTRS("Initialize"), SFMT(this.LSTRS("Screen record '%1' is not defined in the form or it is empty."), this.settings.screen_record))
        RETURN FALSE                                         # Return FALSE because of error
    END IF

# ======== Identify Primary KEY =========
    CALL this.InitPrimaryKey()

# ======== Update form for managing virtual fields
    CALL this.InitVirtualFields(record_fields)

# ======== Get DataType of each form field from screen record ========
    FOR i = 1 TO record_fields.GetSize()
        LET field = ui.AbstractUiElement.ForName(record_fields[i])
        IF field IS NULL THEN                                # Field can not be found in the form
            CALL this.Error(this.LSTRS("Initialize"), SFMT(this.LSTRS("Field '%1' from screen record '%2' doesn't exist in the form '%3'."), this.LSTRC(record_fields[i]), this.settings.screen_record, this.settings.form_file))
            RETURN FALSE                                     # Return FALSE because of error
        ELSE
            IF this.internal.single_row IS NULL THEN
                CALL GetViewType(field) RETURNING this.internal.single_row, this.internal.is_table
            END IF
        END IF

        LET db_table = field.GetFieldTable()
        IF db_table.equalsIgnoreCase(this.settings.table) THEN
            CALL this.InitPopulatingComboBox(record_fields[i])
            CALL select_list.append(record_fields[i])
        ELSE
            LET lookup_index = this.AppendLookup(db_table, field.GetIdentifier())
            IF lookup_index > 0 THEN                         # Not valid foreign key/lookup is ignored
                CALL lookup_list.append(record_fields[i])
                CALL select_list.append(this.internal.fields_lookup[lookup_index].query)
            ELSE
                CALL this.Error(this.LSTRS("Initialize"), SFMT(this.LSTRS("There is no rule for getting value of field '%1' in screen record '%2'."), this.LSTRC(record_fields[i]), this.settings.screen_record))
                RETURN FALSE                                 # Return FALSE because of error
            END IF
        END IF
    END FOR

    FOR i = 1 TO select_list.getSize()
        IF query IS NOT NULL THEN
            LET query = query.Append(", ")
        END IF
        LET query = query.Append(select_list[i])
    END FOR
    LET query = "SELECT " || query || " FROM " || this.settings.table

    LET  sh = BASE.SqlHandle.Create()                        # Instantiate SqlHandle object
    CALL sh.Prepare(query)                                   # Prepare query

    IF sh.GetResultCount() = 0 THEN                          # Nothing can be fetch
        CALL this.Error(this.LSTRS("Initialize"), SFMT(this.LSTRS("Table '%1' doesn't exist in database or some fields are missing in the table. (%2)"), this.LSTRT(this.settings.table), query))
        RETURN FALSE                                         # Return FALSE because of error
    END IF

# Fill array with result column's names and types
    FOR i = 1 TO sh.GetResultCount()
        LET fname = record_fields[i] CLIPPED
        LET ftype = sh.GetResultType(i)

        LET this.internal.fields_form[i].name = fname        # Define name of column
        LET this.internal.fields_form[i].type = ftype        # Define type of column

        IF isIfxDb AND (ftype = "Byte" OR ftype = "Text") THEN
            # Make string with list of blob fields in the form (comma separated) for SELECT statement
            IF this.internal.fields_blobs_s IS NOT NULL THEN
                LET this.internal.fields_blobs_s = this.internal.fields_blobs_s.Append(", ")
            END IF
            LET this.internal.fields_blobs_s = this.internal.fields_blobs_s.Append(select_list[i])
            CALL this.internal.fields_blobs.Append(fname)
            LET lookup_index = this.SearchLookup(fname)
            IF lookup_index > 0 THEN
                LET this.internal.fields_lookup[lookup_index].is_blob = TRUE
            END IF
        ELSE
            # Make string with list of NOT blob fields in the form (comma separated) for scrollable cursor
            IF this.internal.fields_scroll_s IS NOT NULL THEN
                LET this.internal.fields_scroll_s = this.internal.fields_scroll_s.Append(", ")
            END IF
            LET this.internal.fields_scroll_s = this.internal.fields_scroll_s.Append(select_list[i])
            CALL this.internal.fields_scroll.Append(fname)
        END IF

        IF lookup_list.Search("", fname) > 0 THEN
            CALL this.internal.fields_noentry.Append(fname)
        ELSE
            LET this.internal.fields_find[this.internal.fields_find.getSize() + 1].name = fname
            LET this.internal.fields_find[this.internal.fields_find.getSize()].type = ftype

            IF iCaseSearchArray(this.internal.primary_key, fname) > 0 OR ftype.substring(1,6) = "serial" THEN
                CALL this.internal.fields_noentry.Append(fname)  # Keep list of fields which can not be updated, because they are UNIQUE (PRIMARY KEY or SERIAL)
            ELSE
                CALL this.internal.fields_update.Append(fname)   # Keep list of fields which can be updated
            END IF
            IF ftype.substring(1,6).equals("serial") <> 1 THEN
                CALL this.internal.fields_insert.Append(fname)   # Keep list of fields which can be used for INSERT statement
            END IF
        END IF
    END FOR

    IF this.internal.primary_key.GetSize() > 0 THEN
        # Select columns from PRIMARY KEY in scrollable cursor if them are not included already
        FOR i = 1 TO this.internal.primary_key.GetSize()
            CALL this.internal.fields_where.Append(this.internal.primary_key[i])
            IF this.SearchFormField(this.internal.primary_key[i]) = 0 THEN
                IF this.internal.fields_scroll_s IS NOT NULL THEN
                    LET this.internal.fields_scroll_s = this.internal.fields_scroll_s.Append(", ")
                END IF
                LET this.internal.fields_scroll_s = this.internal.fields_scroll_s.Append(this.internal.primary_key[i])
                CALL this.internal.fields_scroll.Append(this.internal.primary_key[i])
            END IF
        END FOR
    ELSE # There is no PRIMARY KEY so we need to select all columns for making WHERE clause for specific (current) row
        # ======== Get DataType of each table field ========
        CALL sh.Prepare("SELECT * FROM " || this.settings.table) # Prepare query

        # Fill array with result column's names and types
        FOR i = 1 TO sh.GetResultCount()
            LET fname = this.settings.table || "." || sh.GetResultName(i) CLIPPED
            LET ftype = sh.GetResultType(i)

            # Make list of simple fields in the table
            IF NOT isIfxDb OR (ftype <> "Byte" AND ftype <> "Text") THEN
                CALL this.internal.fields_where.Append(fname)
                IF this.SearchFormField(this.internal.primary_key[i]) = 0 THEN
                    LET this.internal.fields_scroll_s = this.internal.fields_scroll_s.Append(", ").Append(fname)
                    CALL this.internal.fields_scroll.Append(fname)
                END IF
            END IF
        END FOR
    END IF

# Remove custom ON FILL BUFFER event/action in case it's not paged_mode
    IF NOT this.settings.paged_mode THEN
        LET i = iCaseSearchHashmap(this.settings.actions[TOP_DIALOG_ID], "ON FILL BUFFER")
        IF i > 0 THEN
            CALL this.settings.actions[TOP_DIALOG_ID].delete(i)
        END IF
    END IF

    RETURN TRUE                                              # Return TRUE because of NO error
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) Init() RETURNS BOOL
################################################################################################################################



################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) ExecuteInteractionDisplay() RETURNS BOOL
#
# Private method ExecuteInteractionDisplay of InteractForm object
# Starts DISPLAY ARRAY interaction on form
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ExecuteInteractionDisplay() RETURNS BOOL
    DEFINE event                   STRING
    DEFINE dlg                     UI.DIALOG                 # Dynamic DIALOG
    DEFINE stat, empty, ret        BOOL
    DEFINE i, index                INT
    DEFINE actions, actionsExec, attributes     HASHMAP

    CALL this.GetActions(TOP_DIALOG_ID) RETURNING actions, actionsExec
    LET attributes = this.GetAttributes(TOP_DIALOG_ID)

    LET stat = TRUE
    LET ret = FALSE

    WHILE stat
        LET  stat = FALSE
        LET  this.internal.refresh_buffer = TRUE

        LET  dlg = ui.Dialog.CreateDisplayArrayTo(this.internal.fields_form, this.settings.screen_record)
        CALL AddTrigger(dlg, actions, "ON ACTION QUERY")     # Add custom ON ACTION QUERY action
        CALL AddTrigger(dlg, actions, "ON ACTION Cancel")    # Add custom CANCEL action
        CALL AddTrigger(dlg, actions, "ON ACTION Refresh")   # Add custom REFRESH action

        IF GetAttributeBool(attributes, "INSERT ROW", TRUE) THEN
            CALL AddTrigger(dlg, actions, "ON INSERT")       # Add custom ON INSERT action
        END IF

        IF GetAttributeBool(attributes, "APPEND ROW", TRUE) THEN
            CALL AddTrigger(dlg, actions, "ON APPEND")       # Add custom ON INSERT action
        END IF

        IF GetAttributeBool(attributes, "DELETE ROW", TRUE) THEN
            CALL AddTrigger(dlg, actions, "ON DELETE")       # Add custom ON DELETE action
        END IF

        IF this.internal.fields_update.GetSize() > 0 THEN
            CALL AddTrigger(dlg, actions, "ON UPDATE")       # Add custom ON UPDATE action
        END IF

        IF this.settings.paged_mode THEN
            CALL AddTrigger(dlg, actions, "ON FILL BUFFER")  # Add custom ON FILL BUFFER action
        END IF

        FOR i = 1 TO actions.getSize()
            CALL AddTrigger(dlg, actions, actions.GetKey(i)) # Add custom action
        END FOR

        CALL dlg.SetDialogAttribute("COUNT", -1)
        IF this.internal.single_row THEN
            CALL dlg.SetDialogAttribute("CURRENT ROW DISPLAY", "DIM")
        END IF

        CALL SetAttributes(dlg, attributes)

        WHENEVER ANY ERROR STOP

        WHILE (event := dlg.NextEvent()) IS NOT NULL         # Process dialog and wait for the next event
            CASE event
                WHEN "BEFORE DIALOG"
                    CALL PopulateToolbar(this.internal.single_row)

                    IF NOT this.settings.paged_mode THEN
                        CALL this.Show(-1, -1)
                    END IF

                WHEN "ON FILL BUFFER"
                    IF NOT this.Show(FGL_DIALOG_GETBUFFERSTART(), FGL_DIALOG_GETBUFFERLENGTH()) THEN
                        CONTINUE WHILE                       # Skip executing custom action "ON FILL BUFFER"
                    END IF
            END CASE

            IF this.ExecuteCustomAction(actionsExec, event, dlg) THEN
                CONTINUE WHILE
            END IF

            CASE event

                WHEN "BEFORE ROW"
                    LET index = arr_curr() - this.internal.buffer_start + 1
                    IF index < 1 OR (index > this.internal.buffer.getSize() AND this.internal.buffer.getSize() > 0) THEN
                        CALL this.Show(arr_curr(), 1)
                        LET index = 1
                    END IF
                    LET this.output.selected_data = this.internal.buffer[index]
                    CALL this.ShowNavigationStatus()

                WHEN "ON UPDATE"
                    LET this.internal.curr_interact = INTERACT_UPDATE
                    LET this.internal.scr_line = scr_line()
                    LET this.internal.arr_curr = arr_curr()
                    CALL this.Edit(dlg, scr_line(), arr_curr())
                    LET this.internal.curr_interact = INTERACT_MAIN

                WHEN "ON INSERT"
                    LET this.internal.curr_interact = INTERACT_INSERT
                    LET this.internal.scr_line = scr_line()
                    LET this.internal.arr_curr = arr_curr()
                    CALL this.Add(dlg, scr_line(), arr_curr())
                    LET this.internal.curr_interact = INTERACT_MAIN

                WHEN "ON APPEND"
                    LET this.internal.curr_interact = INTERACT_INSERT
                    LET this.internal.scr_line = scr_line()
                    LET this.internal.arr_curr = arr_curr()
                    CALL this.Add(dlg, scr_line(), arr_curr())
                    LET this.internal.curr_interact = INTERACT_MAIN

                WHEN "ON DELETE"
                    CALL this.Delete(dlg, scr_line(), arr_curr())

                WHEN "ON ACTION QUERY"
                    LET this.internal.curr_interact = INTERACT_QUERY
                    IF this.Query() THEN
                        LET stat = TRUE
                        CALL dlg.Accept()
                    END IF
                    LET this.internal.curr_interact = INTERACT_MAIN

                WHEN "ON ACTION Refresh"
                    CALL this.Refresh(FALSE)

                WHEN "ON ACTION Cancel"
                    LET ret = TRUE                           # Means proceeded by user
                    CALL dlg.Cancel()

                WHEN "AFTER DIALOG"
                    EXIT WHILE

            END CASE

            # Check if array is empty, so we need to hide UPDATE and DELETE actions
            LET empty = dlg.GetArrayLength(this.settings.screen_record) = 0
            CALL dlg.SetActionHidden("UPDATE", empty)
            CALL dlg.SetActionHidden("DELETE", empty)
            IF empty THEN # BEFORE ROW is not executed in this case, so we need to update navigation status
                CALL this.ShowNavigationStatus()
            END IF
        END WHILE

        CALL this.internal.sqlHandle.Close()                 # Close sql handler
        CALL dlg.Close()                                     # Close and release dynamic dialog
    END WHILE

    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) ExecuteInteractionDisplay() RETURNS BOOL
################################################################################################################################



################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) Show(start INT, length INT) RETURNS BOOL
# Private method Show of InteractForm object
# Displays the defined record.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) Show(start INT, length INT) RETURNS BOOL
    DEFINE i, row, index         INT
    DEFINE cname                 STRING
    DEFINE shBlobs               BASE.SqlHandle              # SqlHandle for selecting blob's data
    DEFINE dlg                   UI.DIALOG

    LET dlg = ui.DIalog.GetCurrent()

# Check if we really need to update buffer (performance optimization)
    IF NOT this.internal.refresh_buffer THEN
        IF start = this.internal.buffer_start AND length <= this.internal.buffer.getSize() THEN
            RETURN FALSE
        END IF
    END IF

# Update count of query rows
    CALL this.UpdateCount()
    CALL ui.DIalog.GetCurrent().SetArrayLength(this.settings.screen_record, this.internal.count)

# Fetch data from DB
    LET  this.internal.sqlHandle = BASE.SqlHandle.Create()   # Instantiate the new SqlHandle object
    CALL this.internal.sqlHandle.Prepare("SELECT"
                                         || this.GetTopClause()
                                         || " " || this.internal.fields_scroll_s
                                         || "  FROM " || this.settings.table
                                         || this.GetWhereClause()
                                         || this.GetOrderBy()
                                         || this.GetLimitClause()) # Prepare the query
    CALL this.internal.sqlHandle.OpenScrollCursor()          # Open scroll cursor
    LET  this.internal.refresh_buffer = FALSE

    IF NOT this.settings.paged_mode THEN
        LET start  = 1                                       # Start from the first row
        LET length = this.internal.count                     # Keep all rows
    END IF

    LET this.internal.buffer_start  = start                  # Keep buffer start index

    CALL this.internal.buffer.Clear()                        # Reset buffer store
    FOR row = 1 TO length
        LET index = start + row - 1                          # Calculate index of fetching row
        CALL this.internal.sqlHandle.FetchAbsolute(index)    # Fetch new row from DB
        IF sqlca.sqlcode == NOTFOUND OR status <> 0 THEN     # There is no more rows
            CALL dlg.SetArrayLength(this.settings.screen_record, index - 1) # Set length of DISPLAY ARRAY.
            EXIT FOR                                         # Stop fetching
        END IF
        FOR i = 1 TO this.internal.sqlHandle.GetResultCount()# Set value in each field in the row
            LET cname = this.internal.fields_scroll[i] CLIPPED
            LET this.internal.buffer[row][cname] = this.internal.sqlHandle.GetResultValue(i)
            IF this.SearchFormField(cname) > 0 THEN          # If form field exists
                CALL dlg.SetFieldValue(cname, this.internal.sqlHandle.GetResultValue(i), index) # Fill field with value
            END IF
        END FOR

# Fetch BLOBs of particular row from DB
        IF this.internal.fields_blobs_s IS NOT NULL THEN
            LET shBlobs = BASE.SqlHandle.Create()
            CALL shBlobs.Prepare("SELECT " || this.internal.fields_blobs_s ||
                                 "  FROM " || this.settings.table ||
                                 " WHERE " || this.GetWhereClauseForRow(row))
            CALL this.SetWhereClauseParametersForRow(shBlobs, 1, row)
            CALL shBlobs.Open()
            CALL shBlobs.Fetch()
            FOR i = 1 TO shBlobs.GetResultCount()
                LET cname = this.internal.fields_blobs[i] CLIPPED
                LET this.internal.buffer[row][cname] = shBlobs.GetResultValue(i)
                CALL dlg.SetFieldValue(cname, this.internal.buffer[row][cname], index)
            END FOR
        END IF
    END FOR

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) Show(start INT, length INT) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) Edit(parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
#
#  Private method Edit of InteractForm object
#  Shows INPUT interaction for editing record and saves updated values to the DB
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) Edit(parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
    DEFINE event, cname                       STRING
    DEFINE i, index                           INT
    DEFINE dlg                                UI.DIALOG
    DEFINE sh                                 BASE.SqlHandle
    DEFINE actions, actionsExec, attributes   HASHMAP
    DEFINE doRollback, isAccept, isTouched    BOOL
    DEFINE dlg_fields DYNAMIC ARRAY OF RECORD name STRING, type STRING END RECORD # List of form fields controlled by the SubDialog for updating or inserting

    MESSAGE ""
    LET isAccept = TRUE
    LET isTouched = FALSE

    LET index = arr_curr - this.internal.buffer_start + 1

    IF index < 1 OR (this.settings.paged_mode AND index > this.internal.buffer.getSize()) THEN
        CALL fgl_winmessage(this.LSTRS("Selected row is out of buffer"), this.LSTRS("Make sure that you select the correct row for the update"), "info")
        CALL fgl_dialog_setcurrline(scr_line, arr_curr)
        RETURN
    END IF

    CALL this.GetActions(SUB_DIALOG_UPDATE_ID) RETURNING actions, actionsExec
    LET attributes = this.GetAttributes(SUB_DIALOG_UPDATE_ID)

    LET doRollback = FALSE

    IF this.settings.pessimistic_locking THEN
        CALL BeginWork()                                     # Start transaction
        LET doRollback = TRUE                                # We need to do ROLLBACK (if no COMMIT)
        IF NOT this.LockRow(index) THEN
            CALL RollbackWork()                              # Rollback transaction
            RETURN                                           # Stop editing
        END IF
    END IF

# Make list of field names for interaction
    CALL this.internal.fields_form.copyTo(dlg_fields)
    FOR i = 1 TO dlg_fields.GetSize()
        LET dlg_fields[i].name = dlg_fields[i].name.Append("[").Append(scr_line).Append("]")
    END FOR

# Create interaction dialog
    LET  dlg = ui.Dialog.CreateInputByName(dlg_fields)
    CALL AddTrigger(dlg, actions, "ON ACTION Accept")        # Add custom ACCEPT action
    CALL AddTrigger(dlg, actions, "ON ACTION Cancel")        # Add custom CANCEL action
    CALL AddTrigger(dlg, actions, "ON ACTION Refresh")       # Add custom REFRESH action

    FOR i = 1 TO actions.getSize()
        CALL AddTrigger(dlg, actions, actions.GetKey(i))# Add custom action
    END FOR

    CALL this.AddTriggerOnChange(dlg)

    CALL SetAttributes(dlg, attributes)                      # Set custom attributes

    WHILE (event := dlg.NextEvent()) IS NOT NULL             # Process dialog and wait for the next event
        CASE event
            WHEN "BEFORE DIALOG"
                # Display value in each form field in the input
                FOR i = 1 TO this.internal.fields_form.GetSize()
                    LET cname = this.internal.fields_form[i].name
                    CALL dlg.SetFieldValue(cname, this.internal.buffer[index][cname])
                END FOR

                # Disable all form fields that are primary key
                FOR i = 1 TO this.internal.fields_noentry.GetSize()
                    CALL dlg.SetFieldActive(this.internal.fields_noentry[i], FALSE)
                END FOR

                CALL this.PopulateAllComboBox(TRUE, scr_line, TRUE, 2)
        END CASE

        IF this.ExecuteCustomAction(actionsExec, event, dlg) THEN
            CONTINUE WHILE
        END IF

        CASE event
            WHEN "AFTER DIALOG"
                IF isTouched OR this.IsCurrentRowTouched(dlg) THEN
                    LET isTouched = TRUE
                    CASE this.Question(this.LSTRS("Modify"),
                                       this.LSTRS("Do you want to save changes?"),
                                       this.LSTRS("Yes"),
                                       SFMT("%1|%2|%3", this.LSTRS("Yes"), this.LSTRS("No"), this.LSTRS("Cancel")),
                                       "Question",
                                       isAccept)
                        WHEN this.LSTRS("Yes")                            # Updating is confirmed by user
                            IF NOT this.ValidateData(dlg, scr_line) THEN
                                CONTINUE WHILE
                            END IF

                            IF NOT this.settings.pessimistic_locking THEN
                                CALL BeginWork()                         # Start transaction
                                IF NOT this.LockRow(index) THEN
                                    CALL RollbackWork()                  # Rollback transaction
                                    CONTINUE WHILE
                                END IF
                            END IF

                            IF this.UpdateDB(dlg, index) THEN        # Update values in DB
                                # Updating is done well
                                MESSAGE SFMT(this.LSTRS("Edit %1 Successful operation"), this.LSTRT(this.settings.table))
                                CALL CommitWork()                    # Commit transaction
                                LET doRollback = FALSE               # We don't need to do ROLLBACK
                                LET int_flag = FALSE
                                LET this.internal.refresh_buffer = TRUE  # We need to refresh buffer next time of fetching data

                                # Update parent array dialog with new values
                                FOR i = 1 TO this.internal.fields_form.GetSize()    # For each edited form field
                                    LET cname = this.internal.fields_form[i].name
                                    LET this.internal.buffer[index][cname] = dlg.GetFieldValue(cname)
                                    CALL parent_dlg.SetFieldValue(cname, dlg.GetFieldValue(cname), arr_curr)
                                END FOR
                            ELSE
                                IF NOT this.settings.pessimistic_locking THEN
                                    CALL RollbackWork()                  # Rollback transaction
                                END IF
                                CONTINUE WHILE
                            END IF

                        WHEN this.LSTRS("Cancel")                         # Back focus to the current field and continue updating
                            CONTINUE WHILE
                    END CASE
                END IF

                EXIT WHILE

            WHEN "ON ACTION Refresh"
                IF this.Refresh(isTouched OR this.IsCurrentRowTouched(dlg)) THEN
                    LET isTouched = FALSE                    # Reset touch attribute
                END IF

            WHEN "ON ACTION Accept"
                LET isAccept = TRUE
                CALL dlg.Accept()

            WHEN "ON ACTION Cancel"
                LET isAccept = FALSE
                CALL dlg.Accept()

            OTHERWISE
                CALL this.ManageBuildInEvent(dlg, event)
        END CASE
    END WHILE

    IF doRollback THEN
        CALL RollbackWork()                              # Rollback transaction
    END IF

    IF NOT this.internal.is_table THEN
        CALL this.PopulateAllComboBox(TRUE, scr_line, FALSE, 2)
    END IF

    CALL dlg.Close()
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) Edit(parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) Add(parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
#
#  Private method Add of InteractForm object
#  Prompts to input values and adds the new record.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) Add(parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
    DEFINE event, query, str_set, cname       STRING
    DEFINE i, index                           INT
    DEFINE doCancel, isAccept, isTouched      BOOL
    DEFINE dlg                                UI.DIALOG         # Dynamic DIALOG
    DEFINE actions, actionsExec, attributes   HASHMAP
    DEFINE dlg_fields DYNAMIC ARRAY OF RECORD name STRING, type STRING END RECORD # List of form fields controlled by the SubDialog for updating or inserting

    MESSAGE ""

    LET doCancel = TRUE
    LET isAccept = TRUE
    LET isTouched = FALSE

    CALL this.GetActions(SUB_DIALOG_INSERT_ID) RETURNING actions, actionsExec
    LET attributes = this.GetAttributes(SUB_DIALOG_INSERT_ID)

# Make list of field names for interaction
    CALL this.internal.fields_form.copyTo(dlg_fields)
    FOR i = 1 TO dlg_fields.GetSize()
        LET dlg_fields[i].name = dlg_fields[i].name.Append("[").Append(scr_line).Append("]")
    END FOR

# Create interaction dialog
    LET  dlg = ui.Dialog.CreateInputByName(dlg_fields)       # Create dynamic DIALOG with INPUT
    CALL AddTrigger(dlg, actions, "ON ACTION Accept")        # Add custom ACCEPT action
    CALL AddTrigger(dlg, actions, "ON ACTION Cancel")        # Add custom CANCEL action

    FOR i = 1 TO actions.getSize()
        CALL AddTrigger(dlg, actions, actions.GetKey(i))# Add custom action
    END FOR

    CALL this.AddTriggerOnChange(dlg)
    CALL SetAttributes(dlg, attributes)                      # Set custom attributes

    WHILE (event := dlg.NextEvent()) IS NOT NULL             # Process dialog and wait for the next event

        CASE event
            WHEN "BEFORE DIALOG"
                CALL this.ActivateFieldsForInsert()
                CALL this.PopulateAllComboBox(TRUE, scr_line, TRUE, 2)
                FOR i = 1 TO this.internal.fields_form.GetSize()  # For each edited form field
                    CALL dlg.SetFieldValue(this.internal.fields_form[i].name, NULL)
                END FOR
        END CASE

        IF this.ExecuteCustomAction(actionsExec, event, dlg) THEN
            CONTINUE WHILE
        END IF

        CASE event
            WHEN "AFTER DIALOG"
                IF isTouched OR this.IsCurrentRowTouched(dlg) THEN
                    LET isTouched = TRUE
                    CASE this.Question(this.LSTRS("Add"),
                                       this.LSTRS("Add new item?"),
                                       this.LSTRS("Yes"),
                                       SFMT("%1|%2|%3", this.LSTRS("Yes"), this.LSTRS("No"), this.LSTRS("Cancel")),
                                       "Question",
                                       isAccept)
                        WHEN this.LSTRS("Yes")               # Inserting is confirmed
                            IF NOT this.ValidateData(dlg, scr_line) THEN
                                CONTINUE WHILE
                            END IF

                            CALL BeginWork()                 # Start transaction

                            LET this.internal.refresh_buffer = TRUE  # We need to refresh buffer next time of fetching data

                            IF this.InsertDB(dlg) THEN
                                LET doCancel = FALSE
                                MESSAGE SFMT(this.LSTRS("Insert %1  Successful operation"), this.LSTRT(this.settings.table))
                                CALL CommitWork()            # Commit transaction
                                LET int_flag = FALSE

                                LET index = arr_curr - this.internal.buffer_start + 1
                                IF index > 0 AND (NOT this.settings.paged_mode OR index <= this.internal.buffer.getSize()) THEN
                                    # Update parent array dialog with new values
                                    CALL this.internal.buffer.insert(index)
                                    FOR i = 1 TO this.internal.fields_form.GetSize()  # For each edited form field
                                        LET cname = this.internal.fields_form[i].name
                                        LET this.internal.buffer[index][cname] = dlg.GetFieldValue(cname)
                                        CALL parent_dlg.SetFieldValue(cname, dlg.GetFieldValue(cname), arr_curr)
                                    END FOR
                                END IF
                            ELSE
                                CALL RollbackWork()          # Rollback transaction
                                CONTINUE WHILE
                            END IF

                        WHEN this.LSTRS("Cancel")            # Stop adding without saving
                            CONTINUE WHILE
                    END CASE
                END IF

                EXIT WHILE

            WHEN "ON ACTION Accept"
                LET isAccept = TRUE
                CALL dlg.Accept()

            WHEN "ON ACTION Cancel"
                LET isAccept = FALSE
                CALL dlg.Accept()

            OTHERWISE
                CALL this.ManageBuildInEvent(dlg, event)
        END CASE
    END WHILE

    MESSAGE ""                                               # Clear previous message
    CALL dlg.Close()                                         # Close and release dynamic dialog

    IF doCancel THEN
        CANCEL INSERT
    END IF

    IF NOT this.internal.is_table THEN
        CALL this.PopulateAllComboBox(TRUE, scr_line, FALSE, 2)
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) Add(parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) Delete(parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
#
#  Private method Add of InteractForm object
#  Deletes current record.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) Delete(parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
  DEFINE index                INT

  MESSAGE ""

  LET index = arr_curr - this.internal.buffer_start + 1

  IF index < 1 OR (this.settings.paged_mode AND index > this.internal.buffer.getSize()) THEN
      CALL fgl_winmessage(this.LSTRS("Selected row is out of buffer"), this.LSTRS("Make sure that you select the correct row to delete"), "info")
      CANCEL DELETE
      RETURN
  END IF

  # Confirm deleting
  IF fgl_winbutton(this.LSTRS("Delete"),
                   this.LSTRS("Do you really want to delete this item?"),
                   this.LSTRS("Yes"),
                   SFMT("%1|%2", this.LSTRS("Yes"), this.LSTRS("No")),
                   "Question",
                   1) = this.LSTRS("No")
  THEN
      CANCEL DELETE
      RETURN                                               # Deleting is rejected
  END IF

  LET int_flag = FALSE

  CALL BeginWork()                                         # Start transaction
  LET this.internal.refresh_buffer = TRUE                  # We need to refresh buffer next time of fetching data

  IF NOT this.LockRow(index) THEN
      CALL RollbackWork()                                  # Rollback transaction
      CANCEL DELETE
      RETURN                                               # Stop deleting
  END IF

  IF this.DeleteDB(this.internal.buffer[index]) THEN                     # Check if deleting is successful
      MESSAGE SFMT(this.LSTRS("Delete %1  Successful operation"), this.LSTRT(this.settings.table))
      CALL this.internal.buffer.delete(index)
      CALL CommitWork()                                    # Commit transaction
  ELSE
      CANCEL DELETE
      CALL RollbackWork()                                  # Rollback transaction
  END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) Delete(parent_dlg UI.DIALOG, scr_line INT, arr_curr INT)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) Query() RETURNS BOOL
#
# Private method Query of InteractForm object
# Prompts for query restrictions and creates new WHERE clause string
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) Query() RETURNS BOOL
    DEFINE dlg                  UI.DIALOG                    # Interaction dynamic DIALOG
    DEFINE event, name          STRING
    DEFINE i                    INT
    DEFINE ret                  BOOL
    DEFINE actions, actionsExec, attributes  HASHMAP

    MESSAGE this.LSTRS("Please input query criteria")        # Show prompt message

    LET ret = FALSE

    CALL this.GetActions(SUB_DIALOG_QUERY_ID) RETURNING actions, actionsExec
    LET attributes = this.GetAttributes(SUB_DIALOG_QUERY_ID)

    LET  dlg = ui.Dialog.CreateConstructByName(this.internal.fields_form) # Create dynamic DIALOG with CONSTRUCT
    CALL AddTrigger(dlg, actions, "ON ACTION Accept")        # Add custom ACCEPT action
    CALL AddTrigger(dlg, actions, "ON ACTION Cancel")        # Add custom CANCEL action
    CALL AddTrigger(dlg, actions, "ON ACTION Clear" )        # Add custom CLEAR  action

    FOR i = 1 TO actions.getSize()
        CALL AddTrigger(dlg, actions, actions.GetKey(i))# Add custom action
    END FOR

    CALL SetAttributes(dlg, attributes)                      # Set custom attributes

    WHILE (event := dlg.NextEvent()) IS NOT NULL             # Process dialog and wait for the next event
        CASE event
            WHEN "BEFORE DIALOG"
                # Disable all form fields that are lookup
                FOR i = 1 TO this.internal.fields_lookup.GetSize()
                    CALL dlg.SetFieldActive(this.internal.fields_lookup[i].field, FALSE)
                END FOR
                CALL this.PopulateAllComboBox(TRUE, 1, TRUE, 3)
                FOR i = 1 TO this.internal.construct_vals.getSize()
                    CALL dlg.SetFieldValue(this.internal.construct_vals.getKey(i), this.internal.construct_vals.GetValue(i))
                END FOR
        END CASE

        IF this.ExecuteCustomAction(actionsExec, event, dlg) THEN
            CONTINUE WHILE
        END IF

        CASE event
            WHEN "AFTER DIALOG"
                IF ret THEN
                    LET this.settings.sql_where = ""
                    CALL this.internal.construct_vals.Clear()
                    FOR i = 1 TO this.internal.fields_find.GetSize() # Check each field
                        LET name = this.internal.fields_find[i].name
                        IF dlg.GetFieldValue(name) IS NOT NULL THEN # Check if field has been touched
                            # Add WHERE criteria
                            IF this.settings.sql_where IS NOT NULL THEN
                                LET this.settings.sql_where = this.settings.sql_where.Append(" AND ")
                            END IF
                            LET this.settings.sql_where = this.settings.sql_where.Append(dlg.GetQueryFromField(name))

                            # Store values for reusing them on the next QUERY action
                            LET this.internal.construct_vals[name] = dlg.GetFieldValue(name)
                        END IF
                    END FOR
                END IF
                EXIT WHILE

            WHEN "ON ACTION Clear"                           # Clear all fields
                FOR i = 1 TO this.internal.fields_find.GetSize()
                    CALL dlg.SetFieldValue(this.internal.fields_find[i].name, NULL)
                END FOR

            WHEN "ON ACTION Accept"
                LET ret = TRUE
                CALL dlg.Accept()

            WHEN "ON ACTION Cancel"
                LET ret = FALSE
                CALL dlg.Cancel()

            OTHERWISE
                CALL this.ManageBuildInEvent(dlg, event)
        END CASE
    END WHILE
    CALL dlg.Close()                                         # Close and release dynamic dialog

    IF NOT this.internal.is_table THEN
        CALL this.PopulateAllComboBox(TRUE, 1, FALSE, 3)
    END IF

    MESSAGE ""                                               # Clear any messages
    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) Query() RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput() RETURNS BOOL
#
#  Private method ExecuteInteraction of InteractForm object
#  Starts INPUT ARRAY interaction on form.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput() RETURNS BOOL
    DEFINE event, query, str_set, cname, answer  STRING
    DEFINE dlg                              UI.DIALOG         # Dynamic DIALOG
    DEFINE i, j, index                      INT
    DEFINE actions, actionsExec, attributes HASHMAP
    DEFINE stat, ret, append, isAccept      BOOL
    DEFINE noEntryFieldActive, bundleUpdate BOOL
    DEFINE touched, continue, doRollback    BOOL
    DEFINE inserting                        BOOL
    DEFINE deleted                          DYNAMIC ARRAY OF HASHMAP

    CALL this.GetActions(TOP_DIALOG_ID) RETURNING actions, actionsExec
    LET attributes = this.GetAttributes(TOP_DIALOG_ID)
    LET touched = FALSE
    LET isAccept = TRUE
    LET inserting = FALSE
    LET append = FALSE
    LET stat = TRUE
    LET ret = FALSE
    LET bundleUpdate = NOT (this.settings.paged_mode OR this.internal.single_row)

    WHILE stat
        LET  stat = FALSE
        LET  this.internal.refresh_buffer = TRUE

        LET  dlg = ui.Dialog.CreateInputArrayFrom(this.internal.fields_form, this.settings.screen_record)
        CALL AddTrigger(dlg, actions, "ON ACTION QUERY")     # Add custom ON ACTION QUERY action
        CALL AddTrigger(dlg, actions, "ON ACTION Accept")    # Add custom ON ACTION ACCEPT action
        CALL AddTrigger(dlg, actions, "ON ACTION Cancel")    # Add custom ON ACTION CANCEL action
        CALL AddTrigger(dlg, actions, "ON ACTION Refresh")   # Add custom ON ACTION REFRESH action

        IF bundleUpdate THEN
            CALL AddTrigger(dlg, actions, "ON ACTION Save")  # Add custom ON ACTION SAVE action
            CALL AddTrigger(dlg, actions, "ON ACTION Reset") # Add custom ON ACTION RESET action
        END IF

        IF this.settings.paged_mode THEN
            CALL AddTrigger(dlg, actions, "ON FILL BUFFER")  # Add custom ON FILL BUFFER action
        END IF

        FOR i = 1 TO actions.getSize()
            CALL AddTrigger(dlg, actions, actions.GetKey(i)) # Add custom action
        END FOR

        CALL this.AddTriggerOnChange(dlg)

        CALL dlg.SetDialogAttribute("COUNT", -1)
        CALL SetAttributes(dlg, this.settings.attributes[TOP_DIALOG_ID])

        WHENEVER ANY ERROR STOP

        WHILE (event := dlg.NextEvent()) IS NOT NULL         # Process dialog and wait for the next event
            CASE event
                WHEN "BEFORE DIALOG"
                    CALL PopulateToolbar(this.internal.single_row)
                    IF NOT this.settings.paged_mode THEN
                        CALL this.Show(-1, -1)
                    END IF

                WHEN "ON FILL BUFFER"
                    IF NOT this.Show(FGL_DIALOG_GETBUFFERSTART(), FGL_DIALOG_GETBUFFERLENGTH()) THEN
                        CONTINUE WHILE
                    END IF
            END CASE

            IF this.ExecuteCustomAction(actionsExec, event, dlg) THEN
                CONTINUE WHILE
            END IF

            CASE event
                WHEN "BEFORE ROW"
                    LET noEntryFieldActive = FALSE
                    IF NOT bundleUpdate THEN
                        LET noEntryFieldActive = this.internal.buffer[arr_curr()].KeyExists("LLC_new")
                    END IF

                    # Disable all form fields that are primary key
                    FOR i = 1 TO this.internal.fields_noentry.GetSize()
                        CALL dlg.SetFieldActive(this.internal.fields_noentry[i], noEntryFieldActive)
                    END FOR

                    LET append = FALSE

                    IF this.settings.paged_mode THEN
                        LET index = arr_curr() - this.internal.buffer_start + 1
                        IF index < 1 OR (index > this.internal.buffer.getSize() AND this.internal.buffer.getSize() > 0) THEN
                            CALL this.Show(arr_curr(), 1)
                            LET index = 1
                        END IF
                    END IF

                    LET this.output.selected_data = this.internal.buffer[arr_curr()]
                    CALL this.PopulateAllComboBox(TRUE, scr_line(), TRUE, 2)
                    CALL this.ShowNavigationStatus()

                WHEN "AFTER ROW"                                          # Save update if row is touched
                    IF this.IsCurrentRowTouched(dlg) THEN
                        LET int_flag = FALSE

                        IF bundleUpdate THEN
                            LET this.internal.buffer[arr_curr()]["LLC_touched"] = TRUE
                            LET touched = TRUE
                        ELSE
                            IF inserting THEN
                                IF fgl_winbutton("Add", "Add new item?", "Yes", "Yes|No", "Question", 1) = "Yes" THEN
                                    IF this.ExecuteInteractionInput_Insert(dlg) THEN
                                        LET inserting = FALSE
                                    END IF
                                    CONTINUE WHILE
                                ELSE
                                    LET inserting = FALSE
                                END IF
                            ELSE
                                CASE fgl_winbutton("Modify", "Save changes?", "Yes", "Yes|Cancel|Discard", "Question", 1)
                                    WHEN "Yes"                                    # Updating is confirmed by user
                                        IF NOT this.ExecuteInteractionInput_Update(dlg) THEN
                                            CONTINUE WHILE
                                        END IF

                                    WHEN "Cancel"
                                        CALL dlg.NextField("+CURR")                # Stay in current row
                                        CONTINUE WHILE

                                    OTHERWISE                                      # Updating is rejected
                                        # Revert row data
                                        FOR i = 1 TO this.internal.fields_form.GetSize()    # For each edited form field
                                            LET cname = this.internal.fields_form[i].name
                                            CALL dlg.SetFieldValue(cname, this.internal.buffer[index][cname])
                                        END FOR
                                END CASE
                            END IF
                        END IF
                    END IF

                    IF NOT this.internal.is_table THEN
                        CALL this.PopulateAllComboBox(TRUE, scr_line(), FALSE, 2)
                    END IF

                WHEN "BEFORE INSERT"
                    CALL this.ActivateFieldsForInsert()
                    LET append = arr_curr() > dlg.GetArrayLength()

                WHEN "AFTER INSERT"
                    MESSAGE ""

                    IF this.IsCurrentRowTouched(dlg) THEN
                        # Update parent array dialog with new values
                        CALL this.internal.buffer.insert(arr_curr())
                        LET this.internal.buffer[arr_curr()]["LLC_new"] = TRUE
                        LET touched = TRUE
                        IF NOT bundleUpdate THEN
                            LET inserting = TRUE
                        END IF
                    ELSE
                        CANCEL INSERT
                    END IF

                WHEN "AFTER DELETE"
                    IF bundleUpdate THEN
                        IF NOT this.internal.buffer[arr_curr()].KeyExists("LLC_new") THEN
                            CALL deleted.Append(this.internal.buffer[arr_curr()])
                            LET touched = TRUE
                        END IF
                        CALL this.internal.buffer.Delete(arr_curr())
                    ELSE
                        CALL this.Delete(dlg, scr_line(), arr_curr())
                    END IF

                WHEN "ON ACTION QUERY"
                    LET this.internal.curr_interact = INTERACT_QUERY
                    IF this.Query() THEN
                        LET stat = TRUE
                        CALL dlg.Accept()
                    END IF
                    LET this.internal.curr_interact = INTERACT_MAIN

                WHEN "ON ACTION Accept"
                    LET ret = TRUE                           # Means proceeded by user
                    LET isAccept = FALSE
                    CALL dlg.Accept()

                WHEN "ON ACTION Cancel"
                    LET ret = TRUE                           # Means proceeded by user
                    LET isAccept = TRUE
                    CALL dlg.Cancel()

                WHEN "AFTER DIALOG"
                    IF NOT bundleUpdate THEN
                        EXIT WHILE
                    END IF

                    LET continue = FALSE
                    IF touched THEN
                        CASE this.Question(this.LSTRS("Modify"),
                                           this.LSTRS("Save all changes?"),
                                           this.LSTRS("Yes"),
                                           SFMT("%1|%2|%3", this.LSTRS("Yes"), this.LSTRS("Cancel"), this.LSTRS("Discard all")),
                                           "Question",
                                           isAccept)
                        WHEN this.LSTRS("Yes")
                            IF this.ExecuteInteractionInput_BundleUpdate(dlg, deleted) THEN
                                LET ret = TRUE               # Means proceeded by user
                            ELSE
                                LET continue = TRUE          # Continue dialog
                            END IF

                        WHEN this.LSTRS("Cancel")
                            LET continue = TRUE

                        OTHERWISE
                            # Updating is rejected
                            LET ret = TRUE                   # Means proceeded by user
                        END CASE
                    END IF

                    IF NOT continue THEN
                        EXIT WHILE
                    END IF

                WHEN "ON ACTION Refresh"
                    IF touched THEN
                        CASE fgl_winbutton(this.LSTRS("Refresh"),
                                           this.LSTRS("Do you want to save changes before refreshing data?"),
                                           this.LSTRS("Save Data and Refresh"),
                                           SFMT("%1|%2|%3", this.LSTRS("Save"),
                                                            this.LSTRS("Don't Save"),
                                                            this.LSTRS("Cancel") ),
                                           "Question")

                            WHEN this.LSTRS("Cancel")
                                CONTINUE WHILE

                            WHEN this.LSTRS("Save")
                                IF NOT this.ExecuteInteractionInput_Save(dlg, deleted, bundleUpdate, inserting) THEN
                                    CONTINUE WHILE
                                END IF
                        END CASE

                        LET inserting = FALSE
                        LET touched = FALSE
                        CALL deleted.Clear()
                    END IF
                    CALL this.Refresh(FALSE)

                WHEN "ON ACTION Save"
                    IF this.Question(this.LSTRS("Modify"),
                                        this.LSTRS("Save all changes?"),
                                        this.LSTRS("Save"),
                                        SFMT("%1|%2", this.LSTRS("Save"), this.LSTRS("Cancel")),
                                        "Question",
                                        isAccept) = "Save"
                    THEN
                        IF NOT this.ExecuteInteractionInput_Save(dlg, deleted, bundleUpdate, inserting) THEN
                            CONTINUE WHILE
                        END IF
                    END IF

                    LET inserting = FALSE
                    LET touched = FALSE
                    CALL deleted.Clear()

                WHEN "ON ACTION Reset"
                    IF fgl_winbutton(this.LSTRS("Reset"),
                                     this.LSTRS("Reseting will remove all unsaved data. How do you want to proceed?"),
                                     this.LSTRS("Go Back to Editing"),
                                     SFMT("%1|%2", this.LSTRS("Reset"),
                                                   this.LSTRS("Go Back to Editing") ),
                                     "Question") = "Reset"
                    THEN
                        CALL this.Refresh(FALSE)                        
                        LET inserting = FALSE
                        LET touched = FALSE
                        CALL deleted.Clear()
                    END IF

                OTHERWISE
                    IF append THEN
                        CALL this.ManageAfterFieldOnInserting(dlg, event)
                    END IF
                    CALL this.ManageBuildInEvent(dlg, event)

            END CASE

            # Check if array is empty, so we need to hide UPDATE and DELETE actions
            CALL dlg.SetActionHidden("DELETE", dlg.GetArrayLength(this.settings.screen_record) = 0)
            CALL dlg.SetActionHidden("Save"  , NOT touched)
            CALL dlg.SetActionHidden("Reset" , NOT touched)
        END WHILE

        CALL this.internal.sqlHandle.Close()                 # Close sql handler
        CALL dlg.Close()                                     # Close and release dynamic dialog
    END WHILE

    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput() RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput_Save(
#    dlg ui.Dialog, deleted DYNAMIC ARRAY OF HASHMAP, bundleUpdate BOOL, inserting BOOL)
#    RETURNS BOOL
#
# Private method ExecuteInteractionInput_Save of InteractForm object
# Do all updates like Update/Insert/Delete
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput_Save(
    dlg ui.Dialog, deleted DYNAMIC ARRAY OF HASHMAP, bundleUpdate BOOL, inserting BOOL
) RETURNS BOOL
    IF bundleUpdate THEN
        RETURN this.ExecuteInteractionInput_BundleUpdate(dlg, deleted)
    ELSE
        IF inserting THEN
            RETURN this.ExecuteInteractionInput_Insert(dlg)
        ELSE
            RETURN this.ExecuteInteractionInput_Update(dlg)
        END IF
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput_Save() RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput_BundleUpdate(dlg ui.Dialog, deleted DYNAMIC ARRAY OF HASHMAP)
#         RETURNS BOOL
#
# Private method ExecuteInteractionInput_BundleUpdate of InteractForm object
# Do all updates like Update/Insert/Delete in bundle
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput_BundleUpdate(dlg ui.Dialog, deleted DYNAMIC ARRAY OF HASHMAP) RETURNS BOOL
    DEFINE doRollback             BOOL
    DEFINE i, arr_curr, scr_line  INT

    LET arr_curr = arr_curr()
    LET scr_line = scr_line()

    CALL BeginWork()                 # Start transaction

    LET int_flag = FALSE
    LET doRollback = FALSE

    # Delete rows from DB
    FOR i = 1 TO deleted.GetSize()
        IF this.LockRowBuf(deleted[i]) THEN
            LET doRollback = TRUE
            EXIT FOR
        END IF
        IF NOT this.DeleteDB(deleted[i]) THEN
            LET doRollback = TRUE
            EXIT FOR
        END IF
    END FOR

    IF NOT doRollback THEN
        FOR i = 1 TO this.internal.buffer.GetSize()
            CALL dlg.setCurrentRow(this.settings.screen_record, i)
            IF this.internal.buffer[i].KeyExists("LLC_new") THEN
                # Validate row
                IF NOT this.ValidateData(dlg, 0) THEN
                    LET doRollback = TRUE
                    EXIT FOR
                END IF

                # Insert row
                IF NOT this.InsertDB(dlg) THEN
                    LET doRollback = TRUE
                    EXIT FOR
                END IF
            ELSE IF this.internal.buffer[i].KeyExists("LLC_touched") THEN
                # Validate row
                IF NOT this.ValidateData(dlg, 0) THEN
                    LET doRollback = TRUE
                    EXIT FOR
                END IF

                # Update row
                IF this.LockRow(i) THEN
                    IF NOT this.UpdateDB(dlg, i) THEN       # Update values in DB
                        LET doRollback = TRUE
                        EXIT FOR
                    END IF
                ELSE
                    LET doRollback = TRUE
                    EXIT FOR
                END IF
            END IF END IF
        END FOR
    END IF

    IF doRollback THEN               # There is error, so:
        CALL dlg.setCurrentRow(this.settings.screen_record, i)
        CALL RollbackWork()          # * Rollback transaction
        RETURN FALSE
    END IF

    CALL fgl_dialog_setcurrline(scr_line, arr_curr)
    CALL dlg.setCurrentRow(this.settings.screen_record, arr_curr)
    CALL CommitWork()            # Commit transaction
    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput_BundleUpdate(dlg ui.Dialog, deleted DYNAMIC ARRAY OF HASHMAP)
#             RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput_Insert(dlg ui.Dialog) RETURNS BOOL
#
# Private method ExecuteInteractionInput_Insert of InteractForm object
# Inserts the new record
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput_Insert(dlg ui.Dialog) RETURNS BOOL
    DEFINE cname      STRING
    DEFINE index, i   INT

    IF NOT this.ValidateData(dlg, scr_line()) THEN
        RETURN FALSE
    END IF

    CALL BeginWork()                 # Start transaction

    LET this.internal.refresh_buffer = TRUE  # We need to refresh buffer next time of fetching data

    IF this.InsertDB(dlg) THEN
        MESSAGE SFMT(this.LSTRS("Insert %1  Successful operation"), this.LSTRT(this.settings.table))
        CALL CommitWork()            # Commit transaction

        # Update parent array dialog with new values
        LET index = arr_curr() - this.internal.buffer_start + 1
        CALL this.internal.buffer.insert(scr_line())
        FOR i = 1 TO this.internal.fields_form.GetSize()  # For each edited form field
            LET cname = this.internal.fields_form[i].name
            LET this.internal.buffer[index][cname] = dlg.GetFieldValue(cname)
            CALL dlg.setFieldTouched(cname, FALSE)
        END FOR
        RETURN TRUE
    END IF

    CALL RollbackWork()          # Rollback transaction
    CALL dlg.NextField("+CURR")  # Remain in current row
    RETURN FALSE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput_BundleUpdate(dlg ui.Dialog, deleted DYNAMIC ARRAY OF HASHMAP)
#             RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput_Update(dlg ui.Dialog) RETURNS BOOL
#
# Private method ExecuteInteractionInput_Update of InteractForm object
# Saves updates in the current record
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput_Update(dlg ui.Dialog) RETURNS BOOL
    DEFINE cname      STRING
    DEFINE index, i   INT

    IF NOT this.ValidateData(dlg, scr_line()) THEN
        RETURN FALSE
    END IF

    CALL BeginWork()                          # Start transaction
    LET this.internal.refresh_buffer = TRUE   # We need to refresh buffer next time of fetching data

    LET index = arr_curr() - this.internal.buffer_start + 1
    IF this.LockRow(index) THEN
        IF this.UpdateDB(dlg, index) THEN      # Update values in DB
            # Updating is done well
            MESSAGE SFMT(this.LSTRS("Edit %1  Successful operation"), this.LSTRT(this.settings.table))
            CALL CommitWork()                  # Commit transaction
            # Update buffer with new values
            FOR i = 1 TO this.internal.fields_update.GetSize() # For each edited form field
                LET cname = this.internal.fields_update[i]
                LET this.internal.buffer[index][cname] = dlg.GetFieldValue(cname)
            END FOR
            RETURN TRUE
        ELSE
            CALL RollbackWork()                # Rollback transaction
            CALL dlg.NextField("+CURR")        # Stay in current row
        END IF
    ELSE
        CALL RollbackWork()                    # Rollback transaction
        CALL dlg.NextField("+CURR")            # Continue input in current row
    END IF
    RETURN FALSE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) ExecuteInteractionInput_BundleUpdate(dlg ui.Dialog, deleted DYNAMIC ARRAY OF HASHMAP)
#             RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) AddWhereClauseForCurrentRow(sh BASE.SqlHandle, row_rec HASHMAP) RETURNS STRING
#
# Private method GetFullWhereClause of InteractForm object
# Makes and returns full WHERE clause for the particular current row.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) AddWhereClauseForCurrentRow(sh BASE.SqlHandle, row_rec HASHMAP) RETURNS STRING
    DEFINE tmp, ret STRING
    DEFINE i        INT

    FOR i = 1 TO this.internal.fields_where.GetSize()        # For each column
        IF row_rec[this.internal.fields_where[i]] IS NULL THEN
            LET tmp = "IS NULL"                              # Check for NULL value
        ELSE
            LET tmp = get_query_from_value_type(row_rec[this.internal.fields_where[i]])  # Create constrain using field's variable
        END IF
        IF tmp.GetLength() > 0 THEN
            IF ret.GetLength() > 0 THEN
                LET ret = ret, " AND "
            END IF
            LET ret = ret, this.internal.fields_where[i] CLIPPED, " ", tmp
        END IF
    END FOR

    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) AddWhereClauseForCurrentRow(sh BASE.SqlHandle, row_rec HASHMAP) RETURNS STRING
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) GetWhereClauseForRow(index INT) RETURNS STRING
# Private method GetWhereClauseForRow of InteractForm object
# Makes and returns full WHERE clause for the particular current row.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) GetWhereClauseForRow(index INT) RETURNS STRING
    RETURN this.GetWhereClauseForRowBuf(this.internal.buffer[index])
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) GetWhereClauseForRow(index INT) RETURNS STRING
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) GetWhereClauseForRowBuf(buf HASHMAP) RETURNS STRING
#
# Private method GetWhereClauseForRowBuf of InteractForm object
# Makes and returns full WHERE clause for the particular current row.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) GetWhereClauseForRowBuf(buf HASHMAP) RETURNS STRING
    DEFINE ret, tmp STRING
    DEFINE i        INT

    FOR i = 1 TO this.internal.fields_where.GetSize()        # For each column
        IF buf[this.internal.fields_where[i]] IS NULL THEN
            LET tmp = "IS NULL"                              # Check for NULL value
        ELSE
            LET tmp = " = ?"                                 # Create constrain using field's variable
        END IF
        IF tmp.GetLength() > 0 THEN
            IF ret.GetLength() > 0 THEN
                LET ret = ret, " AND "
            END IF
            LET ret = ret, this.internal.fields_where[i] CLIPPED, " ", tmp
        END IF
    END FOR

    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) GetWhereClauseForRowBuf(buf HASHMAP) RETURNS STRING
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) SetWhereClauseParametersForRow(sh BASE.SqlHandle, param INT, index INT)
#
# Private method SetWhereClauseParametersForRow of InteractForm object
# Set parameters for SqlHandler for WHERE clause for the specific row.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) SetWhereClauseParametersForRow(sh BASE.SqlHandle, param INT, index INT)
    CALL this.SetWhereClauseParametersForRowBuf(sh, param, this.internal.buffer[index])
END FUNCTION


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) SetWhereClauseParametersForRowBuf(sh BASE.SqlHandle, param INT, buf HASHMAP)
#
# Private method SetWhereClauseParametersForRowBuf of InteractForm object
# Set parameters for SqlHandler for WHERE clause for the specific row.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) SetWhereClauseParametersForRowBuf(sh BASE.SqlHandle, param INT, buf HASHMAP)
    DEFINE i INT

    FOR i = 1 TO this.internal.fields_where.GetSize()        # For each column
        IF buf[this.internal.fields_where[i]] IS NOT NULL THEN
            CALL sh.SetParameter(param, buf[this.internal.fields_where[i]])
            LET param = param + 1
        END IF
    END FOR
END FUNCTION
################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) SetWhereClauseParametersForRowBuf(sh BASE.SqlHandle, param INT, buf HASHMAP)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) LockRow(index INT) RETURNS BOOL
# Private method LockRow of InteractForm object
#
# Arguments:
# - index : index of row in data row buffer
#
# Returns FALSE if row is locked or being modified otherwise it returns TRUE.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) LockRow(index INT) RETURNS BOOL
    RETURN this.LockRowBuf(this.internal.buffer[index])
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) LockRow(index INT) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) LockRowBuf(buf HASHMAP) RETURNS BOOL
#
# Private method LockRowBuf of InteractForm object
#
# Arguments:
# - buf : row buffer
#
# Returns FALSE if row is locked or being modified otherwise it returns TRUE.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) LockRowBuf(buf HASHMAP) RETURNS BOOL
    DEFINE sh            BASE.SqlHandle              # SQL handler for query
    DEFINE i             INT
    DEFINE cname, ftype  STRING

    WHENEVER ERROR CONTINUE                          # Continue on error, we will handle it itself

    # Prepare sql handle FOR UPDATE
    LET  sh = BASE.SqlHandle.Create()
    CALL sh.Prepare("SELECT * FROM " || this.settings.table || " WHERE " || this.GetWhereClauseForRowBuf(buf) || " FOR UPDATE")
    CALL this.SetWhereClauseParametersForRowBuf(sh, 1, buf)
    CALL sh.Open()
    CALL sh.Fetch()

    IF sqlca.sqlcode = -244 THEN                     # Check if record is being modified already
        CALL this.Error(this.LSTRT(this.settings.table), this.LSTRS("Another user/session is already modifying record"))
        RETURN FALSE                                 # Return FALSE that means it is being modified
    END IF

    IF sqlca.sqlcode == NOTFOUND THEN                # Check if record is being modified already
        CALL this.Error(this.LSTRT(this.settings.table), SFMT(this.LSTRS("Row in the table '%1' doesn't exist any more or primary key has been modified"), this.LSTRT(this.settings.table)))
        RETURN FALSE                                 # Return FALSE that means it is being modified
    END IF

    IF sqlca.sqlerrd[2] = 104 OR sqlca.sqlcode < 0 THEN
        CALL this.Error(this.LSTRT(this.settings.table), SFMT(this.LSTRS("Row in the table '%1' is locked. Error code %2. %3"), this.LSTRT(this.settings.table), sqlca.sqlcode, err_get(sqlca.sqlcode)))
        RETURN FALSE                                 # Return TRUE that means it is locked
    END IF

    FOR i = 1 TO sh.GetResultCount()                 # Process all columns
        LET cname = this.settings.table || "." || sh.GetResultName(i) CLIPPED
        IF buf.KeyExists(cname) THEN
            LET ftype = sh.GetResultType(i)
            IF NOT(ftype = "Byte" OR ftype = "Text") THEN
                IF NOT Equal(buf[cname], sh.GetResultValue(i)) THEN  # Check if data has been modified
                    CALL this.Error(this.LSTRT(this.settings.table), this.LSTRS("Another user/session has already modified record"))
                    IF this.internal.curr_interact < INTERACT_INSERT THEN
                        CALL this.Refresh(FALSE)
                    END IF
                    RETURN FALSE
                END IF
            END IF
        END IF
    END FOR

    WHENEVER ERROR STOP

    RETURN TRUE                                      # Return TRUE, It means it is locked successfully
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) LockRowBuf(buf HASHMAP) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) UpdateDB(dlg ui.Dialog, index INT) RETURNS BOOL
#
# Private method UpdateDB of InteractForm object
# Updates row in Database
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) UpdateDB(dlg ui.Dialog, index INT) RETURNS BOOL
    DEFINE str_set, query  STRING
    DEFINE sh              BASE.SqlHandle
    DEFINE i               INT

    # Make query for updating record   UPDATE table_name SET(field1,field2,...) = (?,?,...) WHERE where_clause
    FOR i = 1 TO this.internal.fields_update.GetSize()
        IF query.GetLength() > 0 THEN
            LET str_set = str_set, ","
            LET query = query, ","
        END IF
        LET query = query, this.internal.fields_update[i]
        LET str_set = str_set, "?"
    END FOR
    LET query = "UPDATE ",this.settings.table," SET (", query, ") = (", str_set, ") WHERE ", this.GetWhereClauseForRow(index)

    WHENEVER ERROR CONTINUE

    # Perform the prepared UPDATE statement
    LET sh = BASE.SqlHandle.Create()
    CALL sh.Prepare(query)                                   # Prepare query

    # Set all parameters
    FOR i = 1 TO this.internal.fields_update.GetSize()                # For each edited form field
        CALL sh.SetParameter(i, dlg.GetFieldValue(this.internal.fields_update[i]))  # Set parameter
    END FOR
    CALL this.SetWhereClauseParametersForRow(sh, i, index)   # Set parameter of sql handle for current row
    CALL sh.Execute()                                        # Execute updating in the database

    WHENEVER ERROR STOP

    IF sqlca.sqlcode < 0 THEN
        # Updating is failed
        CALL this.Error(this.LSTRT(this.settings.table),
                        SFMT(this.LSTRS("Editing item in the table '%1' failed with the error code %2. %3"), this.LSTRT(this.settings.table), sqlca.sqlcode, err_get(sqlca.sqlcode)))
        RETURN FALSE
    END IF

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) UpdateDB(dlg ui.Dialog, index INT) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) InsertDB(dlg ui.Dialog) RETURNS BOOL
#
#  Private method InsertDB of InteractForm object
#  Inserts row to Database
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) InsertDB(dlg ui.Dialog) RETURNS BOOL
    DEFINE str_set, query  STRING
    DEFINE sh              BASE.SqlHandle
    DEFINE i               INT

    # Insert new row
    WHENEVER ERROR CONTINUE

    # Make query for inserting record   INSERT INTO table_name (field1,field2,...) VALUES (?,?,...)
    FOR i = 1 TO this.internal.fields_insert.GetSize()
        IF i > 1 THEN
            LET str_set = str_set, ","
            LET query = query, ","
        END IF
        LET query = query , this.internal.fields_insert[i]
        LET str_set = str_set, "?"
    END FOR
    LET query = "INSERT INTO ", this.settings.table, " (", query, ") VALUES (", str_set, ")"

    LET  sh = BASE.SqlHandle.Create()
    CALL sh.Prepare(query)               # Prepare sql handle
    # Set parameters with values from the form/input
    FOR i = 1 TO this.internal.fields_insert.GetSize()
        CALL sh.SetParameter(i, dlg.GetFieldValue(this.internal.fields_insert[i]))
    END FOR
    CALL sh.Execute()                    # Execute INSERT into DB

    WHENEVER ERROR STOP

    IF sqlca.sqlcode < 0 THEN
        CALL this.Error(this.LSTRT(this.settings.table),
                        SFMT(this.LSTRS("Adding item to the table '%1' failed with the error code %2. %3"), this.LSTRT(this.settings.table), sqlca.sqlcode, err_get(sqlca.sqlcode)))
        RETURN FALSE
    END IF

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) InsertDB(dlg ui.Dialog, index INT) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) DeleteDB(buf HASHMAP) RETURNS BOOL
#
#  Private method DeleteDB of InteractForm object
#  Deletes row from buf parameter in Database
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) DeleteDB(buf HASHMAP) RETURNS BOOL
    DEFINE sh              BASE.SqlHandle

    # === Deleting ===
    WHENEVER ERROR CONTINUE

    LET  sh = BASE.SqlHandle.Create()
    CALL sh.Prepare("DELETE FROM " || this.settings.table || " WHERE " || this.GetWhereClauseForRowBuf(buf))
    CALL this.SetWhereClauseParametersForRowBuf(sh, 1, buf)  # Set parameter of sql handle for current row
    CALL sh.Execute()                                        # Execute deleting

    WHENEVER ERROR STOP

    IF sqlca.sqlcode < 0 THEN                                # Check if deleting is successful
        CALL this.Error(this.LSTRT(this.settings.table),
                        SFMT(this.LSTRS("Deleting from the table '%1' failed with the error code %2. %3"), this.LSTRT(this.settings.table), sqlca.sqlcode, err_get(sqlca.sqlcode)))
        RETURN FALSE
    END IF

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) DeleteDB(buf HASHMAP) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) InitPrimaryKey()
#
#  Private method InitPrimaryKey of InteractForm object
#  - Collect fields that are Primary Key depends on database type
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) InitPrimaryKey()
    CALL this.internal.primary_key.Clear()

    CASE db_get_database_type()
        WHEN "IFX"                                           # INFORMIX database
            CALL this.InitPrimaryKeyInformix()
        WHEN "MSV"                                           # SQL Server database
            CALL this.InitPrimaryKeySQLServer()
    END CASE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) InitPrimaryKey()
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) InitPrimaryKeyInformix()
#
#  Private method InitPrimaryKeyInformix of InteractForm object
#  - Collect fields that are Primary Key for Informix database
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) InitPrimaryKeyInformix()
    DEFINE query, col STRING
    DEFINE i          INT
    DEFINE sh         BASE.SqlHandle                         # SQL handler for query

    WHENEVER ERROR CONTINUE                                  # Don't stop on error because we will handle error

    LET query =
        "SELECT decode(si.part1,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part1  AND sc.tabid = si.tabid))),",
        "       decode(si.part2,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part2  AND sc.tabid = si.tabid))),",
        "       decode(si.part3,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part3  AND sc.tabid = si.tabid))),",
        "       decode(si.part4,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part4  AND sc.tabid = si.tabid))),",
        "       decode(si.part5,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part5  AND sc.tabid = si.tabid))),",
        "       decode(si.part6,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part6  AND sc.tabid = si.tabid))),",
        "       decode(si.part7,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part7  AND sc.tabid = si.tabid))),",
        "       decode(si.part8,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part8  AND sc.tabid = si.tabid))),",
        "       decode(si.part9,  0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part9  AND sc.tabid = si.tabid))),",
        "       decode(si.part10, 0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part10 AND sc.tabid = si.tabid))),",
        "       decode(si.part11, 0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part11 AND sc.tabid = si.tabid))),",
        "       decode(si.part12, 0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part12 AND sc.tabid = si.tabid))),",
        "       decode(si.part13, 0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part13 AND sc.tabid = si.tabid))),",
        "       decode(si.part14, 0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part14 AND sc.tabid = si.tabid))),",
        "       decode(si.part15, 0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part15 AND sc.tabid = si.tabid))),",
        "       decode(si.part16, 0, \"\", trim((SELECT sc.colname FROM syscolumns sc WHERE sc.colno = si.part16 AND sc.tabid = si.tabid))) ",
        "  FROM sysindexes si,           ",
        "       sysconstraints sc,       ",
        "       systables st             ",
        " WHERE si.idxname = sc.idxname  ",
        "   AND sc.constrtype = \"P\"    ",
        "   AND si.tabid = st.tabid      ",
        "   AND st.tabname LIKE '", this.settings.table, "'"

    LET  sh = BASE.SqlHandle.Create()                        # Instantiate SqlHandle object
    CALL sh.Prepare(query)
    CALL sh.Open()

    CALL sh.Fetch()
    IF sqlca.sqlcode <> NOTFOUND THEN
        FOR i = 1 TO sh.GetResultCount()                     # Process columns of primary key one by one
            LET col = sh.GetResultValue(i) CLIPPED           # Get next column name of primary key
            IF col.GetLength() > 0 THEN                      # If it's empty then there is no more columns in primary key
                CALL this.AddPrimaryKeyField(col)
            ELSE
                EXIT FOR                                     # There is no more columns in primary key
            END IF
        END FOR
    END IF

    WHENEVER ERROR STOP
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) InitPrimaryKeyInformix()
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) InitPrimaryKeySQLServer()
#
#  Private method InitPrimaryKeySQLServer of InteractForm object
#  - Collect fields that are Primary Key for SQL Server database
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) InitPrimaryKeySQLServer()
    DEFINE query  STRING
    DEFINE sh     BASE.SqlHandle                             # SQL handler for query

    WHENEVER ERROR CONTINUE                                  # Don't stop on error because we will handle error

    LET query =
        "SELECT scol.name              ",
        "  FROM syscolumns scol,       ",
        "       sysindexkeys sik,      ",
        "       sysindexes si,         ",
        "       sysobjects so,         ",
        "       sysconstraints sc,     ",
        "       sysobjects soc         ",
        " WHERE so.xtype = 'U'         ",
        "   AND sc.id = so.id          ",
        "   AND soc.id = sc.constid    ",
        "   AND soc.name = si.name     ",
        "   AND si.indid = sik.indid   ",
        "   AND si.id = sik.id         ",
        "   AND scol.id = si.id        ",
        "   AND scol.colid = sik.colid ",
        "   AND so.name = '", this.settings.table, "'"

    LET  sh = BASE.SqlHandle.Create()                        # Instantiate SqlHandle object
    CALL sh.Prepare(query)
    CALL sh.Open()

    WHILE TRUE                                               # Process columns of primary key one by one
        CALL sh.Fetch()
        IF sqlca.sqlcode = NOTFOUND THEN
            EXIT WHILE                                       # There is no more columns in primary key
        END IF
        CALL this.AddPrimaryKeyField(sh.GetResultValue(1) CLIPPED)
    END WHILE

    WHENEVER ERROR STOP
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) InitPrimaryKeySQLServer()
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) AddPrimaryKeyField(col STRING)
#
# Private method AddPrimaryKeyField of InteractForm object
#  - Store column as a Primary Key in InteractForm.primary_key
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) AddPrimaryKeyField(col STRING)
    IF col.GetLength() > 0 THEN                      # If it's empty then there is no more columns in primary key
        CALL this.internal.primary_key.Append(this.settings.table || "." || col)
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) AddPrimaryKeyField(col STRING)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) AppendLookup(table STRING, field STRING) RETURNS INT
#
#  Private method AppendLookup of InteractForm object
#  - Collect, resolve and make query for getting lookup values
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) AppendLookup(table STRING, field STRING) RETURNS INT
    DEFINE scr_rec, where, where_param,lookupTable  STRING
    DEFINE record_fields, split                     DYNAMIC ARRAY OF STRING
    DEFINE i, k                                     INT
    DEFINE ui_element                               ui.AbstractUiElement

    LET scr_rec = table || "." || field
    CALL ui.Window.GetCurrent().GetForm().getScreenRecordFields(scr_rec) RETURNING record_fields
    IF record_fields.getSize() = 0 THEN
        CALL ui.Window.GetCurrent().GetForm().getScreenRecordFields(field) RETURNING record_fields
    END IF

    IF record_fields.getSize() < 2 THEN    # Screen record should contain at least one pair for matching foreign key
        RETURN 0
    END IF

    LET k = this.internal.fields_lookup.getSize() + 1

    LET this.internal.fields_lookup[k].field = table || "." || field
    LET this.internal.fields_lookup[k].is_blob = FALSE

    LET lookupTable = ui.Window.GetCurrent().GetForm().ResolveTableAlias(table)

    FOR i = 1 TO record_fields.getSize() STEP 2
        IF (i + 1) > record_fields.getSize() THEN
            EXIT FOR
        END IF

        IF where IS NOT NULL THEN
            LET where = where || " AND "
            LET where_param = where_param || " AND "
        END IF

        CALL record_fields[i].split(".") RETURNING split
        IF split.getSize() < 2 THEN
            LET where = where.append(record_fields[i])
            LET where_param = where_param.append(record_fields[i])
        ELSE
            IF this.settings.table.equalsIgnoreCase(split[1]) THEN
                CALL this.internal.fields_lookup[k].depends_on.append(split[2]) # record_fields[i]
                LET where = where.append(record_fields[i])
                LET where_param = where_param.append("?")
            ELSE
                LET lookupTable = ui.Window.GetCurrent().GetForm().ResolveTableAlias(split[1])
                LET where = where.append(lookupTable || "." || split[2])
                LET where_param = where_param.append(lookupTable || "." || split[2])
            END IF
        END IF

        LET where = where || " = "
        LET where_param = where_param || " = "

        CALL record_fields[i + 1].split(".") RETURNING split
        IF split.getSize() < 2 THEN
            LET where = where.append(record_fields[i + 1])
            LET where_param = where_param.append(record_fields[i + 1])
        ELSE
            IF this.settings.table.equalsIgnoreCase(split[1]) THEN
                CALL this.internal.fields_lookup[k].depends_on.append(split[2]) # record_fields[i + 1]
                LET where = where.append(record_fields[i + 1])
                LET where_param = where_param.append("?")
            ELSE
                LET lookupTable = ui.Window.GetCurrent().GetForm().ResolveTableAlias(split[1])
                LET where = where.append(lookupTable || "." || split[2])
                LET where_param = where_param.append(lookupTable || "." || split[2])
            END IF
        END IF
    END FOR

    LET lookupTable = ui.Window.GetCurrent().GetForm().ResolveTableAlias(table)

    LET this.internal.fields_lookup[k].query =
           "(SELECT " || field ||
            "  FROM " || lookupTable ||
            " WHERE " || where ||
           ") " || field

    LET this.internal.fields_lookup[k].query_param = "SELECT " || field || " FROM " || lookupTable || " WHERE " || where_param

    RETURN k
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) AppendLookup(table STRING, field STRING) RETURNS INT
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) InitPopulatingComboBox(field STRING)
#
#  Public method InitPopulatingComboBox of InteractForm object
#  - Make dependencies that affect on the list of available values
#  - Populates ComboBox with values from referenced DB table
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) InitPopulatingComboBox(field STRING)
    DEFINE combo                     ui.ComboBox
    DEFINE tabCol                    ui.TableColumn
    DEFINE absObj                    ui.AbstractUiElement
    DEFINE distObj                   ui.DistributedObject
    DEFINE combobox_def              ComboboxDef
    DEFINE i                         INT
    DEFINE query, table, where       STRING
    DEFINE record_fields, split, depends_on  DYNAMIC ARRAY OF STRING

    LET combo = ui.ComboBox.ForName(field)
    IF combo IS NULL THEN
        RETURN                                               # There is no ComboBox with such name
    END IF

# Make main query for getting value-title for combobox
    CALL ui.Window.GetCurrent().GetForm().getScreenRecordFields(field) RETURNING record_fields
    IF record_fields.getSize() < 2 THEN
        RETURN
    END IF

    CALL record_fields[1].split(".") RETURNING split
    IF split.getSize() < 2 THEN
        RETURN
    END IF

    LET table = split[1]

    LET query = "SELECT "
    FOR i = 1 TO record_fields.getSize()
        IF i > 1 THEN
            LET query = query || ", "
        END IF
        LET query = query || record_fields[i]
    END FOR
    LET query = query || " FROM " || table

    LET combobox_def.foreign_key = record_fields[1]

# Make where clause for filtering values for populating
    CALL ui.Window.GetCurrent().GetForm().getScreenRecordFields(PREF_FILTER_COMBOBOX || field) RETURNING record_fields
    IF record_fields.getSize() > 1 THEN # There should be pairs of lookup table field and primary table field
        FOR i = 1 TO record_fields.getSize() STEP 2
            IF (i + 1) > record_fields.getSize() THEN
                EXIT FOR
            END IF

            IF where IS NOT NULL THEN
                LET where = where || " AND "
            END IF

            CALL record_fields[i].split(".") RETURNING split
            IF split.getSize() < 2 THEN
                LET where = where.append(record_fields[i])
            ELSE
                IF this.settings.table.equalsIgnoreCase(split[1]) THEN
                    CALL depends_on.append(split[2])
                    LET where = where.append("?")
                ELSE
                    LET where = where.append(record_fields[i])
                END IF
            END IF

            LET where = where || " = "

            CALL record_fields[i + 1].split(".") RETURNING split
            IF split.getSize() < 2 THEN
                LET where = where.append(record_fields[i + 1])
            ELSE
                IF this.settings.table.equalsIgnoreCase(split[1]) THEN
                    CALL depends_on.append(split[2])
                    LET where = where.append("?")
                ELSE
                    LET where = where.append(record_fields[i + 1])
                END IF
            END IF
        END FOR
    END IF

    LET combobox_def.query      = query
    LET combobox_def.sql_where  = where
    LET combobox_def.depends_on = depends_on
    LET combobox_def.combobox   = combo

    IF this.internal.is_table THEN
        LET absObj = combo
        LET tabCol = absObj.GetContainer()
        LET combobox_def.combobox_edit    = tabCol.GetEditControl()
        LET combobox_def.combobox_control = tabCol.GetControl()
        LET combobox_def.combobox_constr  = tabCol.GetConstructControl()
    ELSE
        LET distObj = combo
        LET combobox_def.splintered = distObj.split()
    END IF

    LET combobox_def.field_bare = GetColumnName(field)

    IF this.settings.comboboxes.contains(field) THEN
        LET combobox_def.sql_where_static  = this.settings.comboboxes[field]
    END IF
    LET this.internal.comboboxes[field].* = combobox_def.*

    CALL this.PopulateComboBox(field, 0, FALSE, 0)
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) InitPopulatingComboBox(field STRING)
################################################################################################################################


################################################################################################################################
# PUBLIC FUNCTION (this InteractForm) PopulateComboBoxWhere(field STRING, where STRING)
#
#  Public method PopulateComboBoxWhere of InteractForm object
#  - Populates ComboBox with values from referenced DB table and defined filter
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) PopulateComboBoxWhere(field STRING, where STRING)
    DEFINE i, role, line INT

    LET i = iCaseSearchHashmap(this.internal.comboboxes, field)
    IF i > 0 THEN                                            # Check if the combobox exists
        LET this.internal.comboboxes[field].sql_where_static = where
        IF this.internal.curr_interact > INTERACT_MAIN OR this.settings.input_mode THEN
            IF this.internal.curr_interact = INTERACT_QUERY THEN
                LET role = 3
                LET line = 1
            ELSE
                LET role = 2
                LET line = scr_line()
            END IF
            CALL this.PopulateComboBox(field, line, TRUE, role)
        END IF
    END IF
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) PopulateComboBoxWhere(field STRING, where STRING)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) PopulateAllComboBox(dependent_only BOOL, scr_line INT, filter BOOL, control_role INT)
#
#  Private method PopulateAllComboBox of InteractForm object
#  - Populates All ComboBoxs with values from referenced DB table
#
#  Parameters:
#  * dependent_only (BOOL) - If TRUE then populate comboboxes that are depend on other values only
#  * scr_line       (INT ) - The index of screen line that should be populated. It should be 0 if populates all rows/lines
#  * filter         (BOOL) - Use filter from dependent field's values
#  * control_role   (INT ) - The role of combobox (0 - General/All; 1 - Common control; 2 - Edit control; 3 - Construct control)
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) PopulateAllComboBox(dependent_only BOOL, scr_line INT, filter BOOL, control_role INT)
    DEFINE i             INT
    DEFINE combobox_def  ComboboxDef

    FOR i = 1 TO this.internal.comboboxes.GetSize()
        LET combobox_def = this.internal.comboboxes.GetValue(i)
        IF dependent_only AND combobox_def.depends_on.getSize() = 0 THEN
            CONTINUE FOR
        END IF
        CALL this.PopulateComboBox(this.internal.comboboxes.GetKey(i), scr_line, filter, control_role)
    END FOR
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) PopulateAllComboBox(dependent_only BOOL, scr_line INT, filter BOOL, control_role INT)
################################################################################################################################



################################################################################################################################
# PUBLIC FUNCTION (this InteractForm) PopulateComboBox(field STRING, scr_line INT, filter BOOL, control_role INT)
#
#  Public method PopulateComboBox of InteractForm object
#  - Populates ComboBox with values from referenced DB table
#
#  Parameters:
#  * field        (STRING) - The filed/combobox name
#  * scr_line     (INT   ) - The index of screen line that should be populated. It should be 0 if populates all rows/lines
#  * filter       (BOOL  ) - Use filter from dependent field's values
#  * control_role (INT   ) - The role of combobox (0 - General/All; 1 - Common control; 2 - Edit control; 3 - Construct control)
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) PopulateComboBox(field STRING, scr_line INT, filter BOOL, control_role INT)
    DEFINE combo         ui.ComboBox
    DEFINE combobox_def  ComboboxDef
    DEFINE i, j          INT
    DEFINE sh            BASE.SqlHandle
    DEFINE val           REFERENCE
    DEFINE query, item_label, ftype, where  STRING

    LET i = iCaseSearchHashmap(this.internal.comboboxes, field)
    IF i = 0 THEN                                            # Check if the combobox exists
        RETURN
    END IF

    LET combobox_def = this.internal.comboboxes.GetValue(i)

    LET query = combobox_def.query
    IF query IS NULL THEN
        RETURN                                               # There is no query defined
    END IF

    IF this.internal.is_table THEN
        CASE control_role
            WHEN 1      # Control
                LET combo = combobox_def.combobox_control
            WHEN 2      # Edit
                LET combo = combobox_def.combobox_edit
            WHEN 3      # Construct
                LET combo = combobox_def.combobox_constr
            OTHERWISE   # All
                LET combo = combobox_def.combobox
        END CASE
    ELSE
        IF scr_line > combobox_def.splintered.getSize() THEN # Check if scr_line is valid index
            LET scr_line = 1
        END IF

        IF scr_line > 0 THEN
            LET combo = combobox_def.splintered[scr_line]
        ELSE
            LET combo = combobox_def.combobox
        END IF
    END IF

    IF combo IS NULL THEN
        RETURN                                               # There is no ComboBox with such name
    END IF

    LET where = combobox_def.sql_where_static

    IF filter THEN
        IF combobox_def.sql_where IS NOT NULL THEN
            IF where IS NULL THEN
                LET where = combobox_def.sql_where
            ELSE
                LET where = where || " AND " || combobox_def.sql_where
            END IF
        END IF
    END IF

    IF where IS NOT NULL THEN
        LET query = query || " WHERE " || where
    END IF

    CALL sh.Prepare(query)

    IF filter THEN
        IF where IS NOT NULL THEN
            LET j = 0
            FOR i = 1 TO combobox_def.depends_on.getSize()
                LET j = where.getIndexOf("?", j + 1)
                IF j > 0 THEN
                    WHENEVER ANY ERROR CONTINUE
                    CALL ui.Dialog.GetCurrent().GetFieldValue(combobox_def.depends_on[i]) RETURNING val
                    IF status <> 0 THEN
                        RETURN                                   # Combobox can not be populated, because referenced value is not constructed
                    END IF
                    WHENEVER ANY ERROR STOP
                    CALL sh.SetParameter(i, val)
                ELSE
                    EXIT FOR
                END IF
            END FOR
        END IF
    END IF

    CALL sh.Open()

    IF sqlca.sqlcode < 0 THEN                                # Check for valid query
        CALL this.Error(this.LSTRT("Combobox populating"), SFMT(this.LSTRS("Populating of combobox '%1' is failed. Error code %2. %3"), this.LSTRC(field), sqlca.sqlcode, err_get(sqlca.sqlcode)))
        RETURN
    END IF

    CALL combo.clear()

    IF combo.GetNotNull() <> TRUE THEN
        CALL combo.AddItem(NULL, "")
    END IF

    WHILE TRUE
        CALL sh.Fetch()
        IF sqlca.sqlcode = NOTFOUND THEN
            EXIT WHILE                                       # There is no more data
        END IF

        LET item_label = ""
        FOR i = 2 TO sh.GetResultCount()
            LET ftype = sh.GetResultType(i)
            IF ftype = "Byte" OR ftype = "Text" THEN
                CONTINUE FOR                                 # Text or Byte can not be used for making title
            END IF
            IF item_label.GetLength() > 0 THEN
                LET item_label = item_label.Append(", ")     # All labels in title are  separated by comma
            END IF
            LET item_label = item_label.Append(sh.GetResultValue(i))
        END FOR

        CALL combo.AddItem(sh.GetResultValue(1), item_label) # Add the new ComboBox item with value and title
    END WHILE
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) PopulateComboBox(field STRING, scr_line INT, filter BOOL, control_role INT)
################################################################################################################################


################################################################################################################################
# PUBLIC FUNCTION (this InteractForm) AddTriggerOnChange(dlg ui.DIALOG)
#  Private method AddOnChangeEvent of InteractForm object
#  - Add ON CHANGE event to fill lookup values
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) AddTriggerOnChange(dlg ui.DIALOG)
    DEFINE i, j       INT
    DEFINE combo_def  ComboboxDef

    FOR i = 1 TO this.internal.fields_lookup.getSize()
        FOR j = 1 TO this.internal.fields_lookup[i].depends_on.getSize()
            CALL dlg.AddTrigger("ON CHANGE FIELD " || this.internal.fields_lookup[i].depends_on[j])
        END FOR
    END FOR

    FOR i = 1 TO this.internal.comboboxes.getSize()
        LET combo_def = this.internal.comboboxes.GetValue(i)
        FOR j = 1 TO combo_def.depends_on.getSize()
            CALL dlg.AddTrigger("ON CHANGE FIELD " || combo_def.depends_on[j])
        END FOR
    END FOR
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) AddTriggerOnChange(dlg ui.DIALOG)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) ManageBuildInEvent(dlg ui.DIALOG, event STRING)
#
#  Private method ManageBuildInEvent of InteractForm object
#  - Manage ON CHANGE event for filling lookup values
#  - Manage BEFORE FIELD event for populating combobox values
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ManageBuildInEvent(dlg ui.DIALOG, event STRING)
    CALL this.ManageOnChangeEvent(dlg, event)
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) ManageBuildInEvent(dlg ui.DIALOG, event STRING)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) ManageOnChangeEvent(dlg ui.DIALOG, event STRING)
#
#  Private method ManageOnChangeEvent of InteractForm object
#  - Manage ON CHANGE event for filling lookup values
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ManageOnChangeEvent(dlg ui.DIALOG, event STRING)
    DEFINE i, j, k, role, line INT
    DEFINE sh            BASE.SqlHandle
    DEFINE combo_def     ComboboxDef

    IF NOT event.subString(1, 16).equals("ON CHANGE FIELD ") THEN
        RETURN
    END IF

    FOR i = 1 TO this.internal.fields_lookup.getSize()
        FOR j = 1 TO this.internal.fields_lookup[i].depends_on.getSize()
            IF event = ("ON CHANGE FIELD " || this.internal.fields_lookup[i].depends_on[j]) THEN
            # Update dependent lookup fields
                LET sh = BASE.SqlHandle.Create()
                CALL sh.Prepare(this.internal.fields_lookup[i].query_param)
                FOR k = 1 TO this.internal.fields_lookup[i].depends_on.getSize()
                    CALL sh.SetParameter(k, dlg.GetFieldValue(this.internal.fields_lookup[i].depends_on[k]))
                END FOR
                CALL sh.Open()
                CALL sh.Fetch()

                IF sqlca.sqlcode = NOTFOUND THEN
                    CALL dlg.SetFieldValue(this.internal.fields_lookup[i].field, "")
                ELSE
                    CALL dlg.SetFieldValue(this.internal.fields_lookup[i].field, sh.GetResultValue(1))
                END IF

                EXIT FOR
            END IF
        END FOR
    END FOR

    IF this.internal.curr_interact = INTERACT_QUERY THEN
        LET role = 3
        LET line = 1
    ELSE
        LET role = 2
        LET line = scr_line()
    END IF

    FOR i = 1 TO this.internal.comboboxes.getSize()
        LET combo_def = this.internal.comboboxes.GetValue(i)
        FOR j = 1 TO combo_def.depends_on.getSize()
            IF event = ("ON CHANGE FIELD " || combo_def.depends_on[j]) THEN
                CALL this.PopulateComboBox(this.internal.comboboxes.GetKey(i), line, TRUE, role)
            END IF
        END FOR
    END FOR
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) ManageOnChangeEvent(dlg ui.DIALOG, event STRING)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) ManageAfterFieldOnInserting(dlg ui.DIALOG, event STRING)
#  Private method ManageAfterFieldOnInserting of InteractForm object
#  - Manage ON AFTER FIELD event for checking appending new row if previous append row is not touched
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ManageAfterFieldOnInserting(dlg ui.DIALOG, event STRING)
    IF event.subString(1, 12).equals("AFTER FIELD ") THEN
        IF fgl_lastaction() = "nextrow" THEN
            IF NOT this.IsCurrentRowTouched(dlg) THEN
                CALL dlg.nextField("+CURR")
            END IF
        END IF
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) ManageAfterFieldOnInserting(dlg ui.DIALOG, event STRING)
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) IsCurrentRowTouched(dlg ui.DIALOG) RETURNS BOOL
# Private method IsCurrentRowTouched of InteractForm object
# Checks if the current row is touched
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) IsCurrentRowTouched(dlg ui.DIALOG) RETURNS BOOL
    DEFINE i INT

    FOR i = 1 TO this.internal.fields_form.GetSize()     # For each edited form field
        IF dlg.getFieldTouched(this.internal.fields_form[i].name) THEN
            RETURN TRUE
        END IF
    END FOR

    RETURN FALSE
END FUNCTION
################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) IsCurrentRowTouched(dlg ui.DIALOG) RETURNS BOOL
################################################################################################################################


################################################################################################################################
# PRIVATE FUNCTION (this InteractForm) Error(title STRING, message STRING)
#    Shows and logs error message
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) Error(title STRING, message STRING)
    IF this.internal.log_to_file THEN
        CALL errorlog(title || ": " || message)
    END IF

    CALL fgl_winmessage(title, message, "error")

END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) Error(title STRING, message STRING)
################################################################################################################################


################################################################################################################################
#
#  Private function ExecuteCustomAction check if custom action is defined and execute it
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ExecuteCustomAction(actions HASHMAP, event STRING, dlg ui.DIALOG) RETURNS BOOL
    DEFINE ret        BOOL
    DEFINE i          INT
    DEFINE execFunc   FUNCTION(iform InteractForm INOUT) RETURNS BOOL

    LET i = iCaseSearchHashmap(actions, event)
    IF i = 0 THEN                                     # Check for custom function for current event
        RETURN FALSE
    END IF

    LET execFunc = actions.GetValue(i)                # Check if custom function is not NULL
    IF execFunc IS NULL THEN
        RETURN FALSE
    END IF

# Execute custom function for current event
    CALL execFunc(this) RETURNING ret

    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) ExecuteCustomAction(actions HASHMAP, event STRING, dlg ui.DIALOG) RETURNS BOOL
################################################################################################################################


################################################################################################################################
#
#  Private function GetWhereClause creates WHERE clause for the main query
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) GetWhereClause() RETURNS STRING
    DEFINE where_clause STRING

    LET where_clause = this.settings.sql_where_static

    IF this.settings.sql_where IS NOT NULL THEN
        IF where_clause IS NOT NULL THEN
            LET where_clause = where_clause.append(" AND ")
        END IF
        LET where_clause = where_clause.append(this.settings.sql_where)
    END IF

    IF where_clause IS NULL THEN
        LET where_clause = " "                                   # Avoid NULL for concatenation
    ELSE
        LET where_clause = " WHERE " || where_clause
    END IF

    RETURN where_clause
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) GetWhereClause() RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Private function GetOrderBy creates ORDER BY clause for the main query
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) GetOrderBy() RETURNS STRING
    DEFINE order_by_clause STRING
    LET order_by_clause = " "                                    # Avoid NULL for concatenation

    IF this.settings.sql_order_by IS NOT NULL THEN
        LET order_by_clause = " ORDER BY " || this.settings.sql_order_by
    END IF

    RETURN order_by_clause
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) GetOrderBy() RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Private function GetTopClause creates FIRST or TOP clause for the main query
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) GetTopClause() RETURNS STRING
    DEFINE top_clause STRING
    LET top_clause = " "                                         # Avoid NULL for concatenation

    IF this.settings.sql_top > 0 THEN
        CASE db_get_database_type()
            WHEN "IFX"                                           # INFORMIX database
                LET top_clause = " FIRST " || this.settings.sql_top
            WHEN "MSV"                                           # SQL Server database
                LET top_clause = " TOP " || this.settings.sql_top
        END CASE
    END IF

    RETURN top_clause
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) GetTopClause() RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Private function GetLimitClause creates LIMITS or FETCH FIRST clause for the main query
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) GetLimitClause() RETURNS STRING
    DEFINE limit_clause STRING
    LET limit_clause = " "                                       # Avoid NULL for concatenation

    IF this.settings.sql_top > 0 THEN
        CASE db_get_database_type()
            WHEN "MYS"                                           # MySQL database
                LET limit_clause = " LIMIT " || this.settings.sql_top
            WHEN "ORA"                                           # ORACLE Server database
                LET limit_clause = " FETCH FIRST " || this.settings.sql_top || " ROWS ONLY"
        END CASE
    END IF

    RETURN limit_clause
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) GetLimitClause() RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Public function SetFieldValue sets the value to the defined field (form field or virtual)
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) SetFieldValue(field STRING, val VARIANT)
    DEFINE dlg ui.Dialog
    LET dlg = ui.Dialog.GetCurrent()
    CALL dlg.SetFieldValue(field, val)
    CALL this.ManageOnChangeEvent(dlg, "ON CHANGE FIELD " || field)
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) SetFieldValue(field STRING, val VARIANT)
################################################################################################################################


################################################################################################################################
#
#  Public function GetFieldValue returns a value of the defined field (form field or virtual)
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) GetFieldValue(field STRING) RETURNS VARIANT
    DEFINE dlg ui.Dialog
    DEFINE val VARIANT
    LET dlg = ui.Dialog.GetCurrent()
    CALL dlg.GetFieldValue(field) RETURNING val
    RETURN val
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) GetFieldValue(field STRING) RETURNS VARIANT
################################################################################################################################


################################################################################################################################
#
#  Private function InitVirtualFields creates and append fields to the form for managing virtual fields from screen record
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) InitVirtualFields(record_fields DYNAMIC ARRAY OF STRING)
    DEFINE i, j, count                  INT
    DEFINE distributedObj               ui.DistributedObject
    DEFINE field, prototype, parent     ui.AbstractUiElement
    DEFINE tabCol                       ui.TableColumn
    DEFINE list                         DYNAMIC ARRAY OF ui.DistributedObject
    DEFINE virtuals                     DYNAMIC ARRAY OF STRING
    DEFINE parentId, tableId, columnId  STRING
    DEFINE isTable                      BOOL

    FOR i = 1 TO record_fields.GetSize()
        LET field = ui.AbstractUiElement.ForName(record_fields[i])
        IF field IS NULL THEN
            CALL virtuals.Append(record_fields[i])
        ELSE
            LET prototype = field
        END IF
    END FOR

    IF prototype IS NULL THEN
        RETURN
    END IF

    LET parent = prototype.GetContainer()

    IF parent IS NULL THEN
        RETURN
    END IF

    LET tabCol = ui.TableColumn.ForName(parent.GetIdentifier())
    LET isTable = tabCol IS NOT NULL

    IF isTable THEN
        LET parent = tabCol.GetContainer()
        LET tableId = parent.GetIdentifier()
    ELSE
        LET distributedObj = prototype
        CALL distributedObj.Split() RETURNING list
        LET count = list.GetSize()
        LET parentId = parent.GetIdentifier()
    END IF

    FOR i = 1 TO virtuals.GetSize()
        IF isTable THEN
            LET columnId = virtuals[i] || "_clm"
            LET tabCol = ui.TableColumn.Create(columnId, tableId)
            LET field = ui.TextField.Create(virtuals[i], columnId)
            CALL tabCol.SetVisible(FALSE)
            CALL tabCol.SetCollapsed(TRUE)
            CALL field.SetVisible(FALSE)
            CALL field.SetCollapsed(TRUE)
            CALL tabCol.Complete()
        ELSE
            FOR j = 1 TO count
                LET field = ui.TextField.Create(virtuals[i], parentId)
                CALL field.SetVisible(FALSE)
                CALL field.SetCollapsed(TRUE)
            END FOR
        END IF
    END FOR
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) InitVirtualFields(record_fields DYNAMIC ARRAY OF STRING)
################################################################################################################################


################################################################################################################################
#
#  Private function SearchFormField makes case insensitive search of record in fields_form
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) SearchFormField(field STRING) RETURNS INT
    DEFINE i INT
    FOR i = 1 TO this.internal.fields_form.GetSize()
        IF this.internal.fields_form[i].name.equalsIgnoreCase(field) THEN
            RETURN i
        END IF
    END FOR
    RETURN 0
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) SearchFormField(field STRING) RETURNS INT
################################################################################################################################


################################################################################################################################
#
#  Private function SearchLookup makes case insensitive search of record in fields_lookup
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) SearchLookup(field STRING) RETURNS INT
    DEFINE i INT
    FOR i = 1 TO this.internal.fields_lookup.GetSize()
        IF this.internal.fields_lookup[i].field.equalsIgnoreCase(field) THEN
            RETURN i
        END IF
    END FOR
    RETURN 0
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) SearchLookup(field STRING) RETURNS INT
################################################################################################################################


################################################################################################################################
#
#  Private function Question shows message box with buttons
#  If it is called on ACCEPT and this.settings.confirm_accept is FALSE then it returns default button immediately
#  If it is called on CANCEL and this.settings.confirm_cancel is FALSE then it returns default button immediately
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) Question(
      title     STRING
    , message   STRING
    , default   STRING
    , buttons   STRING
    , icon      STRING
    , isAccept  BOOL
    ) RETURNS STRING

    IF isAccept THEN
        IF this.settings.confirm_accept < 0 THEN
            RETURN default
        END IF
    ELSE
        IF this.settings.confirm_cancel < 0 THEN
            RETURN "No"
        END IF
    END IF

    RETURN fgl_winbutton(title, message, default, buttons, icon)
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) Question(....) RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Private function GetAttributes returns attributes for defined subdialog
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) GetAttributes(subDialog STRING) RETURNS HASHMAP
    RETURN this.GetHashmapVal(this.settings.attributes, subDialog)
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) GetAttributes(subDialog STRING) RETURNS HASHMAP
################################################################################################################################


################################################################################################################################
#
#  Private function GetActions returns actions for defined subdialog
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) GetActions(subDialog STRING) RETURNS (HASHMAP, HASHMAP)
    DEFINE actions, expanded HASHMAP
    DEFINE act, s STRING
    DEFINE i, j, k, inf INT
    DEFINE fields DYNAMIC ARRAY OF STRING

    CONSTANT onAction = "ON ACTION "
    CONSTANT onChange = "ON CHANGE "

    LET actions = this.GetHashmapVal(this.settings.actions, subDialog)
    CALL actions.CopyTo(expanded)

    FOR i = 1 TO actions.GetSize()
        LET act = actions.GetKey(i)

        # Expand INFIELD field1, field2, ...
        LET inf = act.GetIndexOf(" INFIELD ", onAction.GetLength() + 1)
        IF inf > 0 THEN
            LET expanded[act.SubString(1, inf).Trim()] = actions.GetValue(i)
        END IF

        # Expand ON CHANGE field1, field2, ...
        IF act.SubString(1, onChange.GetLength()).equalsIgnoreCase(onChange) THEN
            CALL act.SubString(onChange.GetLength() + 1, act.GetLength()).Split(",") RETURNING fields
            FOR j = 1 TO fields.GetSize()
                IF fields[j].Trim() = "*" THEN # ON CHANGE all fields
                    FOR k = 1 TO this.internal.fields_form.GetSize()
                        LET expanded[onChange || GetColumnName(this.internal.fields_form[k].name)] = actions.GetValue(i)
                    END FOR
                    EXIT FOR
                END IF
                IF fields.GetSize() > 1 THEN
                    LET expanded[onChange || GetColumnName(fields[j])] = actions.GetValue(i)
                END IF
            END FOR
        END IF
    END FOR

    RETURN actions, expanded
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) GetActions(subDialog STRING) RETURNS HASHMAP
################################################################################################################################


################################################################################################################################
#
#  Private function GetAttributes returns attributes for defined subdialog
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) GetHashmapVal(hash HASHMAP, subDialog STRING) RETURNS HASHMAP
    DEFINE h HASHMAP
    DEFINE i INT
    LET i = iCaseSearchHashmap(hash, subDialog)
    IF i > 0 THEN
        RETURN hash.GetValue(i)
    END IF
    RETURN h
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) GetHashmapVal(hash HASHMAP, subDialog STRING) RETURNS HASHMAP
################################################################################################################################


################################################################################################################################
#
#  Public function LSTRS translates the string to localized string or to human text/name
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) LSTRS(str STRING) RETURNS STRING
    RETURN this.LSTR(str, 0)
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) LSTRS(str STRING) RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Public function LSTRT translates the table name to localized string or to human text/name
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) LSTRT(str STRING) RETURNS STRING
    RETURN this.LSTR(str, 1)
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) LSTRT(str STRING) RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Public function LSTRC translates the column name to localized string or to human text/name
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) LSTRC(str STRING) RETURNS STRING
    RETURN this.LSTR(str, 2)
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) LSTRC(str STRING) RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Private function LSTR translates the string to localized string or to human text/name
#  type: 0 - any
#        1 - table name
#        2 - column name in format <table_name>.<column_name>
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) LSTR(str STRING, type INT) RETURNS STRING
    DEFINE i                 INT
    DEFINE ret, t, a         STRING
    DEFINE field, container  ui.AbstractUiElement
    DEFINE strField          ui.AbstractStringField
    DEFINE tableColumn       ui.TableColumn

    # Check if map has such string translation
    LET i = iCaseSearchHashmap(this.settings.translations, str)
    IF i > 0 THEN
        RETURN this.settings.translations.GetValue(i)
    END IF

    # Lazy searching for translating
    CASE type
        WHEN 1 # table name
            LET t = "$human$" || str
            LET ret = ui.Window.GetCurrent().GetForm().ResolveTableAlias(t)
            IF NOT ret.equals(t) THEN
                LET this.settings.translations[str] = ret
                RETURN ret
            END IF

        WHEN 2 # column name in format <table_name>.<column_name>
            LET strField = ui.AbstractStringField.ForName("lb_" || str)
            IF strField IS NOT NULL THEN
                LET ret = strField.getText()
                LET this.settings.translations[str] = ret
                RETURN ret
            END IF

            LET field = ui.AbstractUiElement.ForName(str)
            IF field IS NOT NULL THEN
                LET container = field.GetContainer()
                IF container IS NOT NULL THEN
                    LET tableColumn = ui.TableColumn.forName(container.GetIdentifier())
                    IF tableColumn IS NOT NULL THEN
                        LET ret = tableColumn.getText()
                        LET this.settings.translations[str] = ret
                        RETURN ret
                    END IF
                END IF
            END IF
    END CASE

    # Get localized string
    LET ret = lstr(str)
    LET this.settings.translations[str] = ret
    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) LSTR(str STRING, type INT) RETURNS STRING
################################################################################################################################


################################################################################################################################
#
#  Private function ActivateFieldsForInsert activates fields for inserting
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ActivateFieldsForInsert()
    DEFINE dlg    ui.Dialog
    DEFINE field  STRING
    DEFINE i      INT

    LET dlg = ui.Dialog.GetCurrent()

    # Disable all form fields that are not in insert list
    FOR i = 1 TO this.internal.fields_form.GetSize()
        CALL dlg.SetFieldActive(this.internal.fields_form[i].name, this.internal.fields_insert.Search("", this.internal.fields_form[i].name) > 0)
    END FOR

    # Disable all form fields that are lookup
    FOR i = 1 TO this.internal.fields_lookup.GetSize()
        CALL dlg.SetFieldActive(this.internal.fields_lookup[i].field, FALSE)
    END FOR
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) ActivateFieldsForInsert()
################################################################################################################################


################################################################################################################################
#
#  Private function ValidateData validates data before putting it into DB.
#  Returns FALSE if validation is failed.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) ValidateData(dlg ui.Dialog, scr_line INT) RETURNS BOOL
    DEFINE i, j, k       INT
    DEFINE combobox_def  ComboboxDef
    DEFINE var           VARIANT
    DEFINE field, deps   STRING
    DEFINE sh            BASE.SqlHandle
    DEFINE val           REFERENCE
    DEFINE query, where  STRING

    FOR i = 1 TO this.internal.comboboxes.GetSize()
        LET combobox_def = this.internal.comboboxes.GetValue(i)
        IF combobox_def.depends_on.getSize() = 0 THEN
            CONTINUE FOR                                     # There is nothing to validate
        END IF

        LET field = this.internal.comboboxes.GetKey(i)
        LET var = dlg.GetFieldValue(field)

        IF var IS NULL THEN
            CONTINUE FOR                                     # Value is not set, ignore it
        END IF

        LET query = combobox_def.query
        IF query IS NULL THEN
            CONTINUE FOR                                 # There is no query defined
        END IF

        LET where = combobox_def.sql_where_static

        IF combobox_def.sql_where IS NOT NULL THEN
            IF where IS NULL THEN
                LET where = combobox_def.sql_where
            ELSE
                LET where = where || " AND " || combobox_def.sql_where
            END IF
        END IF

        IF where IS NULL THEN
            CONTINUE FOR
        END IF

        LET query = query || " WHERE " || where || " AND " || combobox_def.foreign_key || " = ?"

        CALL sh.Prepare(query)

        IF where IS NOT NULL THEN
            LET j = 0
            FOR k = 1 TO combobox_def.depends_on.getSize()
                LET j = where.getIndexOf("?", j + 1)
                IF j > 0 THEN
                    WHENEVER ANY ERROR CONTINUE
                    CALL dlg.GetFieldValue(combobox_def.depends_on[k]) RETURNING val
                    IF status <> 0 THEN
                        CALL this.Error(this.LSTRT("Validating"), SFMT(this.LSTRS("Validating of conditional lookup combobox '%1' is failed. Value of field '%2' can not be retrived"), this.LSTRC(field), combobox_def.depends_on[k]))
                        RETURN FALSE
                    END IF
                    WHENEVER ANY ERROR STOP
                    CALL sh.SetParameter(k, val)
                ELSE
                    EXIT FOR
                END IF
            END FOR
            CALL sh.SetParameter(k, var)
        END IF

        CALL sh.Open()

        IF sqlca.sqlcode < 0 THEN                            # Check for valid query
            CALL this.Error(this.LSTRT("Validating"), SFMT(this.LSTRS("Validating of conditional lookup combobox '%1' is failed. Error code %2. %3"), this.LSTRC(field), sqlca.sqlcode, err_get(sqlca.sqlcode)))
            RETURN FALSE
        END IF

        CALL sh.Fetch()
        IF sqlca.sqlcode = NOTFOUND THEN                     # Value is not valid, show the error message
            FOR j = 1 TO combobox_def.depends_on.getSize()
                IF deps IS NULL THEN
                    LET deps = combobox_def.depends_on[j]
                ELSE
                    IF j = combobox_def.depends_on.getSize() THEN
                        LET deps = deps || ", "
                    ELSE
                        LET deps = deps || " and "
                    END IF
                    LET deps = deps || combobox_def.depends_on[j]
                END IF
            END FOR
            CALL this.Error(this.LSTRS("Validating"),
                            SFMT(this.LSTRS("%1 value '%2' is not valid for the %3"),
                            field, var, deps))
            CALL dlg.NextField(field)
            RETURN FALSE
        END IF
    END FOR

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) ValidateData(dlg ui.Dialog, scr_line INT) RETURNS BOOL
################################################################################################################################


################################################################################################################################
#
#  Private function UpdateCount updates the variable internal.count with count of rows.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) UpdateCount()
    DEFINE sh                BASE.SqlHandle
    DEFINE sql_count         STRING

    LET sql_count = "SELECT COUNT(*) FROM " || this.settings.table || this.GetWhereClause()

    IF db_get_database_type() = "MSV" THEN
        CALL sql_count.Append(" WITH (NOLOCK)")
    END IF

    SET ISOLATION TO DIRTY READ                              # Set such isolation for geting count even if there is locked row

    LET  sh = BASE.SqlHandle.Create()
    CALL sh.Prepare(sql_count)
    CALL sh.Open()
    CALL sh.Fetch()
    LET this.internal.count = sh.GetResultValue(1)

    SET ISOLATION TO COMMITTED READ                          # Back to default isolation

    IF this.settings.sql_top > 0 AND this.internal.count > this.settings.sql_top THEN
        LET this.internal.count = this.settings.sql_top
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) UpdateCount()
################################################################################################################################


################################################################################################################################
#
#  Public function ShowNavigationStatus updates the navigation status in format <arr_curr>/<arr_count>.
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) ShowNavigationStatus()
    DEFINE f ui.AbstractStringField

    IF this.settings.navigation_status IS NULL THEN
        RETURN
    END IF

    LET f = ui.AbstractStringField.ForName(this.settings.navigation_status)
    IF f IS NULL THEN
        CALL this.Error(this.LSTRS("Show Navigation Status"),
                        this.LSTRS("Field " || this.settings.navigation_status || " was not found."))
        RETURN
    END IF
    IF arr_count() > 0 OR this.settings.input_mode THEN   # arr_curr() is 1 even when arr_count() is 0, but "1/0" looks not good
        CALL f.SetText(arr_curr() || "/" || arr_count())
    ELSE
        CALL f.SetText("0/0")
    END IF
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) ShowNavigationStatus()
################################################################################################################################


################################################################################################################################
#
#  Public function ClearNavigationStatus clear the navigation status.
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) ClearNavigationStatus()
    DEFINE f ui.AbstractStringField

    IF this.settings.navigation_status IS NULL THEN
        RETURN
    END IF

    LET f = ui.AbstractStringField.ForName(this.settings.navigation_status)
    IF f IS NOT NULL THEN
        CALL f.SetText("")
    END IF
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) ClearNavigationStatus()
################################################################################################################################


################################################################################################################################
#
#  Public function Refresh renews all buffered data.
#
################################################################################################################################
PUBLIC FUNCTION (this InteractForm) Refresh(withQuestion BOOL) RETURNS BOOL
    DEFINE index, i INT
    DEFINE sh       BASE.SqlHandle
    DEFINE cname    STRING
    DEFINE dlg      ui.Dialog

    CASE this.internal.curr_interact
        WHEN INTERACT_UPDATE
            LET index = this.internal.arr_curr - this.internal.buffer_start + 1
            IF this.IsEqualInDB(index, sh) THEN
                RETURN FALSE                                 # There is nothing to update
            END IF

        WHEN INTERACT_INSERT
            RETURN FALSE                                     # It's inserting action, there is nothing to update

        WHEN INTERACT_QUERY
            RETURN FALSE                                     # It's query action, there is nothing to update
    END CASE

    IF withQuestion AND (this.settings.input_mode OR this.internal.curr_interact = INTERACT_UPDATE) THEN
        IF fgl_winbutton(this.LSTRS("Refresh"),
                         this.LSTRS("Refreshing will remove all unsaved data. Do you want to proceed and refresh?"),
                         this.LSTRS("Refresh"),
                         SFMT("%1|%2", this.LSTRS("Refresh"), this.LSTRS("Cancel")),
                         "Question") = this.LSTRS("Cancel")
        THEN
            RETURN FALSE
        END IF
    END IF

    CASE this.internal.curr_interact
        WHEN INTERACT_MAIN
            LET this.internal.refresh_buffer = TRUE

            IF this.settings.paged_mode THEN
                CALL this.Show(this.internal.buffer_start, this.internal.buffer.getSize())
            ELSE
                CALL this.Show(-1, -1)
            END IF
        WHEN INTERACT_UPDATE
            LET dlg = ui.Dialog.GetCurrent()
            # Update current data
            FOR i = 1 TO sh.GetResultCount()
                LET cname = this.settings.table || "." || sh.GetResultName(i) CLIPPED
                IF this.internal.buffer[index].KeyExists(cname) THEN
                    LET this.internal.buffer[index][cname] = sh.GetResultValue(i)
                    IF this.SearchFormField(cname) > 0 THEN
                        CALL dlg.SetFieldValue(cname, this.internal.buffer[index][cname])
                        CALL dlg.SetFieldTouched(cname, FALSE)
                    END IF
                END IF
            END FOR
            CALL this.PopulateAllComboBox(TRUE, this.internal.scr_line, TRUE, 2)
    END CASE

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) Refresh(withQuestion BOOL) RETURNS BOOL
################################################################################################################################


################################################################################################################################
#
#  Private function IsEqualInDB checks record has been changed in DB.
#
################################################################################################################################
PRIVATE FUNCTION (this InteractForm) IsEqualInDB(index INT, sh BASE.SqlHandle) RETURNS BOOL
    DEFINE buf           HASHMAP
    DEFINE i             INT
    DEFINE cname, ftype  STRING

    LET buf = this.internal.buffer[index]
    LET  sh = BASE.SqlHandle.Create()
    CALL sh.Prepare("SELECT * FROM " || this.settings.table || " WHERE " || this.GetWhereClauseForRowBuf(buf))
    CALL this.SetWhereClauseParametersForRowBuf(sh, 1, buf)
    CALL sh.Open()
    CALL sh.Fetch()

    FOR i = 1 TO sh.GetResultCount()                 # Process all columns
        LET cname = this.settings.table || "." || sh.GetResultName(i) CLIPPED
        IF buf.KeyExists(cname) THEN
            LET ftype = sh.GetResultType(i)
            IF NOT(ftype = "Byte" OR ftype = "Text") THEN
                IF NOT Equal(buf[cname], sh.GetResultValue(i)) THEN  # Check if data has been modified
                    RETURN FALSE
                END IF
            END IF
        END IF
    END FOR

    RETURN TRUE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) IsEqualInDB(index INT)
################################################################################################################################


################################################################################################################################
#
#  PUBLIC function AddTrigger check if custom action is not NULL then add this trigger to dialog
#
################################################################################################################################
PUBLIC FUNCTION AddTrigger(dlg UI.DIALOG, actions HASHMAP, event STRING)
    DEFINE i INT
    LET i = iCaseSearchHashmap(actions, event)
    IF i > 0 THEN
        IF actions.GetValue(i) IS NULL THEN            # Check for custom function for current event
            RETURN
        END IF
    END IF
    CALL dlg.addTrigger(event)
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION AddTrigger(dlg UI.DIALOG, actions HASHMAP, event STRING)
################################################################################################################################


################################################################################################################################
#
#  PUBLIC function SetAttributes adds custom attributes
#
################################################################################################################################
PUBLIC FUNCTION SetAttributes(dlg ui.Dialog, attributes HASHMAP)
    DEFINE i INT
    FOR i = 1 TO attributes.getSize()
        CALL dlg.setDialogAttribute(attributes.GetKey(i), attributes.GetValue(i)) # Add custom attribute
    END FOR
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION SetAttributes(dlg ui.Dialog, attributes HASHMAP)
################################################################################################################################


################################################################################################################################
#
#  Begin transaction
#
################################################################################################################################
PRIVATE FUNCTION BeginWork()
    BEGIN WORK
    CALL RetainUpdateLocks()
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION BeginWork()
################################################################################################################################


################################################################################################################################
#
#  Commit transaction
#
################################################################################################################################
PRIVATE FUNCTION CommitWork()
    CALL DoNotRetainUpdateLocks()
    COMMIT WORK
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION CommitWork()
################################################################################################################################


################################################################################################################################
#
#  Rollback transaction
#
################################################################################################################################
PRIVATE FUNCTION RollbackWork()
    CALL DoNotRetainUpdateLocks()
    ROLLBACK WORK
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION RollbackWork()
################################################################################################################################


################################################################################################################################
#
#  Set isolation to committed read retain update locks
#
################################################################################################################################
PRIVATE FUNCTION RetainUpdateLocks()
    IF db_get_database_type() = "IFX" THEN           # INFORMIX database only
        EXECUTE IMMEDIATE "SET ISOLATION TO COMMITTED READ RETAIN UPDATE LOCKS"
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION RetainUpdateLocks()
################################################################################################################################


################################################################################################################################
#
#  Set isolation to committed read
#
################################################################################################################################
PRIVATE FUNCTION DoNotRetainUpdateLocks()
    IF db_get_database_type() = "IFX" THEN           # INFORMIX database only
        EXECUTE IMMEDIATE "SET ISOLATION TO COMMITTED READ"
    END IF
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION DoNotRetainUpdateLocks()
################################################################################################################################


################################################################################################################################
#
#  Populates toolbar with buttons Update, Insert, Delete, Help, FirstRow, PrevPage, NextPage and LastRow.
#
################################################################################################################################
PRIVATE FUNCTION PopulateToolbar(single_row BOOL)
    CALL ui.ActionView.ForAction("FirstRow", "dialog").SetDefaultView("yes")
    CALL ui.ActionView.ForAction("PrevPage", "dialog").SetDefaultView("yes")
    CALL ui.ActionView.ForAction("NextPage", "dialog").SetDefaultView("yes")
    CALL ui.ActionView.ForAction("LastRow" , "dialog").SetDefaultView("yes")

    CALL ui.ActionView.ForAction("FirstRow", "dialog").SetShowInContextMenu("yes")
    CALL ui.ActionView.ForAction("PrevPage", "dialog").SetShowInContextMenu("yes")
    CALL ui.ActionView.ForAction("NextPage", "dialog").SetShowInContextMenu("yes")
    CALL ui.ActionView.ForAction("LastRow" , "dialog").SetShowInContextMenu("yes")

    CALL ui.ActionView.ForAction("FirstRow", "dialog").SetActionImage("qx://embedded/firstrow.png")
    CALL ui.ActionView.ForAction("PrevPage", "dialog").SetActionImage("qx://embedded/prevrow.png" )
    CALL ui.ActionView.ForAction("NextPage", "dialog").SetActionImage("qx://embedded/nextrow.png" )
    CALL ui.ActionView.ForAction("LastRow" , "dialog").SetActionImage("qx://embedded/lastrow.png" )
    CALL ui.ActionView.ForAction("Query"   , "dialog").SetActionImage("qx://embedded/find.png"    )

    CALL ui.ActionView.ForAction("FirstRow", "dialog").SetComment("First record")
    CALL ui.ActionView.ForAction("LastRow" , "dialog").SetComment("Last record" )

    IF single_row THEN
        CALL ui.ActionView.ForAction("PrevPage", "dialog").SetComment("Previous record")
        CALL ui.ActionView.ForAction("NextPage", "dialog").SetComment("Next record"    )
    END IF

    CALL ui.ActionView.ForAction("FirstRow", "dialog").SetPlace("top")
    CALL ui.ActionView.ForAction("PrevPage", "dialog").SetPlace("top")
    CALL ui.ActionView.ForAction("NextPage", "dialog").SetPlace("top")
    CALL ui.ActionView.ForAction("LastRow" , "dialog").SetPlace("top")
    CALL ui.ActionView.ForAction("Update"  , "dialog").SetPlace("top")
    CALL ui.ActionView.ForAction("Insert"  , "dialog").SetPlace("top")
    CALL ui.ActionView.ForAction("Delete"  , "dialog").SetPlace("top")
    CALL ui.ActionView.ForAction("Query"   , "dialog").SetPlace("top")

    CALL ui.ActionView.ForAction("FirstRow", "dialog").SetOrder(1)
    CALL ui.ActionView.ForAction("PrevPage", "dialog").SetOrder(2)
    CALL ui.ActionView.ForAction("NextPage", "dialog").SetOrder(3)
    CALL ui.ActionView.ForAction("LastRow" , "dialog").SetOrder(4)
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION PopulateToolbar()
################################################################################################################################


################################################################################################################################
#
#  Private function Equal check if variables are equal
#
################################################################################################################################
PRIVATE FUNCTION Equal(l REFERENCE, r REFERENCE) RETURNS BOOL
    IF l IS NULL THEN
        RETURN r IS NULL
    END IF
    IF r IS NULL THEN
        RETURN FALSE
    END IF
    RETURN l == r
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION Equal(l REFERENCE, r REFERENCE) RETURNS BOOL
################################################################################################################################


################################################################################################################################
#
#  Private function GetAttributeBool returns boolean attribute by key or defined default value if key doesn't exist
#
################################################################################################################################
PRIVATE FUNCTION GetAttributeBool(attributes HASHMAP, key STRING, def BOOL) RETURNS BOOL
    DEFINE ret BOOL
    LET ret = def
    IF attributes.KeyExists(key) THEN
        LET ret = attributes[key]
    END IF
    RETURN ret
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION GetAttributeBool(attributes HASHMAP, key STRING, def BOOL) RETURNS BOOL
################################################################################################################################


################################################################################################################################
#
#  Private function iCaseSearchArray makes case insensitive search of string in DYNAMIC ARRAY OF STRING
#
################################################################################################################################
PRIVATE FUNCTION iCaseSearchArray(arr DYNAMIC ARRAY OF STRING, val STRING) RETURNS INT
    DEFINE i INT
    FOR i = 1 TO arr.GetSize()
        IF arr[i].equalsIgnoreCase(val) THEN
            RETURN i
        END IF
    END FOR
    RETURN 0
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION (this InteractForm) iCaseSearchArray(arr DYNAMIC ARRAY OF STRING, val STRING) RETURNS INT
################################################################################################################################


################################################################################################################################
#
#  PUBLIC function iCaseSearchHashmap makes case insensitive search of key in HASHMAP
#
################################################################################################################################
PUBLIC FUNCTION iCaseSearchHashmap(hash HASHMAP, key STRING) RETURNS INT
    DEFINE i INT
    DEFINE s STRING
    FOR i = 1 TO hash.GetSize()
        LET s = hash.GetKey(i)
        IF s.equalsIgnoreCase(key) THEN
            RETURN i
        END IF
    END FOR
    RETURN 0
END FUNCTION
################################################################################################################################
# END PUBLIC FUNCTION (this InteractForm) iCaseSearchHashmap(arr DYNAMIC ARRAY OF STRING, val STRING) RETURNS INT
################################################################################################################################


################################################################################################################################
#
#  Private function GetViewType returns
#      1. TRUE if there is just one row is shown on the screen (means screen record with on row)
#      2. TRUE if there is Table widget is used.
#
################################################################################################################################
PRIVATE FUNCTION GetViewType(field) RETURNS (BOOL, BOOL)
    DEFINE field, parent  ui.AbstractUiElement
    DEFINE distObj        ui.DistributedObject
    DEFINE tabCol         ui.TableColumn
    DEFINE arr            DYNAMIC ARRAY OF ui.DistributedObject

    IF field IS NULL THEN
        RETURN TRUE, FALSE
    END IF

    WHENEVER ERROR CONTINUE

    LET tabCol = field.GetContainer()
    IF status = 0 AND tabCol IS NOT NULL THEN
        RETURN FALSE, TRUE
    END IF

    LET distObj = field
    LET arr = distObj.split()

    WHENEVER ERROR STOP

    RETURN arr.GetSize() < 2, FALSE
END FUNCTION
################################################################################################################################
# PRIVATE FUNCTION GetViewType(field) RETURNS BOOL, BOOL
################################################################################################################################


################################################################################################################################
#
#  Private function GetColumnName splites full field name on table name and column name and returns the column name
#
################################################################################################################################
PRIVATE FUNCTION GetColumnName(fullName STRING) RETURNS STRING
    DEFINE split DYNAMIC ARRAY OF STRING
    CALL fullName.Split(".") RETURNING split
    IF split.GetSize() > 1 THEN
      RETURN split[2].Trim()
    END IF
    RETURN fullName
END FUNCTION
################################################################################################################################
# PRIVATE FUNCTION GetColumnName(fullName STRING) RETURNS STRING
################################################################################################################################