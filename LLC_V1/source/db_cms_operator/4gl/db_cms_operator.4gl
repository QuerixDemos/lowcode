########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7910
# Addresses/Scenario:
# PK is serial and FK contact.cont_id
########################################################################
# DB Table Schema
#	CREATE TABLE operator (
#		operator_id         SERIAL,
#		name                CHAR(10)  NOT NULL UNIQUE,
#		password            CHAR(10),
#		type                CHAR,
#		cont_id             INTEGER, # ref contact_id
#		email_address       VARCHAR(100),
#		
#		PRIMARY KEY (operator_id),
#		FOREIGN KEY (cont_id) REFERENCES contact(cont_id)
#	)
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT
	OPTIONS INPUT WRAP
    
	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Operator")
	CALL fgl_settitle("Operator")       

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Operator Record","Operator Record",	"{CONTEXT}/public/querix/icon/svg/24/ic_operator_24px.svg",				101,TRUE,"Operator Record - Display, Scroll and modify operator data","top") 
			CALL fgl_dialog_setkeylabel("Operator List",	"Operator List",		"{CONTEXT}/public/querix/icon/svg/24/ic_operator_list_24px.svg",	102,TRUE,"Operator List - List all operators","top")			
			CALL fgl_dialog_setkeylabel("Operator",				"Operator Record",	"{CONTEXT}/public/querix/icon/svg/24/ic_operator_edit_24px.svg",	103,TRUE,"(Original form) Operator Record - Display, Scroll and modify operator data","top") 

		ON ACTION "Operator Record"
			CALL db_cms_operator_rec()

		ON ACTION "Operator List"
			CALL db_cms_operator_list() 
					
		ON ACTION "Operator"
			CALL db_cms_operator() #original cms operator form
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################