DATABASE cms_llc
MAIN
	MENU

	# PUBLIC FUNCTION InteractFormFileWithSettings(form_file STRING, setting_id STRING)			
		ON ACTION "Record"
			CALL InteractFormFileWithSettings("contact_company_record","contact-company") #form file name

		ON ACTION "List"
			CALL InteractFormFileWithSettings("contact_company_list","contact-company") #form file name

		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
	
END MAIN