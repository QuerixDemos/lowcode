DATABASE cms_llc
MAIN
	MENU

	# PUBLIC FUNCTION InteractFormFileWithSettings(form_file STRING, setting_id STRING)			
		ON ACTION "Record"
			CALL InteractFormFileWithSettings("contact_short_record","contact-short") #form file name, setting_id

		ON ACTION "List"
			CALL InteractFormFileWithSettings("contact_short_list","contact-short") #form file name, setting_id

		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
	
END MAIN