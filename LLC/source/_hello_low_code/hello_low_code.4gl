DATABASE cms_llc
MAIN
	MENU

	###############################################################################
	#3 ways of invoking LowCode
	# PUBLIC FUNCTION InteractFormFile(formFile STRING)
	# PUBLIC FUNCTION InteractFormFileWithSettings(form_file STRING, setting_id STRING)
	# PUBLIC FUNCTION InteractForm(settings InteractForm_Settings)
	###############################################################################
	
	# PUBLIC FUNCTION InteractFormFile(formFile STRING)
		ON ACTION "Record"
			CALL InteractFormFile("hello_low_code_rec") #form file name

			ON ACTION "List"
			CALL InteractFormFile("hello_low_code_list") #form file name

			ON ACTION "CANCEL"
			EXIT MENU
	END MENU
	
END MAIN