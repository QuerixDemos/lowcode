########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7963
# Addresses/Scenario...
# Single table but byte and char(1000) field
# Uses table alias - contact table is joined twice (lookup)
########################################################################
# DB Table Schema
{
          CREATE TABLE activity (
            activity_id	SERIAL,# UNIQUE,
            open_date	DATE NOT NULL,
            close_date	DATE,
            contact_id	INTEGER NOT NULL,	# ref contact_id
            comp_id		INTEGER,	# ref comp_id
            operator_id     INTEGER NOT NULL,	# ref operator_id
            act_type	INTEGER,		# ref activity_type
            long_desc	TEXT,
            short_desc	CHAR(80),
            #long_ref	INTEGER,		# ref possible blob
            a_owner		INTEGER,	# ref contact_id
            priority	INTEGER,
            
            PRIMARY KEY (activity_id) CONSTRAINT activity_pk,
            FOREIGN KEY (contact_id) REFERENCES contact(cont_id) CONSTRAINT contact_id_fk_contact,
            FOREIGN KEY (comp_id) REFERENCES company(comp_id) CONSTRAINT comp_id_fk_company,
            FOREIGN KEY (operator_id) REFERENCES operator(operator_id) CONSTRAINT operator_id_fk_operator,
            FOREIGN KEY (act_type) REFERENCES activity_type(activity_type_id) CONSTRAINT act_type_fk_activity_type
            
          )

          CREATE TABLE activity_type (
            activity_type_id		SERIAL,
            atype_name	CHAR(15) NOT NULL UNIQUE,
            user_def	SMALLINT,
            PRIMARY KEY (activity_type_id)
          )

			CREATE TABLE operator (
				operator_id             SERIAL, #PK
				name                CHAR(10)  NOT NULL UNIQUE,
				password            CHAR(10),
				type                CHAR,
				cont_id             INTEGER, # ref contact_id
				email_address       VARCHAR(100),
				
				PRIMARY KEY (operator_id),
				FOREIGN KEY (cont_id) REFERENCES contact(cont_id)
			)
}          
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_rec_settings_rec InteractForm_Settings
	DEFINE l_rec_settings_list InteractForm_Settings
	
	DEFER INTERRUPT

	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Activity Lookup 1")
	CALL fgl_settitle("Activity Lookup 1")   

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Activity Details",			"Activity Details",			"{CONTEXT}/public/querix/icon/svg/24/ic_communication_1_24px.svg", 				101,TRUE,"Display and modify activity details (Record)","top") 
			CALL fgl_dialog_setkeylabel("Activity List",   			"Activity List",				"{CONTEXT}/public/querix/icon/svg/24/ic_communication_list_1_24px.svg",		102,TRUE,"List all activities (List)","top")			

		ON ACTION "Activity Details"
			CALL db_cms_activity_lookup1_rec(l_rec_settings_rec)
		
		ON ACTION "Activity List"
			CALL db_cms_activity_lookup1_list(l_rec_settings_list)
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################
{
########################################################################
# FUNCTION db_cms_activity_rec()	
#
# Edit Record
########################################################################
FUNCTION db_cms_questionaire_manager_rec() 
	DEFINE l_settings InteractForm_Settings
	LET l_settings.form_file = "../db_cms_activity/db_cms_activity_rec"

	LET l_settings.paged_mode = TRUE #on fill buffer
#	LET l_settings.input_mode = TRUE	#BOOL - Set to FALSE if DISPLAY ARRAY should be used, otherwise (TRUE) INPUT RECORD/ARRAY is used

	LET l_settings.log_file = "../log/cms_activity.log"	#enable log file
	LET l_settings.pessimistic_locking = TRUE

	#LET l_settings.sql_where =   "activity.act_type =  1"							# The WHERE clause of the main query that can be overwritten as soon the user applies a Search (Construct)
	#LET l_settings.sql_where_static =   "activity activity_id =  1" 	# The WHERE clause of the main query that can NOT be overwritten by user, it's concatenated to sql_where

	#LET l_settings.sql_order_by = "activity_id DESC"					# The ORDER BY clause for the main query
	#LET l_settings.sql_top  = 5																				# The option to limit the base cursor row using the SQL SELECT TOP clause

	LET l_settings.actions[""]["BEFORE DISPLAY"] = FUNCTION custom_before_display
	LET l_settings.actions[""]["AFTER DISPLAY" ] = FUNCTION custom_after_display
	LET l_settings.actions[""]["ON ACTION Vlad"] = FUNCTION custom_on_action_vlad
	
	LET l_settings.actions["UPDATE"]["AFTER INPUT"          ] = FUNCTION custom_after_input
	LET l_settings.actions["UPDATE"]["ON ACTION Hubert"     ] = FUNCTION custom_on_action_hubert
	
	LET l_settings.actions["UPDATE"]["BEFORE INPUT"         ] = FUNCTION custom_update_before_input
	LET l_settings.actions["UPDATE"]["AFTER FIELD priority" ] = FUNCTION check_field_priority
	
	LET l_settings.actions["INSERT"]["BEFORE INPUT"         ] = FUNCTION custom_insert_before_input
	LET l_settings.actions["INSERT"]["AFTER FIELD priority" ] = FUNCTION check_field_priority

	#Control show/remove default events
	LET l_settings.attributes[""]["INSERT ROW"] = TRUE
	LET l_settings.attributes[""]["APPEND ROW"] = TRUE #Note, the LLC library uses DISPLAY/INPUT ARRAY for all data (single record and array record)
	LET l_settings.attributes[""]["DELETE ROW"] = FALSE

	CALL InteractForm(l_settings)	

END FUNCTION
########################################################################
# END FUNCTION db_cms_activity_rec()	
########################################################################

}