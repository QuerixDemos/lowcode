########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses/Scenario:
# rate_id is SERIAL PK
########################################################################
# DB Table Schema
#					CREATE TABLE tax_rate (
#						rate_id             SERIAL,
#						tax_rate            DECIMAL(5,2) NOT NULL,
#						tax_desc            CHAR(80)  NOT NULL UNIQUE,
#						PRIMARY KEY (rate_id)
#					)
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Tax Rate")
	CALL fgl_settitle("Tax Rate")  

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Tax Rate Record","Tax Rate Record","{CONTEXT}/public/querix/icon/svg/24/ic_tax_24px.svg",101,TRUE,"Tax Rate Record - Display, Scroll and modify tax rate data","top") 
			CALL fgl_dialog_setkeylabel("Tax Rate List","Tax Rate List","{CONTEXT}/public/querix/icon/svg/24/ic_tax_list_24px.svg",102,TRUE,"Tax Rate List - List all tax rates","top")			

		ON ACTION "Tax Rate Record"
			CALL db_cms_tax_rate_rec()

		ON ACTION "Tax Rate List"
			CALL db_cms_tax_rate_list()	
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################