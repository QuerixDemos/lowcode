########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="../llc_settings/llc_settings_rec"
	LET l_rec_settings.actions["UPDATE"]["BEFORE INPUT"] = FUNCTION actions_UPDATE_before_input
	CALL InteractForm(l_rec_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="../llc_settings/llc_settings_list"
	LET l_rec_settings.actions["UPDATE"]["BEFORE INPUT"] = FUNCTION actions_UPDATE_before_input

	CALL InteractForm(l_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################


########################################################################
# FUNCTION actions_UPDATE_before_field_test05_int()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_UPDATE_before_input(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING

	CALL fgl_winmessage("actions_UPDATE_before_input","actions_UPDATE_before_input","info")

	LET dlg = ui.Dialog.GetCurrent()

	#retrieve record/field values
	LET l_rec_test05.test05_primary_key = iform.GetFieldValue("test05.test05_primary_key")
	LET l_rec_test05.test05_fk_char = iform.GetFieldValue("test05.test05_fk_char")
	LET l_rec_test05.test05_varchar = iform.GetFieldValue("test05.test05_varchar")
	LET l_rec_test05.test05_int = iform.GetFieldValue("test05.test05_int")
	LET l_rec_test05.test05_date = iform.GetFieldValue("test05.test05_date")

	#Process data validation in case the business rules have changed

	#test05_primary_key
	IF l_rec_test05.test05_primary_key IS NULL THEN	
		CALL fgl_winmessage("Field test05_primary_key can not be NULL","Field test05_primary_key can not be NULL","error")
		CALL dlg.NextField("test05_primary_key")
		CALL ui.Dialog.GetCurrent().Continue()
		RETURN TRUE
	END IF

	#test05_fk_char
	IF l_rec_test05.test05_fk_char IS NULL THEN	
		CALL fgl_winmessage("Field test05_fk_char can not be NULL","Field test05_fk_char can not be NULL","error")
		CALL dlg.NextField("test05_primary_key")
		CALL ui.Dialog.GetCurrent().Continue()
		RETURN TRUE
	END IF

	#test05_varchar
	IF l_rec_test05.test05_varchar IS NULL THEN	
		CALL fgl_winmessage("Field test05_varchar can not be NULL","Field test05_varchar can not be NULL","error")
		CALL dlg.NextField("test05_varchar")
		CALL ui.Dialog.GetCurrent().Continue()
		RETURN TRUE
	END IF

	IF length(l_rec_test05.test05_varchar) < 10 AND length(l_rec_test05.test05_varchar) > 20 THEN	
	#IF l_rec_test05.test05_varchar.GetLength() < 10 AND l_rec_test05.test05_varchar.GetLength() > 20 THEN		
		CALL fgl_winmessage("Field test05_varchar lenght is out of range (10-20 Char)","Field test05_varchar lenght is out of range (10-20 Char)","error")
		CALL dlg.NextField("test05_varchar")
		CALL ui.Dialog.GetCurrent().Continue()
		RETURN TRUE
	END IF

	#test05_int
	IF l_rec_test05.test05_int IS NULL THEN	
		CALL fgl_winmessage("Field test05_int can not be NULL","Field test05_int can not be NULL","error")
		CALL dlg.NextField("test05_int")
		CALL ui.Dialog.GetCurrent().Continue()

		RETURN TRUE
	END IF

	IF l_rec_test05.test05_int > 1000 AND l_rec_test05.test05_int < -1000 THEN	
		CALL fgl_winmessage("Field test05_date is out of range","Field test05_date is out of range","error")
		CALL dlg.NextField("test05_int")
		CALL ui.Dialog.GetCurrent().Continue()
		
		RETURN TRUE
	END IF

	#test05_date
	IF l_rec_test05.test05_date IS NULL THEN	
		CALL fgl_winmessage("Field test05_date can not be NULL","Field test05_date can not be NULL","error")
		CALL dlg.NextField("test05_date")		
		CALL ui.Dialog.GetCurrent().Continue()

		RETURN TRUE
	END IF
	
	IF l_rec_test05.test05_date > TODAY() AND l_rec_test05.test05_date < "01/01/1900" THEN	
		CALL fgl_winmessage("Field test05_date is out of range","Field test05_date is out of range","error")
		CALL dlg.NextField("test05_date")		
		CALL ui.Dialog.GetCurrent().Continue()

		RETURN TRUE
	END IF

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_UPDATE_before_field_test05_int()RETURNS BOOL
########################################################################