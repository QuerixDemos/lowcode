########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_rec"
	LET l_settings.actions["UPDATE"]["BEFORE FIELD test05_int" ] = FUNCTION actions_UPDATE_before_field_test05_int
	CALL InteractForm(l_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_list"
	LET l_settings.actions["UPDATE"]["BEFORE FIELD test05_int" ] = FUNCTION actions_UPDATE_before_field_test05_int

	CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################


########################################################################
# FUNCTION actions_UPDATE_before_field_test05_int()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_UPDATE_before_field_test05_int(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING

	CALL display_dialog_record(iform)

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_UPDATE_before_field_test05_int()RETURNS BOOL
########################################################################