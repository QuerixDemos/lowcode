########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
#
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7963
# Addresses/Scenario...
# Single table but byte and char(1000) field
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_title STRING

	DEFER INTERRUPT
	OPTIONS INPUT WRAP

	LET l_title = os.Path.basename(arg_val(0))
	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText(l_title)
	CALL fgl_settitle(l_title)  

	MENU 
		BEFORE MENU
		CALL fgl_dialog_setkeylabel("Record",	"Record/Detailed View","{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	101,TRUE,"Record/Detailed View - Display, Scroll and modify table data (record view)",	"top") 

		ON ACTION "Record"
			CALL fk_lookup_conditional_rec()
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################


########################################################################
# FUNCTION fk_lookup_conditional_rec()	
#
# Edit Record
########################################################################
FUNCTION fk_lookup_conditional_rec()	
	DEFINE l_settings InteractForm_Settings
	LET l_settings.form_file = "../llc_fk_lookup/fk_lookup_conditional_rec"


	LET l_settings.log_file = "../log/fk_lookup_conditional.log"	#enable log file
	LET l_settings.pessimistic_locking = TRUE

	LET l_settings.actions["UPDATE"]["BEFORE FIELD comp_type"] = FUNCTION before_field_comp_type	
	LET l_settings.actions["INSERT"]["BEFORE FIELD comp_type"] = FUNCTION before_field_comp_type	

	#Control show/remove default events
	LET l_settings.attributes[""]["INSERT ROW"] = TRUE
	LET l_settings.attributes[""]["APPEND ROW"] = TRUE #Note, the LLC library uses DISPLAY/INPUT ARRAY for all data (single record and array record)
	LET l_settings.attributes[""]["DELETE ROW"] = FALSE

	# WE could also initialize a comboBox dropDownBox here with a function call
	#LET l_settings.comboboxes["company.comp_industry"] = "industry_type.industry_type_id IN (2,8,13)"

	CALL InteractForm(l_settings)	

END FUNCTION
########################################################################
# END FUNCTION fk_lookup_conditional_rec()	
########################################################################



########################################################################
# FUNCTION before_field_comp_type(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION before_field_comp_type(iform InteractForm INOUT) RETURNS BOOL
		DEFINE priority INT		
		DEFINE l_comp_industry LIKE company.comp_industry #comp_industry

		LET l_comp_industry = iform.GetFieldValue("company.comp_industry")	#retrieve the value of comp_industry

		CASE l_comp_industry
			WHEN 1 #Software Tester
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,8)") #1Reseller 8=Supplier
			WHEN 2 #Software Reseller
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,3,8,9)") #1Reseller 3=Distributor 8=Supplier 9=VAR
			WHEN 3 #Consultancy
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,2,3,4,5,6,7,8,9)") 
			WHEN 4 #Marketing Agency
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (2,3)") 
			WHEN 5 #Oil Refinery
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1)") 
			WHEN 6 #Other
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,2,3,4,5,6,7,8,9)") 
			WHEN 7 #Recording Studio
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,2,3,4,5,6,7,8,9)") 
			WHEN 8 #Manufacturing
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (2,4)") #2=Enduser 4=Educational
			WHEN 9 #Hardware Manufact.
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,2,3,4,5,6,7,8,9)") 
			WHEN 10 #Education
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,2,3,4,5,6,7,8,9)") 
			WHEN 11 #Brewery
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,4,5,6,9)") 
			WHEN 12 #Entertainment
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,3,7,9)") 
			WHEN 13 #Software Distributor
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,3,8,9)") #1Reseller 3=Distributor 8=Supplier 9=VAR
			WHEN 14 #Fitness
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,9)") 
			WHEN 15 #ISV
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (4,5,6)") 
			WHEN 16 #Internal Maintenance
				CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (3,4,5,6,7,8,9)") 

				OTHERWISE
				CALL fgl_winmessage("ERROR","Invalid Company Industry\n(Internal 4gl error)","ERROR")
		END CASE

		RETURN TRUE

END FUNCTION
########################################################################
# END FUNCTION before_field_comp_type(iform InteractForm INOUT) RETURNS BOOL
########################################################################

#Table industry_type
#1|Software Tester|0|
#2|Software Reseller|0|
#3|Consultancy|0|
#4|Marketing Agency|0|
#5|Oil Refinery|0|
#6|Other|0|
#7|Recording Studio|0|
#8|Manufacturing|0|
#9|Hardware Manufact.|0|
#10|Education|0|
#11|Brewery|0|
#12|Entertainment|0|
#13|Software Distributor|0|
#14|Fitness|0|
#15|ISV|0|
#16|Internal Maintenance|0|