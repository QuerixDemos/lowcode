########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Anomaly
# rate_id is SERIAL PK
########################################################################
# DB Table Schema
{
           CREATE TABLE test01 (
						f1_varchar VARCHAR(20), #textField
						f3_varchar VARCHAR(20),	#checkBox
						f4_varchar VARCHAR(20),	#comboBox
						f5_date DATE,	#calendar
						f6_varchar VARCHAR(20),	#function field
						f7_varchar VARCHAR(20), #radioButton
						f8_smallint SMALLINT,	#spinner
						f9_dt_h_t_s Datetime Hour to Second	#timeEditField
					)
}
########################################################################

########################################################################
# FUNCTION db_test_wig_single_line_widgets_rec()
#
#
########################################################################
FUNCTION db_test_wig_single_line_widgets_rec()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file = "../db_test_wig_single_line_widgets/db_test_wig_single_line_widgets_rec"
	LET l_settings.paged_mode = TRUE
	
	LET l_settings.actions[""]["ON ACTION actFunctionFieldF6"] = FUNCTION db_test_wig_single_line_widgets_rec_actFunctionFieldF6
	CALL InteractForm(l_settings)                                # The main staff function
END FUNCTION
########################################################################
# END FUNCTION db_test_wig_single_line_widgets_rec()
########################################################################


FUNCTION db_test_wig_single_line_widgets_rec_actFunctionFieldF6(iform InteractForm INOUT) RETURNS BOOL
    CALL fgl_winmessage("ON ACTION actFunctionFieldF6 -> db_test_wig_single_line_widgets_rec_actFunctionFieldF6")
    RETURN FALSE # Means that built-in function should not be not prevented
END FUNCTION

########################################################################
# FUNCTION db_test_wig_single_line_widgets_list()
#
#
########################################################################
FUNCTION db_test_wig_single_line_widgets_list()	
    CALL InteractFormFile("../db_test_wig_single_line_widgets/db_test_wig_single_line_widgets_list")                                # The main staff function
END FUNCTION
########################################################################
# END FUNCTION db_test_wig_single_line_widgets_list()
########################################################################