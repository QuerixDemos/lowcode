##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

###################################################################################
# GLOBALS
###################################################################################
GLOBALS

  DEFINE t_cms_info TYPE AS 
    RECORD
      db_version VARCHAR(10),
      db_build   VARCHAR(10),
      db_other   vARCHAR(10)
    END RECORD


END GLOBALS
{
########################################################
# FUNCTION get_required_cms_version()
########################################################

FUNCTION get_required_cms_version()

  RETURN "7.201" #updated 09.08.2022 HuHo

END FUNCTION


######################################################
# FUNCTION db_info(db_name)
######################################################
FUNCTION db_info(db_name)
  DEFINE 
    db_name,db_state_str VARCHAR(100),
    retval,row_count,cms_state SMALLINT,
    l_cms_info_arr DYNAMIC ARRAY OF t_cms_info 

  WHENEVER ERROR CONTINUE



  IF  fgl_find_table("cms_info") THEN

    DECLARE c_cms_info CURSOR FOR 
    SELECT *
      #INTO l_cms_info_arr.*
      FROM cms_info

    LET row_count = 1
    FOREACH c_cms_info INTO l_cms_info_arr[row_count]
      LET row_count = row_count + 1
      #IF row_count > 30 THEN
      #  EXIT FOREACH
      #END IF
    END FOREACH

    LET row_count = row_count - 1

    Let cms_state = compare_cms_version(l_cms_info_arr[1].*)


  ELSE  --Error

    LET cms_state = -2

  END IF

  #WHENEVER ERROR CONTINUE
  WHENEVER ERROR STOP

  RETURN cms_state, l_cms_info_arr[1].*  --db_version, l_cms_info_arr[1].db_build

END FUNCTION

}

########################################################
# FUNCTION open_db(db_name)
########################################################
FUNCTION open_db(db_name)
  DEFINE db_name CHAR(64),
         sqlr    SMALLINT,
         retval  SMALLINT,
         db_state_str VARCHAR(100)

  WHENEVER ERROR CONTINUE
    DATABASE db_name
    LET retval = status
    LET sqlr = SQLCA.SQLCODE
  WHENEVER ERROR STOP
  CASE
    WHEN (sqlr = -329 OR sqlr = -827)
      LET db_state_str = db_name CLIPPED,
        ": Database not found or no system permission."
      ERROR db_state_str CLIPPED

    WHEN (sqlr = -349)
      LET db_state_str = db_name CLIPPED,
        " not opened, you do not have Connect privilege - SQL Code:", sqlr
      ERROR db_state_str CLIPPED

    WHEN (sqlr = -354)
      LET db_state_str = db_name CLIPPED,
        ": Incorrect database name format. - SQL Code:", sqlr
      ERROR db_state_str CLIPPED

    WHEN (sqlr = -377)
      LET db_state_str = "open_db() called with a transaction still incomplete - SQL Code:", sqlr
      ERROR db_state_str CLIPPED

    WHEN (sqlr = -512)
      LET db_state_str = "Unable to open in exclusive mode, db is probably in use. - SQL Code:", sqlr
      ERROR db_state_str CLIPPED

    WHEN (sqlr = 0)
      LET db_state_str = "Connection successful - SQL Code:", sqlr
      #ERROR db_state_str CLIPPED

    OTHERWISE 
      LET db_state_str = "Other error - SQL Code:", sqlr
      ERROR db_state_str CLIPPED
  END CASE
  RETURN retval, db_state_str
END FUNCTION



