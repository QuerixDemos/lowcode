##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

###################################################################################
# This program creates the database schema and test data in the test DATABASE cms_llc
###################################################################################

###################################################################################
# DATABASE
###################################################################################
#DATABASE cms_llc

###################################################################################
# GLOBAL SCOPE Variables
###################################################################################

###################################################################################
# MODULE SCOPE Variables
###################################################################################
  DEFINE modu_monitor STRING
	DEFINE modu_table_name_list DYNAMIC ARRAY OF STRING --VARCHAR(18),
	DEFINE modu_inst_table_count      SMALLINT
	DEFINE modu_qxt_table_count       SMALLINT
	DEFINE modu_cms_table_count       SMALLINT

  DEFINE dt_cms_info TYPE AS 
    RECORD
      db_version VARCHAR(10),
      db_build   VARCHAR(10),
      db_other   vARCHAR(10)
    END RECORD
    
###################################################################################
# MAIN
#
#
###################################################################################
MAIN
	DEFINE i INTEGER
	DEFINE ret SMALLINT
	DEFINE l_db_driver_str STRING
	DEFINE l_cms_info OF dt_cms_info
	DEFINE l_connect_status SMALLINT
	DEFINE l_db_state_str STRING
	DEFINE l_db_connect_msg STRING
	DEFINE l_db_name STRING
	DEFINE l_app_type SMALLINT
	DEFINE l_cms_db_state SMALLINT
	
  CALL fgl_putenv("DBDATE=dmy4/")
	CALL STARTLOG ("db_schema_and_data.log")

  WHENEVER ERROR STOP

  DEFER INTERRUPT

	OPTIONS INPUT WRAP
	
  IF arg_val(1) IS NOT NULL THEN
    LET l_db_name = arg_val(1)
  ELSE
    LET l_db_name = "cms_llc"   #Default is cms_llc
  END IF

  CALL ini_table_name_list()
  LET l_app_type = 0
  LET modu_inst_table_count = modu_cms_table_count



  CALL fgl_settitle("CMS Database Tool")


  CALL fgl_form_open("f_db","db_tools_l2")
  CALL fgl_form_display("f_db")
  DISPLAY "!" TO bt_db_set
  DISPLAY "!" TO bt_db_connect


  #DISPLAY HYDRA_DB_DRIVER
  LET l_db_driver_str = fgl_getenv("HYDRA_DB_DRIVER")
  DISPLAY  l_db_driver_str TO dl_hydra_db_driver


  #Display DBDATE Status
  DISPLAY fgl_getenv("DBDATE") TO dl_dbdate_format

  LET modu_monitor = add_monitor_str(modu_monitor, "DB Tool",0)
  CALL display_monitor(modu_monitor)

  
  #Try to connect to database
  CALL connect_db_tools(l_db_name)
    RETURNING  l_connect_status, l_db_state_str, l_db_connect_msg, l_cms_db_state, l_cms_info.*

{

  WHENEVER ERROR CONTINUE
    DISPLAY db_name TO db_name
    CALL qxt_open_db(db_name) RETURNING l_connect_status, l_db_state_str, l_db_connect_msg
  WHENEVER ERROR STOP
  #WHENEVER ERROR CALL error_func

  #Update modu_monitor window
  CALL db_connect_monitor_msg(l_connect_status,l_db_connect_msg)

  #if connection successful, try to read the cms database version & build information
  #and display it
  IF l_connect_status >= 0 THEN
    CALL db_info("cms")  
  END IF
}

			CALL fgl_setkeylabel("(Re-)Create Database",		"(Re-)Create Database",			"{CONTEXT}/public/querix/icon/svg/24/ic_db_create_24px.svg", 							101,FALSE,"Re-create the entire database","top") 
			CALL fgl_setkeylabel("All Tables",   						"All Tables",								"{CONTEXT}/public/querix/icon/svg/24/ic_db_all_24px.svg",									102,FALSE,"All Tables - DB Operations for ALL tables","top")
			CALL fgl_setkeylabel("Individual Tables",   		"Individual Tables",				"{CONTEXT}/public/querix/icon/svg/24/ic_db_1_24px.svg",										103,FALSE,"Single Table - DB Operations for individual tables","top")

			CALL fgl_setkeylabel("Drop ALL Tables",   			"Drop ALL Tables",					"{CONTEXT}/public/querix/icon/svg/24/ic_db_drop_24px.svg",								104,FALSE,"DB Operations for an individual table","top")			
			CALL fgl_setkeylabel("DROP",   									"Drop Table(s)",						"{CONTEXT}/public/querix/icon/svg/24/ic_db_drop_24px.svg",								108,FALSE,"Drop Table(s)","top")
			CALL fgl_setkeylabel("CREATE",   								"Create Table(s)",					"{CONTEXT}/public/querix/icon/svg/24/ic_db_create_24px.svg",							109,FALSE,"Create Table(s)","top")

			CALL fgl_setkeylabel("UNLOAD",   								"UNLOAD Table(s)",					"{CONTEXT}/public/querix/icon/svg/24/ic_db_download_24px.svg",						110,FALSE,"Unload table(s)","top")
			CALL fgl_setkeylabel("LOAD",   									"LOAD Table(s)",						"{CONTEXT}/public/querix/icon/svg/24/ic_db_upload_24px.svg",							111,FALSE,"Load table(s)","top")
			CALL fgl_setkeylabel("UNLOAD UNL",   						"UNLOAD .UNL",							"{CONTEXT}/public/querix/icon/svg/24/ic_letter_u_24px.svg",								112,FALSE,"Unload table(s) to UNL file","top")
			CALL fgl_setkeylabel("UNLOAD BAK",   						"UNLOAD .BAK",							"{CONTEXT}/public/querix/icon/svg/24/ic_letter_b_24px.svg",								113,FALSE,"Unload table(s) to BAK file","top")
			CALL fgl_setkeylabel("LOAD UNL",   							"LOAD .UNL",								"{CONTEXT}/public/querix/icon/svg/24/ic_letter_u_24px.svg",								114,FALSE,"Load table(s) to UNL file","top")
			CALL fgl_setkeylabel("LOAD BAK",   							"LOAD .BAK",								"{CONTEXT}/public/querix/icon/svg/24/ic_letter_b_24px.svg",								115,FALSE,"Load table(s) to BAK file","top")

			CALL fgl_setkeylabel("modu_monitor",   							"modu_monitor",									"{CONTEXT}/public/querix/icon/svg/24/ic_log_clear_24px.svg",							131,FALSE,"modu_monitor Log for table operatoions","top")			
			CALL fgl_setkeylabel("Clear modu_monitor",   				"Clear modu_monitor",						"{CONTEXT}/public/querix/icon/svg/24/ic_monitor_24px.svg",								133,FALSE,"Clear modu_monitor Log information","top")			
			CALL fgl_setkeylabel("DB", 		  								"DB",												"{CONTEXT}/public/querix/icon/svg/24/ic_db_24px.svg",											135,FALSE,"Connect to a different DB","top")
			CALL fgl_setkeylabel("Set Database",   					"Set Database",							"{CONTEXT}/public/querix/icon/svg/24/ic_db_edit_24px.svg",								137,FALSE,"Enter the database name for the connection","top")			
			CALL fgl_setkeylabel("Connect",   							"Connect",									"{CONTEXT}/public/querix/icon/svg/24/ic_connect_1_24px.svg",							139,FALSE,"Connect/Re-Connect to the database","top")			



  MENU "DB-Create"
		BEFORE MENU

   ON ACTION "(Re-)Create Database" #1st level  
      CALL all_tables(-1,0)

    ON ACTION "All Tables" #1st level
      MENU "All Tables" #2nd level
				BEFORE MENU
				#
				
				ON ACTION "DROP" #2nd level
          CALL all_tables(0,NULL)

				ON ACTION "CREATE"  #2nd level
          CALL all_tables(1,NULL)

				ON ACTION "LOAD"   #2nd level
          MENU "Populate"  #3rd level
						ON ACTION "LOAD UNL" #"Populate from .UNL"
              CALL all_tables(2,0)

						ON ACTION "LOAD BAK"  #"Populate from .BAK"
        	     CALL all_tables(2,1)
              
						ON ACTION "Set Database"
							CALL db_setting(l_db_name, l_app_type) RETURNING l_db_name, l_app_type
            
						COMMAND KEY(F23) "Set Database"
							CALL db_setting(l_db_name, l_app_type) RETURNING l_db_name, l_app_type

						ON ACTION "CONNECT"
							CALL connect_db_tools(l_db_name)
							RETURNING  l_connect_status, l_db_state_str, l_db_connect_msg, l_cms_db_state, l_cms_info.*

						COMMAND KEY(F24) "CONNECT" #"Connect to Database"
							CALL connect_db_tools(l_db_name)
							RETURNING  l_connect_status, l_db_state_str, l_db_connect_msg, l_cms_db_state, l_cms_info.*

						COMMAND "Return"
							EXIT MENU
					END MENU
          
				ON ACTION "UNLOAD"#"Unload ALL Tables" #2nd level  		
					MENU "Export" #3rd level
						ON ACTION "UNLOAD UNL" #"Unload to .UNL files" #3rd level
							CALL all_tables(3,0)
		
						ON ACTION "UNLOAD BAK" #"Unload to .BAK files"   #3rd level
		              CALL all_tables(3,1)
		
						ON ACTION "Set Database" #3rd level
							CALL db_setting(l_db_name, l_app_type) RETURNING l_db_name, l_app_type				
		
						COMMAND KEY(F23) "Set Database" #3rd level
							CALL db_setting(l_db_name, l_app_type) RETURNING l_db_name, l_app_type
		
						ON ACTION "CONNECT" #3rd level
							CALL connect_db_tools(l_db_name)
							RETURNING  l_connect_status, l_db_state_str, l_db_connect_msg, l_cms_db_state, l_cms_info.*
		
						COMMAND KEY(F24) "Connect" #3rd level
							CALL connect_db_tools(l_db_name)
							RETURNING  l_connect_status, l_db_state_str, l_db_connect_msg, l_cms_db_state, l_cms_info.*
		
						COMMAND "Return" #3rd level
							EXIT MENU
					END MENU

        
				ON ACTION "Return" #2nd level
					EXIT MENU
			END MENU

		ON ACTION "Individual Tables" #1st level
			MENU "Individual Tables" #2nd level
	
				ON ACTION "DROP" #2nd level
					CALL table_selection_menu(0,NULL)
	
				ON ACTION "CREATE" #2nd level
					CALL table_selection_menu(1,NULL)
	
				ON ACTION "Load UNL" #2nd level
					CALL table_selection_menu(2,0)
	
				ON ACTION "Load BAK" #2nd level
					CALL table_selection_menu(2,1)
	
				ON ACTION "Unload UNL" #2nd level
					CALL table_selection_menu(3,0)
	
				ON ACTION "Unload BAK" #2nd level
					CALL table_selection_menu(3,1)
	
				ON ACTION "Return" #2nd level
					EXIT MENU
			END MENU

		ON ACTION "modu_monitor"  #1st level
			MENU "modu_monitor"
				ON ACTION "modu_monitor"  
					INPUT modu_monitor WITHOUT DEFAULTS FROM monitor_progress
					END INPUT
				ON ACTION "CLEAR modu_monitor"  
					LET modu_monitor = ""
					CALL display_monitor(modu_monitor)
				ON ACTION "Exit"  
					EXIT MENU
			END MENU
      
    ########################################################  
    # DB
		########################################################		
		ON ACTION "DB"  #1st level
			MENU "db_connect"
			
				ON ACTION "Set Database"
					CALL db_setting(l_db_name, l_app_type) RETURNING l_db_name, l_app_type
					
				COMMAND KEY(F23) "Set Database"
					CALL db_setting(l_db_name, l_app_type) RETURNING l_db_name, l_app_type
		
				ON ACTION "CONNECT"
					CALL connect_db_tools(l_db_name)
					RETURNING  l_connect_status, l_db_state_str, l_db_connect_msg, l_cms_db_state, l_cms_info.*
		
				COMMAND KEY(F24) "Connect"
					CALL connect_db_tools(l_db_name)
					RETURNING  l_connect_status, l_db_state_str, l_db_connect_msg, l_cms_db_state, l_cms_info.*
		
				ON ACTION "Return"
					EXIT MENU
		
			END MENU


		ON ACTION "Set Database"  #1st level
			CALL db_setting(l_db_name, l_app_type) RETURNING l_db_name, l_app_type
				
		COMMAND KEY(F23) 
			CALL db_setting(l_db_name, l_app_type) RETURNING l_db_name, l_app_type          

		ON ACTION "CONNECT"  #1st level
			CALL connect_db_tools(l_db_name)
			RETURNING  l_connect_status, l_db_state_str, l_db_connect_msg, l_cms_db_state, l_cms_info.*
    
    COMMAND KEY(F24) 
			CALL connect_db_tools(l_db_name)
			RETURNING  l_connect_status, l_db_state_str, l_db_connect_msg, l_cms_db_state, l_cms_info.*

		ON ACTION "Exit"   #1st level
			EXIT MENU

	END MENU

END MAIN
############################################################
# END MAIN
############################################################


############################################################
# FUNCTION db_setting(p_db_name, p_app_type)
# RETURN p_db_name, p_app_type
#
############################################################
FUNCTION db_setting(p_db_name, p_app_type)
	DEFINE p_db_name STRING
	DEFINE p_app_type STRING
	
  INPUT p_db_name, p_app_type WITHOUT DEFAULTS FROM db_name, app_type 
    ON KEY (F23,F24, ACCEPT)
      CALL fgl_dialog_update_data()
      CASE p_app_type 
        WHEN 0   --cms
          LET  modu_inst_table_count = modu_cms_table_count
        WHEN 1   --just qxt
          LET  modu_inst_table_count = modu_qxt_table_count
        OTHERWISE
          CALL fgl_winmessage("Error in Main()","Invalid Option for Install Type (CMS or Qxt)","Error")
        END CASE

      EXIT INPUT
  END INPUT
  
  RETURN p_db_name, p_app_type
END FUNCTION
###################################################################################
# END FUNCTION db_setting(p_db_name, p_app_type)
###################################################################################


###################################################################################
# FUNCTION connect_db_tools(p_db_name)
#
#
###################################################################################
FUNCTION connect_db_tools(p_db_name)
	DEFINE p_db_name STRING
  DEFINE l_cms_info OF dt_cms_info 
	DEFINE ret_connect_status SMALLINT
	DEFINE ret_db_state_str STRING	
	DEFINE ret_db_connect_msg STRING
	DEFINE ret_cms_db_state SMALLINT
  #Try to connect to database
  WHENEVER ERROR CONTINUE
    DISPLAY p_db_name TO p_db_name
    CALL qxt_open_db(p_db_name) RETURNING ret_connect_status, ret_db_state_str, ret_db_connect_msg
  WHENEVER ERROR STOP
  #WHENEVER ERROR CALL error_func

  #Update modu_monitor window
  CALL db_connect_monitor_msg(ret_connect_status,ret_db_connect_msg,p_db_name)

  #if connection successful, try to read the cms database version & build information
  #and display it
  IF ret_connect_status >= 0 THEN
    CALL db_info("cms") RETURNING ret_cms_db_state, l_cms_info.*
  END IF

  RETURN ret_connect_status, ret_db_state_str, ret_db_connect_msg, ret_cms_db_state, l_cms_info.*
END FUNCTION
###################################################################################
# END FUNCTION connect_db_tools(p_db_name)
###################################################################################


###################################################################################
# FUNCTION db_connect_monitor_msg(p_connect_status,p_db_connect_msg,p_db_name)
#
#
###################################################################################
FUNCTION db_connect_monitor_msg(p_connect_status, p_db_connect_msg, p_db_name)
	DEFINE p_connect_status SMALLINT
	DEFINE err_msg STRING
	DEFINE p_db_connect_msg STRING
	DEFINE p_db_name STRING
    
  IF p_connect_status < 0 THEN
    DISPLAY p_db_connect_msg TO dl_db_state ATTRIBUTE(RED)
    DISPLAY "Connection failed" TO monitor_progress ATTRIBUTE(RED)
    LET err_msg = "Cannot connect to database ", p_db_name CLIPPED, "\nYou need to create the specified database named ", p_db_name CLIPPED," before you can continue\nAlternatively, change the database (Set DB) Name and try to reconnect"
    CALL fgl_winmessage("Database Error",err_msg,"error")
    #EXIT PROGRAM
  ELSE
    DISPLAY p_db_connect_msg TO dl_db_state ATTRIBUTE(GREEN)
    DISPLAY "Connection successful" TO monitor_progress ATTRIBUTE(GREEN)
    CALL db_state_report()
  END IF
END FUNCTION
###################################################################################
# END FUNCTION db_connect_monitor_msg(p_connect_status,p_db_connect_msg,p_db_name)
###################################################################################


########################################################
# FUNCTION unload_file_name(str,bak)
#
#
########################################################
FUNCTION unload_file_name(file_name,bak)
  DEFINE
    file_name,ret STRING,
    bak SMALLINT,
    dir_name STRING
    
  LET dir_name = "unl"

   IF bak = 0 THEN
    LET ret = trim(dir_name), "/", trim(file_name), ".unl" 
    RETURN ret
  ELSE
    LET ret = trim(dir_name), "/", trim(file_name), ".bak" 
    RETURN ret
  END IF

END FUNCTION
########################################################
# END FUNCTION unload_file_name(str,bak)
########################################################


########################################################
# FUNCTION splash(p_type,p_name,p_file_type)
#  CALL splash(p_action,p_table_name,p_file_type)
#    CALL splash(p_action,p_table_name,p_file_type)
########################################################
FUNCTION splash(p_action,p_name,p_file_type)
  DEFINE 
    p_action    SMALLINT,
    p_name      STRING,
    obj         STRING,
    tmp_str     STRING,
    p_file_type SMALLINT,
    file_name   STRING

  IF p_action >1 THEN
    CASE p_file_type
      WHEN 0
        LET file_name = trim(p_name), ".unl"
      WHEN 1
        LET file_name = trim(p_name), ".bak"
    ENd CASE
  END IF

  CASE p_action
    WHEN 0
      LET obj = "Dropping Table: ", trim(p_name), "..."   --  TO modu_monitor  --AT 4,4
    WHEN 1
      LET obj = "Creating Table: ", trim(p_name), "..." 
    WHEN 2 
      LET obj = "Loading Table: ", trim(p_name), " from ", file_name   
    WHEN 3 
      LET obj = "Unloading Table: ", trim(p_name), " from ", file_name
    WHEN NULL
      #do nothing
    OTHERWISE
      LET tmp_str = "Invalid p_type argument in function splash()!\p_file_type = ", p_file_type
      CALL fgl_winmessage("Error in splash()",tmp_str, "error")
  END CASE


  IF fgl_fglgui() THEN
    LET modu_monitor = add_monitor_str(modu_monitor, obj,1)
    CALL display_monitor(modu_monitor)
  ELSE 
    CALL display_monitor(obj)
  END IF

  #SLEEP 1
END FUNCTION
########################################################
# END FUNCTION splash(p_type,p_name,p_file_type)
########################################################


########################################################
# FUNCTION error_func()
#
#
########################################################
FUNCTION error_func()
  DEFINE msg STRING

  CALL fgl_winmessage("This is a custom error message fuction error_func()","This is a custom error message fuction error_func()","info")
  IF sqlca.sqlcode = 0 THEN
    RETURN
  END IF
  LET msg = "SQL Error: ", sqlca.sqlcode, "\nobj creation failed"
  CALL fgl_message_box(msg)
  ROLLBACK WORK
  EXIT PROGRAM
END FUNCTION
########################################################
# END FUNCTION error_func()
########################################################


######################################################
# FUNCTION add_monitor_str(str, str2,level)
#
#
######################################################
FUNCTION add_monitor_str(str, str2,level)
  DEFINE str, str2 STRING,
    error_str STRING,
    level SMALLINT

  DISPLAY str2 TO dl_current_action 

  CASE level
    WHEN 0
      LET str = trim(str), "\n", trim(str2)
    WHEN 1
      LET str = trim(str), "\n\t", trim(str2)
    WHEN 2
      LET str = trim(str), "\n\t\t", trim(str2)
    WHEN 3
      LET str = trim(str), "\n\t\t\t", trim(str2)
    WHEN 4
      LET str = trim(str), "\n\t\t\t\t", trim(str2)
    OTHERWISE
      LET error_str = "Invalid level specified in add_monitor_str() level=" , level
      CALL fgl_winmessage("Internal 4gl source error - add_monitor_str(str, str2,level)",error_str,"error")
  END CASE


      
  RETURN trim(str)
END FUNCTION
######################################################
# END FUNCTION add_monitor_str(str, str2,level)
######################################################


######################################################
# FUNCTION display_monitor(str)
#
#
######################################################
FUNCTION display_monitor(str)
  DEFINE str STRING

  DISPLAY str TO monitor_progress ATTRIBUTE(GREEN)

END FUNCTION
######################################################
# END FUNCTION display_monitor(str)
######################################################


########################################################
# FUNCTION display_db_info(p_rec_cms_info)
#
#
########################################################
FUNCTION display_db_info(p_rec_cms_info)
  DEFINE p_rec_cms_info OF dt_cms_info 

  DISPLAY "CMS Database version (db_version): " || p_rec_cms_info.db_version || "DB_Build:" || p_rec_cms_info.db_build || "(Required DB_Build=" || get_required_cms_version() || ")" TO dl_cms_info

END FUNCTION
########################################################
# END FUNCTION display_db_info(p_rec_cms_info)
########################################################


########################################################
# FUNCTION display_db_error(p_err_str,p_err_state)
#
#
########################################################
FUNCTION display_db_error(p_err_str,p_err_state)
	DEFINE p_err_str STRING
	DEFINE p_err_state SMALLINT

  IF p_err_state = 0 THEN
    DISPLAY  p_err_str TO dl_cms_error ATTRIBUTE(GREEN)
  ELSE
    DISPLAY  p_err_str TO dl_cms_error ATTRIBUTE(RED)
  END IF

END FUNCTION
########################################################
# END FUNCTION display_db_error(p_err_str,p_err_state)
########################################################


########################################################################
# FUNCTION yes_no(p_title_msg,p_msg)
#
# Question dialog box with the options yes and no
#
# RETURN BOOLEAN TRUE/FALSE
########################################################################
FUNCTION yes_no(p_title_msg,p_msg)
  DEFINE p_title_msg VARCHAR(100)
	DEFINE p_msg VARCHAR(200)
	DEFINE rv  CHAR

  IF p_title_msg = "" THEN
    LET p_title_msg = "CMS-Demo Application"
  END IF

  LET rv = fgl_winquestion (p_title_msg,p_msg,1,"Yes|No","question",0)

  
  IF rv = "Y" THEN
    RETURN TRUE
  ELSE 
    RETURN FALSE
  END IF
END FUNCTION
########################################################################
# END FUNCTION yes_no(p_title_msg,p_msg)
########################################################################


#################################################################
# FUNCTION db_state_report()
#
#
#
# RETURN NONE
#################################################################
FUNCTION db_state_report()
	DEFINE l_cms_info OF dt_cms_info
	DEFINE l_cms_db_state SMALLINT
	
  CALL db_info("cms") RETURNING l_cms_db_state, l_cms_info.*

  CASE l_cms_db_state
    WHEN -1
      CALL display_db_error("Your CMS database seems to be incomplete or outdated - please reinstall the db",-1)
      CALL display_db_info(l_cms_info.*)
    WHEN -2
      CALL display_db_error("Your CMS database seems to be empty (no tables or too old) - please reinstall the database",-1)
      CALL display_db_info(l_cms_info.*)
    WHEN 0
      CALL display_db_error("Your CMS database seems to be OK",0)
      CALL display_db_info(l_cms_info.*)
  END CASE

END FUNCTION
#################################################################
# END FUNCTION db_state_report()
#################################################################

############################################################################################################

#####################################################
# FUNCTION table_operation()
#
#
#
# RETURN err
#####################################################
FUNCTION table_operation(p_table_name,p_action,p_file_type)
  DEFINE 
    err          SMALLINT,
    p_table_name STRING,
    p_action     SMALLINT,
    p_file_type  SMALLINT,
    tmp_str      STRING,
    file_name    STRING,
    local_debug  SMALLINT

  LET local_debug = FALSE

  LET err = -1  -- -1=error 

  IF p_action > 1 THEN   --0=drop and 1=table create
    LET file_name = unload_file_name(p_table_name,p_file_type)

    #Now done with a sepearte function at import call
    #IF p_action = 2 THEN --Load requires at least the unl or bak file 
      #IF NOT fgl_test("e",file_name) THEN
      #  LET tmp_str = "Following string import file does not exist: \n", file_name
      #  CALL fgl_winmessage("File Error in table_operation()" ,tmp_str, "error")
      #  RETURN -1
      #END IF
    #END IF

  END IF

  IF local_debug THEN
    DISPLAY "table_operation() - p_action=",p_action
    DISPLAY "table_operation() - fgl_find_table(p_table_name)=", fgl_find_table(p_table_name)
    DISPLAY "table_operation() - p_table_name=",p_table_name
  END IF

  IF p_action = 0 THEN   --0=drop and 1=table create 2 =load 3=unload

    IF NOT fgl_find_table(p_table_name) THEN
      LET tmp_Str = "Error - Table ", trim(p_table_name), " can not be removed!\nThis table does not exist ! (Create it prior)\nIgnoring your request.."
      CALL fgl_winmessage("Table Drop (Delete) Error",tmp_str,"error")
    RETURN 0

    END IF

  END IF

  IF p_action = 1 THEN   --0=drop and 1=table create 2 =load 3=unload

    IF fgl_find_table(p_table_name) THEN
      LET tmp_Str = "Error - Table ", trim(p_table_name), " can not be created!\nThis table already exists ! (Drop it prior)\nIgnoring your request.."
      CALL fgl_winmessage("Table Create Error",tmp_str,"error")
    RETURN 0

    END IF

  END IF


  IF p_action > 1 THEN  --Load/Unload  --0=drop and 1=table create 2 =load 3=unload

    IF NOT fgl_find_table(p_table_name) THEN
      LET tmp_Str = "Error - Table ", trim(p_table_name), " does not exist!\nSystem can not export/import data from a missing table.\nIgnoring your request.."
      CALL fgl_winmessage("Table Data Import/Export Error",tmp_str,"error")
      RETURN 0
    END IF

  END IF

  IF p_action = 2 THEN  --Load unl file into table

    IF NOT server_side_file_exists(file_name) THEN
      RETURN 0
    END IF

    #'s' returns true if the file is non-zero size - false if it is empty
    IF NOT fgl_test("s",file_name) THEN  --file exists but is empty - Load will not like it...
      LET tmp_Str = "Warning - UNL Load File ", trim(file_name), " is empty!\nSystem can not import data from an empty UNL/BAK file.\nIgnoring your request.."
      CALL fgl_winmessage("Table Data Import Problem",tmp_str,"warning")
      RETURN 0
    END IF

  END IF

  IF local_debug THEN
    DISPLAY "table_operation() - Begin Work"
  END IF

  -- BEGIN WORK

  CALL splash(p_action,p_table_name,p_file_type)
DISPLAY p_table_name
  CASE p_table_name




##############################################################################################################
# Tool Libs related tables
#
# Note - Some tables i.e. Document Manager require BLOB support of the database (Standard Engine can't cope with it)
##############################################################################################################


   ############################################
   # Application name <-> ID  Manager
   ############################################

    WHEN modu_table_name_list[1] -- "qxt_application"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_application
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_application(
            application_id                   SERIAL       NOT NULL,
            application_name                 CHAR(30)     NOT NULL UNIQUE,

          PRIMARY KEY (application_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_application
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_application ORDER BY application_id

      END CASE


   ############################################
   # Language name <-> ID  Manager
   ############################################

    WHEN modu_table_name_list[2] -- "qxt_language" (for language)
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_language
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_language (
            language_id       SERIAL NOT NULL,
            language_name     VARCHAR(20) NOT NULL,
            language_dir      CHAR(2) NOT NULL,
            language_url      VARCHAR(10),
            PRIMARY KEY (language_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_language
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_language  ORDER BY language_id
      END CASE



   ############################################
   # 4GL Tool Lib string Category (for easier management)
   ############################################

    WHEN modu_table_name_list[3] -- "qxt_str_category"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_str_category
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_str_category (
            category_id     INTEGER NOT NULL,
            language_id     INTEGER NOT NULL,
            category_data   VARCHAR(30) NOT NULL,  --used to unique, but did not work

            #FOREIGN KEY (language_id) 
            #  REFERENCES qxt_language (language_id) ,


            FOREIGN KEY (language_id) 
              REFERENCES qxt_language (language_id) ,
           PRIMARY KEY (category_id, language_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN

            LOAD FROM file_name INSERT INTO qxt_str_category

          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_str_category ORDER BY category_id , language_id

      END CASE



   ############################################
   # 4GL Tool Lib string support (multi language support)
   ############################################

    WHEN modu_table_name_list[4] -- "qxt_string_tool"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_string_tool
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_string_tool (
            string_id       INTEGER NOT NULL,
            language_id     INTEGER NOT NULL,
            string_data     VARCHAR(100),
            category1_id    INTEGER,
            category2_id    INTEGER,
            category3_id    INTEGER,

            FOREIGN KEY (language_id) 
              REFERENCES qxt_language (language_id), 
            PRIMARY KEY (string_id, language_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN

            LOAD FROM file_name INSERT INTO qxt_string_tool

          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_string_tool ORDER BY string_id, language_id 

      END CASE


   ############################################
   # 4GL Application string support (multi language support)
   ############################################

    WHEN modu_table_name_list[5] -- "qxt_string_app"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_string_app
          WHENEVER ERROR STOP


        WHEN 1  --create table
          CREATE TABLE qxt_string_app (
            string_id       INTEGER NOT NULL,
            language_id     INTEGER NOT NULL,
            string_data     VARCHAR(200),
            category1_id    INTEGER,
            category2_id    INTEGER,
            category3_id    INTEGER,

            FOREIGN KEY (language_id) 
              REFERENCES qxt_language (language_id), 
            PRIMARY KEY (string_id, language_id)

          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_string_app
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_string_app  ORDER BY string_id, language_id 

      END CASE


   ############################################
   # Help Classic - multilingual Help file map  Manager
   ############################################

    WHEN modu_table_name_list[6] -- "qxt_help_classic"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_help_classic
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_help_classic (
            id             SERIAL NOT NULL,
            filename       VARCHAR(100) NOT NULL,
            PRIMARY KEY (id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN

            LOAD FROM file_name INSERT INTO qxt_help_classic

          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_help_classic ORDER BY id

      END CASE



   ############################################
   # HTML Help (URL)  Manager
   ############################################

    WHEN modu_table_name_list[7] -- "qxt_help_url_map"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_help_url_map
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_help_url_map (
            map_id         INTEGER NOT NULL,
            language_id    INTEGER NOT NULL,
            map_fname      VARCHAR(100) NOT NULL,
            map_ext        VARCHAR(100),

          PRIMARY KEY (map_id, language_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN

            LOAD FROM file_name INSERT INTO qxt_help_url_map

          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_help_url_map  ORDER BY map_id, language_id 

      END CASE


   ############################################
   # HTML Help (BLOB)  Manager
   ############################################
    WHEN modu_table_name_list[8] -- "qxt_help_html_doc"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_help_html_doc
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE qxt_help_html_doc (
            help_html_doc_id     INTEGER NOT NULL,
            language_id          INTEGER NOT NULL,
            help_html_filename   VARCHAR(100),
            help_html_mod_date   DATE,
            help_html_data       TEXT,
          FOREIGN KEY (language_id) 
            REFERENCES qxt_language (language_id),
            #ON DELETE CASCADE,
          PRIMARY KEY (help_html_doc_id, language_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_help_html_doc
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_help_html_doc  ORDER BY help_html_doc_id, language_id 

      END CASE


   ############################################
   # Simple Toolbar  Manager  (For dynamic toolbars)
   ############################################


    WHEN modu_table_name_list[9] --"qxt_toolbar"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_toolbar
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func

        WHEN 1  --create table
          CREATE TABLE qxt_toolbar (
          	tb_prog_id VARCHAR(30),
          	tb_menu_id VARCHAR(30),
          	tb_action VARCHAR(30),
          	tb_label VARCHAR(20),
          	tb_icon VARCHAR(100),
          	tb_position INT,
          	tb_static SMALLINT, --BOOLEAN,
          	tb_tooltip VARCHAR(100),
          	
          	tb_type SMALLINT, --0=button 1=divider--BOOLEAN,  --true = seperator pipe (not a button)
          	tb_scope SMALLINT, --0=dialog 1=global --BOOLEAN,  --scope  true is dialog
          	tb_hide SMALLINT, --0=visible 1=hide/remove --BOOLEAN,  --true will delete that button - false will set it		

          	
          	PRIMARY KEY (tb_prog_id, tb_menu_id, tb_action)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_toolbar
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_toolbar ORDER BY tb_prog_id, tb_menu_id, tb_position ASC

      END CASE


   ############################################
   # Toolbar  Manager  (For dynamic toolbars)
   ############################################

    WHEN modu_table_name_list[10] -- "qxt_icon_size"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_icon_size
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE qxt_icon_size (
            icon_size_name     CHAR(5)     NOT NULL,

            PRIMARY KEY (icon_size_name)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_icon_size
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_icon_size  ORDER BY icon_size_name

      END CASE


    WHEN modu_table_name_list[11] -- "qxt_icon_path"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_icon_path
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE qxt_icon_path (
            icon_path_name     VARCHAR(20)   NOT NULL,
            icon_path_dir      VARCHAR(50)   NOT NULL UNIQUE, 

            PRIMARY KEY (icon_path_name)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_icon_path
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_icon_path  ORDER BY icon_path_name

      END CASE



    WHEN modu_table_name_list[12] -- "qxt_icon_property"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_icon_property
          WHENEVER ERROR STOP


        WHEN 1  --create table
          CREATE TABLE qxt_icon_property (
            icon_property_name        VARCHAR(20)   NOT NULL,
            icon_size_name            CHAR(5)       NOT NULL,
            icon_path_name            VARCHAR(20)   NOT NULL,

            FOREIGN KEY (icon_size_name) 
              REFERENCES qxt_icon_size (icon_size_name),
            FOREIGN KEY (icon_path_name) 
              REFERENCES qxt_icon_path (icon_path_name),
            PRIMARY KEY (icon_property_name)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_icon_property
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_icon_property  ORDER BY icon_property_name

      END CASE



    WHEN modu_table_name_list[13] -- "qxt_icon_category"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_icon_category
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_icon_category (
            icon_category_id     INTEGER NOT NULL,
            language_id          INTEGER NOT NULL,
            icon_category_data   VARCHAR(40) NOT NULL UNIQUE,

            FOREIGN KEY (language_id) 
              REFERENCES qxt_language (language_id) ,
           PRIMARY KEY (icon_category_id, language_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_icon_category
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_icon_category  ORDER BY icon_category_id, language_id ASC

      END CASE



    WHEN modu_table_name_list[14] -- "qxt_icon"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_icon
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_icon (
            icon_filename           VARCHAR(100)  NOT NULL,
            #icon_property_name     VARCHAR(20)  NOT NULL,
            icon_category1_id                    INTEGER NOT NULL,
            icon_category2_id                    INTEGER NOT NULL,
            icon_category3_id                    INTEGER NOT NULL,    

          PRIMARY KEY (icon_filename)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_icon
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_icon  ORDER BY icon_filename ASC

      END CASE




    WHEN modu_table_name_list[15] -- "qxt_tbi_tooltip"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi_tooltip
          WHENEVER ERROR STOP


        WHEN 1  --create table
          CREATE TABLE qxt_tbi_tooltip (
            string_id       INTEGER NOT NULL,
            language_id     INTEGER NOT NULL,
            label_data     VARCHAR(30),
            string_data     VARCHAR(100),
            category1_id    INTEGER,
            category2_id    INTEGER,
            category3_id    INTEGER,

            FOREIGN KEY (language_id) 
              REFERENCES qxt_language (language_id), 
            PRIMARY KEY (string_id, language_id)

          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi_tooltip
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi_tooltip  ORDER BY string_id , language_id ASC

      END CASE

                                                  
    WHEN modu_table_name_list[16] -- "qxt_tbi_event_type"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi_event_type
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_tbi_event_type (
            event_type_id                 SERIAL NOT NULL,
            event_type_name               VARCHAR(10) NOT NULL UNIQUE,

          PRIMARY KEY (event_type_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi_event_type
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi_event_type  ORDER BY event_type_id ASC

      END CASE



    WHEN modu_table_name_list[17] -- "qxt_tbi_obj_event"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi_obj_event
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_tbi_obj_event (
            tbi_obj_event_name               VARCHAR(30) NOT NULL,

          PRIMARY KEY (tbi_obj_event_name)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi_obj_event
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi_obj_event  ORDER BY tbi_obj_event_name ASC

      END CASE



    WHEN modu_table_name_list[18] -- "qxt_tbi_obj_action"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi_obj_action
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_tbi_obj_action (
            tbi_action_id                  SMALLINT    NOT NULL,
            tbi_action_name                VARCHAR(30) NOT NULL UNIQUE,
            string_id                          SMALLINT    NOT NULL,

          PRIMARY KEY (tbi_action_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi_obj_action
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi_obj_action  ORDER BY tbi_action_id ASC

      END CASE



    WHEN modu_table_name_list[19] -- "qxt_tbi_obj"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi_obj
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_tbi_obj (
            tbi_obj_name             VARCHAR(35) NOT NULL,
            event_type_id            INTEGER NOT NULL,
            tbi_obj_event_name       VARCHAR(30) NOT NULL,
            tbi_obj_action_id        SMALLINT    NOT NULL,
            icon_filename            VARCHAR(100) NOT NULL,
            string_id                INTEGER  NOT NULL,
            icon_category1_id        INTEGER NOT NULL,
            icon_category2_id        INTEGER NOT NULL,
            icon_category3_id        INTEGER NOT NULL,         
 
          FOREIGN KEY (event_type_id) 
            REFERENCES qxt_tbi_event_type (event_type_id),
          FOREIGN KEY (icon_filename) 
            REFERENCES qxt_icon (icon_filename),
          FOREIGN KEY (tbi_obj_action_id) 
            REFERENCES qxt_tbi_obj_action (tbi_action_id),
          PRIMARY KEY (tbi_obj_name)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi_obj
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi_obj  ORDER BY tbi_obj_name ASC

      END CASE


    WHEN modu_table_name_list[20] -- "qxt_tbi_scope"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi_scope
          WHENEVER ERROR STOP


        WHEN 1  --create table
          CREATE TABLE qxt_tbi_scope (
            tbi_scope_id                SMALLINT    NOT NULL , --CHECK (qxt_tbi_instance BETWEEN 0 AND 9),
            tbi_scope_name              VARCHAR(30) NOT NULL UNIQUE,
            string_id                   INTEGER     NOT NULL UNIQUE,
          PRIMARY KEY (tbi_scope_id)

          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi_scope
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi_scope  ORDER BY tbi_scope_id ASC

      END CASE



    WHEN modu_table_name_list[21] -- "qxt_tbi_static"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi_static
          WHENEVER ERROR STOP


        WHEN 1  --create table
          CREATE TABLE qxt_tbi_static (
            tbi_static_id    SMALLINT    NOT NULL,
            tbi_static_name  VARCHAR(20) NOT NULL,
            string_id        INTEGER     NOT NULL UNIQUE,

            PRIMARY KEY (tbi_static_id)

          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi_static
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi_static  ORDER BY tbi_static_id ASC

      END CASE



    WHEN modu_table_name_list[22] -- "qxt_tbi"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_tbi(
            application_id     INTEGER      NOT NULL,
            tbi_name           VARCHAR(30)  NOT NULL,
#           tbi_instance      SMALLINT     NOT NULL,  --there can be any number of menus for a menu name i.e.you could clean fgl_setkeylabel() icons at the end of a function

            tbi_obj_name       VARCHAR(35)  NOT NULL,  --toolbar menu item
            event_type_id      INTEGER      NOT NULL,
            tbi_event_name     VARCHAR(30)  NOT NULL,
            tbi_scope_id       SMALLINT     NOT NULL,  -- Scope: 0-global fgl_setkeylabel  1-fgl_dialog_setkeylabel()
            tbi_position       SMALLINT     NOT NULL,  --position order  
            tbi_static_id      SMALLINT     NOT NULL,
            icon_category1_id  INTEGER      NOT NULL,
            icon_category2_id  INTEGER      NOT NULL,
            icon_category3_id  INTEGER      NOT NULL, 

          FOREIGN KEY (event_type_id) 
            REFERENCES qxt_tbi_event_type (event_type_id),
          FOREIGN KEY (application_id) 
            REFERENCES qxt_application (application_id),
          FOREIGN KEY (tbi_obj_name) 
            REFERENCES qxt_tbi_obj (tbi_obj_name),
          FOREIGN KEY (tbi_scope_id) 
            REFERENCES qxt_tbi_scope (tbi_scope_id),
          FOREIGN KEY (tbi_static_id) 
            REFERENCES qxt_tbi_static (tbi_static_id),
          PRIMARY KEY (application_id,tbi_name)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi  ORDER BY application_id, tbi_name ASC

      END CASE


    WHEN modu_table_name_list[23] -- "qxt_tb"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tb
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_tb(
            application_id         INTEGER      NOT NULL,
            tb_name                VARCHAR(30)  NOT NULL,
            tb_instance            SMALLINT     NOT NULL,  --there can be any number of menus for a menu name i.e.you could clean fgl_setkeylabel() icons at the end of a function
            tbi_name               VARCHAR(30)  NOT NULL,  --toolbar menu item
            #instance               SMALLINT     NOT NULL,  -- Scope: 0-global fgl_setkeylabel  1-fgl_dialog_setkeylabel()
            #order_position         SMALLINT     NOT NULL,  --position order  
            #static_state           SMALLINT     NOT NULL,
 

          FOREIGN KEY (application_id) 
            REFERENCES qxt_application (application_id),
          #FOREIGN KEY (application_id,tbi_name) 
          #  REFERENCES qxt_tbi (application_id,tbi_name)
          #  ON DELETE CASCADE,

          #FOREIGN KEY (tbi_instance) 
          #  REFERENCES qxt_tbi_scope (tbi_instance),


          PRIMARY KEY (application_id,tb_name,tb_instance,tbi_name)
          )

        WHEN 2  --load table
          LOAD FROM file_name INSERT INTO qxt_tb

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tb  ORDER BY application_id, tb_name, tb_instance, tbi_name ASC

      END CASE




   ############################################
   # Document (BLOB) Manager
   ############################################

    WHEN modu_table_name_list[24] --"qxt_document"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_document
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_document (
            document_id          SERIAL(1000),
            document_filename    VARCHAR(100) NOT NULL UNIQUE,
            #document_fileext     CHAR(10),
            document_app         CHAR(30),
            document_category1   CHAR(40),
            document_category2   CHAR(40),
            document_category3   CHAR(40),
            document_mod_date    DATE,
            document_desc        TEXT,  --CHAR(1000),  NOTE: Some DB's like Oracle don't allow to have 2 large objs i.e. 2 byte fields or a byte and a TEXT field

            PRIMARY KEY (document_id)

            #document_blob        BYTE
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_document
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_document  ORDER BY document_id ASC

      END CASE


    WHEN modu_table_name_list[25] --"qxt_document_blob"
      # NOTE: Some DB's like Oracle don't allow to have 2 large objs i.e. 2 byte fields or a byte and a TEXT field

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_document_blob
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_document_blob (
            document_id          INTEGER,
            document_blob        BYTE,
            PRIMARY KEY (document_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_document_blob
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_document_blob  ORDER BY document_id ASC

      END CASE




    WHEN modu_table_name_list[26] --"qxt_dde_font"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_dde_font
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_dde_font (
            dde_font_id      INTEGER NOT NULL,
            font_name        VARCHAR(40), --VARCHAR(30),   -- Arial
            font_bold        VARCHAR(30), --SMALLINT,      -- false
            font_size        VARCHAR(30), --SMALLINT,      --  = 10
            strike_through   VARCHAR(30), --SMALLINT,      --  = false
            super_script     VARCHAR(30), --SMALLINT,      --  = false
            lower_script     VARCHAR(30), --SMALLINT,      --  = false
            option_1         VARCHAR(30), --SMALLINT,      --  = false
            option_2         VARCHAR(30), --SMALLINT,      --  = false
            option_3         VARCHAR(30), --SMALLINT,      --  = 1
            font_color       VARCHAR(30), --SMALLINT       --  = 5

            PRIMARY KEY (dde_font_id)

          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_dde_font
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_dde_font  ORDER BY dde_font_id ASC

      END CASE



   ############################################
   # HTML Print Template (BLOB)  Manager
   ############################################
    WHEN modu_table_name_list[27] -- "qxt_print_template"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_print_template
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE qxt_print_template (
            template_id          INTEGER NOT NULL,
            language_id          INTEGER NOT NULL,
            filename             VARCHAR(100),
            mod_date             DATE,
            template_data        TEXT,
          FOREIGN KEY (language_id) 
            REFERENCES qxt_language (language_id)
            ON DELETE CASCADE,
          PRIMARY KEY (template_id, language_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_print_template
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_print_template ORDER BY template_id, language_id ASC

      END CASE

   ############################################
   # Print Image (BLOB)  Manager (used by i.e. html print)
   ############################################
    WHEN modu_table_name_list[28] -- "qxt_print_image"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_print_image
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE qxt_print_image (
            image_id             SERIAL NOT NULL,
            filename             VARCHAR(100),
            mod_date             DATE,
            image_data           BYTE,
          PRIMARY KEY (image_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_print_image
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_print_image

      END CASE


   ############################################
   # qxt_reserve
   ############################################
    WHEN modu_table_name_list[29] -- "qxt_reserve"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_reserve
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE qxt_reserve (
            id             INTEGER NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_reserve
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_reserve

      END CASE

   ############################################
   # qxt_reserve
   ############################################
    WHEN modu_table_name_list[30] -- "qxt_reserve2"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_reserve2
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE qxt_reserve2 (
            id             INTEGER NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_reserve2
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_reserve2

      END CASE





#######################################################################################
# CMS Application related tables
#######################################################################################


    WHEN modu_table_name_list[31] --"cms_info"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE cms_info
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func

        WHEN 1  --create table
          CREATE TABLE cms_info (
            db_version      VARCHAR(10),
            db_build        VARCHAR(10),
            db_other        VARCHAR(10)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO cms_info
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM cms_info

      END CASE


    WHEN modu_table_name_list[32] --"menus"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE menus
          WHENEVER ERROR STOP

        WHEN 1  --create table

          CREATE TABLE menus (
            menu_id		INTEGER NOT NULL,
            #language_id		INTEGER,
            menu_name	       VARCHAR(30),

            #FOREIGN KEY (language_id) 
            #  REFERENCES qxt_language (language_id), 
            PRIMARY KEY (menu_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO menus
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM menus

      END CASE


    WHEN modu_table_name_list[33] --"menu_options"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE menu_options
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func

        WHEN 1  --create table
          CREATE TABLE menu_options (
            option_id	        SERIAL NOT NULL, 
            menu_id		INTEGER NOT NULL,
            option_name_id      INTEGER, --VARCHAR(20),
            option_comment_id   INTEGER, --VARCHAR(60),
            option_keypress	VARCHAR(10),
 
            PRIMARY KEY (option_id,menu_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO menu_options
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM menu_options

      END CASE

#----------------------------------------------------------------------------------------
    WHEN modu_table_name_list[34] --"country"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE country
          WHENEVER ERROR STOP

				WHEN 1  --create table
					CREATE TABLE country (
						country         VARCHAR(40) NOT NULL,   --country lookup
            PRIMARY KEY (country)
					)

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO country
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM country

      END CASE

    WHEN modu_table_name_list[35] --"state"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE state
          WHENEVER ERROR STOP

				WHEN 1  --create table
					CREATE TABLE state (
						country       		  VARCHAR(40) NOT NULL,  #only for legacy compatibility
						country_code         VARCHAR(3) NOT NULL, #only for future compatibility
						state_code           VARCHAR(6) NOT NULL,
						state_code_iso3661   VARCHAR(10),
						state_text           VARCHAR(60),
						state_text_enu       VARCHAR(80),
            PRIMARY KEY (country,state_code) #(country_code,state_code)
					)

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO state
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM state

      END CASE

    WHEN modu_table_name_list[36] --"user_fields"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE user_fields
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE user_fields (
            field_id	SERIAL,
            field_name	VARCHAR(50) NOT NULL UNIQUE,
            field_desc	VARCHAR(50),
            field_type	INTEGER
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO user_fields
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM user_fields

      END CASE


    WHEN modu_table_name_list[37] --"user_field_data"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE user_field_data
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE user_field_data (
            field_id	INTEGER,
            contact_id	INTEGER,
            field_data	VARCHAR(50)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO user_field_data
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM user_field_data

      END CASE



    WHEN modu_table_name_list[38] --"industry_type"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE industry_type
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE industry_type (
            industry_type_id	SERIAL,
            itype_name	VARCHAR(25) NOT NULL, # NULL UNIQUE,
            user_def	SMALLINT,
            PRIMARY KEY (industry_type_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO industry_type
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM industry_type

      END CASE


    WHEN modu_table_name_list[39] --"company_type"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE company_type
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func

        
        WHEN 1  --create table
          CREATE TABLE company_type (
            company_type_id		SERIAL,
            ctype_name	CHAR(25) NOT NULL UNIQUE,
            user_def	SMALLINT,
            base_priority	INTEGER,
            
            PRIMARY KEY (company_type_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO company_type
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM company_type

      END CASE


    WHEN modu_table_name_list[40] --"contact_dept"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE contact_dept
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE contact_dept (
            dept_id		SERIAL, #PK linekd to contact
            dept_name	CHAR(25) NOT NULL UNIQUE,
            user_def	SMALLINT,
				PRIMARY KEY (dept_id)            
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO contact_dept
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM contact_dept

      END CASE



    WHEN modu_table_name_list[41] --"position_type"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE position_type
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE position_type (
            position_type_id     SERIAL,
            ptype_name  CHAR(25) NOT NULL UNIQUE,
            user_def    SMALLINT,
            PRIMARY KEY (position_type_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO position_type
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM position_type

      END CASE

    WHEN modu_table_name_list[42] --"activity_type"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE activity_type
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE activity_type (
            activity_type_id		SERIAL,
            atype_name	CHAR(15) NOT NULL UNIQUE,
            user_def	SMALLINT,
            PRIMARY KEY (activity_type_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO activity_type
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM activity_type

      END CASE

#----------------------------------------------------------------------------------------

    WHEN modu_table_name_list[43] -- "tax_rate"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE tax_rate
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


				WHEN 1  --create table
					CREATE TABLE tax_rate (
						rate_id             SERIAL,
						tax_rate            DECIMAL(5,2) NOT NULL,
						tax_desc            CHAR(80)  NOT NULL UNIQUE,
						PRIMARY KEY (rate_id)
					)

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO tax_rate
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM tax_rate

      END CASE

    WHEN modu_table_name_list[44] -- "pay_method"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE pay_method
          WHENEVER ERROR STOP

		WHEN 1  --create table
			CREATE TABLE pay_method (
				pay_method_id       SERIAL,
				pay_method_name     CHAR(10)  NOT NULL UNIQUE,
				pay_method_desc     CHAR(20) NOT NULL,
				PRIMARY KEY (pay_method_id)
			)

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO pay_method
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM pay_method

      END CASE

#------------------
    WHEN modu_table_name_list[45] --"titles"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE titles
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE titles (
            lang_id		INTEGER,
            title_name	VARCHAR(15)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO titles
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM titles

      END CASE


    WHEN modu_table_name_list[46] -- "delivery_type"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE delivery_type
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE delivery_type (
            delivery_type_id       SERIAL,
            delivery_type_name     CHAR(20)  NOT NULL UNIQUE,
            delivery_rate     MONEY(6,2) NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO delivery_type
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM delivery_type

      END CASE

#------------------
    WHEN  modu_table_name_list[47] --"currency"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE currency
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE currency (
            currency_id		SERIAL,
            currency_name	CHAR(10)   NOT NULL UNIQUE,
            xchg_rate		DECIMAL(5,3)  NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO currency
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM currency

      END CASE
#-------------------------
    WHEN modu_table_name_list[48] -- "stock_item"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE stock_item
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


			WHEN 1  --create table
				CREATE TABLE stock_item (
					stock_id            CHAR(10) NOT NULL,
					item_desc           CHAR(80)  NOT NULL UNIQUE,
					item_cost           MONEY(8,2),
					rate_id             INTEGER NOT NULL,
					quantity            INTEGER DEFAULT 0,
					PRIMARY KEY (stock_id),
					FOREIGN KEY (rate_id) REFERENCES tax_rate (rate_id)
				)

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO stock_item
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM stock_item

      END CASE

#-----------------------      

    WHEN modu_table_name_list[49] --"company"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE company
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE company (
            comp_id     SERIAL(1000) NOT NULL,
            comp_name	CHAR(100) NOT NULL UNIQUE,
            comp_addr1	CHAR(40),
            comp_addr2	CHAR(40),
            comp_addr3	CHAR(40),
            comp_city	CHAR(20),
            comp_zone	CHAR(15),
            comp_zip	CHAR(15),
            comp_country	CHAR(40),
            acct_mgr	INTEGER,		# ref contact_id #Can not add FK constraint as it would become circular
            comp_link	INTEGER,		# future: cross ref link table /Inner Join
            comp_industry	INTEGER,		# ref industry_id
            comp_priority	INTEGER,
            comp_type	INTEGER,		# ref cont_dept_id
            comp_main_cont	INTEGER	NOT NULL,	# ref contact_id  #Can not add FK constraint as it would become circular 
            comp_url	VARCHAR(50),
            comp_notes	CHAR(1000),
 
            PRIMARY KEY (comp_id) CONSTRAINT comp_id_pk,
            #FOREIGN KEY (acct_mgr) REFERENCES contact(cont_id) CONSTRAINT acct_mgr_fk_contact, # not possible due to circular relationship
            FOREIGN KEY (comp_industry) REFERENCES industry_type(industry_type_id) CONSTRAINT industry_type_id_fk_industry_type,
            FOREIGN KEY (comp_type) REFERENCES company_type(company_type_id) CONSTRAINT comp_type_fk_company_type#,
           # FOREIGN KEY (comp_main_cont) REFERENCES contact(cont_id) CONSTRAINT comp_main_cont_fk_contact # not possible due to circular relationship 
                        
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO company
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM company

      END CASE


#-----------------------

    WHEN modu_table_name_list[50] --"contact"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE contact
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func

        WHEN 1  --create table
          CREATE TABLE contact (
            cont_id 	SERIAL(1000) NOT NULL,
            cont_title	VARCHAR(10),
            cont_name 	VARCHAR(20) NOT NULL UNIQUE,
            cont_fname 	VARCHAR(20),
            cont_lname 	VARCHAR(20),
            cont_addr1	VARCHAR(40),
            cont_addr2	VARCHAR(40),
            cont_addr3	VARCHAR(40),
            cont_city	VARCHAR(20),
            cont_zone	VARCHAR(15),
            cont_zip	VARCHAR(15),
            cont_country	VARCHAR(40),
            cont_phone	VARCHAR(15),
            cont_fax	VARCHAR(15),
            cont_mobile	VARCHAR(15),
            cont_email	VARCHAR(50) NOT NULL UNIQUE,
            cont_dept	VARCHAR(15),	# ref cont_dept_id  ... should be integer/serial and linked to contact_dept.dept_id 
            cont_org	INTEGER,	# ref company_id
            cont_position	VARCHAR(15),	# ref position_id
            cont_picture	BYTE,
            cont_password	VARCHAR(15),
            cont_ipaddr	VARCHAR(15),	# IP V4
            cont_usemail	SMALLINT,
            cont_usephone	SMALLINT,
            cont_notes   CHAR(1000),
 
            PRIMARY KEY (cont_id) CONSTRAINT contact_pk,
            FOREIGN KEY (cont_org) REFERENCES company(comp_id) CONSTRAINT cont_org_fk_company
            
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO contact
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM contact

      END CASE









    WHEN modu_table_name_list[51] --"company_link"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE company_link
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE company_link (
            parent_comp_id	INTEGER,	# ref comp_id
            child_comp_id	INTEGER,		# ref comp_id
            PRIMARY KEY (parent_comp_id,child_comp_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO company_link
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM company_link

      END CASE





    WHEN modu_table_name_list[52] --"operator"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE operator
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


		WHEN 1  --create table
			CREATE TABLE operator (
				operator_id             SERIAL, #PK
				name                CHAR(10)  NOT NULL UNIQUE,
				password            CHAR(10),
				type                CHAR,
				cont_id             INTEGER, # ref contact_id
				email_address       VARCHAR(100),
				
				PRIMARY KEY (operator_id),
				FOREIGN KEY (cont_id) REFERENCES contact(cont_id)
			)

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
           LOAD FROM file_name INSERT INTO operator
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM operator

      END CASE


    WHEN modu_table_name_list[53] --"operator_session"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE operator_session
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE operator_session (
          	session_id					VARCHAR(50),
            operator_id         INTEGER,
            session_counter     INTEGER,
            expired							SMALLINT,
            session_created			DATETIME YEAR TO SECOND,
            session_modified  	DATETIME YEAR TO SECOND,
            client_host					VARCHAR(50),
            client_ip						VARCHAR(50),
            reserve1						VARCHAR(50),
            reserve2						VARCHAR(50)--,
            
            --PRIMARY KEY (session_id--),
            
          	--FOREIGN KEY (operator_id) 
            --REFERENCES operator (operator_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO operator_session
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM operator_session ORDER BY operator_id, session_created, session_modified ASC

      END CASE
      
    WHEN modu_table_name_list[54] --"activity"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE activity
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE activity (
            activity_id	SERIAL,# UNIQUE,
            open_date	DATE NOT NULL,
            close_date	DATE,
            contact_id	INTEGER NOT NULL,	# ref contact_id
            comp_id		INTEGER,	# ref comp_id
            operator_id     INTEGER NOT NULL,	# ref operator_id
            act_type	INTEGER,		# ref activity_type
            long_desc	TEXT,
            short_desc	CHAR(80),
            #long_ref	INTEGER,		# ref possible blob
            a_owner		INTEGER,	# ref contact_id
            priority	INTEGER,
            
            PRIMARY KEY (activity_id) CONSTRAINT activity_pk,
            FOREIGN KEY (contact_id) REFERENCES contact(cont_id) CONSTRAINT contact_id_fk_contact,
            FOREIGN KEY (comp_id) REFERENCES company(comp_id) CONSTRAINT comp_id_fk_company,
            FOREIGN KEY (operator_id) REFERENCES operator(operator_id) CONSTRAINT operator_id_fk_operator,
            FOREIGN KEY (act_type) REFERENCES activity_type(activity_type_id) CONSTRAINT act_type_fk_activity_type,
            FOREIGN KEY (a_owner) REFERENCES contact(cont_id) CONSTRAINT contact_id_fk_a_owner
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO activity
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM activity

      END CASE
      
    WHEN modu_table_name_list[55] --"mailbox"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE mailbox
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE mailbox (
            mail_id             SERIAL,				# 
            activity_id         INTEGER NOT NULL UNIQUE,		# ref activity
            operator_id         INTEGER NOT NULL, 		# ref operator.operator_id
            from_email_address  VARCHAR(100),
            from_cont_id     INTEGER,			# ref contact.
            to_email_address    VARCHAR(100),
            to_cont_id       INTEGER,
            recv_date           DATE,
            read_flag           SMALLINT,
            urgent_flag         SMALLINT,
            mail_status         SMALLINT			# inbound, outbound, read, deleted, etc.
           # although the activity provides an implicit contact, we need a contact where no activity is involved. 
           # Eventually, I don't want this to reference activity for obvious reasons, but
           # for now it'll be OK.
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO mailbox
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM mailbox

      END CASE


    WHEN modu_table_name_list[56] -- "invoice"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE invoice
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func

		
		WHEN 1  --create table
			CREATE TABLE invoice (
				invoice_id		SERIAL(1000),
				account_id		INTEGER NOT NULL,
				invoice_date	DATE NOT NULL,
				pay_date          	DATE,
				status              INTEGER NOT NULL,
				tax_total		MONEY(8,2) NOT NULL,
				net_total		MONEY(8,2) NOT NULL,
				inv_total		MONEY(8,2) NOT NULL,
				currency_id		INTEGER,
				for_total         	MONEY(8,2),        -- 
				operator_id		INTEGER NOT NULL,
				pay_method_id       INTEGER NOT NULL,
				pay_method_name     CHAR(20),
				pay_method_desc     CHAR(20),
				del_address_dif	INTEGER NOT NULL,
				del_address1 		CHAR(30),
				del_address2 		CHAR(30),
				del_address3 		CHAR(30),
				del_method 		INT,  --CHAR(20),
				invoice_po		CHAR(20),
				PRIMARY KEY (invoice_id),
				FOREIGN KEY (pay_method_id) REFERENCES pay_method (pay_method_id)
			)

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO invoice
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM invoice

      END CASE



    WHEN  modu_table_name_list[57] --"invoice_line"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE invoice_line
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE invoice_line (
            invoice_id          INTEGER NOT NULL,    -- references invoice
            quantity            INTEGER,             
            stock_id            CHAR(10) NOT NULL,  -- references stock item
            item_tax            INTEGER NOT NULL     -- references tax rates
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO invoice_line
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM invoice_line

      END CASE






   WHEN modu_table_name_list[58] -- "account"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE account
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table

          CREATE TABLE account (
            account_id          SERIAL,
            comp_id             INTEGER  NOT NULL UNIQUE,
            credit_limit        MONEY(8,2),
            discount            DECIMAL(4,2) NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO account
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM account

      END CASE




    WHEN modu_table_name_list[59] -- "title"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE title
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE title (
            title           CHAR(10) NOT NULL UNIQUE
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO title
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM title

      END CASE



    WHEN modu_table_name_list[60] -- "zone"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE zone
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE zone (
            zone_id           SERIAL      NOT NULL UNIQUE,
            zone_name         VARCHAR(40) NOT NULL UNIQUE,
            country_id        INTEGER     NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO zone
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM zone

      END CASE



    WHEN modu_table_name_list[61] -- "status_invoice"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE status_invoice
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE status_invoice
          (
            status_id   SERIAL,
            status_name CHAR(25) NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO status_invoice
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM status_invoice

      END CASE



    WHEN modu_table_name_list[62] -- "state_supply"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE state_supply
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE state_supply
          (
            state_id    SERIAL,
            state_name  CHAR(25) NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO state_supply
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM state_supply

      END CASE



    WHEN modu_table_name_list[63] -- "supplies"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE supplies
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE supplies
          (
            suppl_id    SERIAL,
            operator_id INTEGER NOT NULL,
            suppl_date  DATE,
            state       INTEGER NOT NULL,
            account_id  INTEGER NOT NULL,
            exp_date    DATE
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO supplies
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM supplies

      END CASE



    WHEN modu_table_name_list[64] -- "supplies_line"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE supplies_line
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE supplies_line
          (
            suppl_id    INTEGER NOT NULL,
            stock_id    CHAR(10) NOT NULL,
            quantity    INTEGER
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO supplies_line
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM supplies_line

      END CASE

    WHEN modu_table_name_list[65] -- "activity_birt"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE activity_birt
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE activity_birt (
            sesion_id   INTEGER,
            activity_id	INTEGER,
            open_date	DATE NOT NULL,
            close_date	DATE,
            contact_id	INTEGER NOT NULL,	# ref contact_id
            comp_id		INTEGER,	# ref comp_id
            operator_id INTEGER NOT NULL,	# ref operator_id
            act_type	INTEGER,		# ref activity_type
            long_desc	TEXT,
            short_desc	CHAR(80),
            a_owner		INTEGER,	# ref contact_id
            priority	INTEGER
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO activity_birt
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM activity_birt

      END CASE


    WHEN modu_table_name_list[66] -- "country2"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE country2
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE country2 (
						country_code       nchar(3),  #NOT NULL UNIQUE,
						country_text       nvarchar(60,0) NOT NULL,
						language_code      nchar(3),
						post_code_text     nvarchar(20,0),
						post_code_min_num  smallint,
						post_code_max_num  smallint,
						state_code_text    nvarchar(20,0),
						state_code_min_num smallint,
						state_code_max_num smallint,
						bank_acc_format    smallint,
#            FOREIGN KEY (language_code) 
#              REFERENCES qxt_language (language_id) ,
           PRIMARY KEY (country_code)
					)
        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO country2
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM country2

      END CASE
      
    WHEN modu_table_name_list[67] -- "language"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE language
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE language (
						language_code        nchar(3),
						language_text        nvarchar(80,0) NOT NULL,
						yes_flag             nchar(1),
						no_flag              nchar(1),
						national_text        nvarchar(80,0),
						PRIMARY KEY (language_code)
					)
        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO language
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM language

      END CASE
      
    WHEN modu_table_name_list[68] -- "res_cms_68"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE res_cms_68
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE res_cms_68 (
            res_id   INTEGER
					)
        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO res_cms_68
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM res_cms_68

      END CASE
      
    WHEN modu_table_name_list[69] -- "res_cms_69"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE res_cms_69
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE res_cms_69 (
            res_id   INTEGER
					)
        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO res_cms_69
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM res_cms_69

      END CASE

    WHEN modu_table_name_list[70] -- "res_cms_70"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE res_cms_70
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE res_cms_70 (
            res_id   INTEGER
					)
        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO res_cms_70
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM res_cms_70

      END CASE
      
      
		WHEN modu_table_name_list[71] -- "test01"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE test01
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE test01 (
						f1_varchar VARCHAR(20),
						f3_smallint SMALLINT,
						f4_varchar VARCHAR(20),
						f5_date DATE,
						f6_varchar VARCHAR(20),
						f7_smallint SMALLINT, #boolean 0/1
						f8_smallint SMALLINT,
						f9_dt_h_t_s Datetime Hour to Second,
						PRIMARY KEY (f1_varchar) CONSTRAINT pk_f1_varchar
					)
        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO test01
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM test01

      END CASE



		WHEN modu_table_name_list[72] -- "test02"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE test02
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE test02 (
          	fTextField VARCHAR(20),
            fListBox   VARCHAR(20),
						fCombo VARCHAR(20),
						fRadioButtonListV VARCHAR(20),
						fTextArea VARCHAR(250),
						fBlobText TEXT,
						PRIMARY KEY (fTextField) CONSTRAINT pk_fTextField
					)

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO test02
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM test02

      END CASE


		WHEN modu_table_name_list[73] -- "test03"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE test03
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE test03 (
            math_operation_id   			SERIAL,
						operator_val_1            DECIMAL(10,2) NOT NULL,
						operator_val_2            DECIMAL(10,2) NOT NULL,
						result_sum		            DECIMAL(10,2) NOT NULL,
						result_difference	        DECIMAL(10,2) NOT NULL,
						result_product		        DECIMAL(10,2) NOT NULL,
						result_quotient           DECIMAL(10,2) NOT NULL,
						PRIMARY KEY (math_operation_id) CONSTRAINT pk_math_operation_id
					)
        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO test03
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM test03

      END CASE

		WHEN modu_table_name_list[74] -- "test04"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE test04
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func

 
        WHEN 1  --create table
          CREATE TABLE test04 (
            test04_primary_key SERIAL,
            test04_char_1  CHAR,
            test04_char_2  CHAR(20),
            test04_varchar_1  VARCHAR(20),
            test04_varchar_2  VARCHAR(20),
            test04_boolean_1  BOOLEAN,
            test04_boolean_2  BOOLEAN,
            test04_int_1  INTEGER,
            test04_int_2  INTEGER,
            test04_decimal_1  DECIMAL(8, 2),
            test04_decimal_2  DECIMAL(8, 2),
            test04_money_1  MONEY(6, 2),
            test04_money_2  MONEY(6, 2),
            test04_date_1  DATE,
            test04_date_2  DATE,
            test04_datetime_1  DATETIME YEAR TO SECOND,
            test04_datetime_2  DATETIME YEAR TO SECOND,
            test04_text_1  TEXT,
            test04_byte_1  BYTE,
						PRIMARY KEY (test04_primary_key) CONSTRAINT pk_test04_primary_key           
            )
        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO test04
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM test04

      END CASE



			WHEN modu_table_name_list[75] -- "test06"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE test06
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE test06 (
            test06_primary_key CHAR,
            test06_col1  VARCHAR(20),
						test06_col2  VARCHAR(20),
						test06_col3  VARCHAR(20),

						PRIMARY KEY (test06_primary_key) CONSTRAINT pk_test06_primary_key    
					)
        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO test06
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM test06
					
      END CASE


		WHEN modu_table_name_list[76] -- "test05"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE test05
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE test05 (
            test05_primary_key SERIAL,
            test05_fk_char  CHAR NOT NULL,
            test05_varchar  VARCHAR(20),
            test05_int  INTEGER,
            test05_date  DATE,
						FOREIGN KEY (test05_fk_char) 
						REFERENCES test06 (test06_primary_key) ,
						PRIMARY KEY (test05_primary_key) CONSTRAINT pk_test05_primary_key    
					)
        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO test05
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM test05
					
      END CASE

#---------------------
			WHEN modu_table_name_list[77] -- "test08"
			CASE p_action

				WHEN 0  --drop table
					WHENEVER ERROR CONTINUE
					DROP TABLE test08
					WHENEVER ERROR STOP
					##WHENEVER ERROR CALL error_func


				WHEN 1  --create table
					CREATE TABLE test08 (
						test08_primary_key SERIAL,
						test08_fk_char  CHAR NOT NULL,
						test08_varchar  VARCHAR(20),
						test08_int  INTEGER,
						test08_date  DATE,
						FOREIGN KEY (test08_fk_char) 
						REFERENCES test06 (test06_primary_key) ,
						PRIMARY KEY (test08_primary_key) CONSTRAINT pk_test08_primary_key    
					)
				WHEN 2  --load table
					IF server_side_file_exists(file_name) THEN
						LOAD FROM file_name INSERT INTO test08
					END IF

				WHEN 3  --unload table
					UNLOAD TO file_name SELECT * FROM test08
					
			END CASE


			WHEN modu_table_name_list[78] -- "test07"
			CASE p_action

				WHEN 0  --drop table
					WHENEVER ERROR CONTINUE
					DROP TABLE test07
					WHENEVER ERROR STOP
					##WHENEVER ERROR CALL error_func


				WHEN 1  --create table
					CREATE TABLE test07 (
						test07_primary_key SERIAL,
						test07_foreign_key  INT,  #FK pointing at table test08 PK
						test07_fk_char  CHAR NOT NULL,						
						test07_col1  VARCHAR(20),
						test07_col2  VARCHAR(20),
						test07_col3  DATE,
						FOREIGN KEY (test07_foreign_key) 
						REFERENCES test08 (test08_primary_key) ,
						FOREIGN KEY (test07_fk_char) 
						REFERENCES test06 (test06_primary_key) ,
						PRIMARY KEY (test07_primary_key) CONSTRAINT pk_test07_primary_key    
					)
				WHEN 2  --load table
					IF server_side_file_exists(file_name) THEN
						LOAD FROM file_name INSERT INTO test07
					END IF

				WHEN 3  --unload table
					UNLOAD TO file_name SELECT * FROM test07
					
			END CASE



    OTHERWISE
      LET tmp_str = "Invalid table name argument in function table_operation()!\ntable_name =", p_table_name
      CALL fgl_winmessage("Error",tmp_str,"error")

  END CASE

  -- COMMIT WORK

  #LET modu_monitor = add_monitor_str(modu_monitor, "End of Create Table Operation",0)
  CALL display_monitor(modu_monitor)

  #CALL fgl_winmessage("End of Operation", "All tables where created","info")

  LET err = 1  -- -1=error  0 = failed but not error 1=ok

  RETURN err 

END FUNCTION
###################################################################################
# END FUNCTION table_operation()
###################################################################################




###################################################################################
# FUNCTION ini_table_name_list()
#
#
###################################################################################
FUNCTION ini_table_name_list()
  LET modu_table_name_list[1] = "qxt_application"
  LET modu_table_name_list[2] = "qxt_language"
  LET modu_table_name_list[3] = "qxt_str_category"
  LET modu_table_name_list[4] = "qxt_string_tool"
  LET modu_table_name_list[5] = "qxt_string_app"
  LET modu_table_name_list[6] = "qxt_help_classic"
  LET modu_table_name_list[7] = "qxt_help_url_map"
  LET modu_table_name_list[8] = "qxt_help_html_doc"  
  LET modu_table_name_list[9] = "qxt_toolbar"
  LET modu_table_name_list[10] = "qxt_icon_size"
  LET modu_table_name_list[11] = "qxt_icon_path"
  LET modu_table_name_list[12] = "qxt_icon_property"
  LET modu_table_name_list[13] = "qxt_icon_category"
  LET modu_table_name_list[14] = "qxt_icon"
  LET modu_table_name_list[15] = "qxt_tbi_tooltip"
  LET modu_table_name_list[16] = "qxt_tbi_event_type"
  LET modu_table_name_list[17] = "qxt_tbi_obj_event"
  LET modu_table_name_list[18] = "qxt_tbi_obj_action"
  LET modu_table_name_list[19] = "qxt_tbi_obj"
  LET modu_table_name_list[20] = "qxt_tbi_scope"
  LET modu_table_name_list[21] = "qxt_tbi_static"
  LET modu_table_name_list[22] = "qxt_tbi"
  LET modu_table_name_list[23] = "qxt_tb"
  LET modu_table_name_list[24] = "qxt_document"
  LET modu_table_name_list[25] = "qxt_document_blob"
  LET modu_table_name_list[26] = "qxt_dde_font"
  LET modu_table_name_list[27] = "qxt_print_template"
  LET modu_table_name_list[28] = "qxt_print_image"
  LET modu_table_name_list[29] = "qxt_reserve"
  LET modu_table_name_list[30] = "qxt_reserve2"

  LET modu_qxt_table_count = 30

  # Actual CMS Application 
  # Actual CMS Application 
  LET modu_table_name_list[31] = "cms_info"
  LET modu_table_name_list[32] = "menus"
  LET modu_table_name_list[33] = "menu_options"
  LET modu_table_name_list[34] = "country"
  LET modu_table_name_list[35] = "state"
  LET modu_table_name_list[36] = "user_fields"
  LET modu_table_name_list[37] = "user_field_data"
  LET modu_table_name_list[38] = "industry_type"
  LET modu_table_name_list[39] = "company_type"
  LET modu_table_name_list[40] = "contact_dept"
 
  LET modu_table_name_list[41] = "position_type"
  LET modu_table_name_list[42] = "activity_type"
  LET modu_table_name_list[43] = "tax_rate"
  LET modu_table_name_list[44] = "pay_method"
  LET modu_table_name_list[45] = "titles"
  LET modu_table_name_list[46] = "delivery_type"
  LET modu_table_name_list[47] = "currency"
  LET modu_table_name_list[48] = "stock_item"
  LET modu_table_name_list[49] = "company"
  LET modu_table_name_list[50] = "contact"
 
  LET modu_table_name_list[51] = "company_link"
  LET modu_table_name_list[52] = "operator"
  LET modu_table_name_list[53] = "operator_session"
  LET modu_table_name_list[54] = "activity"
  LET modu_table_name_list[55] = "mailbox"
  LET modu_table_name_list[56] = "invoice"
  LET modu_table_name_list[57] = "invoice_line"
  LET modu_table_name_list[58] = "account"
  LET modu_table_name_list[59] = "title"
  LET modu_table_name_list[60] = "zone"
 
  LET modu_table_name_list[61] = "status_invoice"
  LET modu_table_name_list[62] = "state_supply"
  LET modu_table_name_list[63] = "supplies"
  LET modu_table_name_list[64] = "supplies_line"
  LET modu_table_name_list[65] = "activity_birt" --table for adapting birt to complex query

  LET modu_table_name_list[66] = "country2"
  LET modu_table_name_list[67] = "language"
  LET modu_table_name_list[68] = "res_cms_68"
  LET modu_table_name_list[69] = "res_cms_69"
  LET modu_table_name_list[70] = "res_cms_70"

  LET modu_table_name_list[71] = "test01" 
  LET modu_table_name_list[72] = "test02" 
  LET modu_table_name_list[73] = "test03" 
  LET modu_table_name_list[74] = "test04" 
  LET modu_table_name_list[75] = "test06" #Note test05 requires test06
	LET modu_table_name_list[76] = "test05" 
	LET modu_table_name_list[77] = "test08" 
	LET modu_table_name_list[78] = "test07" 
 

  LET modu_cms_table_count = 78

END FUNCTION
###################################################################################
# END FUNCTION ini_table_name_list()
###################################################################################


###################################################################################
# FUNCTION all_tables(p_action,p_file_type)
#
#
###################################################################################
FUNCTION all_tables(p_action,p_file_type)
  DEFINE
   p_action SMALLINT,
   id       SMALLINT,
   err      SMALLINT,
   tmp_str  STRING,
   tmp_str1 STRING,
   tmp_str2 STRING,
   tmp_str3 STRING,
   p_file_type SMALLINT,
   temp_action  SMALLINT,
   local_debug  SMALLINT,
   stmt      VARCHAR(512)

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "all_tables() - p_action=", p_action
    DISPLAY "all_tables() - p_file_type=", p_file_type
  END IF

  CASE p_action
    WHEN -1  --Re-create entire database
      LET tmp_str1 = "Re-create (Drop,Create/Populate) ALL tables"
      LET tmp_str2 = "Are you sure you want to re-create (drop) all tables?\nYou can not undo this operation and will lose all tables including their data"
      LET tmp_str3 = "End Of Operation - The entire CMS database was re-created"
    WHEN 0  --drop
      LET tmp_str1 = "Drop ALL tables"
      LET tmp_str2 = "Are you sure you want to drop all tables?\nYou can not undo this operation and will lose all tables including their data"
      LET tmp_str3 = "End Of Operation - All CMS database tables were droped"
    WHEN 1  --create
      LET tmp_str1 = "Create ALL tables"
      LET tmp_str2 = "Are you sure you want to create all tables?\nYou can not undo this operation"
      LET tmp_str3 = "End Of Operation - All CMS database tables were created"

    WHEN 2  --load
      LET tmp_str1 = "Populate (Load) ALL tables"
      LET tmp_str2 = "Are you sure you want to populate all tables?\nYou can not undo this operation"
      LET tmp_str3 = "End Of Operation - All CMS database tables populated (unl load)"

    WHEN 3  --unload
      LET tmp_str1 = "Export (UnLoad) ALL tables"
      LET tmp_str2 = "Are you sure you want to export all tables?\nExisting 'Load/Unload files will be overwritten"
      LET tmp_str3 = "End Of Operation - All CMS database table data were exported"
      
    OTHERWISE
      LET tmp_str = "Error in function all_tables()\nFunction argument p_action is invalid\np_action = ", p_action
      CALL fgl_winmessage("Error in function all_tables()",tmp_str,"error")
      RETURN

  END CASE

  IF local_debug THEN
    DISPLAY "all_tables() - yes/no?="
    #DISPLAY "all_tables() - p_file_type=", p_file_type
  END IF

  #Are you sure ?
  IF yes_no(tmp_str1,tmp_str2) THEN

    LET modu_monitor = "" --initialise modu_monitor progress string
    LET modu_monitor = add_monitor_str(modu_monitor, tmp_str1,0)
    CALL display_monitor(modu_monitor)


    #recreate entire database
    IF p_action = -1  THEN --recreate entire database 

      #drop 
      LET temp_action = 0   --0=drop and 1=table create 2 =load 3=unload
      #Note: must drop table in reversed order
      FOR id = modu_inst_table_count TO 1  STEP -1
        IF fgl_find_table(modu_table_name_list[id]) THEN  --only drop, if the table does exist
          CALL table_operation(modu_table_name_list[id],temp_action,p_file_type) RETURNING err
        END IF
      END FOR

      #create
      LET temp_action = 1
      FOR id = 1 TO modu_inst_table_count 
        IF NOT fgl_find_table(modu_table_name_list[id]) THEN  --only drop, if the table does exist
          CALL table_operation(modu_table_name_list[id],temp_action,p_file_type) RETURNING err
        END IF
      END FOR

      #populate
      LET temp_action = 2
      FOR id = 1 TO modu_inst_table_count 
        CALL table_operation(modu_table_name_list[id],temp_action,p_file_type) RETURNING err
      END FOR
      #CALL load_russian_translations()

    ELSE
      #Note: must drop table in reversed order
      IF p_action = 0 THEN  --drop all tables
        FOR id = modu_inst_table_count TO 1 STEP -1
        IF fgl_find_table(modu_table_name_list[id]) THEN  --only drop, if the table does exist
          CALL table_operation(modu_table_name_list[id],p_action,p_file_type) RETURNING err
        END IF
        END FOR
      ELSE
		  IF p_action = 2 THEN  --removing data from all tables
			 DISPLAY "Removing data from all tables. Please wait..."
	 		 #Note: must delete data from tables in reverse order
		    FOR id = modu_inst_table_count TO 1  STEP -1
		      IF fgl_find_table(modu_table_name_list[id]) THEN  --data is deleted if the table exists
				LET stmt = "DELETE FROM ",modu_table_name_list[id]
				PREPARE p_stmt FROM stmt
			   EXECUTE p_stmt
		   	IF SQLCA.SQLCODE <> 0 THEN DISPLAY "Error deleting data from table ",modu_table_name_list[id] CLIPPED,"  SQLCA.SQLCODE = ",SQLCA.SQLCODE END IF
        		END IF
    	    END FOR
		  END IF
        DISPLAY "Loading data into all tables. Please wait..."
        FOR id = 1 TO modu_inst_table_count 
          CALL table_operation(modu_table_name_list[id],p_action,p_file_type) RETURNING err
        END FOR
      END IF

    END IF

    CALL db_state_report()
    LET modu_monitor = add_monitor_str(modu_monitor, tmp_str3,0)
    CALL display_monitor(modu_monitor)
  ELSE
    CALL fgl_winmessage("Operation canceled",trim(tmp_str1) || " operation cancelled at user's request","info")
  END IF

  RETURN err
END FUNCTION
###################################################################################
# END FUNCTION all_tables(p_action,p_file_type)
###################################################################################


########################################################################
# FUNCTION table_selection_menu(p_action,p_file_type)
#
# Display table list for selection & apply action/operation
#
# RETURN err
########################################################################
FUNCTION table_selection_menu(p_action,p_file_type)
  DEFINE
    p_action   SMALLINT,
    tmp_str    STRING,
    id         SMALLINT,
    err        SMALLINT,
    p_file_type SMALLINT


  CALL fgl_window_open("w_table_popup",5,5,"db_tools_form_list_g",TRUE)
  CALL fgl_settitle("Table List")


  CALL set_count(modu_inst_table_count)
  LET int_flag = FALSE

  DISPLAY ARRAY modu_table_name_list TO table_name_list.*
    ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")

    AFTER DISPLAY
      LET id = arr_curr()
      
  END DISPLAY  

  CALL fgl_window_close("w_table_popup")

  IF int_flag THEN  --cancel
    #do nothing
    LET int_flag = FALSE
  ELSE
    CALL table_operation(modu_table_name_list[id],p_action,p_file_type) RETURNING err
  END IF

  RETURN err
END FUNCTION
########################################################################
# END FUNCTION table_selection_menu(p_action,p_file_type)
########################################################################


###########################################################
# FUNCTION validate_file_server_side_exists(p_filename, p_optional_str,p_dialog)
#
# FILE - Validate the existence of a file on the SERVER (with optional message)
#
# RETURN BOOLEAN
###########################################################
FUNCTION server_side_file_exists(p_filename)
  DEFINE 
    p_filename     VARCHAR(250),
    p_dialog       SMALLINT,
    tmp_str        VARCHAR(300),
    msg VARCHAR(200),
    rv  CHAR,
    ret              SMALLINT,
    local_debug      SMALLINT

  LET local_debug = FALSE

  LET rv = "E"
  LET ret = TRUE

  IF local_debug THEN
    DISPLAY "server_side_file_exists() - p_filename=", p_filename
  END IF

  WHILE TRUE 
    #Check if the file exists on the server
    IF fgl_test("e",p_filename) THEN
      IF local_debug THEN
        DISPLAY "server_side_file_exists() - file does exist=", p_filename
      END IF

      LET ret = TRUE
      EXIT WHILE

    ELSE  --file does not exist
      IF local_debug THEN
        DISPLAY "server_side_file_exists() - file does NOT exist=", p_filename
      END IF

      LET tmp_str = "Following file does not exist:\n", trim(p_filename)
      LET rv = fgl_winquestion ("Read/Import UNL/BAK Data",tmp_str,1,"Abort|Retry|Ignore","question",0)
      IF local_debug THEN
        DISPLAY "server_side_file_exists() - User choice ret=", rv
      END IF

      CASE rv
        WHEN "I" 
          LET ret = FALSE
          EXIT WHILE

        WHEN "A"
          EXIT PROGRAM

        WHEN "R" --Retry
          --simply re-run loop

        OTHERWISE
          LET tmp_str = "Error in server_side_file_exists()\nInvalid case rv value\nrv=", rv
          cALL fgl_winmessage("Error",rv,"error")
          EXIT PROGRAM
      END CASE
    END IF
  END WHILE

  IF local_debug THEN
    DISPLAY "server_side_file_exists() - RETURN ret=", ret
  END IF

  RETURN ret


END FUNCTION
###########################################################
# END FUNCTION validate_file_server_side_exists(p_filename, p_optional_str,p_dialog)
###########################################################


###########################################################
# FUNCTION load_russian_translations()
#
# Load russian translations
#
# RETURN TRUE or FALSE
###########################################################
{FUNCTION load_russian_translations()

  DEFINE l_russ_lang_id     SMALLINT,
         l_dir_name         VARCHAR(100),
         l_file_name        VARCHAR(100)

  LET l_russ_lang_id = 8
  LET l_dir_name     = "unl/unl_russian/"
  
WHENEVER ERROR GOTO :lbl_del_lang

  LET l_file_name = l_dir_name, "qxt_language.unl"
  LOAD FROM l_file_name INSERT INTO qxt_language
  LET l_file_name = l_dir_name, "qxt_help_url_map.unl"
  LOAD FROM l_file_name INSERT INTO qxt_help_url_map
  LET l_file_name = l_dir_name, "qxt_icon_category.unl"
  LOAD FROM l_file_name INSERT INTO qxt_icon_category
  LET l_file_name = l_dir_name, "qxt_print_template.unl"
  LOAD FROM l_file_name INSERT INTO qxt_print_template
  LET l_file_name = l_dir_name, "qxt_str_category.unl"
  LOAD FROM l_file_name INSERT INTO qxt_str_category
  LET l_file_name = l_dir_name, "qxt_string_app.unl"
  LOAD FROM l_file_name INSERT INTO qxt_string_app

WHENEVER ERROR STOP

  RETURN TRUE

  LABEL lbl_del_lang:

  DELETE FROM qxt_help_url_map   WHERE language_id = l_russ_lang_id  
  DELETE FROM qxt_icon_category  WHERE language_id = l_russ_lang_id  
  DELETE FROM qxt_print_template WHERE language_id = l_russ_lang_id  
  DELETE FROM qxt_str_category   WHERE language_id = l_russ_lang_id  
  DELETE FROM qxt_string_app     WHERE language_id = l_russ_lang_id  
  DELETE FROM qxt_language       WHERE language_id = l_russ_lang_id  

WHENEVER ERROR STOP
  
  RETURN FALSE

END FUNCTION

}