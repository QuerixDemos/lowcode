########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
DEFINE modu_origin_priority INT
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7963
# Addresses/Scenario...
# Single table but byte and char(1000) field
########################################################################
# DB Table Schema
{
          CREATE TABLE activity (
            activity_id	SERIAL,# UNIQUE,
            open_date	DATE NOT NULL,
            close_date	DATE,
            contact_id	INTEGER NOT NULL,	# ref contact_id
            comp_id		INTEGER,	# ref comp_id
            operator_id     INTEGER NOT NULL,	# ref operator_id
            act_type	INTEGER,		# ref activity_type
            long_desc	TEXT,
            short_desc	CHAR(80),
            #long_ref	INTEGER,		# ref possible blob
            a_owner		INTEGER,	# ref contact_id
            priority	INTEGER,
            
            PRIMARY KEY (activity_id) CONSTRAINT activity_pk,
            FOREIGN KEY (contact_id) REFERENCES contact(cont_id) CONSTRAINT contact_id_fk_contact,
            FOREIGN KEY (comp_id) REFERENCES company(comp_id) CONSTRAINT comp_id_fk_company,
            FOREIGN KEY (operator_id) REFERENCES operator(operator_id) CONSTRAINT operator_id_fk_operator,
            FOREIGN KEY (act_type) REFERENCES activity_type(activity_type_id) CONSTRAINT act_type_fk_activity_type
            
          )

          CREATE TABLE activity_type (
            activity_type_id		SERIAL,
            atype_name	CHAR(15) NOT NULL UNIQUE,
            user_def	SMALLINT,
            PRIMARY KEY (activity_type_id)
          )

			CREATE TABLE operator (
				operator_id             SERIAL, #PK
				name                CHAR(10)  NOT NULL UNIQUE,
				password            CHAR(10),
				type                CHAR,
				cont_id             INTEGER, # ref contact_id
				email_address       VARCHAR(100),
				
				PRIMARY KEY (operator_id),
				FOREIGN KEY (cont_id) REFERENCES contact(cont_id)
			)
}          
########################################################################

########################################################################
# FUNCTION db_cms_activity_lookup3_rec(p_settings InteractForm_Settings)	
#
# Edit Record
########################################################################
FUNCTION db_cms_activity_lookup3_rec(p_settings InteractForm_Settings)	

	LET p_settings.form_file = "../db_cms_activity_lookup3/db_cms_activity_lookup3_rec"
#	LET p_settings.table = "activity"
#	LET p_settings.paged_mode = FALSE #on fill buffer
#	LET p_settings.input_mode = TRUE	#BOOL - Set to FALSE if DISPLAY ARRAY should be used, otherwise (TRUE) INPUT RECORD/ARRAY is used

	LET p_settings.log_file = "../log/cms_activity.log"	#enable log file
	LET p_settings.pessimistic_locking = TRUE

	#LET p_settings.sql_where =   "activity.act_type =  1"							# The WHERE clause of the main query that can be overwritten as soon the user applies a Search (Construct)
	#LET p_settings.sql_where_static =   "activity activity_id =  1" 	# The WHERE clause of the main query that can NOT be overwritten by user, it's concatenated to sql_where

	#LET p_settings.sql_order_by = "activity_id DESC"					# The ORDER BY clause for the main query
	#LET p_settings.sql_top  = 5																				# The option to limit the base cursor row using the SQL SELECT TOP clause

	#LET p_settings.actions[""]["BEFORE DISPLAY"] = 				FUNCTION custom_before_display
	LET p_settings.actions[""]["AFTER DISPLAY" ] = 				FUNCTION custom_after_display
	LET p_settings.actions[""]["ON ACTION Show Output"] = FUNCTION activity_display_output	
	LET p_settings.actions[""]["ON ACTION Vlad"] = 				FUNCTION custom_on_action_vlad
	
	LET p_settings.actions["UPDATE"]["AFTER INPUT"          ] = FUNCTION custom_after_input
	LET p_settings.actions["UPDATE"]["ON ACTION Hubert"     ] = FUNCTION custom_on_action_hubert
	
	LET p_settings.actions["UPDATE"]["BEFORE INPUT"         ] = FUNCTION custom_update_before_input
	LET p_settings.actions["UPDATE"]["AFTER FIELD priority" ] = FUNCTION check_field_priority
	
	LET p_settings.actions["INSERT"]["BEFORE INPUT"         ] = FUNCTION custom_insert_before_input
	LET p_settings.actions["INSERT"]["AFTER FIELD priority" ] = FUNCTION check_field_priority

	#Control show/remove default events
	LET p_settings.attributes[""]["INSERT ROW"] = TRUE
	LET p_settings.attributes[""]["APPEND ROW"] = TRUE #Note, the LLC library uses DISPLAY/INPUT ARRAY for all data (single record and array record)
	LET p_settings.attributes[""]["DELETE ROW"] = FALSE

	LET p_settings.translations["activity.activity_id"] = "Activity identifier"
	LET p_settings.translations["activity.open_date"] = "Open Date"
	LET p_settings.translations["activity.close_date"] = "Close Date"	
	LET p_settings.translations["The value of Priority should be in range 1..10"] = "The value of Priority should be in range 1..10\nThis string was exchanged in the settings"

	CALL InteractForm(p_settings)	

END FUNCTION
########################################################################
# END FUNCTION db_cms_activity_lookup3_rec(p_settings InteractForm_Settings)	
########################################################################


########################################################################
# FUNCTION db_cms_activity_lookup3_list(p_settings InteractForm_Settings)	
#
#
########################################################################
FUNCTION db_cms_activity_lookup3_list(p_settings InteractForm_Settings)

	LET p_settings.form_file = "../db_cms_activity_lookup3/db_cms_activity_lookup3_list"

#	LET p_settings.table = "activity"
#	LET p_settings.paged_mode = FALSE #on fill buffer
#	LET p_settings.input_mode = TRUE	#BOOL - Set to FALSE if DISPLAY ARRAY should be used, otherwise (TRUE) INPUT RECORD/ARRAY is used

	LET p_settings.log_file = "../log/cms_activity.log"	#enable log file
	LET p_settings.pessimistic_locking = TRUE

	#LET p_settings.sql_where =   "activity.act_type =  1"							# The WHERE clause of the main query that can be overwritten as soon the user applies a Search (Construct)
	#LET p_settings.sql_where_static =   "activity activity_id =  1" 	# The WHERE clause of the main query that can NOT be overwritten by user, it's concatenated to sql_where

	#LET p_settings.sql_order_by = "activity_id DESC"					# The ORDER BY clause for the main query
	#LET p_settings.sql_top  = 5																				# The option to limit the base cursor row using the SQL SELECT TOP clause

	LET p_settings.actions[""]["BEFORE DISPLAY"] = 				FUNCTION custom_before_display
	LET p_settings.actions[""]["AFTER DISPLAY" ] = 				FUNCTION custom_after_display
	LET p_settings.actions[""]["ON ACTION Show Output"] = FUNCTION activity_display_output	
	LET p_settings.actions[""]["ON ACTION Vlad"] = 				FUNCTION custom_on_action_vlad
	
	LET p_settings.actions["UPDATE"]["AFTER INPUT"          ] = FUNCTION custom_after_input
	LET p_settings.actions["UPDATE"]["ON ACTION Hubert"     ] = FUNCTION custom_on_action_hubert
	
	LET p_settings.actions["UPDATE"]["BEFORE INPUT"         ] = FUNCTION custom_update_before_input
	LET p_settings.actions["UPDATE"]["AFTER FIELD priority" ] = FUNCTION check_field_priority
	
	LET p_settings.actions["INSERT"]["BEFORE INPUT"         ] = FUNCTION custom_insert_before_input
	LET p_settings.actions["INSERT"]["AFTER FIELD priority" ] = FUNCTION check_field_priority

	#Control show/remove default events
	LET p_settings.attributes[""]["INSERT ROW"] = TRUE
	LET p_settings.attributes[""]["APPEND ROW"] = TRUE #Note, the LLC library uses DISPLAY/INPUT ARRAY for all data (single record and array record)
	LET p_settings.attributes[""]["DELETE ROW"] = FALSE

	#When the operator double clicks a row in the list, invoke INPUT/EDIT
	LET p_settings.attributes[""]["DOUBLECLICK"] = "Update"     # Attribute for executing UPDATE on DOUBLECLICK

	LET p_settings.translations["activity.activity_id"] = "Activity identifier"
	LET p_settings.translations["activity.open_date"] = "Open Date"
	LET p_settings.translations["activity.close_date"] = "Close Date"	
	LET p_settings.translations["The value of Priority should be in range 1..10"] = "The value of Priority should be in range 1..10\nThis string was exchanged in the settings"

	CALL InteractForm(p_settings)	                            
END FUNCTION
########################################################################
# END FUNCTION db_cms_activity_lookup3_list(p_settings InteractForm_Settings)	
########################################################################



########################################################################
# FUNCTION activity_display_output(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION activity_display_output(iform InteractForm INOUT) RETURNS BOOL
	DEFINE js STRING
	#CALL fgl_winmessage("activity_display_output")
	#DISPLAY iform.output
	
	LET js = util.JSON.stringify(iform.output)
	LET js = "<InteractForm>.output HashMap","\n", "Current data set in json format", "\n\n", js
	CALL fgl_winmessage("<InteractForm>.output HashMap",js,"info")
	
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION activity_display_output(iform InteractForm INOUT) RETURNS BOOL
########################################################################


########################################################################
# FUNCTION custom_before_display() RETURNS BOOL
#
#
########################################################################
FUNCTION custom_before_display(iform InteractForm INOUT) RETURNS BOOL
	CALL fgl_winmessage(iform.LSTRS("BEFORE DISPLAY"))
	RETURN FALSE # Means that built-in function should not be prevented TRUE=overwrite/replace  FALSE=extend/add event functionality
END FUNCTION

########################################################################
# FUNCTION custom_after_display() RETURNS BOOL
#
#
########################################################################
FUNCTION custom_after_display(iform InteractForm INOUT) RETURNS BOOL
	CALL fgl_winmessage(iform.LSTRS("AFTER DISPLAY"))
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION


########################################################################
# FUNCTION custom_before_input() RETURNS BOOL
#
#
########################################################################
FUNCTION custom_before_input(iform InteractForm INOUT) RETURNS BOOL
	CALL fgl_winmessage(iform.LSTRS("BEFORE INPUT"))
	RETURN FALSE # Means that built-in function should not be prevented  
END FUNCTION


########################################################################
# FUNCTION custom_after_input() RETURNS BOOL
#
#
########################################################################
FUNCTION custom_after_input(iform InteractForm INOUT) RETURNS BOOL
	CALL fgl_winmessage(iform.LSTRS("AFTER INPUT"))
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION


########################################################################
# FUNCTION custom_on_action_hubert() RETURNS BOOL
#
#
########################################################################
FUNCTION custom_on_action_hubert(iform InteractForm INOUT) RETURNS BOOL
	CALL fgl_winmessage(iform.LSTRS("ON ACTION Hubert"))
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION


########################################################################
# FUNCTION custom_on_action_vlad() RETURNS BOOL
#
#
########################################################################
FUNCTION custom_on_action_vlad(iform InteractForm INOUT) RETURNS BOOL
	CALL fgl_winmessage(iform.LSTRS("ON ACTION Vlad"))
	RETURN FALSE # Means that built-in function should not be prevented
	#NOTE: For custom events (not build in), it doesn't matter if you return true or false 
END FUNCTION


########################################################################
# FUNCTION custom_update_before_input() RETURNS BOOL
#
#
########################################################################
FUNCTION custom_update_before_input(iform InteractForm INOUT) RETURNS BOOL
    CALL fgl_winmessage(iform.LSTRS("(UPDATE) BEFORE INPUT"))
    LET modu_origin_priority = iform.GetFieldValue("activity.priority")
    RETURN FALSE
END FUNCTION


########################################################################
# FUNCTION check_field_priority() RETURNS BOOL
# Validation for activity.priority (range 1-10)
#
########################################################################
FUNCTION check_field_priority(iform InteractForm INOUT) RETURNS BOOL
    DEFINE priority INT
    DEFINE dlg      ui.Dialog
    LET dlg = ui.Dialog.GetCurrent()
    LET priority = iform.GetFieldValue("activity.priority")
    IF priority < 1 OR priority > 10 THEN
        CALL iform.SetFieldValue("activity.priority", modu_origin_priority)
        CALL fgl_winmessage(iform.LSTRS("Wrong value"), iform.LSTRS("The value of Priority should be in range 1..10"), "warning")
        CALL dlg.NextField("+CURR")
    END IF
    RETURN FALSE
END FUNCTION
########################################################################
# END FUNCTION check_field_priority() RETURNS BOOL
########################################################################


########################################################################
# FUNCTION custom_insert_before_input() RETURNS BOOL
# BEFORE INPUT on INSERT
# 
########################################################################
FUNCTION custom_insert_before_input(iform InteractForm INOUT) RETURNS BOOL
    LET modu_origin_priority = 1
    CALL iform.SetFieldValue("activity.priority", modu_origin_priority)
    RETURN FALSE 
END FUNCTION
########################################################################
# END FUNCTION custom_insert_before_input() RETURNS BOOL
########################################################################