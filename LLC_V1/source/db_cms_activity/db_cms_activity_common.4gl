########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
DEFINE modu_origin_priority INT
########################################################################
# INFO
########################################################################

{
########################################################################
# FUNCTION activity_display_output(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION activity_display_output(iform InteractForm INOUT) RETURNS BOOL
	DEFINE js STRING
	#CALL fgl_winmessage("activity_display_output")
	#DISPLAY iform.output
	
	LET js = util.JSON.stringify(iform.output)
	LET js = "<InteractForm>.output HashMap","\n", "Current data set in json format", "\n\n", js
	CALL fgl_winmessage("<InteractForm>.output HashMap",js,"info")
	
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION activity_display_output(iform InteractForm INOUT) RETURNS BOOL
########################################################################
}