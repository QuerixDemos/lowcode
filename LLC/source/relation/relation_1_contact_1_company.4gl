########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses/Scenario
# two tables joined 1:1
########################################################################


########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_rec_contact_company_settings InteractForm_Settings


  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Contact-Company")
	CALL fgl_settitle("Contact-Company")    

	MENU 
		BEFORE MENU			
			CALL fgl_dialog_setkeylabel("Contact 1 rec Company 1 rec 1:1", "Contact 1 rec Company 1 rec 1:1",   "{CONTEXT}/public/querix/icon/svg/24/ic_contact_24px.svg", 101,TRUE,"Display and modify contact rec / company details rec","top") 
			CALL fgl_dialog_setkeylabel("Contact 1 list Company 1 list 1:1","Contact 1 list Company 1 list 1:1","{CONTEXT}/public/querix/icon/svg/24/ic_contacts_24px.svg",102,TRUE,"Display and modify contact list/ company details rec","top") 

	
		ON ACTION "Contact 1 rec Company 1 rec 1:1"
      CALL contact_rec_1_company_1_rec_1_1(l_rec_contact_company_settings)

		ON ACTION "Contact 1 list Company 1 list 1:1"
      CALL contact_list_1_company_1_rec_1_1(l_rec_contact_company_settings)
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
######################################################################## 

########################################################################
# FUNCTION contact_rec_1_company_1_rec_1_1(p_rec_settings InteractForm_Settings)
########################################################################
FUNCTION contact_rec_1_company_1_rec_1_1(p_rec_settings InteractForm_Settings)	
    LET p_rec_settings.form_file = "../relation/relation_1_contact_rec_1_company_rec"
		
    LET p_rec_settings.id = "1 contact 1 company"
    CALL InteractForm(p_rec_settings)                           
END FUNCTION
########################################################################
# END FUNCTION contact_rec_1_company_1_rec_1_1(p_rec_settings InteractForm_Settings)	
########################################################################


########################################################################
# FUNCTION contact_list_1_company_1_rec_1_1(p_rec_settings InteractForm_Settings)
########################################################################
FUNCTION contact_list_1_company_1_rec_1_1(p_rec_settings InteractForm_Settings)	
    LET p_rec_settings.form_file = "../relation/relation_1_contact_list_1_company_rec"
		
    LET p_rec_settings.id = "1 contact 1 company"
    CALL InteractForm(p_rec_settings)                           
END FUNCTION
########################################################################
# END FUNCTION contact_list_1_company_1_rec_1_1(p_rec_settings InteractForm_Settings)	
########################################################################