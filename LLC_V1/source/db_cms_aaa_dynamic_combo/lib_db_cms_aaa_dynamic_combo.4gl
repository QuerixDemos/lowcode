########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
#
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7963
# Addresses/Scenario...
# Single table but byte and char(1000) field
########################################################################

########################################################################
# FUNCTION db_cms_aaa_dynamic_combo_rec()	
#
# Edit Record
########################################################################
FUNCTION db_cms_aaa_dynamic_combo_rec()	
	DEFINE l_settings InteractForm_Settings
	LET l_settings.form_file = "../db_cms_aaa_dynamic_combo/db_cms_aaa_dynamic_combo_rec"


	LET l_settings.log_file = "../log/db_cms_aaa.log"	#enable log file
	LET l_settings.pessimistic_locking = TRUE

	LET l_settings.actions["UPDATE"]["BEFORE FIELD comp_type"] = FUNCTION before_field_comp_type	
	LET l_settings.actions["INSERT"]["BEFORE FIELD comp_type"] = FUNCTION before_field_comp_type	

	#Control show/remove default events
	LET l_settings.attributes[""]["INSERT ROW"] = TRUE
	LET l_settings.attributes[""]["APPEND ROW"] = TRUE #Note, the LLC library uses DISPLAY/INPUT ARRAY for all data (single record and array record)
	LET l_settings.attributes[""]["DELETE ROW"] = FALSE

	LET l_settings.comboboxes["company.comp_industry"] = "industry_type.industry_type_id IN (2,8,13)"

	CALL InteractForm(l_settings)	

END FUNCTION
########################################################################
# END FUNCTION db_cms_aaa_dynamic_combo_rec()	
########################################################################



########################################################################
# FUNCTION before_field_comp_type(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION before_field_comp_type(iform InteractForm INOUT) RETURNS BOOL
    DEFINE priority INT
    DEFINE dlg      ui.Dialog
    DEFINE regex util.REGEX
    DEFINE match util.MATCH_RESULTS
    
    DEFINE l_comp_industry LIKE company.comp_industry #comp_industry
    LET dlg = ui.Dialog.GetCurrent()
    LET l_comp_industry = iform.GetFieldValue("company.comp_industry")
    CASE l_comp_industry
    	WHEN 2 #Software Reseller
    	    CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,3,8,9)") #1Reseller 3=Distributor 8=Supplier 9=VAR

    	WHEN 8 #Manufacturing
    	    CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (2,4)") #2=Enduser 4=Educational

    	WHEN 13 #Software Distributor
    	    CALL iform.PopulateComboBoxWhere("company.comp_type", "company_type.company_type_id IN (1,3,8,9)") #1Reseller 3=Distributor 8=Supplier 9=VAR

    	OTHERWISE
    		CALL fgl_winmessage("ERROR","Invalid Company Industry\n(Internal 4gl error)","ERROR")
    END CASE

    RETURN TRUE

#		LET regex = /^[1,3,9]/
#		LET r = util.REGEX.match(l_comp_industry, regex)        
#    IF NOT r
#        CALL iform.SetFieldValue("activity.priority", modu_origin_priority)
#        CALL fgl_winmessage(iform.LSTRS("Wrong value"), iform.LSTRS("The value of Priority should be in range 1..10"), "warning")
#        CALL dlg.NextField("+CURR")
#    END IF

END FUNCTION
########################################################################
# END FUNCTION before_field_comp_type(iform InteractForm INOUT) RETURNS BOOL
########################################################################