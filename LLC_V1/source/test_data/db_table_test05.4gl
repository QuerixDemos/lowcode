########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses/Scenario:
# rate_id is SERIAL PK
########################################################################
# DB Table Schema
#CREATE TABLE test05 (
#	test05_primary_key SERIAL,
#	test05_fk_char  CHAR,
#	test05_varchar  VARCHAR(20),
#	test05_int  INTEGER,
#	test05_date  DATE,
#				PRIMARY KEY (test05_primary_key) CONSTRAINT pk_test05_primary_key    
#			)
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT

	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Test05")
	CALL fgl_settitle("Test05")  

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Test05 Record","Test05 Record","{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	101,TRUE,"Test05 Record - Display, Scroll and modify Test05 table data (record view)",	"top") 
			CALL fgl_dialog_setkeylabel("Test05 List",	"Test05 List",	"{CONTEXT}/public/querix/icon/svg/24/ic_list_24px.svg",		102,TRUE,"Test05 List - List all tTest05 table data (array view)",						"top")			

		ON ACTION "Test05 Record"
			CALL db_table_test05_rec()

		ON ACTION "Test05 List"
			CALL db_table_test05_list()	
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################


########################################################################
# FUNCTION db_table_test05_rec()
#
#
########################################################################
FUNCTION db_table_test05_rec()	
    CALL InteractFormFile("../test_data/db_table_test05_rec")                                # The main staff function
END FUNCTION
########################################################################
# END FUNCTION db_table_test05_rec()
########################################################################


########################################################################
# FUNCTION db_table_test05_list()
#
#
########################################################################
FUNCTION db_table_test05_list()	
	DEFINE settings InteractForm_Settings
    LET settings.paged_mode = TRUE
    LET settings.form_file = "../test_data/db_table_test05_list"
    --LET settings.display = TRUE
    CALL InteractForm(settings)
END FUNCTION
########################################################################
# END FUNCTION db_table_test05_list()
########################################################################