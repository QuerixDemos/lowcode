########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="../llc_settings/llc_settings_rec"
	LET l_rec_settings.actions["INSERT"]["AFTER FIELD test05_int" ] = FUNCTION actions_insert_after_field_test05_int
	CALL InteractForm(l_rec_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="../llc_settings/llc_settings_list"
	LET l_rec_settings.actions["INSERT"]["AFTER FIELD test05_int" ] = FUNCTION actions_insert_after_field_test05_int

	CALL InteractForm(l_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################


########################################################################
# FUNCTION actions_insert_after_field_test05_int()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_insert_after_field_test05_int(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING

	#JUST for demo purpose - we work in another function
	#You can do any field validation here and update/change the data
	CALL display_dialog_record(iform)
	
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_insert_after_field_test05_int()RETURNS BOOL
########################################################################