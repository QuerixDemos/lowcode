########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
PUBLIC DEFINE md_subtitle STRING #for demo subTitle text
PUBLIC DEFINE md_info STRING #for demo description text
PUBLIC DEFINE md_code_sample STRING #for demo code sniplet
PUBLIC DEFINE md_language STRING #Language 
PUBLIC DEFINE md_msg STRING #string for messages
########################################################################
# INFO
########################################################################
# settings.log_file
#
########################################################################
# DB Table Schema
#CREATE TABLE test05 (
#    test05_primary_key SERIAL,
#    test05_fk_char  CHAR,
#    test05_varchar  VARCHAR(20),
#    test05_int  INTEGER,
#    test05_date  DATE,
#                PRIMARY KEY (test05_primary_key) CONSTRAINT pk_test05_primary_key    
#            )
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_title STRING

	DEFER INTERRUPT
	OPTIONS INPUT WRAP

	LET l_title = os.Path.basename(arg_val(0))
	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText(l_title)
	CALL fgl_settitle(l_title)  

	#Demo info to be displayed on screen
	LET md_info = 
		"InteractForm_Settings.views[n].sql_order_by\n",
		"The ORDER BY clause for the main query"
	LET md_code_sample = 
		"LET l_rec_settings.views[\"test05\"].sql_order_by = \"test05_date DESC\"	"

	MENU 
		BEFORE MENU
		CALL fgl_dialog_setkeylabel("Record",	"Record/Detailed View","{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	101,TRUE,"Record/Detailed View - Display, Scroll and modify table data (record view)",	"top") 
		CALL fgl_dialog_setkeylabel("List",		"Array/List View",		"{CONTEXT}/public/querix/icon/svg/24/ic_list_24px.svg",		102,TRUE,"Array/List View - List all table data (array view)",							"top")			

		ON ACTION "Record"
			CALL settings_rec()

		ON ACTION "List"
			CALL settings_list()	
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################

FUNCTION settings_rec()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="../settings/llc_settings_rec"
	LET l_rec_settings.id = "settings"  #settings action on append is null is identical/ LowCode action processing are managed in the 4gl code

	LET l_rec_settings.views["test05"].sql_order_by = "test05_date DESC"	
	#LET l_rec_settings.views["test05"].sql_where = "test05_fk_char='A'"	#SQL WHERE clause (STATIC, will not be overwritten by search/construct)
	#LET l_rec_settings.views["test05"].sql_where_search = "test05_primary_key=6"	#SQL WHERE clause (temp, not static, will be overwritten by search/construct)
	# sql_where_search     STRING                # The WHERE clause of the main query that can be overwritten as soon the user applies a Search (Construct)
	# sql_where            STRING                # The WHERE clause of the main query that can NOT be overwritten by user
	# sql_order_by         STRING                # The ORDER BY clause for the main query
	# sql_top              INT                   # The option to limit the base cursor row using the SQL SELECT TOP clause

	#Display some information
	LET l_rec_settings.views["test05"].navigation_status="nav_page_of"	#Display current cursor location to with label by identifier
	LET l_rec_settings.views["test05"].actions[""]["BEFORE DIALOG" ] = FUNCTION display_info

	CALL InteractForm(l_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="../settings/llc_settings_list"
	LET l_rec_settings.id = "settings"  #settings action on append is null is identical/ LowCode action processing are managed in the 4gl code
		
	LET l_rec_settings.views["test05"].sql_order_by = "test05_date DESC"
	#LET l_rec_settings.views["test05"].sql_where = "test05_fk_char='A'"	#SQL WHERE clause (STATIC, will not be overwritten by search/construct)
	#LET l_rec_settings.views["test05"].sql_where_search = "test05_primary_key=6"	#SQL WHERE clause (temp, not static, will be overwritten by search/construct)

	#Display some information
	LET l_rec_settings.views["test05"].navigation_status="nav_page_of"	#Display current cursor location to with label by identifier
	LET l_rec_settings.views["test05"].actions[""]["BEFORE DIALOG" ] = FUNCTION display_info

	CALL InteractForm(l_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################


########################################################################
# FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL
#
# Display some useful information to explain the demo
########################################################################
FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL

	IF md_language IS NOT NULL THEN
		LET md_info = md_language, "\n", md_info
	END IF
	DISPLAY md_subtitle TO lb_SubTitle
	DISPLAY md_info TO lb_info
	DISPLAY md_code_sample TO code_sample

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL
########################################################################


########################################################################
# FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING


	LET dlg = ui.Dialog.GetCurrent()
    LET l_rec_test05.test05_primary_key = iform.GetFieldValue("test05.test05_primary_key")
    LET l_rec_test05.test05_fk_char = iform.GetFieldValue("test05.test05_fk_char")
    LET l_rec_test05.test05_varchar = iform.GetFieldValue("test05.test05_varchar")
    LET l_rec_test05.test05_int = iform.GetFieldValue("test05.test05_int")
    LET l_rec_test05.test05_date = iform.GetFieldValue("test05.test05_date")

	#For demo purpose - range 0-1000
	LET l_msg = "The variable test05_int=", trim(l_rec_test05.test05_int), " was outside of the valid range 0-1000 and was corrected to "
	IF l_rec_test05.test05_int < 0 THEN

		LET l_rec_test05.test05_int = 0	#min
		LET l_msg = l_msg , " ", trim(l_rec_test05.test05_int) 
		CALL fgl_winmessage("test05_int",l_msg,"error")
	ELSE
		IF l_rec_test05.test05_int > 1000 THEN
			LET l_rec_test05.test05_int = 1000 #max
			LET l_msg = l_msg , " ", trim(l_rec_test05.test05_int) 
			CALL fgl_winmessage("test05_int",l_msg,"error")
		END IF
	END IF
	
	CALL iform.SetFieldValue("test05.test05_int", l_rec_test05.test05_int)

	LET l_msg = md_msg,
	"\nRecord Data:",
	"\n", trim(l_rec_test05.test05_primary_key) , 
	"\n", trim(l_rec_test05.test05_fk_char), 
	"\n", trim(l_rec_test05.test05_varchar), 
	"\n", trim(l_rec_test05.test05_int), 
	"\n", trim(l_rec_test05.test05_date)

	CALL fgl_winmessage("Record Data",l_msg,"info")
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
########################################################################


