########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7909
# Anomaly/Difference
# No PK but the column is NOT NULL unique and only one column
########################################################################
# DB Table Schema
#	CREATE TABLE country2 (
#		country_code         nchar(3)                                no
#		country_text         nvarchar(60,0)                          no
#		language_code        nchar(3)                                yes
#		post_code_text       nvarchar(20,0)                          yes
#		post_code_min_num    smallint                                yes
#		post_code_max_num    smallint                                yes
#		state_code_text      nvarchar(20,0)                          yes
#		state_code_min_num   smallint                                yes
#		state_code_max_num   smallint                                yes
#		bank_acc_format      smallint
#	)
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Country2")
	CALL fgl_settitle("Country2")       

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Country Record","Country","{CONTEXT}/public/querix/icon/svg/24/ic_country_24px.svg",101,TRUE,"Country Record - Display and modify contact data","top") 
			CALL fgl_dialog_setkeylabel("Country List","Country List","{CONTEXT}/public/querix/icon/svg/24/ic_country_list_24px.svg",102,TRUE,"Country List - List all contacts","top")			
		
		ON ACTION "Country Record"
			CALL db_cms_country2_rec()
			
		ON ACTION "Country List"
			CALL db_cms_country2_list()
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################