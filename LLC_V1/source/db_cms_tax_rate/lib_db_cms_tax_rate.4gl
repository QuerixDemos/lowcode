GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"

########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Anomaly
# rate_id is SERIAL PK
########################################################################
# DB Table Schema
#					CREATE TABLE tax_rate (
#						rate_id             SERIAL,
#						tax_rate            DECIMAL(5,2) NOT NULL,
#						tax_desc            CHAR(80)  NOT NULL UNIQUE,
#						PRIMARY KEY (rate_id)
#					)
########################################################################

########################################################################
# FUNCTION db_cms_tax_rate_rec()
#
#
########################################################################
FUNCTION db_cms_tax_rate_rec()	
    CALL InteractFormFile("../db_cms_tax_rate/db_cms_tax_rate_rec")                                # The main staff function
END FUNCTION
########################################################################
# END FUNCTION db_cms_tax_rate_rec()
########################################################################


########################################################################
# FUNCTION db_cms_tax_rate_list()
#
#
########################################################################
FUNCTION db_cms_tax_rate_list()	
	DEFINE settings InteractForm_Settings
    LET settings.paged_mode = TRUE
    LET settings.form_file = "../db_cms_tax_rate/db_cms_tax_rate_list"
    --LET settings.display = TRUE
    CALL InteractForm(settings)
END FUNCTION
########################################################################
# END FUNCTION db_cms_tax_rate_list()
########################################################################