########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Anomaly/Difference
# 4 FK fields for lookup - single table but byte and char(1000) field
########################################################################
# DB Table Schema
{
          CREATE TABLE company (
            comp_id     SERIAL(1000) NOT NULL,
            comp_name	CHAR(100) NOT NULL UNIQUE,
            comp_addr1	CHAR(40),
            comp_addr2	CHAR(40),
            comp_addr3	CHAR(40),
            comp_city	CHAR(20),
            comp_zone	CHAR(15),
            comp_zip	CHAR(15),
            comp_country	CHAR(40),
            acct_mgr	INTEGER,		# ref contact_id
            comp_link	INTEGER,		# future: cross ref link table /Inner Join
            comp_industry	INTEGER,		# ref industry_id
            comp_priority	INTEGER,
            comp_type	INTEGER,		# ref cont_dept_id
            comp_main_cont	INTEGER	NOT NULL,	# ref contact_id
            comp_url	VARCHAR(50),
            comp_notes	CHAR(1000),
 
            PRIMARY KEY (comp_id) CONSTRAINT comp_id_pk,
            FOREIGN KEY (acct_mgr) REFERENCES contact(cont_id) CONSTRAINT acct_mgr_fk_contact,
            FOREIGN KEY (comp_industry) REFERENCES industry_type(industry_type_id) CONSTRAINT industry_type_id_fk_industry_type,
            FOREIGN KEY (comp_type) REFERENCES company_type(company_type_id) CONSTRAINT comp_type_fk_company_type,
            FOREIGN KEY (comp_main_cont) REFERENCES contact(cont_id) CONSTRAINT comp_main_cont_fk_contact 
                        
          )

          CREATE TABLE industry_type (
            industry_type_id	SERIAL,
            itype_name	VARCHAR(25) NOT NULL UNIQUE,
            user_def	SMALLINT
            
            
					CREATE TABLE company_type (
            company_type_id		SERIAL,
            ctype_name	CHAR(25) NOT NULL UNIQUE,
            user_def	SMALLINT,
            base_priority	INTEGER
          )
}          
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_rec_settings_rec InteractForm_Settings
	DEFINE l_rec_settings_list InteractForm_Settings
		
  DEFER INTERRUPT

	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Company Lookup 1")
	CALL fgl_settitle("Company Lookup 1")   

	#You could initialise the LLC settings here
	LET l_rec_settings_rec.navigation_status="nav_page_of"
	LET l_rec_settings_list.navigation_status="nav_page_of"

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Company Details","Company Details","{CONTEXT}/public/querix/icon/svg/24/ic_company_24px.svg",			101,	TRUE,	"Display and modify Company Details (Record View)",	"top") 
			CALL fgl_dialog_setkeylabel("Company List",		"Company List",		"{CONTEXT}/public/querix/icon/svg/24/ic_company_list_24px.svg",	102,	TRUE,	"List all Companies (Array)",												"top")			

		ON ACTION "Company Details"
			CALL db_cms_company_lookup1_rec(l_rec_settings_rec)
		
		ON ACTION "Company List"
			CALL db_cms_company_lookup1_list(l_rec_settings_list)
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################