########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
################################################################
# MODULE scope variables
################################################################
PRIVATE DEFINE md_log_console STRING
PUBLIC DEFINE md_subtitle STRING #for demo subTitle text
PUBLIC DEFINE md_info STRING #for demo description text
PUBLIC DEFINE md_code_sample STRING #for demo code sniplet
PUBLIC DEFINE md_language STRING #Language 
PUBLIC DEFINE md_msg STRING #string for messages
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7910
# Addresses/Scenario:
# PK is serial and FK contact.cont_id
########################################################################
# DB Schema table math_operation
#
#		WHEN 1  --create table
#          CREATE TABLE test03 (
#            math_operation_id   			SERIAL,
#						operator_val_1            DECIMAL(10,2) NOT NULL,
#						operator_val_2            DECIMAL(10,2) NOT NULL,
#						result_sum		            DECIMAL(10,2) NOT NULL,
#						result_difference	        DECIMAL(10,2) NOT NULL,
#						result_product		        DECIMAL(10,2) NOT NULL,
#						result_quotient           DECIMAL(10,2) NOT NULL,
#						PRIMARY KEY (math_operation_id) CONSTRAINT pk_math_operation_id
#					)
################################################################

########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7909
# Anomaly/Difference
# No PK but the column is NOT NULL unique and only one column
########################################################################
# DB Table Schema
#          CREATE TABLE test03 (
#            math_operation_id   			SERIAL,
#						operator_val_1            DECIMAL(10,2) NOT NULL,
#						operator_val_2            DECIMAL(10,2) NOT NULL,
#						result_sum		            DECIMAL(10,2) NOT NULL,
#						result_difference	        DECIMAL(10,2) NOT NULL,
#						result_product		        DECIMAL(10,2) NOT NULL,
#						result_quotient           DECIMAL(10,2) NOT NULL,
#						PRIMARY KEY (math_operation_id) CONSTRAINT pk_math_operation_id
#					)
########################################################################
########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Virtual Fields")
	CALL fgl_settitle("Virtual Fields")       

	#Demo info to be displayed on screen
	LET md_subtitle = "Virtual Fields are defined in the corresponding main LowCode screen record (form)"
	LET md_info = 
		"Virtual fields are defined in the screen record but are not shown in the UI/screen.\nVirtual fields can be managed from the 4gl business logic like any other field and get handled by the SQL-DB Layer of LowCode",
		"Example has two numeric fields (operands) in the screen and 4 virtual fields to keep the result of math opertions"
	LET md_code_sample = 
		"Example Code shows how to get and set values of virtual fields\n",
		"CALL iform.SetFieldValue(\"field identifier\",\"value\")",
		"LET value = iform.GetFieldValue(\"field identifier\")"

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Virtual Fields Record",	"Virtual Fields Record",	"{CONTEXT}/public/querix/icon/svg/24/ic_calculation_2_24px.svg",		101,TRUE,"Virtual Fields - Record Display and modify Math Operation data (no virtual fields are shown)","top") 
			CALL fgl_dialog_setkeylabel("Virtual Fields List",		"Virtual Fields List",		"{CONTEXT}/public/querix/icon/svg/24/ic_calculation_1_24px.svg",		102,TRUE,"Virtual Fields - List all Math Operation Records (no virtual fields are shown)","top")			
			CALL fgl_dialog_setkeylabel("Show all Table Data",		"Show all Table Data",		"{CONTEXT}/public/querix/icon/svg/24/ic_view_list_24px.svg",				103,TRUE,"Show ALL Fields (including virtual fields) - List all Math Operation Records (to view stored data of virtual fields)","top")

 		
		ON ACTION "Virtual Fields Record"
			CALL db_cms_virtual_field_rec()
			
		ON ACTION "Virtual Fields List"
			CALL db_cms_virtual_field_list()

		ON ACTION "Show all Table Data"
			CALL db_cms_virtual_field_list_all()
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################


########################################################################
# FUNCTION db_cms_virtual_field_rec()
#
#
########################################################################
#FUNCTION db_cms_virtual_field_rec(l_settings InteractForm_Settings INOUT) RETURNS BOOL
FUNCTION db_cms_virtual_field_rec()
	DEFINE l_rec_settings InteractForm_Settings
	
	LET md_log_console = NULL  #clear console screen variable

	LET l_rec_settings.form_file = "../misc/misc_virtual_field_rec"
	LET l_rec_settings.id = "virtual_field"
	LET l_rec_settings.log_file = "../log/db_cms_virtual_field.log"	#enable log file
	LET l_rec_settings.actions[""]["ON ACTION Show Data"] = FUNCTION math_on_action_show_data	
	LET l_rec_settings.actions[""]["AFTER ROW"] = 					FUNCTION math_on_action_show_data
	LET l_rec_settings.actions["UPDATE"]["AFTER INPUT"] =		FUNCTION math_after_input
	LET l_rec_settings.actions["INSERT"]["AFTER INPUT"] = 	FUNCTION math_after_input
	LET l_rec_settings.views["test03"].navigation_status="nav_page_of"		#Display current cursor location to with label by identifier

	CALL InteractForm(l_rec_settings)	
END FUNCTION
########################################################################
# END FUNCTION db_cms_virtual_field_rec()
########################################################################




########################################################################
# FUNCTION db_cms_virtual_field_list()
#
#
########################################################################
FUNCTION db_cms_virtual_field_list()
	DEFINE l_rec_settings InteractForm_Settings
	
	LET md_log_console = NULL  #clear console screen variable

	LET l_rec_settings.form_file = "../misc/misc_virtual_field_list"
	LET l_rec_settings.id = "virtual_field"
	LET l_rec_settings.log_file = "../log/db_cms_virtual_field.log"	#enable log file
	LET l_rec_settings.actions[""]["ON ACTION Show Data"] = FUNCTION math_on_action_show_data	
	LET l_rec_settings.actions[""]["AFTER ROW"] = 					FUNCTION math_on_action_show_data
	LET l_rec_settings.actions["UPDATE"]["AFTER INPUT"] =		FUNCTION math_after_input
	LET l_rec_settings.actions["INSERT"]["AFTER INPUT"] = 	FUNCTION math_after_input
	LET l_rec_settings.views["test03"].navigation_status="nav_page_of"		#Display current cursor location to with label by identifier

	CALL InteractForm(l_rec_settings)	    
END FUNCTION
########################################################################
# END FUNCTION db_cms_virtual_field_list()
########################################################################


########################################################################
# FUNCTION db_cms_virtual_field_list_all()
#
#
########################################################################
FUNCTION db_cms_virtual_field_list_all()
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file = "../misc/misc_virtual_field_all"
	LET l_rec_settings.id = "virtual_field"
	LET l_rec_settings.log_file = "../log/db_cms_virtual_field.log"	#enable log file
	LET l_rec_settings.actions[""]["ON ACTION Show Data"] = FUNCTION math_on_action_show_data	
	LET l_rec_settings.actions["UPDATE"]["AFTER INPUT"] = FUNCTION math_after_input
	LET l_rec_settings.actions["INSERT"]["AFTER INPUT"] = FUNCTION math_after_input
	LET l_rec_settings.views["test03"].navigation_status="nav_page_of"		#Display current cursor location to with label by identifier

	CALL InteractForm(l_rec_settings)	    
END FUNCTION
########################################################################
# END FUNCTION db_cms_virtual_field_list_all()
########################################################################


########################################################################
# FUNCTION math_after_input(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION math_after_input(iform InteractForm INOUT) RETURNS BOOL
  DEFINE l_operator_val_1 LIKE test03.operator_val_1
  DEFINE l_operator_val_2 LIKE test03.operator_val_2

	#CALL fgl_winmessage("math_after_input","math_after_input","info")
    
  LET l_operator_val_1 = iform.GetFieldValue("operator_val_1")	#get the data/value from the first operator field
  LET l_operator_val_2 = iform.GetFieldValue("operator_val_2")	#get the data/value from the second operator field
      
	CALL iform.SetFieldValue("result_sum", 				l_operator_val_1 + l_operator_val_2)
	CALL iform.SetFieldValue("result_difference",	l_operator_val_1 - l_operator_val_2)
	CALL iform.SetFieldValue("result_product",		l_operator_val_1 * l_operator_val_2)
	CALL iform.SetFieldValue("result_quotient",		l_operator_val_1 / l_operator_val_2)
    
	#DISPLAY "operator_val_1=", iform.GetFieldValue("operator_val_1")    
	#DISPLAY "operator_val_2=", iform.GetFieldValue("operator_val_2")
	#DISPLAY "result_sum=", iform.GetFieldValue("result_sum")    
	#DISPLAY "result_difference=", iform.GetFieldValue("result_difference")
	#DISPLAY "result_product=", iform.GetFieldValue("result_product")    
	#DISPLAY "result_quotient=", iform.GetFieldValue("result_quotient")
	    
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION math_after_input(iform InteractForm INOUT) RETURNS BOOL
########################################################################


########################################################################
# Below are only some reusable demo functions used by multiple
# demo applications to show the data 
#
#
########################################################################

########################################################################
# FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL
#
# Display some useful information to explain the demo
########################################################################
FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL

	IF md_language IS NOT NULL THEN
		LET md_info = md_language, "\n", md_info
	END IF
	DISPLAY md_subtitle TO lb_SubTitle
	DISPLAY md_info TO lb_info
	DISPLAY md_code_sample TO code_sample

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL
########################################################################


########################################################################
# FUNCTION math_on_action_show_data(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION math_on_action_show_data(iform InteractForm INOUT) RETURNS BOOL
	DEFINE tmp_log STRING 
	#LET md_log_console = md_log_console, "---------------------------------------------------"
	LET tmp_log = tmp_log,"\n", "operator_val_1=", iform.GetFieldValue("operator_val_1")    
	LET tmp_log = tmp_log,"\n", "operator_val_2=", iform.GetFieldValue("operator_val_2")
	LET tmp_log = tmp_log,"\n", "result_sum=", iform.GetFieldValue("result_sum")    
	LET tmp_log = tmp_log,"\n", "result_difference=", iform.GetFieldValue("result_difference")
	LET tmp_log = tmp_log,"\n", "result_product=", iform.GetFieldValue("result_product")    
	LET tmp_log = tmp_log,"\n", "result_quotient=", iform.GetFieldValue("result_quotient")

	#LET md_log_console = tmp_log, "\n", "---------------------------------------------------", "\n", md_log_console,"\n" 

	DISPLAY tmp_log TO log_console
	    
	RETURN FALSE
END FUNCTION
########################################################################
# END FUNCTION math_on_action_show_data(iform InteractForm INOUT) RETURNS BOOL
########################################################################

