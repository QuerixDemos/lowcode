GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"

GLOBALS

CONSTANT SCREEN_RECORD = "sr"

TYPE QueryView_Settings RECORD
      query           STRING               # Query that should be performed
    , window_title    STRING               # Title of new window
    , actions         HASHMAP OF HASHMAP   # Custom actions "SubDialog - Action - Function"
    , view_attributes HASHMAP OF HASHMAP   # Custom actions "SubDialog - Attribute - Value"
END RECORD


TYPE QueryView_Internal RECORD
      fields          DYNAMIC ARRAY OF RECORD fname STRING, ftype STRING END RECORD # List of form fields controlled by the dialog
END RECORD


TYPE QueryView RECORD    # The type LLC that executes a query, shows data and returns the selected row data as a result
      settings        QueryView_Settings
    , selected_data   HASHMAP              # Will keep selected row data
    , inside          QueryView_Internal
END RECORD

END GLOBALS


#########################################################################################################
#
#  Function QueryView
#  Creates InteractForm object and performs its interaction using provided settings
#
#  Arguments:
#  - settings (QueryView_Settings) - Settings for LLC QueryView object
#
#########################################################################################################
PUBLIC FUNCTION QueryView(settings QueryView_Settings) RETURNS BOOL
    DEFINE qv QueryView
    LET qv.settings.* = settings.*
    RETURN qv.Interact()
END FUNCTION


#########################################################################################################
#
#  Function InteractQuery
#  Creates InteractForm object and performs its interaction using currently opened form
#
#  Arguments:
#  - window_title (STRING) - title of the open window
#  - query        (STRING) - query that should be used for interacting
#
#########################################################################################################
PUBLIC FUNCTION InteractQuery(window_title STRING, query STRING) RETURNS BOOL
    DEFINE settings QueryView_Settings
    LET settings.query = query
    LET settings.window_title = window_title
    RETURN QueryView(settings)
END FUNCTION


###############################################################################################
#
#  Creates template form file with Table widget if it is not exist
#
###############################################################################################
PRIVATE FUNCTION CreateFormFileWithTable(file_name STRING, table_id STRING)
    DEFINE xml_form TEXT

    LOCATE xml_form IN FILE file_name

    LET xml_form =
"<?xml version=\"1.0\" encoding=\"UTF-8\"?><form xmlns=\"http://namespaces.querix.com/2015/fglForms\">
    <form.rootContainer>
        <Table identifier=\"" || table_id || "\"/>
    </form.rootContainer>
</form>"
END FUNCTION


###############################################################################################
#
#  Function CreateTable
#  Fill table from template form with columns that defined in 'fields' array
#
###############################################################################################
PUBLIC FUNCTION OpenFormWithTable(
      fields DYNAMIC ARRAY OF RECORD fname, ftype STRING END RECORD
    , field_table STRING
    ) RETURNS FORM

    DEFINE column_ident, field_ident, form_file, table_ident STRING
    DEFINE c_arr    DYNAMIC ARRAY OF ui.TableColumn    # List of table columns for inserting to table
    DEFINE widget   ui.AbstractUiElement
    DEFINE sr_arr   DYNAMIC ARRAY OF STRING            # List of field names for creating screen record
    DEFINE i        INT
    DEFINE t        ui.Table
    DEFINE c        ui.TableColumn
    DEFINE fm       FORM

    LET form_file = "llc_dynamic_form_table_" || field_table || ".fm2"
    LET table_ident = field_table || "_tbl"

    IF NOT os.Path.Exists(form_file) THEN
        CALL CreateFormFileWithTable(form_file, table_ident)
    END IF

    CALL fm.Open(form_file)
    CALL fm.Display()

    LET t = ui.Table.ForName(table_ident)
    IF t IS NULL THEN                                  # Check if table exists
        DISPLAY "ERROR: Table ", table_ident, " is not created"
        RETURN fm                                      # Table was not created
    END IF

    FOR i = 1 TO fields.GetSize()
        LET field_ident = fields[i].fname CLIPPED      # Define text field identifier
        LET column_ident = "c_" || field_ident         # Define table column identifier
        LET c = ui.TableColumn.Create(column_ident, table_ident)  # Create TABLE COLUMN into the table
        CALL c.SetText(field_ident)                    # Set header of column
        CALL CreateWidget(column_ident, field_ident, fields[i].ftype) RETURNING widget
        IF field_table IS NOT NULL THEN
            CALL widget.SetFieldTable(field_table)
        END IF
        CALL c.Complete()                              # Complete table column (internal)
        LET c_arr[i] = c                               # Collect it in columns array
        LET sr_arr[i] = field_ident                    # Collect field names for creating screen record
    END FOR

    CALL t.SetTableColumns(c_arr)                      # Insert table columns into the table
    CALL t.Complete()                                  # Complete table (internal)
    CALL fm.SetScreenRecordFields(field_table, sr_arr) # Insert screen record

    RETURN fm
END FUNCTION


################################################################
#
#  Creates widget, that depends on data type
#
################################################################
PRIVATE FUNCTION CreateWidget(parent STRING, name STRING, type STRING) RETURNS ui.AbstractUiElement
    DEFINE spinner ui.Spinner

    CALL type.ToLowerCase()

    IF type.Substring(1,3) = "int" OR type = "bigint" THEN
        LET spinner = ui.Spinner.Create(name, parent)  # Create SPINNER in the column
        CALL spinner.SetStep(1)
        CALL spinner.SetMinValue(-2147483647)
        CALL spinner.SetMaxValue(2147483647)
        RETURN spinner
    END IF

    IF type.Substring(1,6) = "serial" OR type = "bigserail" THEN
        LET spinner = ui.Spinner.Create(name, parent)  # Create SPINNER in the column
        CALL spinner.SetStep(1)
        CALL spinner.SetMinValue(0)
        CALL spinner.SetMaxValue(2147483647)
        RETURN spinner
    END IF

    IF type.Substring(1,8) = "datetime" THEN
        RETURN ui.TimeEditField.Create(name, parent)   # Create TIMEEDITFIELD in the column
    END IF

    IF type.Substring(1,4) = "date" THEN
        RETURN ui.Calendar.Create(name, parent)        # Create CALENDAR in the column
    END IF

    IF type = "Text" THEN
        RETURN ui.TextArea.Create(name, parent)        # Create TextArea in the column
    END IF

    IF type = "Byte" THEN
        RETURN ui.BlobViewer.Create(name, parent)      # Create BlobViewer in the column
    END IF

    RETURN ui.TextField.Create(name, parent)           # Create TEXT FIELD in the column
END FUNCTION


################################################################
#
#  Method InteractQuery of QueryView object
#  Shows table with data from defined query
#  process DISPLAY ARRAY and fill RESULT with selected row data
#
################################################################
PUBLIC FUNCTION (this QueryView) Interact() RETURNS BOOL
    DEFINE event, fieldTable, cols, tabs, where_clause STRING
    DEFINE ret, is_single_table BOOL
    DEFINE dlg        UI.DIALOG                        # Dynamic DIALOG
    DEFINE sh         base.SqlHandle                   # SQL handler for query
    DEFINE i          INT
    DEFINE fm         FORM
    DEFINE iForm      InteractForm_Settings

    LET  ret = FALSE
    CALL this.selected_data.Clear()                    # Clear previous result
    CALL this.inside.fields.Clear()                    # Clear fields list

    LET  sh = base.SqlHandle.Create()                  # Instantiate SqlHandle object
    CALL sh.Prepare(this.settings.query)               # Prepare query

    # Fill array with result column's names and types
    FOR i = 1 TO sh.GetResultCount()
        LET this.inside.fields[i].fname = sh.GetResultName(i)  # Define name of column
        LET this.inside.fields[i].ftype = sh.GetResultType(i)  # Define type of column
    END FOR

    CALL ParseQuery(this.settings.query) RETURNING cols, tabs, where_clause, is_single_table

    IF is_single_table THEN
        LET fieldTable = tabs CLIPPED
    ELSE
        LET fieldTable = SCREEN_RECORD
    END IF

    LET fm = OpenFormWithTable(this.inside.fields, fieldTable)# Initialize form with TABLE widget
    IF this.settings.window_title IS NOT NULL THEN
        CALL ui.Window.GetCurrent().SetText(this.settings.window_title)
    END IF

    IF is_single_table THEN
        LET iForm.views[fieldTable].screen_record = fieldTable
        LET iForm.views[fieldTable].sql_where = where_clause
        LET iForm.views[fieldTable].actions = this.settings.actions
        LET iForm.views[fieldTable].view_attributes = this.settings.view_attributes
        LET iForm.views[fieldTable].paged_mode = TRUE
        LET ret = InteractForm(iForm)
        #LET this.selected_data = this.views[fieldTable].GetSelectedData()
    ELSE
        LET ret = this.InteractComplex()
    END IF

    CALL fm.Close()
    RETURN ret
END FUNCTION


################################################################
#
#  Private method InteractComplexQuery of QueryView object
#  Shows table with data from defined query
#  process DISPLAY ARRAY and fill RESULT with selected row data
#
################################################################
PRIVATE FUNCTION (this QueryView) InteractComplex() RETURNS BOOL
    DEFINE event, fields_blobs, fields_scroll, fname, ftype, cols, tabs, where_clause STRING
    DEFINE i, bgn, len, r, idx, row_id, cnt INT
    DEFINE dlg                    UI.DIALOG            # Dynamic DIALOG
    DEFINE sh, shBlobs            base.SqlHandle       # SQL handler for query
    DEFINE ret, fRes, isIfxDb     BOOL
    DEFINE actions, attrs         HASHMAP

    LET isIfxDb = db_get_database_type() = "IFX"
    LET ret = FALSE
    CALL this.selected_data.Clear()                    # Clear previous result
    CALL this.inside.fields.Clear()                    # Clear fields list

    CALL ParseQuery(this.settings.query) RETURNING cols, tabs, where_clause

    IF cols IS NULL THEN
        CALL fgl_winmessage("The query could not be processed", "Can not parse query", "error")
    END IF

    IF where_clause IS NULL THEN
        LET where_clause = "1=1"                       # Set default WHERE clause
    END IF

    LET  sh = base.SqlHandle.Create()                  # Instantiate SqlHandle object
    CALL sh.Prepare(this.settings.query)               # Prepare query

    FOR i = 1 TO sh.GetResultCount()
        LET fname = sh.GetResultName(i)
        LET ftype = sh.GetResultType(i)

        LET this.inside.fields[i].fname = fname        # Define name of column
        LET this.inside.fields[i].ftype = ftype        # Define type of column

        IF isIfxDb THEN
            IF ftype = "Byte" OR ftype = "Text" THEN
                # Make string with list of blob fields in the form (comma separated) for SELECT statement
                IF fields_blobs IS NOT NULL THEN
                    LET fields_blobs = fields_blobs.Append(", ")
                END IF
                LET fields_blobs = fields_blobs.Append(fname)
            ELSE
                # Make string with list of NOT blob fields in the form (comma separated) for scrollable cursor
                IF fields_scroll IS NOT NULL THEN
                    LET fields_scroll = fields_scroll.Append(", ")
                END IF
                LET fields_scroll = fields_scroll.Append(fname)
            END IF
        END IF
    END FOR

    IF fields_blobs IS NOT NULL THEN                   # There is at least one blob, so we need it escape it from scrollable cursor
        LET  sh = base.SqlHandle.Create()              # Instantiate SqlHandle object
        # Prepare scrollable cursor for query without BLOB columns
        CALL sh.Prepare("SELECT " || fields_scroll || ", rowid" ||
                        "  FROM " || tabs ||
                        " WHERE " || where_clause)
    END IF

    LET cnt = sh.GetResultCount()
    IF fields_blobs IS NOT NULL THEN                   # If There is at least one BLOB data for fetching
        LET cnt = cnt - 1                              # Exclude 'rowid' column from fetching for SetFieldValue
    END IF

    CALL sh.OpenScrollCursor()                         # Open cursor
    CALL ui.Dialog.Init()

# Get actions hashmap
    LET i = iCaseSearchHashmap(this.settings.actions, TOP_DIALOG_ID)
    IF i > 0 THEN
        LET actions = this.settings.actions.GetValue(i)
    END IF

# Get attributes hashmap
    LET i = iCaseSearchHashmap(this.settings.view_attributes, TOP_DIALOG_ID)
    IF i > 0 THEN
        LET attrs = this.settings.view_attributes.GetValue(i)
    END IF

    LET  dlg = ui.Dialog.CreateDisplayArrayTo(this.inside.fields, SCREEN_RECORD) # Create dynamic DIALOG with DISPLAY ARRAY
    CALL AddTrigger(dlg, actions, "ON ACTION Cancel")  # Add custom CANCEL action
    CALL AddTrigger(dlg, actions, "ON FILL BUFFER")    # Add custom ON FILL BUFFER action

    FOR i = 1 TO actions.getSize()
        CALL AddTrigger(dlg, actions, actions.GetKey(i)) # Add custom action
    END FOR

    CALL SetAttributes(dlg, attrs)

    WHILE (event := dlg.NextEvent()) IS NOT NULL       # Process dialog and wait for the next event
        IF this.ExecuteCustomAction(actions, event) THEN
            CONTINUE WHILE
        END IF

        CASE event
            WHEN "BEFORE DIALOG"
                IF iCaseSearchHashmap(actions, "ON ACTION Accept") = 0 THEN
                    CALL dlg.SetActionHidden("Accept", true) # Hide action Accept
                END IF

            WHEN "BEFORE ROW"
                FOR i = 1 TO sh.GetResultCount()       # Fill RESULT with selected row data
                    LET this.selected_data[sh.GetResultName(i) CLIPPED] = dlg.GetFieldValue(sh.GetResultName(i)) CLIPPED
                END FOR

            WHEN "ON FILL BUFFER"
                LET bgn = FGL_DIALOG_GETBUFFERSTART()
                LET len = FGL_DIALOG_GETBUFFERLENGTH()
                FOR r = 1 TO len
                    LET idx = bgn + r - 1
                    CALL sh.FetchAbsolute(idx)         # Fetch new row from DB
                    IF sqlca.sqlcode == NOTFOUND OR status <> 0 THEN
                       CALL dlg.SetArrayLength(SCREEN_RECORD, idx - 1) # Set length of DISPLAY ARRAY.
                       EXIT FOR
                    END IF
                    FOR i = 1 TO cnt # Set value in each field in the row
                        CALL dlg.SetFieldValue(sh.GetResultName(i), sh.GetResultValue(i), idx)
                    END FOR
                    IF fields_blobs IS NOT NULL THEN   # Fetch BLOBs of particular row from DB
                        LET shBlobs = base.SqlHandle.Create()
                        CALL shBlobs.Prepare("SELECT " || fields_blobs ||
                                             "  FROM " || tabs         ||
                                             " WHERE " || where_clause ||
                                             "   AND rowid = " || sh.GetResultValue(cnt + 1))
                        CALL shBlobs.Open()
                        CALL shBlobs.Fetch()
                        FOR i = 1 TO shBlobs.GetResultCount()
                            CALL dlg.SetFieldValue(shBlobs.GetResultName(i) CLIPPED, shBlobs.GetResultValue(i), idx)
                        END FOR
                    END IF
                END FOR

             WHEN "ON ACTION Cancel"                   # Dialog is finished.
                LET ret = TRUE                         # Return TRUE, that means it is accepted by customer
                EXIT WHILE                             # Stop processing events
        END CASE
    END WHILE

    CALL dlg.Close()                                   # Close and release dynamic dialog
    CALL sh.Close()                                    # Close sql handler
    RETURN ret
END FUNCTION


################################################################################################################################
#
#  Private function ExecuteCustomAction check if custom action is defined and execute it
#
################################################################################################################################
PRIVATE FUNCTION (this QueryView) ExecuteCustomAction(actions HASHMAP, event STRING) RETURNS BOOL
    DEFINE execFunc FUNCTION() RETURNS BOOL
    DEFINE i        INT

    LET i = iCaseSearchHashmap(actions, event)

    IF i > 0 THEN                                 # Check for custom function for current event
        LET execFunc = actions.GetValue(i)
        IF execFunc IS NOT NULL THEN
            RETURN execFunc()                     # Execute custom function for current event
        END IF
    END IF

    RETURN FALSE
END FUNCTION


################################################################################################################################
#
#  Private Function ParseQuery
#  Parses query for getting:
#     1. (STRING) columns list
#     2. (STRING) tables list in FROM block (STRING)
#     3. (STRING) where clause
#     4. ( BOOL ) TRUE if query uses just single table
#
################################################################################################################################
PRIVATE FUNCTION ParseQuery(query STRING) # RETURNS columns STRING, tables STRING, where_clause STRING, is_single_table BOOL
    DEFINE regex util.REGEX
    DEFINE match util.MATCH_RESULTS

    LET regex = /^\s*SELECT\s+(.+)\s+FROM\s+([\w,\.\s]+)\s*(\s+WHERE\s+(.+))?\s*$/i
    CALL util.REGEX.search(query, regex) RETURNING match

    IF match.matched(0) THEN
        RETURN match.str(1), match.str(2), match.str(4), match.str(2).getIndexOf(",", 1) = 0
    END IF

    RETURN "", "", "", FALSE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION ParseQuery(query STRING)
################################################################################################################################