########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses/Scenario
# two tables joined 1:1
########################################################################


########################################################################
# MAIN
#
# Relationship 1 company has got 1 main contact  
# company.comp_main_cont -> contact.cont_id
########################################################################
MAIN	
	DEFINE l_rec_company_contact_settings InteractForm_Settings

  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Contact-Company")
	CALL fgl_settitle("Contact-Company")    

	MENU 
		BEFORE MENU			
			CALL fgl_dialog_setkeylabel("Company rec Main Contact rec 1:1", "Company rec Main Contact rec 1:1", "{CONTEXT}/public/querix/icon/svg/24/ic_company_24px.svg",     101,TRUE,"Display and modify company with main contact details","top") 
			CALL fgl_dialog_setkeylabel("Company list Main Contact rec 1:1","Company list Main Contact rec 1:1","{CONTEXT}/public/querix/icon/svg/24/ic_company_list_24px.svg",101,TRUE,"Display and modify company List with main contact details","top") 

		ON ACTION "Company rec Main Contact rec 1:1"
      CALL company_1_contact_1_rec(l_rec_company_contact_settings)

		ON ACTION "Company list Main Contact rec 1:1"
      CALL company_1_contact_1_list(l_rec_company_contact_settings)
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
######################################################################## 

########################################################################
# FUNCTION company_1_contact_1_rec(p_rec_settings InteractForm_Settings)
########################################################################
FUNCTION company_1_contact_1_rec(p_rec_settings InteractForm_Settings)	
    LET p_rec_settings.form_file = "../relation/relation_1_company_rec_1_main_contact_rec"
		
    LET p_rec_settings.id = "1 company 1 main contact"
    CALL InteractForm(p_rec_settings)                           
END FUNCTION
########################################################################
# END FUNCTION company_1_contact_1_rec(p_rec_settings InteractForm_Settings)	
########################################################################

########################################################################
# FUNCTION company_1_contact_1_list(p_rec_settings InteractForm_Settings)
########################################################################
FUNCTION company_1_contact_1_list(p_rec_settings InteractForm_Settings)	
    LET p_rec_settings.form_file = "../relation/relation_1_company_list_1_main_contact_rec"
		
    LET p_rec_settings.id = "1 company 1 main contact"
    CALL InteractForm(p_rec_settings)                           
END FUNCTION
########################################################################
# END FUNCTION company_1_contact_1_list(p_rec_settings InteractForm_Settings)	
########################################################################