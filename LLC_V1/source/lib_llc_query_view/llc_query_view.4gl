GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"

GLOBALS

CONSTANT SCREEN_RECORD = "sr"

TYPE QueryView_Settings RECORD
      query           STRING               # Query that should be performed
    , window_title    STRING               # Title of new window
    , actions         HASHMAP OF HASHMAP   # Custom actions "SubDialog - Action - Function"
    , attributes      HASHMAP OF HASHMAP   # Custom actions "SubDialog - Attribute - Value"
END RECORD


TYPE QueryView_Output RECORD
      selected_data   HASHMAP              # Will keep selected row data
END RECORD


TYPE QueryView_Internal RECORD
      fields          DYNAMIC ARRAY OF RECORD name STRING, type STRING END RECORD # List of form fields controlled by the dialog
END RECORD

TYPE QueryView RECORD    # The type LLC that executes a query, shows data and returns the selected row data as a result
      settings   QueryView_Settings
    , output     QueryView_Output
    , internal   QueryView_Internal
END RECORD

END GLOBALS

################################################################
#
#  Method InteractQuery of QueryView object
#  Shows table with data from defined query
#  process DISPLAY ARRAY and fill RESULT with selected row data
#
################################################################
PUBLIC FUNCTION (this QueryView) Interact() RETURNS BOOL
    DEFINE event, fieldTable, columns, tables, where_clause STRING
    DEFINE ret, is_single_table BOOL
    DEFINE dlg        UI.DIALOG                        # Dynamic DIALOG
    DEFINE sh         base.SqlHandle                   # SQL handler for query
    DEFINE i          INT
    DEFINE fm         FORM
    DEFINE iForm      InteractForm

    LET  ret = FALSE
    CALL this.output.selected_data.Clear()             # Clear previous result
    CALL this.internal.fields.Clear()                  # Clear fields list

    LET  sh = base.SqlHandle.Create()                  # Instantiate SqlHandle object
    CALL sh.Prepare(this.settings.query)               # Prepare query

    # Fill array with result column's names and types
    FOR i = 1 TO sh.GetResultCount()
        LET this.internal.fields[i].name = sh.GetResultName(i)  # Define name of column
        LET this.internal.fields[i].type = sh.GetResultType(i)  # Define type of column
    END FOR

    CALL ParseQuery(this.settings.query) RETURNING columns, tables, where_clause, is_single_table

    IF is_single_table THEN
        LET fieldTable = tables CLIPPED
    ELSE
        LET fieldTable = SCREEN_RECORD
    END IF

    LET fm = OpenFormWithTable(this.internal.fields, fieldTable)# Initialize form with TABLE widget
    IF this.settings.window_title IS NOT NULL THEN
        CALL ui.Window.GetCurrent().SetText(this.settings.window_title)
    END IF

    IF is_single_table THEN
        LET iForm.settings.sql_where = where_clause
        LET iForm.settings.actions = this.settings.actions
        LET iForm.settings.attributes = this.settings.attributes
        LET iForm.settings.paged_mode = TRUE
        LET ret = iForm.Interact()
        LET this.output.selected_data = iForm.output.selected_data
    ELSE
        LET ret = this.InteractComplex()
    END IF

    CALL fm.Close()
    RETURN ret
END FUNCTION


################################################################
#
#  Private method InteractComplexQuery of QueryView object
#  Shows table with data from defined query
#  process DISPLAY ARRAY and fill RESULT with selected row data
#
################################################################
PRIVATE FUNCTION (this QueryView) InteractComplex() RETURNS BOOL
    DEFINE event, fields_blobs, fields_scroll, fname, ftype, columns, tables, where_clause STRING
    DEFINE i, start, length, row, index, rowid, count INT
    DEFINE dlg                    UI.DIALOG            # Dynamic DIALOG
    DEFINE sh, shBlobs            base.SqlHandle       # SQL handler for query
    DEFINE ret, fRes, isIfxDb     BOOL
    DEFINE actions, attributes    HASHMAP

    LET isIfxDb = db_get_database_type() = "IFX"
    LET ret = FALSE
    CALL this.output.selected_data.Clear()             # Clear previous result
    CALL this.internal.fields.Clear()                  # Clear fields list

    CALL ParseQuery(this.settings.query) RETURNING columns, tables, where_clause

    IF columns IS NULL THEN
        CALL fgl_winmessage("The query could not be processed", "Can not parse query", "error")
    END IF

    IF where_clause IS NULL THEN
        LET where_clause = "1=1"                       # Set default WHERE clause
    END IF

    LET  sh = base.SqlHandle.Create()                  # Instantiate SqlHandle object
    CALL sh.Prepare(this.settings.query)               # Prepare query

    FOR i = 1 TO sh.GetResultCount()
        LET fname = sh.GetResultName(i)
        LET ftype = sh.GetResultType(i)

        LET this.internal.fields[i].name = fname       # Define name of column
        LET this.internal.fields[i].type = ftype       # Define type of column

        IF isIfxDb THEN
            IF ftype = "Byte" OR ftype = "Text" THEN
                # Make string with list of blob fields in the form (comma separated) for SELECT statement
                IF fields_blobs IS NOT NULL THEN
                    LET fields_blobs = fields_blobs.Append(", ")
                END IF
                LET fields_blobs = fields_blobs.Append(fname)
            ELSE
                # Make string with list of NOT blob fields in the form (comma separated) for scrollable cursor
                IF fields_scroll IS NOT NULL THEN
                    LET fields_scroll = fields_scroll.Append(", ")
                END IF
                LET fields_scroll = fields_scroll.Append(fname)
            END IF
        END IF
    END FOR

    IF fields_blobs IS NOT NULL THEN                   # There is at least one blob, so we need it escape it from scrollable cursor
        LET  sh = base.SqlHandle.Create()              # Instantiate SqlHandle object
        # Prepare scrollable cursor for query without BLOB columns
        CALL sh.Prepare("SELECT " || fields_scroll || ", rowid" ||
                        "  FROM " || tables ||
                        " WHERE " || where_clause)
    END IF

    LET count = sh.GetResultCount()
    IF fields_blobs IS NOT NULL THEN                   # If There is at least one BLOB data for fetching
        LET count = count - 1                          # Exclude 'rowid' column from fetching for SetFieldValue
    END IF

    CALL sh.OpenScrollCursor()                         # Open cursor
    CALL ui.Dialog.Init()

# Get actions hashmap
    LET i = iCaseSearchHashmap(this.settings.actions, TOP_DIALOG_ID)
    IF i > 0 THEN
        LET actions = this.settings.actions.GetValue(i)
    END IF

# Get attributes hashmap
    LET i = iCaseSearchHashmap(this.settings.attributes, TOP_DIALOG_ID)
    IF i > 0 THEN
        LET attributes = this.settings.attributes.GetValue(i)
    END IF

    LET  dlg = ui.Dialog.CreateDisplayArrayTo(this.internal.fields, SCREEN_RECORD) # Create dynamic DIALOG with DISPLAY ARRAY
    CALL AddTrigger(dlg, actions, "ON ACTION Cancel")  # Add custom CANCEL action
    CALL AddTrigger(dlg, actions, "ON FILL BUFFER")    # Add custom ON FILL BUFFER action

    FOR i = 1 TO actions.getSize()
        CALL AddTrigger(dlg, actions, actions.GetKey(i)) # Add custom action
    END FOR

    CALL SetAttributes(dlg, attributes)

    WHILE (event := dlg.NextEvent()) IS NOT NULL       # Process dialog and wait for the next event
        IF this.ExecuteCustomAction(actions, event) THEN
            CONTINUE WHILE
        END IF

        CASE event
            WHEN "BEFORE DIALOG"
                IF iCaseSearchHashmap(actions, "ON ACTION Accept") = 0 THEN
                    CALL dlg.SetActionHidden("Accept", true) # Hide action Accept
                END IF

            WHEN "BEFORE ROW"
                FOR i = 1 TO sh.GetResultCount()       # Fill RESULT with selected row data
                    LET this.output.selected_data[sh.GetResultName(i) CLIPPED] = dlg.GetFieldValue(sh.GetResultName(i)) CLIPPED
                END FOR

            WHEN "ON FILL BUFFER"
                LET start = FGL_DIALOG_GETBUFFERSTART()
                LET length = FGL_DIALOG_GETBUFFERLENGTH()
                FOR row = 1 TO length
                    LET index = start + row - 1
                    CALL sh.FetchAbsolute(index)       # Fetch new row from DB
                    IF sqlca.sqlcode == NOTFOUND OR status <> 0 THEN
                       CALL dlg.SetArrayLength(SCREEN_RECORD, index - 1) # Set length of DISPLAY ARRAY.
                       EXIT FOR
                    END IF
                    FOR i = 1 TO count # Set value in each field in the row
                        CALL dlg.SetFieldValue(sh.GetResultName(i), sh.GetResultValue(i), index)
                    END FOR
                    IF fields_blobs IS NOT NULL THEN   # Fetch BLOBs of particular row from DB
                        LET shBlobs = base.SqlHandle.Create()
                        CALL shBlobs.Prepare("SELECT " || fields_blobs ||
                                             "  FROM " || tables       ||
                                             " WHERE " || where_clause ||
                                             "   AND rowid = " || sh.GetResultValue(count + 1))
                        CALL shBlobs.Open()
                        CALL shBlobs.Fetch()
                        FOR i = 1 TO shBlobs.GetResultCount()
                            CALL dlg.SetFieldValue(shBlobs.GetResultName(i) CLIPPED, shBlobs.GetResultValue(i), index)
                        END FOR
                    END IF
                END FOR

             WHEN "ON ACTION Cancel"                   # Dialog is finished.
                LET ret = TRUE                         # Return TRUE, that means it is accepted by customer
                EXIT WHILE                             # Stop processing events
        END CASE
    END WHILE

    CALL dlg.Close()                                   # Close and release dynamic dialog
    CALL sh.Close()                                    # Close sql handler
    RETURN ret
END FUNCTION


################################################################################################################################
#
#  Private function ExecuteCustomAction check if custom action is defined and execute it
#
################################################################################################################################
PRIVATE FUNCTION (this QueryView) ExecuteCustomAction(actions HASHMAP, event STRING) RETURNS BOOL
    DEFINE execFunc FUNCTION() RETURNS BOOL
    DEFINE i        INT
    
    LET i = iCaseSearchHashmap(actions, event)
    
    IF i > 0 THEN              # Check for custom function for current event
        LET execFunc = actions.GetValue(i)
        IF execFunc IS NOT NULL THEN
            RETURN execFunc()                     # Execute custom function for current event
        END IF
    END IF
    
    RETURN FALSE
END FUNCTION


################################################################################################################################
#
#  Private Function ParseQuery
#  Parses query for getting:
#     1. (STRING) columns list
#     2. (STRING) tables list in FROM block (STRING)
#     3. (STRING) where clause
#     4. ( BOOL ) TRUE if query uses just single table
#
################################################################################################################################
PRIVATE FUNCTION ParseQuery(query STRING) # RETURNS columns STRING, tables STRING, where_clause STRING, is_single_table BOOL
    DEFINE regex util.REGEX
    DEFINE match util.MATCH_RESULTS

    LET regex = /^\s*SELECT\s+(.+)\s+FROM\s+([\w,\.\s]+)\s*(\s+WHERE\s+(.+))?\s*$/i
    CALL util.REGEX.search(query, regex) RETURNING match

    IF match.matched(0) THEN
        RETURN match.str(1), match.str(2), match.str(4), match.str(2).getIndexOf(",", 1) = 0
    END IF

    RETURN "", "", "", FALSE
END FUNCTION
################################################################################################################################
# END PRIVATE FUNCTION ParseQuery(query STRING)
################################################################################################################################