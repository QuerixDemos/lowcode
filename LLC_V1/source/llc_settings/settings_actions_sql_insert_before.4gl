########################################################################
# NOTE: This is not implemented in Low Code
# Just a placeholder for future functionality
########################################################################


########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_settings InteractForm_Settings

	CALL fgl_winmessage("Do not use this demo","This is just a placeholder program","error")

	LET l_settings.form_file="../llc_settings/llc_settings_rec"
	LET l_settings.actions["INSERT"]["BEFORE INSERT" ] = FUNCTION actions_insert_before_insert
	LET l_settings.actions["INSERT"]["AFTER INSERT" ] = FUNCTION actions_insert_after_insert

	CALL InteractForm(l_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_settings InteractForm_Settings

	CALL fgl_winmessage("Do not use this demo","This is just a placeholder program","error")
	
	LET l_settings.form_file="../llc_settings/llc_settings_list"
	LET l_settings.actions["INSERT"]["BEFORE INSERT" ] = FUNCTION actions_insert_before_insert
	LET l_settings.actions[""]["BEFORE INSERT" ] = FUNCTION actions_insert_before_insert
	#LET l_settings.actions["INSERT"]["AFTER INSERT" ] = FUNCTION actions_insert_after_insert

	CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################


########################################################################
# FUNCTION actions_insert_before_insert()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_insert_before_insert(iform InteractForm INOUT) RETURNS BOOL

	CALL fgl_winmessage("BEFORE INSERT","BEFORE INSERT","INFO")

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_insert_before_insert()RETURNS BOOL
########################################################################

########################################################################
# FUNCTION actions_insert_after_insert()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_insert_after_insert(iform InteractForm INOUT) RETURNS BOOL

	CALL fgl_winmessage("AFTER INSERT","AFTER INSERT","INFO")

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_insert_after_insert()RETURNS BOOL
########################################################################