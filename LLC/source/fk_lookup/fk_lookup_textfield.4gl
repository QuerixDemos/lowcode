########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# settings.log_file
#
########################################################################
# DB Table Schema
#CREATE TABLE test05 (
#    test05_primary_key SERIAL,
#    test05_fk_char  CHAR,
#    test05_varchar  VARCHAR(20),
#    test05_int  INTEGER,
#    test05_date  DATE,
#                PRIMARY KEY (test05_primary_key) CONSTRAINT pk_test05_primary_key    
#            )
########################################################################


########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_title STRING

	DEFER INTERRUPT
	OPTIONS INPUT WRAP

	LET l_title = os.Path.basename(arg_val(0))
	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText(l_title)
	CALL fgl_settitle(l_title)  

	MENU 
		BEFORE MENU
		CALL fgl_dialog_setkeylabel("Record",	"Record/Detailed View","{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	101,TRUE,"Record/Detailed View - Display, Scroll and modify table data (record view)",	"top") 

		ON ACTION "Record"
			CALL settings_rec()
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################


########################################################################
# Below are only some reusable demo functions used by multiple
# demo applicaitons to show the data 
#
#
########################################################################


########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_rec_settings InteractForm_Settings

	LET l_rec_settings.form_file="../fk_lookup/fk_lookup_textfield_record"
	LET l_rec_settings.id = "lookup"

	LET l_rec_settings.views["test05"].navigation_status="nav_page_of"	

	CALL InteractForm(l_rec_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING

	LET dlg = ui.Dialog.GetCurrent()
    LET l_rec_test05.test05_primary_key = iform.GetFieldValue("test05.test05_primary_key")
    LET l_rec_test05.test05_fk_char = iform.GetFieldValue("test05.test05_fk_char")
    LET l_rec_test05.test05_varchar = iform.GetFieldValue("test05.test05_varchar")
    LET l_rec_test05.test05_int = iform.GetFieldValue("test05.test05_int")
    LET l_rec_test05.test05_date = iform.GetFieldValue("test05.test05_date")

	#For demo purpose - range 0-1000
	LET l_msg = "The variable test05_int=", trim(l_rec_test05.test05_int), " was outside of the valid range 0-1000 and was corrected to "
	IF l_rec_test05.test05_int < 0 THEN

		LET l_rec_test05.test05_int = 0	#min
		LET l_msg = l_msg , " ", trim(l_rec_test05.test05_int) 
		CALL fgl_winmessage("test05_int",l_msg,"error")
	ELSE
		IF l_rec_test05.test05_int > 1000 THEN
			LET l_rec_test05.test05_int = 1000 #max
			LET l_msg = l_msg , " ", trim(l_rec_test05.test05_int) 
			CALL fgl_winmessage("test05_int",l_msg,"error")
		END IF
	END IF
	
	CALL iform.SetFieldValue("test05.test05_int", l_rec_test05.test05_int)

	LET l_msg = "Record Data:",
	"\n", trim(l_rec_test05.test05_primary_key) , 
	"\n", trim(l_rec_test05.test05_fk_char), 
	"\n", trim(l_rec_test05.test05_varchar), 
	"\n", trim(l_rec_test05.test05_int), 
	"\n", trim(l_rec_test05.test05_date)

	CALL fgl_winmessage("INSERT - AFTER FIELD test05_int",l_msg,"info")
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
########################################################################
