########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
PUBLIC DEFINE md_subtitle STRING #for demo subTitle text
PUBLIC DEFINE md_info STRING #for demo description text
PUBLIC DEFINE md_code_sample STRING #for demo code sniplet
PUBLIC DEFINE md_language STRING #Language 
PUBLIC DEFINE md_msg STRING #string for messages
########################################################################
# INFO
########################################################################
# settings.log_file
#
########################################################################
# DB Table Schema
#CREATE TABLE test05 (
#    test05_primary_key SERIAL,
#    test05_fk_char  CHAR,
#    test05_varchar  VARCHAR(20),
#    test05_int  INTEGER,
#    test05_date  DATE,
#                PRIMARY KEY (test05_primary_key) CONSTRAINT pk_test05_primary_key    
#            )
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_title STRING

	DEFER INTERRUPT
	OPTIONS INPUT WRAP

	LET l_title = os.Path.basename(arg_val(0))
	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText(l_title)
	CALL fgl_settitle(l_title)  

	#Demo info to be displayed on screen
	LET md_subtitle = "InteractForm_Settings.views[n].actions[scope][event] = Function or NULL\n"
	LET md_info = "'actions': Managing Built-In and Customized Action Events\nExample: ON CHANGE * (wildcard for any field) call function actions_insert_on_change() "
	LET md_code_sample = 
		"LET l_rec_settings.views[\"test05\"].actions[\"INSERT\"][\"ON CHANGE *\" ] = FUNCTION actions_insert_on_change", "\n",
		"LET l_rec_settings.views[\"test05\"].actions[\"UPDATE\"][\"ON CHANGE *\" ] = FUNCTION actions_update_on_change"

	MENU 
		BEFORE MENU
		CALL fgl_dialog_setkeylabel("Record",	"Record/Detailed View","{CONTEXT}/public/querix/icon/svg/24/ic_details_24px.svg ",	101,TRUE,"Record/Detailed View - Display, Scroll and modify table data (record view)",	"top") 
		CALL fgl_dialog_setkeylabel("List",		"Array/List View",		"{CONTEXT}/public/querix/icon/svg/24/ic_list_24px.svg",		102,TRUE,"Array/List View - List all table data (array view)",							"top")			

		ON ACTION "Record"
			CALL settings_rec()

		ON ACTION "List"
			CALL settings_list()	
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################


########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_rec_settings InteractForm_Settings

	# Disable an Action by assigning NULL as the function name
	# LET l_rec_settings.views["test05"].actions[\"\"][\"ON APPEND\"] = NULL
	
	LET l_rec_settings.form_file="../settings/llc_settings_rec"
	LET l_rec_settings.id = "settings"  #settings action on append is null is identical/ LowCode action processing are managed in the 4gl code

	#LowCode Actions are controlled in 4gl
	LET l_rec_settings.views["test05"].actions["INSERT"]["ON CHANGE *" ] = 		FUNCTION actions_insert_on_change
	LET l_rec_settings.views["test05"].actions["UPDATE"]["ON CHANGE *" ] = 		FUNCTION actions_update_on_change

	#Display some information
	LET l_rec_settings.views["test05"].navigation_status="nav_page_of"	#Display current cursor location to with label by identifier
	LET l_rec_settings.views["test05"].actions[""]["BEFORE DIALOG" ] = FUNCTION display_info

	CALL InteractForm(l_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_rec_settings InteractForm_Settings

	# Disable an Action by assigning NULL as the function name
	# LET l_rec_settings.views["test05"].actions[\"\"][\"ON APPEND\"] = NULL
	
	LET l_rec_settings.form_file="../settings/llc_settings_list"
	LET l_rec_settings.id = "settings"  #settings action on append is null is identical/ LowCode action processing are managed in the 4gl code

	#LowCode Actions are controlled in 4gl	
	LET l_rec_settings.views["test05"].actions["INSERT"]["ON CHANGE *" ] = 		FUNCTION actions_insert_on_change
	LET l_rec_settings.views["test05"].actions["UPDATE"]["ON CHANGE *" ] = 		FUNCTION actions_update_on_change

	#Display some information
	LET l_rec_settings.views["test05"].navigation_status="nav_page_of"	#Display current cursor location to with label by identifier
	LET l_rec_settings.views["test05"].actions[""]["BEFORE DIALOG" ] = FUNCTION display_info

	CALL InteractForm(l_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################


########################################################################
# FUNCTION actions_insert_on_change()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_insert_on_change(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING

	#JUST for demo purpose - we work in another function
	#You can do any field validation here and update/change the data
	LET dlg = ui.Dialog.getCurrent()
	LET md_msg = sfmt("INSERT Record (New) -> ON CHANGE ! Field %1 has changed", dlg.getCurrentItem() )

	CALL display_dialog_record(iform)
	
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_insert_on_change()RETURNS BOOL
########################################################################

########################################################################
# FUNCTION actions_update_on_change()RETURNS BOOL
#
#
########################################################################
FUNCTION actions_update_on_change(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING

	#JUST for demo purpose - we work in another function
	#You can do any field validation here and update/change the data
	LET dlg = ui.Dialog.getCurrent()
	LET md_msg = sfmt("UPDATE Record (Edit) -> ON CHANGE ! Field %1 has changed", dlg.getCurrentItem() )
	CALL display_dialog_record(iform)
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION actions_update_on_change()RETURNS BOOL
########################################################################


########################################################################
# Below are only some reusable demo functions used by multiple
# demo applications to show the data 
#
#
########################################################################

########################################################################
# FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL
#
# Display some useful information to explain the demo
########################################################################
FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL

	IF md_language IS NOT NULL THEN
		LET md_info = md_language, "\n", md_info
	END IF
	DISPLAY md_subtitle TO lb_SubTitle
	DISPLAY md_info TO lb_info
	DISPLAY md_code_sample TO code_sample

	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION display_info(iform InteractForm INOUT) RETURNS BOOL
########################################################################

########################################################################
# FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog
	DEFINE l_rec_test05 RECORD LIKE test05.*
	DEFINE l_msg STRING

	LET dlg = ui.Dialog.GetCurrent()
    LET l_rec_test05.test05_primary_key = iform.GetFieldValue("test05.test05_primary_key")
    LET l_rec_test05.test05_fk_char = iform.GetFieldValue("test05.test05_fk_char")
    LET l_rec_test05.test05_varchar = iform.GetFieldValue("test05.test05_varchar")
    LET l_rec_test05.test05_int = iform.GetFieldValue("test05.test05_int")
    LET l_rec_test05.test05_date = iform.GetFieldValue("test05.test05_date")

	#For demo purpose - range 0-1000
	LET l_msg = "The variable test05_int=", trim(l_rec_test05.test05_int), " was outside of the valid range 0-1000 and was corrected to "
	IF l_rec_test05.test05_int < 0 THEN

		LET l_rec_test05.test05_int = 0	#min
		LET l_msg = l_msg , " ", trim(l_rec_test05.test05_int) 
		CALL fgl_winmessage("test05_int",l_msg,"error")
	ELSE
		IF l_rec_test05.test05_int > 1000 THEN
			LET l_rec_test05.test05_int = 1000 #max
			LET l_msg = l_msg , " ", trim(l_rec_test05.test05_int) 
			CALL fgl_winmessage("test05_int",l_msg,"error")
		END IF
	END IF
	
	CALL iform.SetFieldValue("test05.test05_int", l_rec_test05.test05_int)

	LET l_msg = md_msg,
	"\nRecord Data:",
	"\n", trim(l_rec_test05.test05_primary_key) , 
	"\n", trim(l_rec_test05.test05_fk_char), 
	"\n", trim(l_rec_test05.test05_varchar), 
	"\n", trim(l_rec_test05.test05_int), 
	"\n", trim(l_rec_test05.test05_date)

	CALL fgl_winmessage("Record Data",l_msg,"info")
	RETURN FALSE # Means that built-in function should not be prevented
END FUNCTION
########################################################################
# END FUNCTION display_dialog_record(iform InteractForm INOUT) RETURNS BOOL
########################################################################


########################################################################
# FUNCTION take_settings_from_4gl()
#
#
########################################################################
FUNCTION take_settings_from_4gl()

	MENU "unused" ATTRIBUTES ( STYLE="popup" )
		COMMAND "From Form"
			RETURN FALSE
		COMMAND "FROM Form and 4GL"
			RETURN TRUE
	END MENU	
	
	END FUNCTION


