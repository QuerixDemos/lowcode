########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses/Scenario:
# rate_id is SERIAL PK
########################################################################
# DB Table Schema
#CREATE TABLE test07 (
#	test07_primary_key SERIAL,
#	test07_foreign_key  INT,  #FK pointing at table test07 PK
#	test07_fk_char  CHAR NOT NULL,						
#	test07_col1  VARCHAR(20),
#	test07_col2  VARCHAR(20),
#	test07_col3  DATE,
#	FOREIGN KEY (test07_foreign_key) 
#	REFERENCES test08 (test08_primary_key) ,
#	FOREIGN KEY (test07_fk_char) 
#	REFERENCES test06 (test06_primary_key) ,
#	PRIMARY KEY (test07_primary_key) CONSTRAINT pk_test07_primary_key    
#)
#CREATE TABLE test08 (
#	test08_primary_key SERIAL,
#	test08_fk_char  CHAR NOT NULL,
#	test08_varchar  VARCHAR(20),
#	test08_int  INTEGER,
#	test08_date  DATE,
#	FOREIGN KEY (test08_fk_char) 
#	REFERENCES test06 (test06_primary_key) ,
#	PRIMARY KEY (test08_primary_key) CONSTRAINT pk_test08_primary_key   
#################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT

	OPTIONS INPUT WRAP

#	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Master Detail (1 table)")
	CALL fgl_settitle("Master Detail (1 table)")  

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Master Detail","Master Detail","{CONTEXT}/public/querix/icon/svg/24/ic_tax_24px.svg",101,TRUE,"Master Detail View (Single Table)","top") 

		ON ACTION "Master Detail"
			CALL db_cms_relation_master_detail_rec()
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################


########################################################################
# FUNCTION db_cms_relation_master_detail_rec()
#
#
########################################################################
FUNCTION db_cms_relation_master_detail_rec()	
    CALL InteractFormFile("../relation/relation_master_detail")                                # The main staff function
END FUNCTION
########################################################################
# END FUNCTION db_cms_relation_master_detail_rec()
########################################################################

