########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses/Scenario
# two tables joined 1:1
########################################################################


########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_rec_company_contact_activity_settings InteractForm_Settings


  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Company-Contacts")
	CALL fgl_settitle("Company-Contacts")    

	MENU 
		BEFORE MENU			
#			CALL fgl_dialog_setkeylabel("Company 1 Contact n Rec","Contact & Company 1:1","{CONTEXT}/public/querix/icon/svg/24/ic_contact_24px.svg",101,TRUE,"Display and modify contact / company details","top") 
			
			CALL fgl_dialog_setkeylabel("Company 1 Rec Contact n List Activity n List 1:n:n", "Company 1 Rec Contact n List Activity n List 1:n:n",		"{CONTEXT}/public/querix/icon/svg/24/ic_company_24px.svg",     101,TRUE,"Display and modify company Rec with contacts list and activity list","top") 
			CALL fgl_dialog_setkeylabel("Company 1 List Contact n List Activity n List 1:n:n","Company 1 List Contact n List Activity n List 1:n:n",	"{CONTEXT}/public/querix/icon/svg/24/ic_company_list_24px.svg",101,TRUE,"Display and modify company List with contacts list and activity list","top") 

		ON ACTION "Company 1 Rec Contact n List Activity n List 1:n:n"
      CALL company_1_rec_contact_n_list_activity_n_list_1_n_n(l_rec_company_contact_activity_settings)

		ON ACTION "Company 1 List Contact n List Activity n List 1:n:n"
      CALL company_1_list_contact_n_list_activity_n_list_1_n_n(l_rec_company_contact_activity_settings)
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
######################################################################## 


########################################################################
# FUNCTION company_1_rec_contact_n_list_activity_n_list_1_n_n(p_rec_settings InteractForm_Settings)
########################################################################
FUNCTION company_1_rec_contact_n_list_activity_n_list_1_n_n(p_rec_settings InteractForm_Settings)	
    LET p_rec_settings.form_file = "../demo/demo_sql_begin_commit_rollback_work"
		
    LET p_rec_settings.id = "company_1_contact_n"
    CALL InteractForm(p_rec_settings)                           
END FUNCTION
########################################################################
# END FUNCTION company_1_rec_contact_n_list_activity_n_list_1_n_n(p_rec_settings InteractForm_Settings)	
########################################################################


########################################################################
# FUNCTION company_1_list_contact_n_list_activity_n_list_1_n_n(p_rec_settings InteractForm_Settings)
########################################################################
FUNCTION company_1_list_contact_n_list_activity_n_list_1_n_n(p_rec_settings InteractForm_Settings)	
    LET p_rec_settings.form_file = "../relation/relation_1_company_rec_n_contact_list_n_activity_list"
		
    LET p_rec_settings.id = "company_1_contact_n"
    CALL InteractForm(p_rec_settings)                           
END FUNCTION
########################################################################
# END FUNCTION company_1_list_contact_n_list_activity_n_list_1_n_n(p_rec_settings InteractForm_Settings)	
########################################################################
