########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_rec"
	LET l_settings.actions[""]["ON INSERT" ] = NULL	#Hide/Disable INSERT Record/Row from the toolbar

	CALL InteractForm(l_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_list"
	LET l_settings.actions[""]["ON INSERT" ] = NULL	#Hide/Disable INSERT Record/Row from the toolbar

	CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################