########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses/Scenario:
# rate_id is SERIAL PK
########################################################################
# DB Table Schema
#CREATE TABLE test05 (
#	test05_primary_key SERIAL,
#	test05_fk_char  CHAR,
#	test05_varchar  VARCHAR(20),
#	test05_int  INTEGER,
#	test05_date  DATE,
#				PRIMARY KEY (test05_primary_key) CONSTRAINT pk_test05_primary_key    
#			)
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT

	OPTIONS INPUT WRAP

#	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Test04")
	CALL fgl_settitle("Test04")  

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Test04 Record","Test04 Record","{CONTEXT}/public/querix/icon/svg/24/ic_tax_24px.svg",101,TRUE,"Test04 Record - Display, Scroll and modify Test04 table data","top") 
			CALL fgl_dialog_setkeylabel("Test04 List","Test04 List","{CONTEXT}/public/querix/icon/svg/24/ic_tax_list_24px.svg",102,TRUE,"Test04 List - List all tTest04 table data","top")			

		ON ACTION "Test04 Record"
			CALL db_table_test04_rec()

		ON ACTION "Test04 List"
			CALL db_table_test04_list()	
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################


########################################################################
# FUNCTION db_table_test04_rec()
#
#
########################################################################
FUNCTION db_table_test04_rec()	
    CALL InteractFormFile("../test_data/db_table_test04_rec")                                # The main staff function
END FUNCTION
########################################################################
# END FUNCTION db_table_test04_rec()
########################################################################


########################################################################
# FUNCTION db_table_test04_list()
#
#
########################################################################
FUNCTION db_table_test04_list()	
	DEFINE settings InteractForm_Settings
    LET settings.paged_mode = TRUE
    LET settings.form_file = "../test_data/db_table_test04_list"
    --LET settings.display = TRUE
    CALL InteractForm(settings)
END FUNCTION
########################################################################
# END FUNCTION db_table_test04_list()
########################################################################