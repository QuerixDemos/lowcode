########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
DEFINE m_pk LIKE test05.test05_primary_key
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_rec"
	#Note: DoubleClick for UPDATE makes not really too much  sense in single record view.. but it works
	LET l_settings.attributes[""]["DOUBLECLICK"] = "EDIT" 
	LET l_settings.actions[""]["ON ACTION EDIT"] = FUNCTION settings_rec
	LET l_settings.actions[""]["BEFORE ROW"] = FUNCTION actions_set_m_pk

	#**************************************************************
	#how an I call  CALL dialog.setActionHidden("EDIT",TRUE)
	LET l_settings.actions["INSERT"]["BEFORE INPUT"] = FUNCTION settings_before_input
	LET l_settings.actions["UPDATE"]["BEFORE INPUT"] = FUNCTION settings_before_input	
#	LET l_settings.actions[""]["BEFORE INPUT"] = FUNCTION settings_before_input	
	LET l_settings.actions[""]["BEFORE DISPLAY"] = FUNCTION settings_before_input	
	#**************************************************************

	CALL InteractForm(l_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_list"

	LET l_settings.attributes[""]["DOUBLECLICK"] = "EDIT" 
	LET l_settings.actions[""]["ON ACTION EDIT"] = FUNCTION settings_rec
	LET l_settings.actions[""]["BEFORE ROW"] = FUNCTION actions_set_m_pk

	#**************************************************************
	#how an I call  CALL dialog.setActionHidden("EDIT",TRUE)
	LET l_settings.actions["INSERT"]["BEFORE INPUT"] = FUNCTION settings_before_input
	LET l_settings.actions["UPDATE"]["BEFORE INPUT"] = FUNCTION settings_before_input	
#	LET l_settings.actions[""]["BEFORE INPUT"] = FUNCTION settings_before_input	
	LET l_settings.actions[""]["BEFORE DISPLAY"] = FUNCTION settings_before_input	
	#**************************************************************
	
	CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################

FUNCTION settings_before_input(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog

	LET dlg = ui.Dialog.GetCurrent()

	CALL dlg.setActionHidden("EDIT",TRUE)
	#CALL dialog.setActionHidden("EDIT",TRUE)
	RETURN FALSE
END FUNCTION

########################################################################
# FUNCTION actions_set_m_pk(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION actions_set_m_pk(iform InteractForm INOUT) RETURNS BOOL
	DEFINE dlg ui.Dialog

	LET dlg = ui.Dialog.GetCurrent()
	LET m_pk = iform.GetFieldValue("test05.test05_primary_key")

	RETURN FALSE
END FUNCTION
########################################################################
# FUNCTION actions_set_m_pk(iform InteractForm INOUT) RETURNS BOOL
########################################################################