########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7963
# Addresses/Scenario...
# Single table but byte and char(1000) field
########################################################################
# DB Table Schema
{
          CREATE TABLE activity (
            activity_id	SERIAL,# UNIQUE,
            open_date	DATE NOT NULL,
            close_date	DATE,
            contact_id	INTEGER NOT NULL,	# ref contact_id
            comp_id		INTEGER,	# ref comp_id
            operator_id     INTEGER NOT NULL,	# ref operator_id
            act_type	INTEGER,		# ref activity_type
            long_desc	TEXT,
            short_desc	CHAR(80),
            #long_ref	INTEGER,		# ref possible blob
            a_owner		INTEGER,	# ref contact_id
            priority	INTEGER,
            
            PRIMARY KEY (activity_id) CONSTRAINT activity_pk,
            FOREIGN KEY (contact_id) REFERENCES contact(cont_id) CONSTRAINT contact_id_fk_contact,
            FOREIGN KEY (comp_id) REFERENCES company(comp_id) CONSTRAINT comp_id_fk_company,
            FOREIGN KEY (operator_id) REFERENCES operator(operator_id) CONSTRAINT operator_id_fk_operator,
            FOREIGN KEY (act_type) REFERENCES activity_type(activity_type_id) CONSTRAINT act_type_fk_activity_type
            
          )

          CREATE TABLE activity_type (
            activity_type_id		SERIAL,
            atype_name	CHAR(15) NOT NULL UNIQUE,
            user_def	SMALLINT,
            PRIMARY KEY (activity_type_id)
          )

			CREATE TABLE operator (
				operator_id             SERIAL, #PK
				name                CHAR(10)  NOT NULL UNIQUE,
				password            CHAR(10),
				type                CHAR,
				cont_id             INTEGER, # ref contact_id
				email_address       VARCHAR(100),
				
				PRIMARY KEY (operator_id),
				FOREIGN KEY (cont_id) REFERENCES contact(cont_id)
			)
}          
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_rec_settings_rec InteractForm_Settings
	DEFINE l_rec_settings_list InteractForm_Settings
		
  DEFER INTERRUPT

	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Activity")
	CALL fgl_settitle("Activity")   

	#You could initialise the LLC settings here
	#LET l_rec_settings_rec.xxxx = yyyyy
	LET l_rec_settings_rec.navigation_status="nav_page_of"
	LET l_rec_settings_list.navigation_status="nav_page_of"

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Activity Details",			"Activity Details",			"{CONTEXT}/public/querix/icon/svg/24/ic_communication_1_24px.svg", 				101,TRUE,"Display and modify activity details (Record)","top") 
			CALL fgl_dialog_setkeylabel("Activity List",   			"Activity List",				"{CONTEXT}/public/querix/icon/svg/24/ic_communication_list_1_24px.svg",		102,TRUE,"List all activities (List)","top")			

		ON ACTION "Activity Details"
			CALL db_cms_activity_rec(l_rec_settings_rec)
		
		ON ACTION "Activity List"
			CALL db_cms_activity_list(l_rec_settings_list)
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################