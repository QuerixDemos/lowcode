########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Anomaly
# Single table but byte and char(1000) field
########################################################################
# DB Table Schema
{
          CREATE TABLE company (
            comp_id     SERIAL(1000) NOT NULL,
            comp_name	CHAR(100) NOT NULL UNIQUE,
            comp_addr1	CHAR(40),
            comp_addr2	CHAR(40),
            comp_addr3	CHAR(40),
            comp_city	CHAR(20),
            comp_zone	CHAR(15),
            comp_zip	CHAR(15),

            comp_country	CHAR(40),
            acct_mgr	INTEGER,		# ref company_id
            comp_link	INTEGER,		# future: cross ref link table /Inner Join
            comp_industry	INTEGER,		# ref industry_id
            comp_priority	INTEGER,
            comp_type	INTEGER,		# ref cont_dept_id
            comp_main_cont	INTEGER	NOT NULL,	# ref contact_id
            comp_url	VARCHAR(50),
            comp_notes	CHAR(1000),
 
            PRIMARY KEY (comp_id) CONSTRAINT comp_id_pk,
            FOREIGN KEY (acct_mgr) REFERENCES contact(cont_id) CONSTRAINT acct_mgr_fk_contact,
            FOREIGN KEY (comp_industry) REFERENCES industry_type(industry_type_id) CONSTRAINT industry_type_id_fk_industry_type,
            FOREIGN KEY (comp_type) REFERENCES company_type(company_type_id) CONSTRAINT comp_type_fk_company_type,
            FOREIGN KEY (comp_main_cont) REFERENCES contact(cont_id) CONSTRAINT comp_main_cont_fk_contact 
                        
          )

          CREATE TABLE industry_type (
            industry_type_id	SERIAL,
            itype_name	VARCHAR(25) NOT NULL UNIQUE,
            user_def	SMALLINT
            
            
					CREATE TABLE company_type (
            company_type_id		SERIAL,
            ctype_name	CHAR(25) NOT NULL UNIQUE,
            user_def	SMALLINT,
            base_priority	INTEGER
          )
}   
########################################################################

########################################################################
# FUNCTION db_cms_company_rec(p_settings InteractForm_Settings)
#
# Edit Record
########################################################################
FUNCTION db_cms_company_rec(p_settings InteractForm_Settings) 

	LET p_settings.form_file = "../db_cms_company/db_cms_company_rec"
	#LET p_settings.table = "company"	
	#LET p_settings.paged_mode = FALSE #on fill buffer
	#LET p_settings.input_mode = FALSE	#BOOL - Set to FALSE if DISPLAY ARRAY should be used, otherwise (TRUE) INPUT RECORD/ARRAY is used
	LET p_settings.log_file = "../log/cms_company.log"	#enable log file
	#LET p_settings.pessimistic_locking = FALSE			

	#LET p_settings.sql_where =   			"xxx =  1"	# The WHERE clause of the main query that can be overwritten as soon the user applies a Search (Construct)
	#LET p_settings.sql_where_static =	"yyy =  1"	# The WHERE clause of the main query that can NOT be overwritten by user, it's concatenated to sql_where

	#LET p_settings.sql_order_by = "xxxx DESC"			# The ORDER BY clause for the main query
	#LET p_settings.sql_top  = 5										# The option to limit the base cursor row using the SQL SELECT TOP clause

	#Actions
	#LET p_settings.actions[""]["BEFORE DISPLAY"] = FUNCTION xxxx_before_display
	#LET p_settings.actions[""]["AFTER DISPLAY" ] = FUNCTION xxxx_after_display
	#LET p_settings.actions[""]["ON ACTION Vlad"] = FUNCTION xxx_on_action_vlad

	#LET p_settings.actions["UPDATE"]["AFTER INPUT"          ] = FUNCTION xxx_after_input
	#LET p_settings.actions["UPDATE"]["ON ACTION Hubert"     ] = FUNCTION xxx_on_action_hubert
	
	#LET p_settings.actions["UPDATE"]["BEFORE INPUT"         ] = FUNCTION xxx_update_before_input
	#LET p_settings.actions["UPDATE"]["AFTER FIELD priority" ] = FUNCTION xxx_check_field_priority
	
	#LET p_settings.actions["INSERT"]["BEFORE INPUT"         ] = FUNCTION xxx_insert_before_input
	#LET p_settings.actions["INSERT"]["AFTER FIELD priority" ] = FUNCTION xxx_check_field_priority

	#Control show/remove default events
	#LET p_settings.attributes[""]["INSERT ROW"] = TRUE
	#LET p_settings.attributes[""]["APPEND ROW"] = TRUE #Note, the LLC library uses DISPLAY/INPUT ARRAY for all data (single record and array record)
	#LET p_settings.attributes[""]["DELETE ROW"] = FALSE

	#Translation
	#LET p_settings.translations["xxx.activity_id"] = "Activity identifier"
	#LET p_settings.translations["xxx.open_date"] = "Open Date"
	#LET p_settings.translations["xxx.close_date"] = "Close Date"	
	#LET p_settings.translations["The value of Priority should be in range 1..10"] = "The value of Priority should be in range 1..10\nThis string was exchanged in the settings"

	#Invoke LLC runtime engine
	CALL InteractForm(p_settings)	

END FUNCTION
########################################################################
# END FUNCTION db_cms_company_rec()	
########################################################################


########################################################################
# FUNCTION db_cms_company_list(p_settings InteractForm_Settings)
#
#
########################################################################
FUNCTION db_cms_company_list(p_settings InteractForm_Settings)

	LET p_settings.form_file = "../db_cms_company/db_cms_company_list"
	#LET p_settings.table = "company"	
	#LET p_settings.paged_mode = FALSE #on fill buffer
	#LET p_settings.input_mode = FALSE	#BOOL - Set to FALSE if DISPLAY ARRAY should be used, otherwise (TRUE) INPUT RECORD/ARRAY is used
	LET p_settings.log_file = "../log/cms_company.log"	#enable log file
	#LET p_settings.pessimistic_locking = FALSE

	#LET p_settings.sql_where =   			"xxx =  1"	# The WHERE clause of the main query that can be overwritten as soon the user applies a Search (Construct)
	#LET p_settings.sql_where_static =	"yyy =  1"	# The WHERE clause of the main query that can NOT be overwritten by user, it's concatenated to sql_where

	#LET p_settings.sql_order_by = "xxxx DESC"			# The ORDER BY clause for the main query
	#LET p_settings.sql_top  = 5										# The option to limit the base cursor row using the SQL SELECT TOP clause

	#Actions
	#LET p_settings.actions[""]["BEFORE DISPLAY"] = FUNCTION xxxx_before_display
	#LET p_settings.actions[""]["AFTER DISPLAY" ] = FUNCTION xxxx_after_display
	#LET p_settings.actions[""]["ON ACTION Vlad"] = FUNCTION xxx_on_action_vlad

	#LET p_settings.actions["UPDATE"]["AFTER INPUT"          ] = FUNCTION xxx_after_input
	#LET p_settings.actions["UPDATE"]["ON ACTION Hubert"     ] = FUNCTION xxx_on_action_hubert
	
	#LET p_settings.actions["UPDATE"]["BEFORE INPUT"         ] = FUNCTION xxx_update_before_input
	#LET p_settings.actions["UPDATE"]["AFTER FIELD priority" ] = FUNCTION xxx_check_field_priority
	
	#LET p_settings.actions["INSERT"]["BEFORE INPUT"         ] = FUNCTION xxx_insert_before_input
	#LET p_settings.actions["INSERT"]["AFTER FIELD priority" ] = FUNCTION xxx_check_field_priority

	#Control show/remove default events
	#LET p_settings.attributes[""]["INSERT ROW"] = TRUE
	#LET p_settings.attributes[""]["APPEND ROW"] = TRUE #Note, the LLC library uses DISPLAY/INPUT ARRAY for all data (single record and array record)
	#LET p_settings.attributes[""]["DELETE ROW"] = FALSE

	#Translation
	#LET p_settings.translations["xxx.activity_id"] = "Activity identifier"
	#LET p_settings.translations["xxx.open_date"] = "Open Date"
	#LET p_settings.translations["xxx.close_date"] = "Close Date"	
	#LET p_settings.translations["The value of Priority should be in range 1..10"] = "The value of Priority should be in range 1..10\nThis string was exchanged in the settings"

	#Invoke LLC runtime engine
	CALL InteractForm(p_settings)	

END FUNCTION
########################################################################
# END FUNCTION db_cms_company_list()	
########################################################################