########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Anomaly/Difference
# We are demonstrating 'conditional' dynamic comboBox
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT

	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Conditional Combo")
	CALL fgl_settitle("Conditional Combo")       

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Dynamic Combo Demo","Dynamic Combo Demo","{CONTEXT}/public/querix/icon/svg/24/ic_company_24px.svg",101,TRUE,"Dynamic ComboBox Demo","top") 
#			CALL fgl_dialog_setkeylabel("Company List","Company List","{CONTEXT}/public/querix/icon/svg/24/ic_company_list_24px.svg",102,TRUE,"List all Companys","top")			

#		ON ACTION "Company List"
#			CALL db_cms_company_lookup2_list()
		
		ON ACTION "Dynamic Combo Demo"
			CALL db_cms_aaa_dynamic_combo_rec()
			
		ON ACTION "CANCEL"
			EXIT MENU			
	END MENU	
	
END MAIN
########################################################################
# END MAIN
########################################################################