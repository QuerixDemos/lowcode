########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
# GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7958
# Addresses/Scenario:
# Test different single line widgets
########################################################################
# DB Table Schema
{
           CREATE TABLE test01 (
						f1_varchar VARCHAR(20), #textField
						f3_varchar VARCHAR(20),	#checkBox
						f4_varchar VARCHAR(20),	#comboBox
						f5_date DATE,	#calendar
						f6_varchar VARCHAR(20),	#function field
						f7_smallint VARCHAR(20), #radioButton
						f8_smallint SMALLINT,	#spinner
						f9_dt_h_t_s Datetime Hour to Second	#timeEditField
					)
}          
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT

	OPTIONS INPUT WRAP


	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("SingleLineWidget")
	CALL fgl_settitle("Single Line Widgets")   
#	CALL setModuleId("ACTIVITY")                          # Set module identifier
#	CALL Interact("operator_det_l3")                                # The main staff function

	MENU 
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Widget (SL) Record",			"Details",		"{CONTEXT}/public/querix/icon/svg/24/ic_line_single_24px.svg", 	101,TRUE,"Single line widget (Record)","top") 
			CALL fgl_dialog_setkeylabel("Widget (SL) List",   			"List",				"{CONTEXT}/public/querix/icon/svg/24/ic_line_multi_24px.svg",		102,TRUE,"List Single line widgets (List)","top")			

		ON ACTION "Widget (SL) List"
			CALL db_test_wig_single_line_widgets_list()
		
		ON ACTION "Widget (SL) Record"
			CALL db_test_wig_single_line_widgets_rec()
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################