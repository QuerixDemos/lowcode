
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"

########################################################################
# GLOBAL Scope Variables
########################################################################
#GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
# https://querix.atlassian.net/browse/LYC-7914
########################################################################
# Addresses
#    BLOB column in table
########################################################################
#
#
#          CREATE TABLE qxt_document_blob (
#            document_id          INTEGER NOT NULL UNIQUE,
#            document_blob        BYTE,
#            PRIMARY KEY (document_id)
#          )
#
# Is referenced/used by qxt_document
#
# DB Table Schema
#          CREATE TABLE qxt_document (
#            document_id          SERIAL(1000),
#            document_filename    VARCHAR(100) NOT NULL UNIQUE,
#            document_app         CHAR(30),
#            document_category1   CHAR(40),
#            document_category2   CHAR(40),
#            document_category3   CHAR(40),
#            document_mod_date    DATE,
#            document_desc        TEXT,  --CHAR(1000),  NOTE: Some DB's like Oracle don't allow to have 2 large objs i.e. 2 byte fields or a byte and a TEXT field
#
#            PRIMARY KEY (document_id)
#          )
########################################################################

########################################################################
# FUNCTION db_qxt_document_blob_rec()
#
#
########################################################################
FUNCTION db_qxt_document_blob_rec()
	DEFINE l_settings InteractForm_Settings
	LET l_settings.form_file = "../db_qxt_document_blob/db_qxt_document_blob_rec"
	LET l_settings.actions[""]["BEFORE DIALOG"] = FUNCTION on_fill_buffer
	CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION db_qxt_document_blob_rec()
########################################################################


########################################################################
# FUNCTION db_qxt_document_blob_list()
#
#
########################################################################
FUNCTION db_qxt_document_blob_list()	
	DEFINE l_settings InteractForm_Settings
	LET l_settings.form_file = "../db_qxt_document_blob/db_qxt_document_blob_list"
	LET l_settings.actions[""]["BEFORE DIALOG"] = FUNCTION on_fill_buffer
    {
	LET l_settings.paged_mode = TRUE
	LET l_settings.actions[""]["ON FILL BUFFER"] = FUNCTION on_fill_buffer
	}
	CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION db_qxt_document_blob_list()
########################################################################


########################################################################
# FUNCTION on_fill_buffer(iform InteractForm INOUT) RETURNS BOOL
#
#
########################################################################
FUNCTION on_fill_buffer(iform InteractForm INOUT) RETURNS BOOL
	DEFINE i  INT
	DEFINE sh BASE.SqlHandle
	DEFINE blob BYTE
    DISPLAY "On Fill Buf", FGL_DIALOG_GETBUFFERSTART(), " | ", FGL_DIALOG_GETBUFFERLENGTH()
	LET  sh = BASE.SqlHandle.Create()                      # Instantiate SqlHandle object

    FOR i = 1 TO iform.internal.buffer.GetSize()
	    CALL sh.Prepare("SELECT document_filename FROM qxt_document WHERE document_id = ?")
        CALL sh.SetParameter(1, iform.internal.buffer[i]["qxt_document_blob.document_id"])  
	    CALL sh.Open()
	    CALL sh.Fetch()
        LET blob = iform.internal.buffer[i]["qxt_document_blob.document_blob"]
        CALL blob.SetEditor(os.Path.extension(sh.GetResultValue(1)))
	    CALL sh.Close()
    END FOR
    
    RETURN FALSE
END FUNCTION
########################################################################
# END FUNCTION on_fill_buffer(iform InteractForm INOUT) RETURNS BOOL
########################################################################