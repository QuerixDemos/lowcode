###################################################################################
# GLOBALS
###################################################################################
GLOBALS
	DEFINE t_cms_info TYPE AS 
		RECORD
			db_version VARCHAR(10),
			db_build   VARCHAR(10),
			db_other   vARCHAR(10)
		END RECORD
END GLOBALS


########################################################
# FUNCTION db_version_check(p_db_name)
#
#
########################################################
FUNCTION db_version_check(p_db_name)
	DEFINE p_db_name VARCHAR(100)
	DEFINE l_cms_state INT
	DEFINE l_rec_cms_info t_cms_info
	DEFINE l_msg STRING
	
	DEFINE l_version_is STRING
	DEFINE l_version_must STRING
	
	LET l_version_is = trim(get_required_cms_version())
	LET l_version_must = trim(l_rec_cms_info.db_build)
	CALL db_info("cms") RETURNing l_cms_state, l_rec_cms_info.*  --db_version, l_cms_info_arr[1].db_build
	
	IF  l_version_is != l_version_must THEN
		LET l_msg = 
			"This application requires cms version ", 
			get_required_cms_version(), 
			"\n", 
			"Your cms database uses version ", 
			trim(l_rec_cms_info.db_version),
			"\n",
			"Please run db_schema_tools and re-create your cms database"

		CALL fgl_winmessage("Your CMS DB needs updating",l_msg,"error")
	END IF
	
END FUNCTION
########################################################
# END FUNCTION db_version_check(p_db_name)
########################################################


########################################################
# FUNCTION get_required_cms_version()
########################################################
FUNCTION get_required_cms_version()
	
  RETURN "7.201"

END FUNCTION
########################################################
# END FUNCTION get_required_cms_version()
########################################################

########################################################
# FUNCTION get_required_cms_build()
########################################################
FUNCTION get_required_cms_build()
	
  RETURN "7.201"

END FUNCTION
########################################################
# END FUNCTION get_required_cms_build()
########################################################

######################################################
# FUNCTION db_info(db_name)
#
#
######################################################
FUNCTION db_info(db_name)
	DEFINE db_name,db_state_str VARCHAR(100)
	DEFINE retval,row_count,cms_state SMALLINT
	DEFINE l_cms_info_arr DYNAMIC ARRAY OF t_cms_info 

	WHENEVER ERROR CONTINUE

	IF  fgl_find_table("cms_info") THEN

    DECLARE c_cms_info CURSOR FOR 
    SELECT *
      #INTO l_cms_info_arr.*
      FROM cms_info

    LET row_count = 1
    FOREACH c_cms_info INTO l_cms_info_arr[row_count]
      LET row_count = row_count + 1
      #IF row_count > 30 THEN
      #  EXIT FOREACH
      #END IF
    END FOREACH

    LET row_count = row_count - 1

    Let cms_state = compare_cms_version(l_cms_info_arr[1].*)


  ELSE  --Error

    LET cms_state = -2

  END IF

  #WHENEVER ERROR CONTINUE
  WHENEVER ERROR STOP

  RETURN cms_state, l_cms_info_arr[1].*  --db_version, l_cms_info_arr[1].db_build

END FUNCTION
######################################################
# END FUNCTION db_info(db_name)
######################################################


########################################################
# FUNCTION compare_cms_version(p_rec_cms_info2)
#
#
########################################################
FUNCTION compare_cms_version(p_rec_cms_info2)
  DEFINE p_rec_cms_info2 OF t_cms_info 

  IF get_required_cms_version() = p_rec_cms_info2.db_build THEN
    RETURN 0

  ELSE
    RETURN -1
  END IF

END FUNCTION
########################################################
# END FUNCTION compare_cms_version(p_rec_cms_info2)
########################################################