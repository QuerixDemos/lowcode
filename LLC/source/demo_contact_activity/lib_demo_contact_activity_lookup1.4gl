########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses:
# 1:n relationship contact-activity
########################################################################
# DB Table Schema
{
          CREATE TABLE contact (
            cont_id 	SERIAL(1000) NOT NULL,
            cont_title	VARCHAR(10),
            cont_name 	VARCHAR(20) NOT NULL UNIQUE,
            cont_fname 	VARCHAR(20),
            cont_lname 	VARCHAR(20),
            cont_addr1	VARCHAR(40),
            cont_addr2	VARCHAR(40),
            cont_addr3	VARCHAR(40),
            cont_city	VARCHAR(20),
            cont_zone	VARCHAR(15),
            cont_zip	VARCHAR(15),
            cont_country	VARCHAR(40),
            cont_phone	VARCHAR(15),
            cont_fax	VARCHAR(15),
            cont_mobile	VARCHAR(15),
            cont_email	VARCHAR(50) NOT NULL UNIQUE,
            cont_dept	VARCHAR(15),	# ref cont_dept_id
            cont_org	INTEGER,	# ref company_id
            cont_position	VARCHAR(15),	# ref position_id
            cont_picture	BYTE,
            cont_password	VARCHAR(15),
            cont_ipaddr	VARCHAR(15),	# IP V4
            cont_usemail	SMALLINT,
            cont_usephone	SMALLINT,
            cont_notes   CHAR(1000),
 
            PRIMARY KEY (cont_id) CONSTRAINT contact_pk,
            FOREIGN KEY (cont_org) REFERENCES company(comp_id) CONSTRAINT cont_org_fk_company          

          )

          CREATE TABLE activity (
            activity_id	SERIAL UNIQUE,
            open_date	DATE NOT NULL,
            close_date	DATE,
            contact_id	INTEGER NOT NULL,	# ref contact_id
            comp_id		INTEGER,	# ref comp_id
            operator_id     INTEGER NOT NULL,	# ref operator_id
            act_type	INTEGER,		# ref activity_type
            long_desc	TEXT,
            short_desc	CHAR(80),
            #long_ref	INTEGER,		# ref possible blob
            a_owner		INTEGER,	# ref contact_id
            priority	INTEGER,
            
            PRIMARY KEY (activity_id) CONSTRAINT activity_pk,
            FOREIGN KEY (contact_id) REFERENCES contact(cont_id) CONSTRAINT contact_id_fk_contact,
            FOREIGN KEY (comp_id) REFERENCES company(cont_id) CONSTRAINT comp_id_fk_company,
            FOREIGN KEY (operator_id) REFERENCES operator(operator_id) CONSTRAINT operator_id_fk_operator,
            FOREIGN KEY (act_type) REFERENCES activity_type(atype_name) CONSTRAINT act_type_fk_activity_type
            
          )
#--------------------
company

comp_id              serial                                  no
comp_name            char(100)                               no
comp_addr1           char(40)                                yes
comp_addr2           char(40)                                yes
comp_addr3           char(40)                                yes
comp_city            char(20)                                yes
comp_zone            char(15)                                yes
comp_zip             char(15)                                yes
comp_country         char(40)                                yes
acct_mgr             integer                                 yes
comp_link            integer                                 yes
comp_industry        integer                                 yes
comp_priority        integer                                 yes
comp_type            integer                                 yes
comp_main_cont       integer                                 no
comp_url             varchar(50,0)                           yes
comp_notes           char(1000)                              yes
#------------------
operator

operator_id          serial                                  no
name                 char(10)                                no
password             char(10)                                yes
type                 char(1)                                 yes
cont_id              integer                                 yes
email_address        varchar(100,0)                          yes
#--------------------------------
activity 

activity_type
type_id              serial                                  no
atype_name           char(15)                                no
user_def             smallint                                yes

}         
# comp_id -> comp_name
# operator_id -> operator_name
# act_type -> atype_name
########################################################################

########################################################################
# FUNCTION contact_lookup1_rec(p_rec_settings InteractForm_Settings)	
#
# Edit Record
########################################################################
FUNCTION contact_lookup1_rec(p_rec_settings InteractForm_Settings)	
LET p_rec_settings.form_file = "../demo_contact_activity/contact_activity_lookup1_rec"
LET p_rec_settings.id = "Contact Only"
CALL InteractForm(p_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION contact_lookup1_rec(p_rec_settings InteractForm_Settings)	
########################################################################

########################################################################
# FUNCTION activity_lookup1_rec(p_rec_settings InteractForm_Settings)	
#
# Edit Record
########################################################################
FUNCTION activity_lookup1_rec(p_rec_settings InteractForm_Settings)	
LET p_rec_settings.form_file = "../demo_contact_activity/contact_activity_lookup1_rec"
LET p_rec_settings.id = "Activity Only"
CALL InteractForm(p_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION activity_lookup1_rec(p_rec_settings InteractForm_Settings)	
########################################################################

########################################################################
# FUNCTION contact_activity_lookup1_rec(p_rec_settings InteractForm_Settings)	
#
# Edit Record
########################################################################
FUNCTION contact_activity_lookup1_rec(p_rec_settings InteractForm_Settings)	
	LET p_rec_settings.form_file = "../demo_contact_activity/contact_activity_lookup1_rec"
    LET p_rec_settings.id = "Contact - Activity"
	CALL InteractForm(p_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION contact_activity_lookup1_rec(p_rec_settings InteractForm_Settings)	
########################################################################

########################################################################
# FUNCTION contact_lookup1_list(p_rec_settings InteractForm_Settings)	
#
# Edit List
########################################################################
FUNCTION contact_lookup1_list(p_rec_settings InteractForm_Settings)	
	LET p_rec_settings.form_file = "../demo_contact_activity/contact_activity_lookup1_list"
    LET p_rec_settings.id = "Contact Only"
	CALL InteractForm(p_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION contact_lookup1_list(p_rec_settings InteractForm_Settings)	
########################################################################

########################################################################
# FUNCTION activity_lookup1_list(p_rec_settings InteractForm_Settings)	
#
# Edit List
########################################################################
FUNCTION activity_lookup1_list(p_rec_settings InteractForm_Settings)	
LET p_rec_settings.form_file = "../demo_contact_activity/contact_activity_lookup1_list"
LET p_rec_settings.id = "Activity Only"
CALL InteractForm(p_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION activity_lookup1_list(p_rec_settings InteractForm_Settings)	
########################################################################

########################################################################
# FUNCTION contact_activity_lookup1_list(p_rec_settings InteractForm_Settings)	
#
# Edit List
########################################################################
FUNCTION contact_activity_lookup1_list(p_rec_settings InteractForm_Settings)	
LET p_rec_settings.form_file = "../demo_contact_activity/contact_activity_lookup1_list"
LET p_rec_settings.id = "Contact - Activity"
CALL InteractForm(p_rec_settings)
END FUNCTION
########################################################################
# END FUNCTION contact_activity_lookup1_list(p_rec_settings InteractForm_Settings)	
########################################################################