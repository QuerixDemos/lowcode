{

  LET sql_stmt = "SELECT contact.cont_id, ",
                   "contact.cont_name, ",
                   "contact.cont_fname, ",
                   "contact.cont_lname, ",
                   "company.comp_name, ",
                   "contact.cont_phone ",
                 "FROM contact, OUTER company ",
                 "WHERE company.comp_id = contact.cont_org "

  LET sql_stmt = "SELECT " , 
                   "company.comp_id, ",
                   "company.comp_name, ",
                   "company.comp_city, ",
                   "company.comp_country, ",
                   "contact.cont_name, ",
                   "operator.name ",
                 "FROM company, OUTER contact, OUTER operator ",
                 "WHERE company.acct_mgr = operator.cont_id ",
                   "AND company.comp_main_cont = contact.cont_id "
                   
                   
}

########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# Addresses/Scenario
# two tables joined 1:1
########################################################################
# DB Table Schema
{
          CREATE TABLE contact (
            cont_id 	SERIAL(1000) NOT NULL,
            cont_title	VARCHAR(10),
            cont_name 	VARCHAR(20) NOT NULL UNIQUE,
            cont_fname 	VARCHAR(20),
            cont_lname 	VARCHAR(20),
            cont_addr1	VARCHAR(40),
            cont_addr2	VARCHAR(40),
            cont_addr3	VARCHAR(40),
            cont_city	VARCHAR(20),
            cont_zone	VARCHAR(15),
            cont_zip	VARCHAR(15),
            cont_country	VARCHAR(40),
            cont_phone	VARCHAR(15),
            cont_fax	VARCHAR(15),
            cont_mobile	VARCHAR(15),
            cont_email	VARCHAR(50) NOT NULL UNIQUE,
            cont_dept	VARCHAR(15),	# ref cont_dept_id
            cont_org	INTEGER,	# ref company_id
            cont_position	VARCHAR(15),	# ref position_id
            cont_picture	BYTE,
            cont_password	VARCHAR(15),
            cont_ipaddr	VARCHAR(15),	# IP V4
            cont_usemail	SMALLINT,
            cont_usephone	SMALLINT,
            cont_notes   CHAR(1000),
 
            PRIMARY KEY (cont_id) CONSTRAINT contact_pk,
            FOREIGN KEY (cont_org) REFERENCES company(comp_id) CONSTRAINT cont_org_fk_company          

          )
          CREATE TABLE company (
            comp_id     SERIAL(1000) NOT NULL,
            comp_name	CHAR(100) NOT NULL UNIQUE,
            comp_addr1	CHAR(40),
            comp_addr2	CHAR(40),
            comp_addr3	CHAR(40),
            comp_city	CHAR(20),
            comp_zone	CHAR(15),
            comp_zip	CHAR(15),
            comp_country	CHAR(40),
            acct_mgr	INTEGER,		# ref contact_id
            comp_link	INTEGER,		# future: cross ref link table /Inner Join
            comp_industry	INTEGER,		# ref industry_id
            comp_priority	INTEGER,
            comp_type	INTEGER,		# ref cont_dept_id
            comp_main_cont	INTEGER	NOT NULL,	# ref contact_id
            comp_url	VARCHAR(50),
            comp_notes	CHAR(1000),
 
            PRIMARY KEY (comp_id) CONSTRAINT comp_id_pk,
            FOREIGN KEY (acct_mgr) REFERENCES contact(cont_id) CONSTRAINT acct_mgr_fk_contact,
            FOREIGN KEY (comp_industry) REFERENCES industry_type(industry_type_id) CONSTRAINT industry_type_id_fk_industry_type,
            FOREIGN KEY (comp_type) REFERENCES company_type(company_type_id) CONSTRAINT comp_type_fk_company_type,
            FOREIGN KEY (comp_main_cont) REFERENCES contact(cont_id) CONSTRAINT comp_main_cont_fk_contact 
                        
          )

          CREATE TABLE industry_type (
            industry_type_id	SERIAL,
            itype_name	VARCHAR(25) NOT NULL UNIQUE,
            user_def	SMALLINT
            
            
					CREATE TABLE company_type (
            company_type_id		SERIAL,
            ctype_name	CHAR(25) NOT NULL UNIQUE,
            user_def	SMALLINT,
            base_priority	INTEGER
          )

}          
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
	DEFINE l_rec_contact_settings_rec InteractForm_Settings
	DEFINE l_rec_contact_settings_list InteractForm_Settings
	DEFINE l_rec_company_settings_rec InteractForm_Settings
	DEFINE l_rec_company_settings_list InteractForm_Settings
	DEFINE l_rec_contact_company_settings_rec InteractForm_Settings
	DEFINE l_rec_contact_company_settings_list InteractForm_Settings

  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Contact-Company")
	CALL fgl_settitle("Contact-Company")    

	MENU 
		BEFORE MENU
			#CALL fgl_dialog_setkeylabel("Contact Details","Contact","{CONTEXT}/public/querix/icon/svg/24/ic_contact_24px.svg",101,TRUE,"Display and modify contact details (Record)","top") 
			#CALL fgl_dialog_setkeylabel("Contact List","Contact List","{CONTEXT}/public/querix/icon/svg/24/ic_contacts_24px.svg",102,TRUE,"List all contacts (List)","top")			

		ON ACTION "Contact Details"
			CALL contact_rec(l_rec_contact_settings_rec)

		ON ACTION "Contact List"
			CALL contact_list(l_rec_contact_settings_list)

    ON ACTION "Company Details"
      CALL company_rec(l_rec_company_settings_list)

		ON ACTION "Company List"
			CALL company_list(l_rec_company_settings_list)
	
		ON ACTION "Contact (Rec) Company (Rec)"
      CALL contact_rec_company_rec(l_rec_contact_company_settings_list)

		ON ACTION "Contact (List) Company (Rec)"
			CALL contact_list_company_rec(l_rec_contact_company_settings_list)
	
    ON ACTION "Company (Rec) Contact (Rec)"
      CALL company_rec_contact_rec(l_rec_contact_company_settings_list)

    ON ACTION "Company (List) Contact (Rec)"
      CALL company_list_contact_rec(l_rec_contact_company_settings_list)
	
    ON ACTION "Contact (List) Company (List)"
      CALL contact_list_company_list(l_rec_contact_company_settings_list)
	
    ON ACTION "Contact (Rec) Company (Rec) Contact (Rec)"
       CALL contact_rec_company_rec_contact_rec(l_rec_contact_company_settings_list)
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
######################################################################## 