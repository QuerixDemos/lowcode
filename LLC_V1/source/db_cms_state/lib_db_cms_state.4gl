########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# MODULE Scope Variables
########################################################################

########################################################################
# INFO
########################################################################
# https://querix.atlassian.net/browse/LYC-7912
# Addresses/Scenario:
# PK CHAR and one FK rate_id integer
########################################################################
# DB Table Schema
#					CREATE TABLE state (
#						country         VARCHAR(40) NOT NULL,  #only for legacy compatibility
#						country_code         VARCHAR(3) NOT NULL, #only for future compatibility
#						state_code           VARCHAR(6) NOT NULL,
#						state_code_iso3661   VARCHAR(10),
#						state_text           VARCHAR(30),
#						state_text_enu       VARCHAR(30),
#            PRIMARY KEY (country_code,state_code)
#					)
########################################################################


########################################################################
# FUNCTION db_cms_state_rec((p_where_static STRING, p_where STRING)
#
#
########################################################################
FUNCTION db_cms_state_rec(p_where_static STRING, p_where STRING)
	DEFINE l_settings InteractForm_Settings

	#Example: WHERE clause i.e. "country = 'Germany'"
	LET l_settings.sql_where_static = p_where_static 	# The WHERE clause of the main query that can NOT be overwritten by user, it's concatenated to sql_where
	LET l_settings.sql_where = p_where					# The WHERE clause of the main query that will be overwritten as soon the user applies a Search (Construct)

	LET l_settings.form_file = "../db_cms_state/db_cms_state_rec"

	LET l_settings.paged_mode = FALSE #on fill buffer
	LET l_settings.log_file = "../log/cms_state.log"	#enable log file
	LET l_settings.input_mode = FALSE	#BOOL - Set to FALSE if DISPLAY ARRAY should be used, otherwise (TRUE) INPUT RECORD/ARRAY is used
	LET l_settings.pessimistic_locking = FALSE
	LET l_settings.sql_order_by = "country, state_text ASC"					# The ORDER BY clause for the main query
	LET l_settings.sql_top  = 500																				# The option to limit the base cursor row using the SQL SELECT TOP clause
	
	CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION db_cms_state_rec()
########################################################################

########################################################################
# FUNCTION db_cms_state_list(p_where_static STRING, p_where STRING)
#
#	Display States with an optional where filter
#
########################################################################
FUNCTION db_cms_state_list(p_where_static STRING, p_where STRING)
	DEFINE l_settings InteractForm_Settings

	#Example: WHERE clause i.e. "country = 'Germany'"
	LET l_settings.sql_where_static = p_where_static 	# The WHERE clause of the main query that can NOT be overwritten by user, it's concatenated to sql_where
	LET l_settings.sql_where = p_where					# The WHERE clause of the main query that will be overwritten as soon the user applies a Search (Construct)

	LET l_settings.form_file = "../db_cms_state/db_cms_state_list"
	
	LET l_settings.paged_mode = FALSE #on fill buffer
	LET l_settings.log_file = "../log/cms_state.log"	#enable log file
	LET l_settings.input_mode = FALSE	#BOOL - Set to FALSE if DISPLAY ARRAY should be used, otherwise (TRUE) INPUT RECORD/ARRAY is used
	LET l_settings.pessimistic_locking = FALSE
	LET l_settings.sql_order_by = "country, state_text ASC"					# The ORDER BY clause for the main query
	LET l_settings.sql_top  = 500																				# The option to limit the base cursor row using the SQL SELECT TOP clause


	#Control show/remove default events
	LET l_settings.attributes[""]["INSERT ROW"] = TRUE
	LET l_settings.attributes[""]["APPEND ROW"] = TRUE #Note, the LLC library uses DISPLAY/INPUT ARRAY for all data (single record and array record)
	LET l_settings.attributes[""]["DELETE ROW"] = TRUE


	CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION db_cms_state_list((p_where_static STRING, p_where STRING)
########################################################################