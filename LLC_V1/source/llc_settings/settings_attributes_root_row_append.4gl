########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
GLOBALS "../lib_llc_interact_form/llc_interact_form.4gl"
########################################################################
# FUNCTION settings_rec()
#
#
########################################################################
FUNCTION settings_rec()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_rec"

	#Note, the LLC library uses DISPLAY/INPUT ARRAY for all data (single record and array record)	
	LET l_settings.attributes[""]["INSERT ROW"] = FALSE 
	LET l_settings.attributes[""]["APPEND ROW"] = TRUE 
	LET l_settings.attributes[""]["DELETE ROW"] = FALSE 

	CALL InteractForm(l_settings)

END FUNCTION
########################################################################
# END FUNCTION settings_rec()
########################################################################


########################################################################
# FUNCTION settings_list()
#
#
########################################################################
FUNCTION settings_list()	
	DEFINE l_settings InteractForm_Settings

	LET l_settings.form_file="../llc_settings/llc_settings_list"

		#Note, the LLC library uses DISPLAY/INPUT ARRAY for all data (single record and array record)	
		LET l_settings.attributes[""]["INSERT ROW"] = FALSE 
		LET l_settings.attributes[""]["APPEND ROW"] = TRUE 
		LET l_settings.attributes[""]["DELETE ROW"] = FALSE 
		
	CALL InteractForm(l_settings)
END FUNCTION
########################################################################
# END FUNCTION settings_list()
########################################################################