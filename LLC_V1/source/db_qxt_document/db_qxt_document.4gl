########################################################################
# DATABASE
########################################################################
DATABASE cms_llc
########################################################################
# GLOBAL Scope Variables
########################################################################
#GLOBALS "../common/glob_GLOBALS.4gl"
########################################################################
# MODULE Scope Variables
########################################################################
########################################################################
# INFO
########################################################################
# anomaly
#    BLOB TEXT column in table
########################################################################
# https://querix.atlassian.net/browse/LYC-7914
########################################################################
# DB Table Schema
#          CREATE TABLE qxt_document (
#            document_id          SERIAL(1000),
#            document_filename    VARCHAR(100) NOT NULL UNIQUE,
#            document_app         CHAR(30),
#            document_category1   CHAR(40),
#            document_category2   CHAR(40),
#            document_category3   CHAR(40),
#            document_mod_date    DATE,
#            document_desc        TEXT,  --CHAR(1000),  NOTE: Some DB's like Oracle don't allow to have 2 large objs i.e. 2 byte fields or a byte and a TEXT field
#
#            PRIMARY KEY (document_id)
#          )
########################################################################

########################################################################
# MAIN
#
#
########################################################################
MAIN	
  DEFER INTERRUPT
	OPTIONS INPUT WRAP

	CALL db_version_check("cms_llc")
	CALL ui.Interface.setText("Document")
	CALL fgl_settitle("Document") 

	MENU
		BEFORE MENU
			CALL fgl_dialog_setkeylabel("Document Record","Document Record","{CONTEXT}/public/querix/icon/svg/24/ic_document_outline_24px.svg",101,TRUE,"Document Record - Display, Scroll and modify document data","top") 
			CALL fgl_dialog_setkeylabel("Original Document","Original Document","{CONTEXT}/public/querix/icon/svg/24/ic_document_outline_24px.svg",101,TRUE,"Original Document - Display, Scroll and modify document data","top")
			CALL fgl_dialog_setkeylabel("Document List","Document List","{CONTEXT}/public/querix/icon/svg/24/ic_document_multiple_outline_24px.svg",102,TRUE,"Document List - List all documents","top")			
		
		ON ACTION "Document Record"
			CALL db_qxt_document_rec()

		ON ACTION "Original Document"
			CALL db_qxt_document_rec2()
			
		ON ACTION "Document List"
			CALL db_qxt_document_list()
			
		ON ACTION "CANCEL"
			EXIT MENU
	END MENU
END MAIN
########################################################################
# END MAIN
########################################################################